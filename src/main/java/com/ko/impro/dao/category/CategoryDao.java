package com.ko.impro.dao.category;

import java.util.List;

import com.ko.impro.dto.CategoryDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Controller to handle the  CRUD operations  for Category
 *
 */
public interface CategoryDao {

	/**
	 * Load the all the categories
	 * @param isMaintainence
	 * @return List<CategoryDto>
	 * @throws ImproException
	 */
	List<CategoryDto> loadCategory() throws ImproException;

	/**
	 * Method to add category
	 * @param categoryDto
	 * @throws ImproException
	 */
	void addCategory(CategoryDto categoryDto) throws ImproException;

	/**
	 * Method to delete category
	 * @param categoryDto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteCategory(CategoryDto categoryDto) throws ImproException;

	/**
	 * Method to edit category
	 * @param categoryDto
	 * @return String
	 * @throws ImproException
	 */
	String editCategory(CategoryDto categoryDto) throws ImproException;
	
	/**
	 * Method to validate duplicate category
	 * @param categoryDesc
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicateCategory(String categoryDesc) throws ImproException;
	/**
	 * Load Category for specific Category Code
	 * @param categoryCode
	 * @return CategoryDto
	 * @throws ImproException
	 */
	CategoryDto loadCategory(int categoryCode) throws ImproException;
	
	/**
	 * To check given any combination categoryCode present in 
	 * Impro_Category_SubCategory table 
	 * or not and if present check and count every combination in product table.
	 * @param categoryCode
	 * @return boolean value
	 */
	boolean checkPresenceInProduct(int categoryCode) throws ImproException;
}
