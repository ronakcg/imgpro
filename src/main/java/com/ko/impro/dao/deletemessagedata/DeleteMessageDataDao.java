package com.ko.impro.dao.deletemessagedata;

import java.util.List;

import com.ko.impro.dto.DeleteMessagesDataDto;
import com.ko.impro.exception.ImproException;

/**'
 * @author hlekshmy
 *  * DeleteMessageDataDao to handle the  CRUD operations for DeleteMessageData
 */

public interface DeleteMessageDataDao {

	/**
	 * Load the DeleteMessagesDat details
	 * @param boolean isMaintainence
	 * @return List<DeleteMessagesDataDto>
	 * @throws ImproException
	 */
	List<DeleteMessagesDataDto> loadDeleteMessagesData() throws ImproException;

	
	/**
	 * Method to add DeleteMessagesData
	 * @param DeleteMessagesDataDto
	 * @throws ImproException
	 */
	void addDeleteMessagesData(DeleteMessagesDataDto deltMsgDataDto) throws ImproException;

	/**
	 * Method to delete the DeleteMessagesData
	 * @param DeleteMessagesDataDto DeleteMessagesDataDto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteDeleteMessagesData(DeleteMessagesDataDto deltMsgDataDto) throws ImproException;

	/**
	 * Method to edit DeleteMessagesData
	 * @param DeleteMessagesDataDto dto
	 * @return String message
	 * @throws ImproException
	 */
	String editDeleteMessagesData(DeleteMessagesDataDto dto) throws ImproException;
	
	/**
	 * Method to load DeleteMessagesData based on DeleteMessagesDataCode
	 * @param id
	 * @return DeleteMessagesDataDto
	 * @throws ImproException
	 */
	DeleteMessagesDataDto loadDeleteMessagesData(int deltMsgDataId) throws ImproException;
	
	/**
	 * Method to check duplicate DeleteMessagesData Description
	 * @param name
	 * @return boolean 
	 * @throws ImproException
	 */
	boolean checkDuplicateDeleteMessagesData(String name) throws ImproException;

}
