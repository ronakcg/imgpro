package com.ko.impro.dao.packages;

import java.util.List;

import com.ko.impro.dto.PackageDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * PackageDao to handle the  CRUD operations for Package
 */
public interface PackageDao {

	/**
	 * Load the Packages
	 * @return List<PackageDto>
	 * @throws ImproException
	 */
	List<PackageDto> loadPackages() throws ImproException;
	
	/**
	 * Method to add new package
	 * @param packageDto
	 * @throws ImproException
	 */
	void addPackage(PackageDto packageDto) throws ImproException;
	
	/**
	 * Method to delete package based on it's presence in product
	 * @param packageDto
	 * @return String
	 * @throws ImproException
	 */
	String deletePackage(PackageDto packageDto) throws ImproException;
	
	/**
	 * Method to edit package
	 * @param packageDto
	 * @return String
	 * @throws ImproException
	 */
	String editPackage(PackageDto packageDto) throws ImproException;
	
	/**
	 * To check the duplicate packageName
	 * @param String packageName
	 * @return boolean
	 *  @throws ImproException
	 */
	boolean checkDuplicatePackageName(String packageName) throws ImproException;
	
	/**
	 * To load Package based on given package code
	 * @param packageId
	 * @return PackageDto
	 *  @throws ImproException
	 */
	PackageDto loadPackage(int packageId) throws ImproException;
}
