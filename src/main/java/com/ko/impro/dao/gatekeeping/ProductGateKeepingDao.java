package com.ko.impro.dao.gatekeeping;

import java.util.List;

import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.exception.ImproException;

/**
 * @author Vilas
 * Data Access Object of ProductGateKeeping - Methods declarations
 */
public interface ProductGateKeepingDao {

	/**
	 * Load the complete/incomplete records
	 * 
	 * @param isInComplete
	 * @return List<ProductGateKeepingDto>
	 * @throws ImproException
	 */
	List<ProductGateKeepingDto> loadGateKeeping(boolean isInComplete) throws ImproException;

	/**
	 * Pull the gateKeeping Details
	 * 
	 * @param gatekeepingId
	 * @return ProductGateKeepingDto
	 * @throws ImproException
	 */
	ProductGateKeepingDto loadGateKeeping(int gatekeepingId) throws ImproException;

	/**
	 * To purge a   productGatekeeping  rec ord
	 * @param int gatekeepingId
	 * @throws ImproException
	 */
	void purgeProductGateKeeping(int gatekeepingId) throws ImproException;

	/**
	 * Method to trigger StoredProcedure [dbo].[ProductUpdateFromGatekeeping]
	 * 
	 * @param dto
	 * @return ResponseEntity<GateKeepingResponseDto>
	 */
	void pushProducts() throws ImproException;

	/**
	 * Method to edit Gatekeeping
	 * 
	 * @param gatekeepingDto
	 * @throws ImproException
	 */
	void editGateKeeping(ProductGateKeepingDto gatekeepingDto) throws ImproException;

	
}
