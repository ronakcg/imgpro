package com.ko.impro.dao.impl.manufacturer;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.manufacturer.ManufacuturerDao;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.manufacturer.Manufacturers;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author hlekshmy 
 * This class is an implementation of Manufacturer Interface
 */
@Repository
public class ManufacturerDaoImpl extends BaseDaoImpl implements ManufacuturerDao {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ManufacturerDaoImpl.class);
	
	/**
	 * This is to calculate number of count of rows in product table
	 */
	private int count;
	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ManufacturerDto> loadManufacuturer() throws ImproException {
		LOG.info("ManufacturerDaoImpl-->loadManufacuturer()-->Begin");
		Session session=null;
		List<ManufacturerDto> manList=null;
		try {
			session = sessionFactory.openSession();		
			ManufacturerDto dto=null;
			Query query=null;
				query = session.createQuery("select m.manufacturerCode,m.manufacturerName from Manufacturers m order by m.manufacturerName asc");
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				manList=new ArrayList<ManufacturerDto>();
				for (Object[] rs : list) {
					dto = setManufacturerDto(rs, dto);
					manList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ManufacturerDaoImpl-->loadManufacuturer()-->End");
		return manList; 

	}

	/**
	 * Set the ManufacturerDto
	 * @param resultSet
	 * @param dto
	 * @return ManufacturerDto
	 */
	private ManufacturerDto setManufacturerDto(Object[] resultSet, ManufacturerDto dto) {
		dto=new ManufacturerDto();
		dto.setManufacturerCode(resultSet[0].toString());
		dto.setManufacturerName(ApplicationUtil.checkNull(resultSet[1]));
		return dto;	
	}

	@Transactional(rollbackFor = ImproException.class)
	@Override
	public void addManufacuturer(ManufacturerDto manufacturerDto) throws ImproException {
		LOG.info("ManufacturerDaoImpl-->addManufacuturer()-->BEGIN");		
		try {				
			Manufacturers dto=new Manufacturers();			
			dto.setManufacturerName(manufacturerDto.getManufacturerName());
			
			entityMgr.persist(dto);
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_MANUFACTURER,String.valueOf(dto.getManufacturerCode()),AppConstants.TRANSACTION_TYPE_ADD);
		} catch(PersistenceException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error(" ManufacturerDaoImpl-->addManufacuturer--> Exception occured "+ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		}
		LOG.info("ManufacturerDaoImpl-->addManufacuturer()-->End");			
	}
	
	@Transactional(rollbackFor = ImproException.class)
	@Override
	public String deleteManufacuturer(ManufacturerDto dto) throws ImproException { 
		LOG.info("ManufacturerDaoImpl-->deleteManufacuturer()-->Begin");
		Session session = null;		
		Query query=null;
		String msg="";
		try {
			session = sessionFactory.openSession();
			if(checkPresenceInProduct(Integer.parseInt(dto.getManufacturerCode()))) {						// Checking Manufacturer in Products
				return "Manufacturer are currently in use on ("+count+") Products ";
			}else {				
				query=session.createQuery("Delete from Manufacturers WHERE manufacturerCode=:manufacturerCode");
				
				query.setParameter("manufacturerCode", Integer.parseInt(dto.getManufacturerCode()));	
				deleteBrand(dto);
				deleteBrandExtendedProdDesc(dto);
			}
			query.executeUpdate();
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_MANUFACTURER,String.valueOf(dto.getManufacturerCode()),AppConstants.TRANSACTION_TYPE_DELETE);
			msg= AppConstants.DATA_DELETE_SUCC_MSG;
		} catch(ImproException e){
			msg=AppConstants.FAILURE_MSG;
			LOG.error("ManufacturerDaoImpl-->deleteManufacuturer()-->End");
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ManufacturerDaoImpl-->deleteManufacuturer()-->End");	
		return msg;
	}
	
	
	@Transactional(rollbackFor = ImproException.class)
	@Override
	public String editManufacuturer(ManufacturerDto dto) throws ImproException { // need to call the store proc
		LOG.info("ManufacturerDaoImpl-->editManufacuturer()-->Begin");
		Session session = null;		
		Query query=null;
		String msg="";
		try {
			session = sessionFactory.openSession();
			
					query=session.createQuery("Update Manufacturers set manufacturerName=:manufacturerName "
							+ " WHERE manufacturerCode=:manufacturerCode");
					query.setParameter("manufacturerName", dto.getManufacturerName());
					query.setParameter("manufacturerCode", Integer.parseInt(dto.getManufacturerCode()));			
					query.executeUpdate();	
					auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_MANUFACTURER,String.valueOf(dto.getManufacturerCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
			msg= AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch(HibernateException e){
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ManufacturerDaoImpl-->editManufacuturer()-->End");	
		return msg;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ManufacturerDto loadManufacuturer(int manufacturerId) throws ImproException {
		LOG.info("ManufacturerDaoImpl-->loadManufacuturer(int)-->Begin");
		Session session=null;		
		ManufacturerDto dto=null;
		try {
			session = sessionFactory.openSession();		
			Query query = session.createQuery("select m.manufacturerCode,m.manufacturerName "
					+ "from Manufacturers m where m.manufacturerCode=:manufacturerCode ");
			query.setParameter("manufacturerCode", manufacturerId);			
			List<Object[]> list = (List<Object[]>) query.list();
			if (null!=list && !list.isEmpty()) {				
				for (Object[] rs : list) {
					dto = setManufacturerDto(rs, dto);					
				}
			}
		}catch(HibernateException e){
			LOG.error("ManufacturerDaoImpl-->loadManufacuturer(int)-->End");
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ManufacturerDaoImpl-->loadManufacuturer(int)-->End");
		return dto;
	}

	@Override
	public boolean checkDuplicateManufacuturer(String name) throws ImproException {
		LOG.info("ManufacturerDaoImpl-->checkDuplicateManufacuturer()-->BEGIN");
		Session session = null;
		Integer count =0;
		boolean flag = false;
		StringBuilder searchQuery = new StringBuilder();
		try {
			session = sessionFactory.openSession();
			searchQuery.append("select count(manufacturerName) from Manufacturers where manufacturerName=:name");			
			String sql=searchQuery.toString();
			Query query = session.createSQLQuery(sql);
			query.setParameter("name", name);
			if(((Integer) query.uniqueResult()) > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		}catch (HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("ManufacturerDaoImpl-->checkDuplicateManufacuturer()-->Exception"+e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}		
		LOG.info("ManufacturerDaoImpl-->checkDuplicateManufacuturer()-->End");
		return flag;
	}
	
	@Override
	public boolean checkPresenceInProduct(int manufacturerCode) throws ImproException {
		LOG.info("ManufacturerDaoImpl-->checkPresenceInProduct()-->BEGIN");
		Session session = null;
		Integer count;
		boolean flag=false;
		StringBuilder searchQuery = new StringBuilder();
		try {
			session = sessionFactory.openSession();
			searchQuery.append("select count(ProductCode) from Products where BrandCode in (select BrandCode FROM "
					+ "Brands where BrandMfgCode="+manufacturerCode+")");			
			String sql=searchQuery.toString();
			Query query = session.createSQLQuery(sql);
			count = (Integer) query.uniqueResult();
			if((count) > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		}catch (HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("ManufacturerDaoImpl-->checkPresenceInProduct()-->Exception"+e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}		
		LOG.info("ManufacturerDaoImpl-->checkPresenceInProduct()-->End");
		return flag;
	}
	
	/**
	 * Update the brand extendedProdDesc  relation ship table 
	 * when manufacturer is deleted
	 * @param dto
	 * @param flag
	 * @throws ImproException
	 */
	public void deleteBrandExtendedProdDesc(ManufacturerDto dto) throws ImproException{
		LOG.info("ManufacturerDaoImpl-->deleteBrandExtendedProdDesc()-->End");
		try {			
			entityMgr.createNativeQuery("Delete from [CCE_IMGPro].[dbo].[ImgPro_Brand_Extended_Desc] WHERE brandCode in"
					+ " (select BrandCode FROM Brands where BrandMfgCode=?)")
			.setParameter(1, Integer.parseInt(dto.getManufacturerCode()))
			.executeUpdate();				
			
			LOG.info("ManufacturerDaoImpl-->deleteBrandExtendedProdDesc()-->End");
		} catch(PersistenceException e){
			throw new ImproException(e.getMessage());
		}
	}
	
	/**
	 * Update the brand   relation ship table 
	 * when manufacturer is deleted
	 * @param dto
	 * @param flag
	 * @throws ImproException
	 */
	private void deleteBrand(ManufacturerDto dto) throws ImproException{
		LOG.info("ManufacturerDaoImpl-->deleteBrand()-->End");
		Session session = null;		
		try {			
			session = sessionFactory.openSession();			
			Query query=null;
			
				query=session.createQuery("Delete from Brands WHERE manufacturers.manufacturerCode=:manufacturerCode");
				query.setParameter("manufacturerCode", Integer.parseInt(dto.getManufacturerCode()));				
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_BRANDS,String.valueOf(dto.getManufacturerCode()),AppConstants.TRANSACTION_TYPE_DELETE);
				
			LOG.info("ManufacturerDaoImpl-->deleteBrand()-->End");
		} catch(HibernateException e){
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
	

}
