package com.ko.impro.dao.impl.shape;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.shape.ShapeDao;
import com.ko.impro.dto.ShapeDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.shape.Shapes;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;


/**
 * @author hlekshmy : 
 * This class is an implementation of ShapeDao Interface
 */
@Repository
public class ShapeDaoImpl extends BaseDaoImpl implements ShapeDao {
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ShapeDaoImpl.class);

	/**
	 * This is to calculate number of count of rows in product table
	 */
	private int count;
	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;

	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired
	private AuditLogDao auditLog;

	@SuppressWarnings("unchecked")
	@Override
	public List<ShapeDto> loadShapes() throws ImproException {
		LOG.info("ShapeDaoImpl-->loadshapeID()-->BEGIN");
		Session session = null;
		List<ShapeDto> ShapedtoList = null;
		try {
			session = sessionFactory.openSession();
			Query query =null;
			ShapeDto dto = null;
			query = session.createQuery("select s.shapeCode,s.shapeId from Shapes s order by s.shapeId asc ");
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				ShapedtoList = new ArrayList<ShapeDto>();
				for (Object[] rs : list) {
					dto = setShapeDto(rs, dto);
					ShapedtoList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ShapeDaoImpl-->loadshapeID()-->END");
		return ShapedtoList;
	}

	/**
	 * Set the ShapeDto
	 * @param rs
	 * @param dto
	 * @return ShapeDto
	 */
	private ShapeDto setShapeDto(Object[] resultSet, ShapeDto dto) {
		dto = new ShapeDto();
		dto.setShapeCode(ApplicationUtil.checkNull(resultSet[0]));
		dto.setShapeId(ApplicationUtil.checkNull(resultSet[1]));
		return dto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ShapeDto loadshapeID(int shapeId) throws ImproException {
		LOG.info("ShapeDaoImpl-->loadshapeID(int)-->Begin");
		Session session = null;
		ShapeDto dto = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery("select s.shapeCode,s.shapeId from Shapes s where s.shapeId=:shapeId");
			query.setParameter("shapeId", shapeId);
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				for (Object[] rs : list) {
					dto = setShapeDto(rs, dto);
				}
			}
		} catch (HibernateException e) {
			LOG.error("ShapeDaoImpl-->loadshapeID(int)-->End");
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ShapeDaoImpl-->loadshapeID(int)-->End");
		return dto;
	}

	@Override
	public boolean checkDuplicateShape(String name) throws ImproException {
		LOG.info("ShapeDaoImpl-->checkDuplicateShape()-->BEGIN");
		Session session = null;
		boolean flag=false;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery("select count(shapeCode) from Shapes where shapeCode= :name");
			query.setParameter("name", name);
			if ((Long) query.uniqueResult() > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ShapeDaoImpl-->checkDuplicateShape()-->End");
		return flag;
	}


	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addShapes(ShapeDto shapeDto) throws ImproException {
		LOG.info("ShapeDaoImpl-->addShape()-->BEGIN");
		try {				
			Shapes shapes=new Shapes();			
			shapes.setShapeCode(shapeDto.getShapeCode());
			entityMgr.persist(shapes);
			auditLog.addLog(AppConstants.USER_NAME, AppConstants.TABLE_SHAPES, shapeDto.getShapeCode(), AppConstants.TRANSACTION_TYPE_ADD);
		} catch(PersistenceException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error(" ShapeDaoImpl-->addShape--> Exception occured "+ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		}
		LOG.info("ShapeDaoImpl-->addShape()-->End");	
	}
	
	
	//@Override
	public boolean checkPresenceInPackages(int shapeId) throws ImproException {
		LOG.info("ShapeDaoImpl-->checkPresenceInPackages()-->BEGIN");
		Session session = null;
		count =0;
		boolean flag = false;
		StringBuilder searchQuery = new StringBuilder();
		try {
			session = sessionFactory.openSession();
			searchQuery.append("select count(shapeId) from Packages where shapeId="+shapeId);
			String sql=searchQuery.toString();
			Query query = session.createSQLQuery(sql);
			count = (Integer) query.uniqueResult();
			if((count) > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		}catch (HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("ShapeDaoImpl-->checkPresenceInPackages()-->Exception"+e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}		
		LOG.info("ShapeDaoImpl-->checkPresenceInPackages()-->End");
		return flag;
	}
	
	/**
	 * Check the presence of package/shape in product
	 * @param shapeId
	 * @return boolean 
	 */
	private boolean isPackageSizeActive(int shapeId) {
		LOG.info("ShapeDaoImpl-->isPackageSizeActive()-->Begin");
		Session session = null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session
					.createSQLQuery("select count(*) from products p1"
							+ "  INNER JOIN Packages p2 ON p1.PackageCode=p2.PackageCode"
							+ "  INNER JOIN shapes p3 ON p2.ShapeId=p3.shapeId"
							+ "  and p3.shapeId=:shapeId ")
					.setParameter("shapeId", shapeId).uniqueResult();
			if (count > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ShapeDaoImpl-->isPackageSizeActive()-->End");
		return flag;
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editShapes(ShapeDto dto) throws ImproException {
		LOG.info("ShapeDaoImpl-->editShapes()-->BEGIN");
		Session session = null;
		Query query = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
		
				query = session.createQuery("Update Shapes set shapeCode = :shapeCode WHERE shapeId = :shapeId");					
				query.setParameter("shapeCode", dto.getShapeCode());
				query.setParameter("shapeId",Integer.parseInt(dto.getShapeId()));
				query.executeUpdate();
				auditLog.addLog(AppConstants.USER_NAME, AppConstants.TABLE_SHAPES, dto.getShapeCode(), AppConstants.TRANSACTION_TYPE_CHANGE);			
			
			msg = AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch (HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("ShapeDaoImpl-->editShapes()-->Exception " + e.getMessage());
			}
			msg = AppConstants.FAILURE_MSG;
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return msg;
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deleteShapes(ShapeDto dto) throws ImproException {
		LOG.info("ShapeDaoImpl-->deleteShapes()-->Begin");
		Session session = null;
		Query query = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
			if (isPackageSizeActive(Integer.parseInt(dto.getShapeId()))) {
				return "Package-Shape combination are currently active on (" + count + ") Products ";
			} else {
				query = session.createQuery(
						"Delete from Shapes WHERE shapeId = :shapeId");
				query.setParameter("shapeId", Integer.parseInt(dto.getShapeId()));
				query.executeUpdate();
				auditLog.addLog(AppConstants.USER_NAME, AppConstants.TABLE_SHAPES, dto.getShapeCode(), AppConstants.TRANSACTION_TYPE_DELETE);

				query = session.createQuery("Delete from Packages WHERE shapes.shapeId=:shapeId");
				query.setParameter("shapeId", Integer.parseInt(dto.getShapeId()));
				query.executeUpdate();
				auditLog.addLog(AppConstants.USER_NAME, AppConstants.TABLE_PACKAGES, dto.getShapeCode(), AppConstants.TRANSACTION_TYPE_DELETE);
			}

			msg = AppConstants.DATA_DELETE_SUCC_MSG;
		} catch (HibernateException e) {
			msg = AppConstants.FAILURE_MSG;
			LOG.error("ShapeDaoImpl-->deleteShapes()-->End");
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ShapeDaoImpl-->deleteShapes()-->End");
		return msg;
	}
	

}
