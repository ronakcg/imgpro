package com.ko.impro.dao.impl.extendedproductdesc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.extendedproductdesc.ExtendedProductDescDao;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.brand.Brands;
import com.ko.impro.model.extendedproductdesc.ExtendedProductDesc;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author hlekshmy 
 * This class is an implementation of ExtendedProductDescDao Interface
 */
@Repository
public class ExtendedProductDescDaoImpl implements ExtendedProductDescDao {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ExtendedProductDescDaoImpl.class);
	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;
	

	/**
	 * This is to calculate number of count of rows in product table
	 */
	private int count;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ExtendedProductDescDto> loadExtendedProductDesc() throws ImproException {
		Session session=null;
		List<ExtendedProductDescDto> extProdList=null;
		try {
			session = sessionFactory.openSession();		
			ExtendedProductDescDto dto=null;
			Query query =null;
			
				query = session.createQuery("select e.extendedDescId,e.longProductDesc,e.shortProductDesc from ExtendedProductDesc e order by e.extendedDescId asc");
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				extProdList=new ArrayList<ExtendedProductDescDto>();
				for (Object[] rs : list) {
					dto = setExtendedProductDescDto(rs, dto);
					extProdList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		return extProdList; 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ExtendedProductDescDto> loadExtendedProductDesc(List<String> brandList) throws ImproException {
		Session session=null;
		ExtendedProductDescDto dto=null;
		List<ExtendedProductDescDto> extProdList=null;
		List<Integer> brandIdIntList = brandList.stream()
	            .map(Integer::valueOf).collect(Collectors.toList());
		try {
			session = sessionFactory.openSession();		
			String sql="select distinct exdesc.extendedDescId,exdesc.longProductDesc,exdesc.shortProductDesc  FROM ExtendedProductDesc exdesc join exdesc.brands brand " +
			          " where brand.brandCode in (:brandList) order by exdesc.longProductDesc asc";
			Query query = session.createQuery(sql);
			query.setParameterList("brandList", brandIdIntList).list();
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				extProdList=new ArrayList<ExtendedProductDescDto>();
				for (Object[] rs : list) {
					dto = setExtendedProductDescDto(rs, dto);
					extProdList.add(dto);
				}
			}
		} catch(HibernateException e) {
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return extProdList; 
	}
	
	/**
	 * Set the ExtendedProductDescDto
	 * @param rs
	 * @param dto
	 * @return ExtendedProductDescDto
	 */
	private ExtendedProductDescDto setExtendedProductDescDto(Object[] resultSet, ExtendedProductDescDto dto) {
		dto=new ExtendedProductDescDto();
		dto.setExtendedDescId(resultSet[0].toString());
		dto.setLongProductDesc(ApplicationUtil.checkNull(resultSet[1]));
		dto.setShortProductDesc(ApplicationUtil.checkNull(resultSet[2]));
		return dto;	
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addExtendedProductDesc(ExtendedProductDescDto extdedProdDescDto) throws ImproException {
		LOG.info("ExtendedProductDescDaoImpl-->addExtendedProductDesc()-->BEGIN");
		try {				
			ExtendedProductDesc extendedProdDesc=new ExtendedProductDesc();			
			extendedProdDesc.setLongProductDesc(extdedProdDescDto.getLongProductDesc());
			extendedProdDesc.setShortProductDesc(extdedProdDescDto.getShortProductDesc());
			Set<Brands> brandSet= new HashSet<>();
			Brands brand= new Brands();
			brand.setBrandCode(Integer.parseInt(extdedProdDescDto.getBrandDto().getBrandCode()));
			brandSet.add(brand);
			extendedProdDesc.setBrands(brandSet);
			entityMgr.persist(extendedProdDesc);
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_EXTENDED_PRODUCT_DESCRIPTION,String.valueOf(extendedProdDesc.getExtendedDescId()),AppConstants.TRANSACTION_TYPE_ADD);
			LOG.info("ExtendedProductDescDaoImpl-->addExtendedProductDesc()-->End");		
		} catch(PersistenceException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error(" ExtendedProductDescDaoImpl-->addExtendedProductDesc--> Exception occured "+ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		}
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deleteExtendedProductDesc(ExtendedProductDescDto dto) throws ImproException {
		LOG.info("ExtendedProductDescDaoImpl-->deleteExtendedProductDesc()-->Begin");
		Session session = null;		
		Query query=null;
		String msg="";
		try {
			session = sessionFactory.openSession();
			if(checkExtendedDescInProducts(Integer.parseInt(dto.getExtendedDescId()))) {							
				return "ExtendedDesc are currently in use on ("+count+") Products";
			} else {				
				query=session.createQuery("Delete from ExtendedProductDesc WHERE extendedDescId=:extendedDescId");
			   query.setParameter("extendedDescId", Integer.parseInt(dto.getExtendedDescId()));
			   query.executeUpdate();
			   auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_EXTENDED_PRODUCT_DESCRIPTION,String.valueOf(dto.getExtendedDescId()),AppConstants.TRANSACTION_TYPE_DELETE);
			   
			   Query query2=session.createSQLQuery("Delete from [CCE_IMGPro].[dbo].[ImgPro_Brand_Extended_Desc] " + 
			   		"where [CCE_IMGPro].[dbo].[ImgPro_Brand_Extended_Desc].Extended_Desc_id=:extendedDescId");
			   query2.setParameter("extendedDescId", Integer.parseInt(dto.getExtendedDescId()));
			   query2.executeUpdate();
			}
			
			msg= AppConstants.DATA_DELETE_SUCC_MSG;
		} catch(HibernateException e){
			msg=AppConstants.FAILURE_MSG;
			if(LOG.isErrorEnabled()) {
				LOG.error("ExtendedProductDescDaoImpl-->deleteExtendedProductDesc()-->End");
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ExtendedProductDescDaoImpl-->deleteExtendedProductDesc()-->End");	
		return msg;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editExtendedProductDesc(ExtendedProductDescDto dto) throws ImproException {
		LOG.info("ExtendedProductDescDaoImpl-->editExtendedProductDesc()-->BEGIN");
		Session session = null;	
		Query query=null;
		String msg="";
		try {
			session = sessionFactory.openSession();
			
				query=session.createQuery("Update ExtendedProductDesc set longProductDesc=:longProductDesc ,shortProductDesc=:shortProductDesc "
						+ "WHERE extendedDescId=:extendedDescId");
				query.setParameter("longProductDesc", dto.getLongProductDesc());
				query.setParameter("shortProductDesc", dto.getShortProductDesc());
				query.setParameter("extendedDescId", Integer.parseInt(dto.getExtendedDescId()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_EXTENDED_PRODUCT_DESCRIPTION,String.valueOf(dto.getExtendedDescId()),AppConstants.TRANSACTION_TYPE_CHANGE);
			msg= AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch(HibernateException e){
			if(LOG.isErrorEnabled()) {
				LOG.error("ExtendedProductDescDaoImpl-->editExtendedProductDesc()-->Exception "+e.getMessage());
			}
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return msg;
	}

	@Override
	public ExtendedProductDescDto loadExtendedProductDesc(int ExtnProdId) throws ImproException {
		LOG.info("ExtendedProductDescDaoImpl-->loadExtendedProductDesc(int id)-->Begin");
		Session session=null;
		Query query;
		ExtendedProductDescDto dto=null;
		try {
			session = sessionFactory.openSession();	
			query=session.createSQLQuery("select e.Extended_Desc_id,e.Long_Product_Desc, e.Short_Product_Desc,b.BrandName,b.brandCode"
					+ "  from [CCE_IMGPro].[dbo].[Brands] b " + 
					"INNER JOIN  [CCE_IMGPro].[dbo].[ImgPro_Brand_Extended_Desc] be ON be.BrandCode=b.BrandCode " + 
					"INNER JOIN [CCE_IMGPro].[dbo].[ImgPro_Extended_Product_Desc] e ON e.Extended_Desc_id=be.Extended_Desc_id " + 
					"and e.Extended_Desc_id=:extendedDescId").
					setParameter("extendedDescId", ExtnProdId);
			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				for (Object[] rs : list) {
					dto= new ExtendedProductDescDto();
					dto.setExtendedDescId(rs[0].toString());
					BrandDto brandDto= new BrandDto();
					dto.setBrandDto(brandDto);
					dto.setLongProductDesc(ApplicationUtil.checkNull(rs[1]));
					dto.setShortProductDesc(ApplicationUtil.checkNull(rs[2]));	
					brandDto.setBrandName(ApplicationUtil.checkNull(rs[3]));
					brandDto.setBrandCode(ApplicationUtil.checkNull(rs[4]));
				}
			}
		} 
		catch(HibernateException e) {
			LOG.info(e.getMessage());
		}
		finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ExtendedProductDescDaoImpl-->loadExtendedProductDesc(int id)-->End");
		return dto;
	}

	@Override
	public boolean checkDuplicateExtendedProductDesc(String name,boolean isLongDesc) throws ImproException {
		LOG.info("ExtendedProductDescDaoImpl-->checkDuplicateExtendedProductDesc()-->BEGIN");
		Session session = null;
		Query query=null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			if(isLongDesc) 
				query=session.createQuery("select count(longProductDesc) from ExtendedProductDesc where longProductDesc= :name");
			else 
				query =session.createQuery("select count(shortProductDesc) from ExtendedProductDesc where shortProductDesc= :name");
			query.setParameter("name",name);
			if ((Integer) query.uniqueResult() > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		} catch(HibernateException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("ExtendedProductDescDaoImpl-->checkDuplicateExtendedProductDesc()-->Exception" +ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ExtendedProductDescDaoImpl-->checkDuplicateExtendedProductDesc()-->End");
		return flag;
	}
	
	/**
	 * Check extended Desc in Products
	 * @param extendeDescId
	 * @return boolean 
	 * @throws ImproException
	 */
	private boolean checkExtendedDescInProducts(int extendeDescId)  throws ImproException {
		LOG.info("ExtendedProductDescDaoImpl-->checkExtendedDescInProducts()-->BEGIN");
		Session session=null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();		
			count=(Integer)session.createSQLQuery("select count(*)  from Products p  where p.Extended_Desc_id =:extendeDescId ")
			.setParameter("extendeDescId", extendeDescId).uniqueResult();
			if(count > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ExtendedProductDescDaoImpl-->checkExtendedDescInProducts()-->End");
		return flag;
	}

	
}
