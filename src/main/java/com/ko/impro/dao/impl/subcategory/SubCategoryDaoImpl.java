package com.ko.impro.dao.impl.subcategory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.impl.category.CategoryDaoImpl;
import com.ko.impro.dao.subcategory.SubCategoryDao;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.subcategory.SubCategory;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author hlekshmy
 * This class is an implementation of SubCategoryDao Interface
 */
@Repository
public class SubCategoryDaoImpl  extends BaseDaoImpl implements SubCategoryDao {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(CategoryDaoImpl.class);

	/**
	 * This is to calculate number of count of rows in product table
	 */
	private long count;
	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	 
	@SuppressWarnings("unchecked")
	@Override
	public List<SubCategoryDto> loadSubCategory() throws ImproException {
		LOG.info("SubCategoryDaoImpl-->loadSubCategory()-->Begin");
		Session session=null;
		List<SubCategoryDto> subcategoryList=null;
		try {
			session = sessionFactory.openSession();		
			SubCategoryDto dto=null;
			Query query=null;
			query = session.createQuery("SELECT c.subCategoryCode,c.subCategoryDescription FROM SubCategory c ORDER BY subCategoryDescription asc");
			
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				subcategoryList=new ArrayList<SubCategoryDto>();
				for (Object[] rs : list) {
					dto = setSubCategoryDto(rs, dto);
					subcategoryList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SubCategoryDaoImpl-->loadSubCategory()-->Begin");
		return subcategoryList;
	}

	/**
	 * Set the SubCategoryDto
	 * @param rs
	 * @param dto
	 * @return SubCategoryDto
	 */
	private SubCategoryDto setSubCategoryDto(Object[] resultSet, SubCategoryDto dto) {
		dto=new SubCategoryDto();
		dto.setSubCategoryCode(resultSet[0].toString());
		dto.setSubCategoryDescription(ApplicationUtil.checkNull(resultSet[1]));
		return dto;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SubCategoryDto> loadSubCategory(List<String> categoryList) throws ImproException {
		LOG.info("SubCategoryDaoImpl-->loadSubCategory(List<String> categoryList)-->Begin");
		Session session=null;
		List<SubCategoryDto> subCategoryList=null;
		List<Short> subCatShortList = categoryList.stream()
	            .map(Short::valueOf).collect(Collectors.toList());
		try {
			session = sessionFactory.openSession();		
			SubCategoryDto dto=null;
			String sql="SELECT DISTINCT subcat.subCategoryCode,subcat.subCategoryDescription "
					+ "FROM Category cat join cat.subCategories subcat "
					+ "WHERE cat.categoryCode in(:categoryList) "
					+ "ORDER BY subcat.subCategoryDescription asc";
	      
			Query query = session.createQuery(sql);
			query.setParameterList("categoryList", subCatShortList);
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				subCategoryList=new ArrayList<SubCategoryDto>();
				for (Object[] rs : list) {
					dto = setSubCategoryDto(rs, dto);
					subCategoryList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SubCategoryDaoImpl-->loadSubCategory(List<String> categoryList)-->End");
		return subCategoryList; 
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public SubCategoryDto loadSubCategory(int subCateid) throws ImproException {
		LOG.info("Sub-CategoryDaoImpl-->loadSubCategory()-->Begin");
		int count=0;
		Session session = null;
		List<CategoryDto> CategoryList = null;
		SubCategoryDto subCategoryDto = new SubCategoryDto();
		
		try {
			session = sessionFactory.openSession();
			List<Object[]> list = session.createSQLQuery("SELECT s.SubCategoryCode,s.SubCategoryDescription,"
					+ "	c.CategoryCode,c.CategoryDescription"
					+ " FROM [CCE_IMGPro].[dbo].[Category] c"
					+ " INNER JOIN [CCE_IMGPro].[dbo].[ImgPro_Category_SubCategory] ics ON c.CategoryCode=ics.CategoryCode"
					+ " INNER JOIN [CCE_IMGPro].[dbo].[SubCategory] s ON s.SubCategoryCode=ics.SubCategoryCode"
					+ " WHERE s.SubCategoryCode=:SubSegmentCode")
					.setParameter("SubSegmentCode", subCateid)
					.list();
			
			if (null != list && !list.isEmpty()) {
				CategoryList = new ArrayList<CategoryDto>();
				for (Object[] rs : list) {
					if(count==0) {
						subCategoryDto.setSubCategoryCode(rs[0].toString());
						subCategoryDto.setSubCategoryDescription(ApplicationUtil.checkNull(rs[1]));
						CategoryDto catDto=new CategoryDto();
						catDto.setCategoryCode(rs[2].toString());
						catDto.setCategoryDescription(ApplicationUtil.checkNull(rs[3]));
						CategoryList.add(catDto);
						count++;
					} else {
						CategoryDto catDto=new CategoryDto();
						catDto.setCategoryCode(rs[2].toString());
						catDto.setCategoryDescription(ApplicationUtil.checkNull(rs[3]));
						CategoryList.add(catDto);
					}
					subCategoryDto.setCategoryDtolist(CategoryList);
				}
			}	
		} catch(HibernateException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("Sub-CategoryDaoImpl-->loadSubCategory()-->Excpetion"+ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("Sub-CategoryDaoImpl-->loadSubCategory()-->End");
		return subCategoryDto;
	}	

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public int addSubCategory(SubCategoryDto subcategoryDto) throws ImproException {
		int genSubCatCode=0;
		LOG.info("Sub-CategoryServiceImpl-->addSubCategory()-->Begin");
		try {
			SubCategory subcategory = new SubCategory();
			subcategory.setSubCategoryDescription(subcategoryDto.getSubCategoryDescription());
			entityMgr.persist(subcategory);
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_SUBCATEGORY,String.valueOf(subcategory.getSubCategoryCode()),AppConstants.TRANSACTION_TYPE_ADD);
			genSubCatCode = subcategory.getSubCategoryCode();
		} catch(PersistenceException ex) {
			throw new ImproException(ex.getMessage());
		} 
		
		LOG.info("Sub-CategoryServiceImpl-->addSubCategory()-->End");
		return genSubCatCode;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deleteSubCategory(SubCategoryDto subcategoryDto) throws ImproException {
		LOG.info("Sub-CategoryDaoImpl-->deleteSubCategory()-->Begin");
		Query query;
		Session session = null;
		String msg="";
		try {
			session = sessionFactory.openSession();
					if(checkPresenceInProductForCategory(Integer.parseInt(subcategoryDto.getSubCategoryCode()))) {		
						return "SubCategory-Category are currently in use on ("+count+") Products ";
					} else if(checkPresenceInProductForFlavor(Integer.parseInt(subcategoryDto.getSubCategoryCode()))){
						return "SubCategory-Flavor are currently in use on ("+count+") Products ";
					} else if(checkPresenceInProductForSegment(Integer.parseInt(subcategoryDto.getSubCategoryCode()))) {
						return "SubCategory-Segment are currently in use on ("+count+") Products ";
					} else {
						query=session.createSQLQuery("DELETE FROM [CCE_IMGPro].[dbo].[ImgPro_Category_SubCategory] WHERE SubCategoryCode = :SubCategoryCode");
						query.setParameter("SubCategoryCode", subcategoryDto.getSubCategoryCode());
						query.executeUpdate();
						
						query=session.createSQLQuery("DELETE FROM [CCE_IMGPro].[dbo].[ImgPro_Flavor_SubCategory] WHERE SubCategoryCode = :SubCategoryCode");
						query.setParameter("SubCategoryCode", subcategoryDto.getSubCategoryCode());
						query.executeUpdate();
						
						query=session.createSQLQuery("DELETE FROM [CCE_IMGPro].[dbo].[ImgPro_Segment_SubCategory] WHERE SubCategoryCode = :SubCategoryCode");
						query.setParameter("SubCategoryCode", subcategoryDto.getSubCategoryCode());
						query.executeUpdate();
						
						query=session.createQuery("DELETE FROM SubCategory WHERE SubCategoryCode=:SubCategoryCode");
						query.setParameter("SubCategoryCode", Short.parseShort(subcategoryDto.getSubCategoryCode()));
						query.executeUpdate();
						auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_SUBCATEGORY,String.valueOf(subcategoryDto.getSubCategoryCode()),AppConstants.TRANSACTION_TYPE_DELETE);
						msg= AppConstants.DATA_DELETE_SUCC_MSG;
					}
		} catch(HibernateException ex) {
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
			
        LOG.info("Sub-CategoryDaoImpl-->deleteSubCategory()-->End");
        return msg;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editSubCategory(SubCategoryDto subcategoryDto) throws ImproException {
		LOG.info("Sub-CategoryDaoImpl-->editSubCategory()-->Begin");
		
		Query query=null;
		Session session = null;
		String msg="";
		try {
			session = sessionFactory.openSession();
			
				query=session.createQuery("UPDATE SubCategory SET SubCategoryDescription=:SubCategoryDescription WHERE SubCategoryCode=:SubCategoryCode");
				query.setParameter("SubCategoryDescription",subcategoryDto.getSubCategoryDescription());
				query.setParameter("SubCategoryCode", Short.parseShort(subcategoryDto.getSubCategoryCode()));
				query.executeUpdate();
				
			
			// To add New Category to sub category. It will reflect on Join table
			if(subcategoryDto.getAddCategoryDtolist() != null ) {
				for(CategoryDto categoryDto : subcategoryDto.getAddCategoryDtolist()) {
					entityMgr.createNativeQuery("INSERT INTO  [CCE_IMGPro].[dbo].[ImgPro_Category_SubCategory] VALUES (?,?)")
					.setParameter(1, Short.parseShort(categoryDto.getCategoryCode()))
					.setParameter(2, subcategoryDto.getSubCategoryCode())
					.executeUpdate();
				}
			}
			
			//To Remove existing Category from sub category. It will reflect on Join table			
			if(subcategoryDto.getRemoveCategoryDtoList() != null && subcategoryDto.getRemoveCategoryDtoList().size() > 0) {
				List <String> remvCatLst=new ArrayList<String>();
				for(CategoryDto categoryDto : subcategoryDto.getRemoveCategoryDtoList()) {	
					remvCatLst.add(categoryDto.getCategoryCode());
				}
				List<Short> subCatShortList = remvCatLst.stream().map(Short::valueOf).collect(Collectors.toList());
				if(checkPresenceInProduct(remvCatLst,Integer.parseInt(subcategoryDto.getSubCategoryCode()))) {
					return "SubCategory-Category are currently in use on ("+count+") Products ";
				} else {
					query=session.createSQLQuery("DELETE FROM [CCE_IMGPro].[dbo].[ImgPro_Category_SubCategory]"
							+ " WHERE CategoryCode in(:removeCategoryList)  AND SubCategoryCode=:SubCategoryCode");
					query.setParameterList("removeCategoryList", subCatShortList);
					query.setParameter("SubCategoryCode", subcategoryDto.getSubCategoryCode());
					query.executeUpdate();
				}
			}
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_SUBCATEGORY,String.valueOf(subcategoryDto.getSubCategoryCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
			msg= AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch(PersistenceException | HibernateException ex) {
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());
			
		} finally {
			HibernateUtil.closeSession(session);
		}
        LOG.info("Sub-CategoryDaoImpl-->editSubCategory()-->End");
        return msg;
	}
	
	@Override
	public boolean checkDuplicateSubCategory(String subCategoryDesc) throws ImproException {
		LOG.info("Sub-CategoryDaoImpl-->checkDuplicateSubCategory()-->Begin");
		Session session = null;
		Query query=null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery("select count(subCategoryDescription) from SubCategory where subCategoryDescription= :subCategoryDescription");
			query.setParameter("subCategoryDescription",subCategoryDesc);
			if((Integer) query.uniqueResult() > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		} catch(HibernateException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("Sub-CategoryDaoImpl-->checkDuplicateSubCategory()-->Exception" +ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("Sub-CategoryDaoImpl-->checkDuplicateSubCategory()-->END");
		return flag;
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addCategoyMapping(SubCategoryDto subcategoryDto, int genSubCatCode) throws ImproException {
		LOG.info("Sub-CategoryDaoImpl-->addCategoyMapping()-->Begin");
		try {
			if(subcategoryDto.getCategoryDtolist() != null && subcategoryDto.getCategoryDtolist().size()!=0) {
				for(CategoryDto dto : subcategoryDto.getCategoryDtolist()) {
					entityMgr.createNativeQuery("INSERT INTO [CCE_IMGPro].[dbo].[ImgPro_Category_SubCategory] "
							+ "([CategoryCode] ,[SubCategoryCode]) VALUES(?,?)")
					.setParameter(1, Short.parseShort(dto.getCategoryCode()))
					.setParameter(2, Short.parseShort(String.valueOf(genSubCatCode)))
					.executeUpdate();
				}
			}
		} catch(PersistenceException ex) {
			throw new ImproException(ex.getMessage());
		} 
		LOG.info("Sub-CategoryDaoImpl-->addCategoyMapping()-->End");
	}
	
	@Override
	public boolean checkPresenceInProduct(List<String> catCodeRemvLst,int subCategoryCode) throws ImproException {
		LOG.info("Sub-CategoryDaoImpl-->checkPresenceInProduct()-->BEGIN");
		Session session = null;
		boolean flag = false;
		List<Short> subCatShortList = catCodeRemvLst.stream().map(Short::valueOf).collect(Collectors.toList());
		try {
			session = sessionFactory.openSession();
			count = (Integer) session.createSQLQuery("SELECT COUNT(*)  FROM Products "
					+ " WHERE categoryCode in(:categoryCodeRemoveList) AND subCategoryCode=:subCategoryCode")
					.setParameterList("categoryCodeRemoveList",subCatShortList)
					.setParameter("subCategoryCode",subCategoryCode).uniqueResult();
			if(count > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("Sub-CategoryDaoImpl-->checkPresenceInProduct()-->End");
		return flag;
	}
	
	@Override
	public boolean checkPresenceInProductForCategory(int subCategoryCode) throws ImproException {
	
		LOG.info("Sub-CategoryDaoImpl-->checkPresenceInProductForCategory()-->Begin");
		Session session = null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session.createSQLQuery("SELECT COUNT(*) FROM products p1"
							+ "  INNER JOIN ImgPro_Category_SubCategory p2 ON p1.categoryCode=p2.categoryCode"
							+ "  AND p1.subCategoryCode=p2.subCategoryCode AND p2.subCategoryCode=:subCategoryCode")
							.setParameter("subCategoryCode", subCategoryCode).uniqueResult();
			if (count > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("Sub-CategoryDaoImpl-->checkPresenceInProductForCategory()-->End");
		return flag;
	}
	
	@Override
	public boolean checkPresenceInProductForFlavor(int subCategoryCode) throws ImproException {
		
		LOG.info("Sub-CategoryDaoImpl-->checkPresenceInProductForFlavor()-->Begin");
		Session session = null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session.createSQLQuery("SELECT COUNT(*) FROM products p1"
							+ "  INNER JOIN ImgPro_Flavor_SubCategory p2 ON p1.FlavorCode=p2.FlavorCode"
							+ "  AND p1.subCategoryCode=p2.subCategoryCode AND p2.subCategoryCode=:subCategoryCode")
							.setParameter("subCategoryCode", subCategoryCode).uniqueResult();
			if (count > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("Sub-CategoryDaoImpl-->checkPresenceInProductForFlavor()-->End");
		return flag;
	}
	
	@Override
	public boolean checkPresenceInProductForSegment(int subCategoryCode) throws ImproException {
		LOG.info("Sub-CategoryDaoImpl-->checkPresenceInProductForSegment()-->Begin");
		Session session = null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session.createSQLQuery("SELECT COUNT(*) FROM products p1"
					+ " INNER JOIN ImgPro_Segment_SubCategory p2 ON p1.SegmentCode=p2.SegmentCode"
					+ " AND p1.subCategoryCode=p2.subCategoryCode AND p2.subCategoryCode=:subCategoryCode")
							.setParameter("subCategoryCode", subCategoryCode).uniqueResult();
			if (count > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("Sub-CategoryDaoImpl-->checkPresenceInProductForSegment()-->End");
		return flag;
	}
	
}