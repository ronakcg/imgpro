package com.ko.impro.dao.impl.containertype;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.containerType.ContainerTypeDao;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.containertype.ContainerType;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * Description : This class is an implementation of ConatainerType Interface
 */
@Repository
public class ContainerTypeDaoImpl extends BaseDaoImpl implements ContainerTypeDao {
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ContainerTypeDaoImpl.class);
	
	/**
	 * This is to calculate number of count of rows in product table
	 */
	private int count;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	

	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<ContainerTypeDto> loadContainerTypes() throws ImproException {
		LOG.info("ContainerTypeDaoImpl-->loadContainerTypes()-->Begin");
		Session session=null;
		List<ContainerTypeDto> conList=null;
		try {
			session = sessionFactory.openSession();		
			ContainerTypeDto dto = null;
			Query query=null;
				query = session.createQuery("select c.containerTypeCode,c.containerTypeDescription from "
						+ " ContainerType c order by c.containerTypeDescription asc");
			
			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				conList=new ArrayList<ContainerTypeDto>();
				for (Object[] rs : list) {
					dto = setContainerTypeDto(rs, dto);
					conList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ContainerTypeDaoImpl-->loadContainerTypes()-->End");
		return conList; 
	}
	
	/**
	 * Set the set ContainerTypeDto
	 * @param rs
	 * @param dto
	 * @return ContainerTypeDto
	 */
	private ContainerTypeDto setContainerTypeDto(Object[] resultSet, ContainerTypeDto dto) {
		dto = new ContainerTypeDto();
		dto.setContainerTypeCode(resultSet[0].toString());
		dto.setContainerTypeDescription(ApplicationUtil.checkNull(resultSet[1]));
		return dto;	
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addContainerType(ContainerTypeDto containerTypeDto) throws ImproException {
		LOG.info("ContainerTypeDaoImpl-->addContainerType()-->BEGIN");
		try {				
			ContainerType containerType=new ContainerType();			
			containerType.setContainerTypeDescription(containerTypeDto.getContainerTypeDescription());
			entityMgr.persist(containerType);
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_CONTAINERTYPE,String.valueOf(containerType.getContainerTypeCode()),AppConstants.TRANSACTION_TYPE_ADD);
		} catch(PersistenceException ex) { 
			if(LOG.isErrorEnabled()) {
				LOG.error(" ContainerTypeDaoImpl-->addContainerType--> Exception occured "+ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		}
		LOG.info("ContainerTypeDaoImpl-->addContainerType()-->End");	
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deleteContainerType(ContainerTypeDto dto) throws ImproException {
		LOG.info("ContainerTypeDaoImpl-->deleteContainerType()-->Begin");
		Session session = null;
		Query query = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
			if (isContainerTypeActive(Integer.parseInt(dto.getContainerTypeCode()))) {
				return "ContainerType-Package combination  are currently in use on (" + count + ") Products ";
			} else {
				query = session.createQuery(
						"Delete from ContainerType WHERE containerTypeCode=:containerTypeCode");
				query.setParameter("containerTypeCode", Short.parseShort(dto.getContainerTypeCode()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_CONTAINERTYPE,String.valueOf(dto.getContainerTypeCode()),AppConstants.TRANSACTION_TYPE_DELETE);
				
				query = session.createQuery("Delete from Packages WHERE containerType.containerTypeCode=:containerTypeCode");
				query.setParameter("containerTypeCode", Short.parseShort(dto.getContainerTypeCode()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PACKAGES,String.valueOf(dto.getContainerTypeCode()),AppConstants.TRANSACTION_TYPE_DELETE);

			}
			msg = AppConstants.DATA_DELETE_SUCC_MSG;
		} catch (HibernateException e) {
			msg = AppConstants.FAILURE_MSG;
			LOG.error("ContainerTypeDaoImpl-->deleteContainerType()-->End");
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ContainerTypeDaoImpl-->deleteContainerType()-->End");
		return msg;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editContainerType(ContainerTypeDto dto) throws ImproException {
		LOG.info("ContainerTypeDaoImpl-->editContainerType()-->BEGIN");
		Session session = null;
		Query query = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
				query = session.createQuery(
						"Update ContainerType set containerTypeDescription=:containerTypeDescription  WHERE containerTypeCode=:containerTypeCode");
				query.setParameter("containerTypeDescription", dto.getContainerTypeDescription());
				query.setParameter("containerTypeCode", Short.parseShort(dto.getContainerTypeCode()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_CONTAINERTYPE,String.valueOf(dto.getContainerTypeCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
			msg = AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch (HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("ContainerTypeDaoImpl-->editContainerType()-->Exception " + e.getMessage());
			}
			msg = AppConstants.FAILURE_MSG;
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return msg;
	}

	@Override
	public ContainerTypeDto loadContainerType(int containerID) throws ImproException {
		LOG.info("ContainerTypeDaoImpl-->loadContainerTypes(int containerID)-->Begin");
		Session session = null;
		ContainerTypeDto dto = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery("select c.containerTypeCode,c.containerTypeDescription from ContainerType c "
							+ " where c.containerTypeCode=:containerTypeCode");
			query.setParameter("containerTypeCode", (short)containerID);
			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				for (Object[] rs : list) {
					dto = setContainerTypeDto(rs, dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ContainerTypeDaoImpl-->loadContainerTypes(int containerID)-->End");
		return dto;
	}

	@Override
	public boolean checkDuplicateContainerType(String contnrTypDesc) throws ImproException {
		LOG.info("ContainerTypeDaoImpl-->checkDuplicateContainerType()-->BEGIN");
		Session session = null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			Query query =session.createQuery("select count(containerTypeDescription) from ContainerType "
					+ "where containerTypeDescription= :containerTypeDescription");
					query.setParameter("containerTypeDescription",contnrTypDesc);
			if ((Long) query.uniqueResult() > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		} catch(HibernateException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("ContainerTypeDaoImpl-->checkDuplicateContainerType()-->Exception" +ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ContainerTypeDaoImpl-->checkDuplicateContainerType()-->End");
		return flag;
	}
	
	/**
	 * Check the presence of packageCode/ContainerTypeCode in product
	 * @param ContainerTypeCode
	 * @return boolean 
	 */
	private boolean isContainerTypeActive(int containerTypeCode) {
		LOG.info("ContainerTypeDaoImpl-->isContainerTypeActive()-->Begin");
		Session session = null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session
					.createSQLQuery("select count(*) from products p1"
							+ "  INNER JOIN Packages p2 ON p1.PackageCode=p2.PackageCode"
							+ "  INNER JOIN ContainerType p3 ON p2.ContainerType=p3.ContainerTypeCode"
							+ "  and p3.ContainerTypeCode=:containerTypeCode")
					.setParameter("containerTypeCode", containerTypeCode).uniqueResult();
			if (count > 0) {
				LOG.info("ContainerTypeDaoImpl-->isContainerTypeActive()-->End");
				flag =  true;
			} else {
				LOG.info("ContainerTypeDaoImpl-->isContainerTypeActive()-->End");
				flag =  false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		return flag;
	}

}