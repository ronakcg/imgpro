package com.ko.impro.dao.impl.product;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.product.ArchiveProductDao;
import com.ko.impro.dao.product.ProductDao;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author nvohra
 * Implementation class of all Dao level methods.
 */
@Repository
public class ArchiveProductDaoImpl extends BaseDaoImpl implements ArchiveProductDao  {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ArchiveProductDaoImpl.class);
	
	@Autowired(required = true)
	AuditLogDao auditDao;
	
	@Autowired(required = true)
	ProductDao prodDao;
	
	@PersistenceContext
	EntityManager entityMgr;
	
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Method to archive the products to the product table
	 * @param  List<ProductDto> prodDtoList,String deleteReason,String userId
	 */
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void archiveProducts(List<ProductDto> prodDtoList,String deleteReason,String userId) throws ImproException {
		LOG.info("ProductDaoImpl-->archiveProduct()-->Begin");
        Session session = HibernateUtil.getSessionFactory(entityMgr).openSession();
        try {
        	Transaction tx = session.beginTransaction();
        	int count = 0;
        	for(ProductDto dto:prodDtoList) {
        		String sql="Update [CCE_IMGPro].[dbo].[Products] set [CCE_IMGPro].[dbo].[Products].archiveFlag=:archiveFlag, [CCE_IMGPro].[dbo].[Products].ArchiveDate"
        				+ "=CONVERT(VARCHAR(23), GETDATE(), 121), [CCE_IMGPro].[dbo].[Products].ArchiveReason=:archiveReason"
	        			+ " where [CCE_IMGPro].[dbo].[Products].productcode=:productcode and [CCE_IMGPro].[dbo].[Products].ProductId=:ProductId";
	        	
        		    if(LOG.isInfoEnabled()) {
        		    	LOG.info("archiveProduct query "+sql);
        		    }
	        		SQLQuery query = session.createSQLQuery(sql);
	        		query.setParameter("archiveFlag",true);
	        		query.setParameter("archiveReason", deleteReason);
	        		query.setParameter("productcode", Integer.parseInt(dto.getProductCode()));
	        		query.setParameter("ProductId", dto.getBitMapName());
	        		query.executeUpdate();
	        		auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PRODUCTS,String.valueOf(dto.getProductCode()),AppConstants.TRANSACTION_TYPE_ARCHIVE);
        		
    			count++;
    		    if (count % 50== 0) { 
    				session.flush(); 
    				session.clear(); 
    			}

        	   }
        	 tx.commit();
		} catch(Exception ex) {
		    if(LOG.isErrorEnabled()) {
		    	LOG.error("ProductDaoImpl-->archiveProduct()-->Excetion" +ex.getMessage());
		    }
			throw new ImproException(ex.getMessage());
		} finally {
			session.close();
		}
		
		LOG.info("ProductDaoImpl-->archiveProduct()-->End");


	}
	

	@Override
	public ArchivedProductDto getArchivedProductDetails(String id) throws ImproException {
		LOG.info("ProductDaoImpl-->getArchivedProductDetails()-->Begin");
		Session session = null;
		ArchivedProductDto deletedProductDto=null;
		try {
			session = sessionFactory.openSession();
			ProductSearchDto productSearchDto=new ProductSearchDto();
			productSearchDto.setProductCode(id);
			productSearchDto.setActiveSearch(false);
			List<ArchivedProductDto> deletedProductDtoList=prodDao.searchArchivedProducts(productSearchDto,false);
			deletedProductDto = new ArchivedProductDto();
			deletedProductDto=deletedProductDtoList.get(0);
			
			/*
			 * The query will fetch additional details of an archived product
			 */
			String getProductQuery=("SELECT pk.UnitHeight,pk.UnitWidth,pk.UnitDepth FROM CCE_IMGPro.[dbo].products p INNER JOIN CCE_IMGPro.[dbo].Packages pk ON pk.PackageCode=p.PackageCode " +  
					"WHERE productcode=:productCode and ProductId=:ProductId");
			if(LOG.isInfoEnabled()) {
			LOG.info("Archive Details Query"+getProductQuery);
			}
			Query query=session.createSQLQuery(getProductQuery);
			query.setParameter("productCode",deletedProductDto.getProductCode());
			query.setParameter("ProductId", deletedProductDto.getBitMapName());
			@SuppressWarnings("unchecked")
			List<Object[]>productDetails=(List<Object[]>)query.list();
			
			if (productDetails.size() > 0) {
				for (Object[] rs : productDetails) {
					deletedProductDto = setDeletedProductDetailsDto(rs,deletedProductDto);
				}
			}
		}
		catch(Exception e) {
			if(LOG.isErrorEnabled()) {
			LOG.error("ProductDaoImpl-->getArchivedProductDetails()-->Exception"+e.getMessage());
			}
			throw new ImproException(e.getMessage());
		}
		finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ProductDaoImpl-->getArchivedProductDetails()-->End");
		return deletedProductDto;
	}

	/**
	 * This method will set additional values into the ArchivedProductDTO
	 * @param rs
	 * @param productDto
	 * @return
	 */
	private ArchivedProductDto setDeletedProductDetailsDto(Object []rs,ArchivedProductDto productDto) {
		PackageDto dto=new PackageDto();
		dto.setUnitHeight(ApplicationUtil.checkNull(rs[0]));
		dto.setUnitWidth(ApplicationUtil.checkNull(rs[1]));
		dto.setUnitDepth(ApplicationUtil.checkNull(rs[2]));
		dto.setPackageCode(productDto.getPkgdto().getPackageCode());
		dto.setPackageName(productDto.getPkgdto().getPackageName());
		productDto.setPkgdto(dto);
		return productDto;
	}

	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void purgeArchivedProducts(List<ArchivedProductDto> archivedProductDtoList) throws ImproException {
		LOG.info("ArchivedProductDaoImpl-->purgeArchivedProducts()-->Begin");
        Session session = HibernateUtil.getSessionFactory(entityMgr).openSession();
        try {
        	Transaction tx = session.beginTransaction();
        	int count = 0;
        	for(ArchivedProductDto dto: archivedProductDtoList) {
    		    String purgeQueryStr = "Delete FROM [CCE_IMGPro].[dbo].[Products]  WHERE productcode=:productcode AND ProductId=:ProductId";
        		SQLQuery purgeQuery = session.createSQLQuery(purgeQueryStr.toString());
        		purgeQuery.setParameter("productcode",dto.getProductCode());
        		purgeQuery.setParameter("ProductId",dto.getBitMapName());
    			purgeQuery.executeUpdate();
    			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PRODUCTS,String.valueOf(dto.getProductCode()),AppConstants.TRANSACTION_TYPE_PURGE);
    			count++;
    		    if (count % 50== 0) { 
    				session.flush(); 
    				session.clear(); 
    			}
     	   }
     	 tx.commit();
		} catch(Exception ex) {
			throw new ImproException(ex.getMessage());
		} finally {
			session.close();
		}
        
        LOG.info("ArchivedProductDaoImpl-->purgeArchivedProducts()-->End");
    	
	}
	
}
	


	

