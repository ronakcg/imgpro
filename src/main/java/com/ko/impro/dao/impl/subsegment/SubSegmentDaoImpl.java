package com.ko.impro.dao.impl.subsegment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.impl.segment.SegmentDaoImpl;
import com.ko.impro.dao.subsegment.SubSegmentDao;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.subsegment.SubSegment;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author hlekshmy : 
 * This class is an implementation of SubSegmentDao Interface
 */
@Repository
public class SubSegmentDaoImpl extends BaseDaoImpl implements SubSegmentDao {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SegmentDaoImpl.class);

	/**
	 * This is to calculate number of count of rows in product table
	 */
	private int count;
	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;

	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<SubSegmentDto> loadSubSegment() throws ImproException {
		LOG.info("SubSegmentDaoImpl()-->loadSubSegment()-->Begin");
		Session session = null;
		List<SubSegmentDto> subSegmentList = null;
		try {
			session = sessionFactory.openSession();
			SubSegmentDto dto = null;
			Query query = null;
			query = session.createQuery("SELECT sc.subSegmentCode,sc.subSegmentDescription FROM SubSegment sc ORDER BY sc.subSegmentDescription asc");
				
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				subSegmentList = new ArrayList<SubSegmentDto>();
				for (Object[] rs : list) {
					dto = setSubSegmentDto(rs);
					subSegmentList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SubSegmentDaoImpl()-->loadSubSegment()-->End");
		return subSegmentList;
	}

	@Override
	public List<SubSegmentDto> loadSubSegment(List<String> segmentList) throws ImproException {
		LOG.info("SubSegmentDaoImpl()-->loadSubSegment(List<String> segmentList)()-->Begin");
		Session session = null;
		List<SubSegmentDto> subSegmentList = null;
		List<Short> subSegShortList = segmentList.stream().map(Short::valueOf).collect(Collectors.toList());
		try {
			session = sessionFactory.openSession();
			SubSegmentDto dto = null;
			String sql = "SELECT DISTINCT subseg.subSegmentCode,subseg.subSegmentDescription FROM Segment seg join seg.subSegments subseg "
					+ " WHERE seg.segmentCode in(:segmentList) "
					+ " ORDER BY subseg.subSegmentDescription asc";
			Query query = session.createQuery(sql);
			query.setParameterList("segmentList", subSegShortList);

			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				subSegmentList = new ArrayList<SubSegmentDto>();
				for (Object[] rs : list) {
					dto = setSubSegmentDto(rs);
					subSegmentList.add(dto);

				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SubSegmentDaoImpl()-->loadSubSegment(List<String> segmentList)()-->End");
		return subSegmentList;
	}

	/**
	 * Set the SubSegmentDto
	 * @param rs
	 * @param dto
	 * @return SubSegmentDto
	 */
	private SubSegmentDto setSubSegmentDto(Object[] resultSet) {
		SubSegmentDto dto = new SubSegmentDto();
		dto.setSubSegmentCode(resultSet[0].toString());
		dto.setSubSegmentDescription(ApplicationUtil.checkNull(resultSet[1]));
		return dto;
	}

	

	@SuppressWarnings("unchecked")
	@Override
	public SubSegmentDto loadSubSegment(int subSegId) throws ImproException {
		LOG.info("SubSegmentDaoImpl-->loadSubSegment()-->Begin");
		int count=0;
		List<SegmentDto> segmentList = null;
		Session session = null;
		SubSegmentDto subSegmentDto = new SubSegmentDto();
		try {
			session = sessionFactory.openSession();
			List<Object[]> list = session.createSQLQuery(
					"SELECT ss.subSegmentCode,ss.SubSegmentDescription,s.SegmentCode,s.SegmentDescription "
					+ "FROM [CCE_IMGPro].[dbo].[Segment] s " + 
					"INNER JOIN [CCE_IMGPro].[dbo].[ImgPro_Segment_Subsegment] sss ON s.SegmentCode=sss.SegmentCode " + 
					"INNER JOIN [CCE_IMGPro].[dbo].[SubSegment] ss ON ss.SubSegmentCode=sss.SubSegmentCode " + 
					"WHERE ss.SubSegmentCode=:SubSegmentCode")
					.setParameter("SubSegmentCode", subSegId)
					.list();
			
			if (null != list && !list.isEmpty()) {
				segmentList= new ArrayList<>();
				for (Object[] rs : list) {
					SegmentDto segmentDto= new SegmentDto();
					if(count==0) {
						subSegmentDto.setSubSegmentCode(ApplicationUtil.checkNull(rs[0]));
						subSegmentDto.setSubSegmentDescription(ApplicationUtil.checkNull(rs[1]));
						segmentDto.setSegmentCode(ApplicationUtil.checkNull(rs[2]));
						segmentDto.setSegmentDescription(ApplicationUtil.checkNull(rs[3]));
						segmentList.add(segmentDto);
						count++;
					} else {
						segmentDto.setSegmentCode(ApplicationUtil.checkNull(rs[2]));
						segmentDto.setSegmentDescription(ApplicationUtil.checkNull(rs[3]));						
						segmentList.add(segmentDto);
					}
					subSegmentDto.setSegmentDtolist(segmentList);
				}
			}
		} catch (HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("SubSegmentDaoImpl-->loadSubSegment()-->Exception " + e.getMessage());
			}
			throw new ImproException(e.getMessage());
		}
		LOG.info("SubSegmentDaoImpl-->loadSubSegment()-->End");
		return subSegmentDto;
	}


	@Override
	@Transactional(rollbackFor = ImproException.class)
	public int addSubSegment(SubSegmentDto subSegmentDto) throws ImproException {
		int genSubSegCode = 0;
		LOG.info("SubSegmentServiceImpl-addSubSegment()-->Begin");
		try {
			SubSegment subSegment = new SubSegment();
			subSegment.setSubSegmentDescription(subSegmentDto.getSubSegmentDescription());
			entityMgr.persist(subSegment);
			auditDao.addLog(AppConstants.USER_NAME, AppConstants.TABLE_SUBSEGEMENT,
					String.valueOf(subSegment.getSubSegmentCode()), AppConstants.TRANSACTION_TYPE_ADD);
			genSubSegCode = subSegment.getSubSegmentCode();
		} catch (PersistenceException ex) {
			throw new ImproException(ex.getMessage());
		}
		LOG.info("SubSegmentServiceImpl-addSubSegment()-->End");
		return genSubSegCode;

	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deleteSubSegment(SubSegmentDto subSegmentDto) throws ImproException {
		LOG.info("SubSegmentDaoImpl()-->deleteSubSegment()-->Begin");
		Query query = null;
		Session session = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
				if (checkPresenceInProduct(Integer.parseInt(subSegmentDto.getSubSegmentCode()))) {
					return "SubSegment are currently in use on ("+count+") Products ";
				} else {
					query = session.createQuery("DELETE FROM [CCE_IMGPro].[dbo].[ImgPro_Segment_Subsegment] WHERE SubSegmentCode =:SubSegmentCode ");
					query.setParameter("SubSegmentCode", Short.parseShort(subSegmentDto.getSubSegmentCode()));
					query.executeUpdate();

					query = session.createQuery("DELETE FROM SubSegment WHERE SubSegmentCode=:SubSegmentCode");
					query.setParameter("SubSegmentCode", Short.parseShort(subSegmentDto.getSubSegmentCode()));
					query.executeUpdate();
					auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_SUBSEGEMENT,String.valueOf(subSegmentDto.getSubSegmentCode()),AppConstants.TRANSACTION_TYPE_DELETE);
					msg = AppConstants.DATA_DELETE_SUCC_MSG;
				}
				 query.executeUpdate();
				msg = AppConstants.DATA_DELETE_SUCC_MSG;
		} catch (HibernateException ex) {
			msg = AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SubSegmentDaoImpl()-->deleteSubSegment()-->End");
		return msg;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editSubSegment(SubSegmentDto subSegmentDto) throws ImproException {
		LOG.info("SubSegmentDaoImpl()-->editSubSegment()-->Begin");
		Query query = null;
		Session session = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
			
			query = session.createQuery("UPDATE SubSegment SET SubSegmentDescription=:SubSegmentDescription WHERE SubSegmentCode=:SubSegmentCode");
			query.setParameter("SubSegmentDescription", subSegmentDto.getSubSegmentDescription());
			query.setParameter("SubSegmentCode", Short.parseShort(subSegmentDto.getSubSegmentCode()));
			query.executeUpdate();				
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_SUBSEGEMENT,String.valueOf(subSegmentDto.getSubSegmentCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
				
			// To add New Segment to subSegment. It will reflect on Join table
			if (subSegmentDto.getAddSegmentDtoList() != null) {
				for (SegmentDto segmentDto : subSegmentDto.getAddSegmentDtoList()) {
					entityMgr.createNativeQuery("INSERT INTO  [CCE_IMGPro].[dbo].[ImgPro_Segment_Subsegment] VALUES (?,?)")
							.setParameter(1, Short.parseShort(segmentDto.getSegmentCode()))
							.setParameter(2, subSegmentDto.getSubSegmentCode())
							.executeUpdate();
				}
			}

			// To Remove existing Segment from subSegment. It will reflect on Join table
			if (subSegmentDto.getRemoveSegmentDtoList() != null && !subSegmentDto.getRemoveSegmentDtoList().isEmpty()) {
				List<String> segmentList = new ArrayList<String>();
				for (SegmentDto segmentDto : subSegmentDto.getRemoveSegmentDtoList()) {
					segmentList.add(segmentDto.getSegmentCode());
				}
				if (validateInProduct(Integer.parseInt(subSegmentDto.getSubSegmentCode()), segmentList)) {
					return "Segment cannot be removed.Segment-SubSegment combination are currently in use on (" + count
							+ ") Products ";
				} else {
					query = session.createQuery("DELETE FROM [CCE_IMGPro].[dbo].[ImgPro_Segment_Subsegment] WHERE SegmentCode IN(:segmentList) AND SubSegmentCode=: SubSegmentCode");
					query.setParameterList("segmentList", segmentList);
					query.setParameter("SubSegmentCode", subSegmentDto.getSubSegmentCode());
					query.executeUpdate();
				}

			}
			msg = AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch (PersistenceException | HibernateException ex) {
			msg = AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());

		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SubSegmentDaoImpl()-->editSubSegment()-->End");
		return msg;
	}
	
	
	/**
	 * Fetch the count ofsegment-SubSegment combination from products table
	 * @param subSegmentCode
	 * @param segmentList
	 * @return
	 */
	private boolean validateInProduct(int subSegmentCode,List<String> segmentList) {
		LOG.info("SubSegmentDaoImpl-validateInProduct(int subSegmentCode,List<String> segmentList)-->Begin");
		List<Short> segShortList = segmentList.stream().map(Short::valueOf).collect(Collectors.toList());
		Session session = null;
		boolean validate;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session.createSQLQuery("SELECT COUNT(*) FROM products p1"
							+ "  INNER JOIN ImgPro_Segment_Subsegment p2 ON p1.SegmentCode=p2.SegmentCode"
							+ "  AND p1.subSegmentCode=p2.subSegmentCode AND p1.SegmentCode in(:segmentList)")
							.setParameter("segmentCode", subSegmentCode)
							.setParameterList("segmentList", segShortList)
							.uniqueResult();
			if (count > 0) {
				validate= true;
			} else {
				validate= false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SubSegmentDaoImpl-->checkPresenceInProduct()-->End");
		return validate;
	}
	
	@Override
	public boolean checkDuplicateSubSegment(String subSegmentDesc) throws ImproException {
		LOG.info("SubSegmentDaoImpl-->checkDuplicateSubSegment()-->Begin");
		Session session = null;
		boolean duplicate=false;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery("SELECT COUNT(subSegmentDescription) FROM SubSegment WHERE subSegmentDescription= :subSegmentDescription");
			query.setParameter("subSegmentDescription", subSegmentDesc);
			if ((Long) query.uniqueResult() > 0) {
				duplicate= true;
			} else {
				duplicate= false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SubSegmentDaoImpl-->checkDuplicateSubSegment()-->End");
		return duplicate;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addSegmentSubSegmentMapping(SubSegmentDto subSegmentDto, int genSubSegmtCode)
			throws ImproException {
		LOG.info("SubSegmentDaoImpl()-->addSegmentSubSegmentMapping()-->Begin");
		try {
			if (!subSegmentDto.getSegmentDtolist().isEmpty()) {
				for (SegmentDto dto : subSegmentDto.getSegmentDtolist()) {
					count = entityMgr.createNativeQuery("INSERT INTO [CCE_IMGPro].[dbo].[ImgPro_Segment_Subsegment] ([SegmentCode] ,[SubSegmentCode]) "
					+ "VALUES(?,?)")
					.setParameter(1, Short.parseShort(dto.getSegmentCode()))
					.setParameter(2, Short.parseShort(String.valueOf(genSubSegmtCode)))
					.executeUpdate();
				}
			}
		} catch (PersistenceException ex) {
			throw new ImproException(ex.getMessage());
		}
		LOG.info("SubSegmentDaoImpl()-->addSegmentSubSegmentMapping()-->End");
	}

	/**
	 * Check the presence in products for Segment/SubSegement combination
	 * @param subSegmentCode
	 * @return boolean
	 */
	private boolean checkPresenceInProduct(int subSegmentCode) {
		LOG.info("SubSegmentDaoImpl-->checkPresenceInProduct()-->Begin");
		Session session = null;
		boolean validate;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session.createSQLQuery("SELECT COUNT(*) FROM products p1"
					+ "  INNER JOIN ImgPro_Segment_Subsegment p2 ON p1.SegmentCode=p2.SegmentCode"
					+ "  and p1.subSegmentCode=p2.subSegmentCode")
					.setParameter("segmentCode", subSegmentCode).uniqueResult();
			if (count > 0) {
				validate= true;
			} else {
				validate= false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SubSegmentDaoImpl-->checkPresenceInProduct()-->End");
		return validate;
	}
	
}
