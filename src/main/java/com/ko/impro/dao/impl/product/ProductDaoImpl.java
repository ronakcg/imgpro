package com.ko.impro.dao.impl.product;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.product.ProductDao;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.dto.EditImageDto;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.HiResImageDto;
import com.ko.impro.dto.JpgImageDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.dto.MissingImagesDto;
import com.ko.impro.dto.MockImageDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.StdImageDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.product.Products;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author nvohra Implementation class for all ProductDao methods.
 */
@Repository
public class ProductDaoImpl extends BaseDaoImpl implements ProductDao {

	/**
	 * Enum containing the list of columns used for sorting
	 *
	 */
	private static enum COLUMNNAME {
		PRODUCTNAME, UPC, DATEMAINTAINED, MANUFACTURERNAME, BRANDNAME, PACKAGENAME, PACKAGESIZEDESCIPTION, CATEGORYDESCRIPTION, SUBCATEGORYDESCRIPTION, FLAVORDESCRIPTION, SEGMENTDESCRIPTION, SUBSEGMENTDESCRIPTION,
	};

	/**
	 * enum defining the sort order
	 *
	 */
	private static enum COLUMNORDER {
		ASC, DESC
	};

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ProductDaoImpl.class);

	@PersistenceContext
	EntityManager entityMgr;

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired(required = true)
	AuditLogDao auditDao;

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public boolean restoreProduct(ArchivedProductDto dto) throws ImproException {
		LOG.info("ProductDaoImpl-->restoreProduct()-->Begin");
		try {
			if (checkDuplicateProduct(dto.getBitMapName(),true)) {
				return true;
			} else {
				String sql = "Update [CCE_IMGPro].[dbo].[Products]  set [CCE_IMGPro].[dbo].[Products].archiveFlag=:archiveFlag, [CCE_IMGPro].[dbo].[Products].ArchiveDate=:archiveDate, "
						+ "[CCE_IMGPro].[dbo].[Products].ArchiveReason=:archiveReason"
						+ " where [CCE_IMGPro].[dbo].[Products].productcode=:productcode and [CCE_IMGPro].[dbo].[Products].ProductId=:ProductId";
				if (LOG.isInfoEnabled()) {
					LOG.info("restoreProduct query " + sql);
				}
				entityMgr.createNativeQuery(sql)
				.setParameter("archiveFlag", false)
				.setParameter("archiveDate", null)
				.setParameter("archiveReason", "")
				.setParameter("productcode", Integer.parseInt(dto.getProductCode()))
				.setParameter("ProductId", dto.getBitMapName())
				.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.TABLE_PRODUCTS,
						String.valueOf(dto.getProductCode()), AppConstants.TRANSACTION_TYPE_RESTORE);
				LOG.info("ProductDaoImpl-->restoreProduct()-->End");
				return false;
			}
		} catch (Exception ex) {
			if (LOG.isErrorEnabled()) {
				LOG.error("ProductDaoImpl-->restoreProduct()-->Exception " + ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		} 
	}

	/**
	 * Methods for adding filter to the query
	 * 
	 * @param productSearchDto
	 * @param searchQuery
	 * @param isPagination
	 * @return
	 */
	public StringBuilder setFilter(ProductSearchDto productSearchDto, StringBuilder searchQuery) {
		if (null != productSearchDto.getId() && !"".equals(productSearchDto.getId())) {
			searchQuery.append(" AND ProductId =:ProductId");
		} 
		if (null != productSearchDto.getUpc() && !"".equals(productSearchDto.getUpc())) {
			searchQuery.append(" AND upc = :upc");
		}
		if (null != productSearchDto.getProductCode() && !"".equals(productSearchDto.getProductCode())) {
			searchQuery.append(" AND ProductCode = :ProductCode");
		}

		if (CollectionUtils.isNotEmpty(productSearchDto.getBrandList())) {
			if (1 == productSearchDto.getBrandList().size()) {
				searchQuery.append(" AND p.BrandCode =(:brandList)");
			} else {
				searchQuery.append(" AND p.BrandCode IN (:brandList)");
			}
		}
		if (null != productSearchDto.getProductName() && !"".equals(productSearchDto.getProductName())) {
			searchQuery.append("AND p.ProductName LIKE (:productName)");

		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getManufactList())) {
			if (1 == productSearchDto.getManufactList().size()) {
				searchQuery.append(" AND p.ManufacturerCode = :manufactList");
			} else {
				searchQuery.append(" AND p.ManufacturerCode IN (:manufactList)");
			}
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getPackageList())) {
			if (1 == productSearchDto.getPackageList().size()) {
				searchQuery.append(" AND p.PackageCode = :packageList");
			} else {
				searchQuery.append(" AND p.PackageCode IN (:packageList)");
			}
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getPackageSizeList())) {
			if (1 == productSearchDto.getPackageSizeList().size()) {
				searchQuery.append(" AND pks.PackageSizeCode = :packageSizeList");
			} else {
				searchQuery.append(" AND pks.PackageSizeCode IN (:packageSizeList)");
			}
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getCategoryList())) {
			if (1 == productSearchDto.getCategoryList().size()) {
				searchQuery.append(" AND p.CategoryCode = :categoryList");
			} else {
				searchQuery.append(" AND p.CategoryCode IN (:categoryList)");
			}
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getSubCategoryList())) {
			if (1 == productSearchDto.getSubCategoryList().size()) {
				searchQuery.append(" AND p.SubCategoryCode = :subCategoryList");
			} else {
				searchQuery.append(" AND p.SubCategoryCode IN (:subCategoryList)");
			}
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getFlavorList())) {
			if (1 == productSearchDto.getFlavorList().size()) {
				searchQuery.append(" AND p.flavorCode = :flavorList");
			} else {
				searchQuery.append(" AND p.flavorCode IN (:flavorList)");
			}
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getSegmentList())) {
			if (1 == productSearchDto.getSegmentList().size()) {
				searchQuery.append(" AND p.SegmentCode = :segmentList");
			} else {
				searchQuery.append(" AND p.SegmentCode IN (:segmentList)");
			}
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getSubSegmentList())) {
			if (1 == productSearchDto.getSubSegmentList().size()) {
				searchQuery.append(" AND p.SubSegmentCode = :subSegmentList");
			} else {
				searchQuery.append(" AND p.SubSegmentCode IN (:subSegmentList)");
			}
		}
		if (productSearchDto.isActiveSearch()) {
			if (null != productSearchDto.getFromDate() && !"".equals(productSearchDto.getFromDate())) {
				if (null == productSearchDto.getToDate() || "".equals(productSearchDto.getToDate())) {
					searchQuery.append(" AND p.DateMaintained > :fromDate");
				}
			}
			if (null == productSearchDto.getFromDate() || "".equals(productSearchDto.getToDate())) {
				if (null != productSearchDto.getToDate() && !"".equals(productSearchDto.getToDate())) {
					searchQuery.append(" AND p.DateMaintained < :toDate");
				}
			}
			if (null != productSearchDto.getFromDate() && !"".equals(productSearchDto.getFromDate())) {
				if (null != productSearchDto.getToDate()) {
					searchQuery.append(" AND p.DateMaintained between :fromDate AND :toDate");
				}
			}
		} else {
			if (null != productSearchDto.getFromDate() && !"".equals(productSearchDto.getFromDate())) {
				if (null == productSearchDto.getToDate() || "".equals(productSearchDto.getToDate())) {
					searchQuery.append(" AND p.ArchiveDate > :fromDate");
				}
			}
			if (null == productSearchDto.getFromDate() || "".equals(productSearchDto.getToDate())) {
				if (null != productSearchDto.getToDate() && !"".equals(productSearchDto.getToDate())) {
					searchQuery.append(" AND p.ArchiveDate < :toDate");
				}
			}
			if (null != productSearchDto.getFromDate() && !"".equals(productSearchDto.getFromDate())) {
				if (null != productSearchDto.getToDate()) {
					searchQuery.append(" AND p.ArchiveDate between :fromDate AND :toDate");
				}
			}
			
		}

		return searchQuery;
	}

	/**
	 * Methods for setting data for filter values
	 * 
	 * @param productSearchDto
	 * @param query
	 */
	public void setFilterData(ProductSearchDto productSearchDto, Query query) {
		if (CollectionUtils.isNotEmpty(productSearchDto.getBrandList())) {

			query.setParameterList("brandList", productSearchDto.getBrandList());
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getManufactList())) {

			query.setParameterList("manufactList", productSearchDto.getManufactList());
		}
		if (null != productSearchDto.getProductName() && !"".equals(productSearchDto.getProductName())) {
			query.setParameter("productName", "%" + productSearchDto.getProductName() + "%");
		}
		if (null != productSearchDto.getUpc() && !"".equals(productSearchDto.getUpc())) {
			query.setParameter("upc", productSearchDto.getUpc());
		}

		if (null != productSearchDto.getProductCode() && !"".equals(productSearchDto.getProductCode())) {
			query.setParameter("ProductCode", productSearchDto.getProductCode());
		}

		if (null != productSearchDto.getId() && !"".equals(productSearchDto.getId())) {
			query.setParameter("ProductId", productSearchDto.getId());
		}

		if (CollectionUtils.isNotEmpty(productSearchDto.getPackageList())) {
			query.setParameterList("packageList", productSearchDto.getPackageList());
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getPackageSizeList())) {
			query.setParameterList("packageSizeList", productSearchDto.getPackageSizeList());
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getCategoryList())) {
			query.setParameterList("categoryList", productSearchDto.getCategoryList());
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getSubCategoryList())) {
			query.setParameterList("subCategoryList", productSearchDto.getSubCategoryList());
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getFlavorList())) {
			query.setParameterList("flavorList", productSearchDto.getFlavorList());
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getSegmentList())) {
			query.setParameterList("segmentList", productSearchDto.getSegmentList());
		}
		if (CollectionUtils.isNotEmpty(productSearchDto.getSubSegmentList())) {
			query.setParameterList("subSegmentList", productSearchDto.getSubSegmentList());
		}

		if (null != productSearchDto.getFromDate() && !"".equals(productSearchDto.getFromDate())) {
			query.setParameter("fromDate", ApplicationUtil.setFormattedDate(productSearchDto.getFromDate()));
		}
		if (null != productSearchDto.getToDate() && !"".equals(productSearchDto.getToDate())) {
			query.setParameter("toDate", ApplicationUtil.setFormattedDate(productSearchDto.getToDate()));
		}

	}

	/**
	 * This method sets values into the ProductDto class from the resultset
	 * 
	 * @param rs
	 * @param dto
	 * @return
	 * @throws ImproException
	 */
	private ProductDto setProductListDto(Object[] rs, ProductDto dto) throws ImproException {
		dto = new ProductDto();
		dto.setProductCode(rs[0].toString());

		dto.setProductName(ApplicationUtil.checkNull(rs[1]));
		dto.setUpc(ApplicationUtil.checkNull(rs[2]));
		dto.setDateMaintained(ApplicationUtil.getformattedDate(ApplicationUtil.checkNull(rs[3])));

		ManufacturerDto manufacturerDto = new ManufacturerDto();
		manufacturerDto.setManufacturerCode(rs[4].toString());
		manufacturerDto.setManufacturerName(ApplicationUtil.checkNull(rs[5]));
		dto.setManufacturerDto(manufacturerDto);

		BrandDto brandDto = new BrandDto();
		brandDto.setBrandCode(rs[6].toString());
		brandDto.setBrandName(ApplicationUtil.checkNull(rs[7]));
		brandDto.setShortBrandName(ApplicationUtil.checkNull(rs[8]));
		dto.setBrandDto(brandDto);

		PackageDto packageDto = new PackageDto();
		packageDto.setPackageCode(rs[9].toString());
		packageDto.setPackageName(ApplicationUtil.checkNull(rs[10]));
		dto.setPkgdto(packageDto);

		PackageSizeDto packageSizeDto = new PackageSizeDto();
		packageSizeDto.setPackageSizeCode(ApplicationUtil.checkNull(rs[11]));
		packageSizeDto.setPackageSizeDescription(ApplicationUtil.checkNull(rs[12]));
		dto.setPackageSizeDto(packageSizeDto);

		FlavorDto flavorDto = new FlavorDto();
		flavorDto.setFlavorCode(rs[13].toString());
		flavorDto.setFlavorDescription(ApplicationUtil.checkNull(rs[14]));
		dto.setFlavorDto(flavorDto);

		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setCategoryCode(rs[15].toString());
		categoryDto.setCategoryDescription(ApplicationUtil.checkNull(rs[16]));
		dto.setCategoryDto(categoryDto);

		SubCategoryDto subCategoryDto = new SubCategoryDto();
		subCategoryDto.setSubCategoryCode(rs[17].toString());
		subCategoryDto.setSubCategoryDescription(ApplicationUtil.checkNull(rs[18]));
		dto.setSubCategoryDto(subCategoryDto);

		SegmentDto segmentDto = new SegmentDto();
		segmentDto.setSegmentCode(rs[19].toString());
		segmentDto.setSegmentDescription(ApplicationUtil.checkNull(rs[20]));
		dto.setSegmentDto(segmentDto);

		SubSegmentDto subSegmentDto = new SubSegmentDto();
		subSegmentDto.setSubSegmentCode(rs[21].toString());
		subSegmentDto.setSubSegmentDescription(ApplicationUtil.checkNull(rs[22]));

		dto.setSubSegmentDto(subSegmentDto);

		dto.setArchiveFlag(ApplicationUtil.setBoolean(ApplicationUtil.checkNull(rs[23])));

		ExtendedProductDescDto extendedProductDescDto = new ExtendedProductDescDto();
		extendedProductDescDto.setExtendedDescId(ApplicationUtil.checkNull(rs[24]));
		extendedProductDescDto.setLongProductDesc(ApplicationUtil.checkNull(rs[25]));
		extendedProductDescDto.setShortProductDesc(ApplicationUtil.checkNull(rs[26]));
		dto.setExtendedProductDescDto(extendedProductDescDto);

		MissingImagesDto missingImagesDto = new MissingImagesDto();
		missingImagesDto.setJpg(ApplicationUtil.setImageFlag(rs[27]));
		missingImagesDto.setLowRes1(ApplicationUtil.setImageFlag(rs[28]));
		missingImagesDto.setLowRes2(ApplicationUtil.setImageFlag(rs[29]));
		missingImagesDto.setLowRes3(ApplicationUtil.setImageFlag(rs[30]));
		missingImagesDto.setLowRes7(ApplicationUtil.setImageFlag(rs[31]));
		missingImagesDto.setLowRes8(ApplicationUtil.setImageFlag(rs[32]));
		missingImagesDto.setLowRes9(ApplicationUtil.setImageFlag(rs[33]));
		missingImagesDto.setHiRes1(ApplicationUtil.setImageFlag(rs[34]));
		missingImagesDto.setHiRes2(ApplicationUtil.setImageFlag(rs[35]));
		missingImagesDto.setHiRes3(ApplicationUtil.setImageFlag(rs[36]));
		missingImagesDto.setHiRes7(ApplicationUtil.setImageFlag(rs[37]));
		missingImagesDto.setHiRes8(ApplicationUtil.setImageFlag(rs[38]));
		missingImagesDto.setHiRes9(ApplicationUtil.setImageFlag(rs[39]));
		missingImagesDto.setMock1(ApplicationUtil.setImageFlag(rs[40]));
		missingImagesDto.setMock2(ApplicationUtil.setImageFlag(rs[41]));
		missingImagesDto.setMock3(ApplicationUtil.setImageFlag(rs[42]));
		missingImagesDto.setMock7(ApplicationUtil.setImageFlag(rs[43]));
		missingImagesDto.setMock8(ApplicationUtil.setImageFlag(rs[44]));
		missingImagesDto.setMock9(ApplicationUtil.setImageFlag(rs[45]));

		dto.setMissingImagesDto(missingImagesDto);
		dto.setBitMapName(ApplicationUtil.checkNull(rs[46]));
		dto.setOldBitMapName(dto.getBitMapName());
		
		dto.setShortProductName(ApplicationUtil.checkNull(rs[47]));
		dto.setUpc12(ApplicationUtil.checkNull(rs[48]));
		dto.setDateAdded(ApplicationUtil.getformattedDate(ApplicationUtil.checkNull(rs[50])));
		dto.setConsumptionType(ApplicationUtil.checkNull(rs[52]));
		dto.setNielsenUpc(ApplicationUtil.checkNull(rs[53]));
		dto.setNielsenDesc(ApplicationUtil.checkNull(rs[54]));

		dto.setAcnCategory(ApplicationUtil.checkNull(rs[55]));
		dto.setAcnSubCategory(ApplicationUtil.checkNull(rs[56]));
		dto.setAcnSegment(ApplicationUtil.checkNull(rs[57]));
		dto.setAcnSubSegment(ApplicationUtil.checkNull(rs[58]));
		dto.setAcnBeverageType(ApplicationUtil.checkNull(rs[59]));
		dto.setAcnKeyManufacturer(ApplicationUtil.checkNull(rs[60]));
		dto.setAcnBrand(ApplicationUtil.checkNull(rs[61]));
		dto.setAcnKeyFlavor(ApplicationUtil.checkNull(rs[62]));
		dto.setAcnLongName(ApplicationUtil.checkNull(rs[63]));
		dto.setAcnConsumptionType(ApplicationUtil.checkNull(rs[64]));
		return dto;
	}

	/**
	 * This method sets values into the ProductDto class from the resultset
	 * 
	 * @param rs
	 * @param dto
	 * @return
	 * @throws ImproException
	 */
	private ArchivedProductDto setArchiveProductListDto(Object[] rs, ArchivedProductDto dto) throws ImproException {
		dto = new ArchivedProductDto();
		dto.setProductCode(rs[0].toString());

		dto.setProductName(ApplicationUtil.checkNull(rs[1]));
		dto.setUpc(ApplicationUtil.checkNull(rs[2]));
		dto.setDateMaintained(ApplicationUtil.getformattedDate(ApplicationUtil.checkNull(rs[3])));

		ManufacturerDto manufacturerDto = new ManufacturerDto();
		manufacturerDto.setManufacturerCode(rs[4].toString());
		manufacturerDto.setManufacturerName(ApplicationUtil.checkNull(rs[5]));
		dto.setManufacturerDto(manufacturerDto);

		BrandDto brandDto = new BrandDto();
		brandDto.setBrandCode(rs[6].toString());
		brandDto.setBrandName(ApplicationUtil.checkNull(rs[7]));
		brandDto.setShortBrandName(ApplicationUtil.checkNull(rs[8]));
		dto.setBrandDto(brandDto);

		PackageDto packageDto = new PackageDto();
		packageDto.setPackageCode(rs[9].toString());
		packageDto.setPackageName(ApplicationUtil.checkNull(rs[10]));
		dto.setPkgdto(packageDto);

		PackageSizeDto packageSizeDto = new PackageSizeDto();
		packageSizeDto.setPackageSizeCode(ApplicationUtil.checkNull(rs[11]));
		packageSizeDto.setPackageSizeDescription(ApplicationUtil.checkNull(rs[12]));
		dto.setPackageSizeDto(packageSizeDto);

		FlavorDto flavorDto = new FlavorDto();
		flavorDto.setFlavorCode(rs[13].toString());
		flavorDto.setFlavorDescription(ApplicationUtil.checkNull(rs[14]));
		dto.setFlavorDto(flavorDto);

		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setCategoryCode(rs[15].toString());
		categoryDto.setCategoryDescription(ApplicationUtil.checkNull(rs[16]));
		dto.setCategoryDto(categoryDto);

		SubCategoryDto subCategoryDto = new SubCategoryDto();
		subCategoryDto.setSubCategoryCode(rs[17].toString());
		subCategoryDto.setSubCategoryDescription(ApplicationUtil.checkNull(rs[18]));
		dto.setSubCategoryDto(subCategoryDto);

		SegmentDto segmentDto = new SegmentDto();
		segmentDto.setSegmentCode(rs[19].toString());
		segmentDto.setSegmentDescription(ApplicationUtil.checkNull(rs[20]));
		dto.setSegmentDto(segmentDto);

		SubSegmentDto subSegmentDto = new SubSegmentDto();
		subSegmentDto.setSubSegmentCode(rs[21].toString());
		subSegmentDto.setSubSegmentDescription(ApplicationUtil.checkNull(rs[22]));

		dto.setSubSegmentDto(subSegmentDto);

		dto.setArchiveFlag(ApplicationUtil.setBoolean(ApplicationUtil.checkNull(rs[23])));

		ExtendedProductDescDto extendedProductDescDto = new ExtendedProductDescDto();
		extendedProductDescDto.setExtendedDescId(ApplicationUtil.checkNull(rs[24]));
		extendedProductDescDto.setLongProductDesc(ApplicationUtil.checkNull(rs[25]));
		extendedProductDescDto.setShortProductDesc(ApplicationUtil.checkNull(rs[26]));
		dto.setExtendedProductDescDto(extendedProductDescDto);

		MissingImagesDto missingImagesDto = new MissingImagesDto();
		missingImagesDto.setJpg(ApplicationUtil.setImageFlag(rs[27]));
		missingImagesDto.setLowRes1(ApplicationUtil.setImageFlag(rs[28]));
		missingImagesDto.setLowRes2(ApplicationUtil.setImageFlag(rs[29]));
		missingImagesDto.setLowRes3(ApplicationUtil.setImageFlag(rs[30]));
		missingImagesDto.setLowRes7(ApplicationUtil.setImageFlag(rs[31]));
		missingImagesDto.setLowRes8(ApplicationUtil.setImageFlag(rs[32]));
		missingImagesDto.setLowRes9(ApplicationUtil.setImageFlag(rs[33]));
		missingImagesDto.setHiRes1(ApplicationUtil.setImageFlag(rs[34]));
		missingImagesDto.setHiRes2(ApplicationUtil.setImageFlag(rs[35]));
		missingImagesDto.setHiRes3(ApplicationUtil.setImageFlag(rs[36]));
		missingImagesDto.setHiRes7(ApplicationUtil.setImageFlag(rs[37]));
		missingImagesDto.setHiRes8(ApplicationUtil.setImageFlag(rs[38]));
		missingImagesDto.setHiRes9(ApplicationUtil.setImageFlag(rs[39]));
		missingImagesDto.setMock1(ApplicationUtil.setImageFlag(rs[40]));
		missingImagesDto.setMock2(ApplicationUtil.setImageFlag(rs[41]));
		missingImagesDto.setMock3(ApplicationUtil.setImageFlag(rs[42]));
		missingImagesDto.setMock7(ApplicationUtil.setImageFlag(rs[43]));
		missingImagesDto.setMock8(ApplicationUtil.setImageFlag(rs[44]));
		missingImagesDto.setMock9(ApplicationUtil.setImageFlag(rs[45]));

		dto.setMissingImagesDto(missingImagesDto);
		dto.setBitMapName(ApplicationUtil.checkNull(rs[46]));
		dto.setOldBitMapName(dto.getBitMapName());
		
		dto.setShortProductName(ApplicationUtil.checkNull(rs[47]));
		dto.setUpc12(ApplicationUtil.checkNull(rs[48]));
		dto.setArchiveDate(ApplicationUtil.getformattedDate(ApplicationUtil.checkNull(rs[49])));
		dto.setDateAdded(ApplicationUtil.getformattedDate(ApplicationUtil.checkNull(rs[50])));
		dto.setArchiveReason(ApplicationUtil.checkNull(rs[51]));
		dto.setConsumptionType(ApplicationUtil.checkNull(rs[52]));
		dto.setNielsenUpc(ApplicationUtil.checkNull(rs[53]));
		dto.setNielsenDesc(ApplicationUtil.checkNull(rs[54]));

		dto.setAcnCategory(ApplicationUtil.checkNull(rs[55]));
		dto.setAcnSubCategory(ApplicationUtil.checkNull(rs[56]));
		dto.setAcnSegment(ApplicationUtil.checkNull(rs[57]));
		dto.setAcnSubSegment(ApplicationUtil.checkNull(rs[58]));
		dto.setAcnBeverageType(ApplicationUtil.checkNull(rs[59]));
		dto.setAcnKeyManufacturer(ApplicationUtil.checkNull(rs[60]));
		dto.setAcnBrand(ApplicationUtil.checkNull(rs[61]));
		dto.setAcnKeyFlavor(ApplicationUtil.checkNull(rs[62]));
		dto.setAcnLongName(ApplicationUtil.checkNull(rs[63]));
		dto.setAcnConsumptionType(ApplicationUtil.checkNull(rs[64]));		return dto;
	}

	/**
	 * This method is used to add filter for missing images
	 * 
	 * @param productSearchDto
	 * @param searchQuery
	 * @return StringBuilder serachQuery
	 * @throws ImproException
	 */
	private StringBuilder setMissingImagesSearchFilter(ProductSearchDto productSearchDto, StringBuilder searchQuery)
			throws ImproException {
		boolean flag = false;
		searchQuery.append(" AND ");
		searchQuery.append(" ( ");
		if (productSearchDto.getMissingImagesDto().isIsjpg() || productSearchDto.getMissingImagesDto().isSelectAll()) {
			searchQuery.append("JPG=:JPG");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes1()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("LowRes1=:LowRes1");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes2()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("LowRes2=:LowRes2");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes3()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("LowRes3=:LowRes3");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes7()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("LowRes7=:LowRes7");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes8()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("LowRes8=:LowRes8");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes9()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("LowRes9=:LowRes9");
			flag = true;
		}

		if (productSearchDto.getMissingImagesDto().isIshiRes1()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("HiRes1=:HiRes1");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIshiRes2()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("HiRes2=:HiRes2");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIshiRes3()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("HiRes3=:HiRes3");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIshiRes7()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("HiRes7=:HiRes7");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIshiRes8()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("HiRes8=:HiRes8");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIshiRes9()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("HiRes9=:HiRes9");
			flag = true;
		}

		if (productSearchDto.getMissingImagesDto().isIsmock1()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("Mock1=:Mock1");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIsmock2()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("Mock2=:Mock2");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIsmock3()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("Mock3=:Mock3");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIsmock7()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("Mock7=:Mock7");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIsmock8()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("Mock8=:Mock8");
			flag = true;
		}
		if (productSearchDto.getMissingImagesDto().isIsmock9()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			if (flag == true) {
				searchQuery.append(" OR ");
			}
			searchQuery.append("Mock9=:Mock9");
			flag = true;
		}
		searchQuery.append(" ) ");
		return searchQuery;

	}

	/**
	 * This method is used to set values for the missing images filter
	 * 
	 * @param productSearchDto
	 * @param query
	 * @throws ImproException
	 */
	private void setMissingImagesSearchFilterData(ProductSearchDto productSearchDto, Query query)
			throws ImproException {
		if (productSearchDto.getMissingImagesDto().isIsjpg() || productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("JPG", true);
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes1()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("LowRes1", true);
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes2()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("LowRes2", true);
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes3()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("LowRes3", true);
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes7()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("LowRes7", true);
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes8()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("LowRes8", true);
		}
		if (productSearchDto.getMissingImagesDto().isIslowRes9()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("LowRes9", true);
		}

		if (productSearchDto.getMissingImagesDto().isIshiRes1()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("HiRes1", true);
		}
		if (productSearchDto.getMissingImagesDto().isIshiRes2()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("HiRes2", true);
		}
		if (productSearchDto.getMissingImagesDto().isIshiRes3()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("HiRes3", true);
		}
		if (productSearchDto.getMissingImagesDto().isIshiRes7()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("HiRes7", true);
		}
		if (productSearchDto.getMissingImagesDto().isIshiRes8()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("HiRes8", true);
		}
		if (productSearchDto.getMissingImagesDto().isIshiRes9()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("HiRes9", true);
		}

		if (productSearchDto.getMissingImagesDto().isIsmock1()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("Mock1", true);
		}
		if (productSearchDto.getMissingImagesDto().isIsmock2()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("Mock2", true);
		}
		if (productSearchDto.getMissingImagesDto().isIsmock3()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("Mock3", true);
		}
		if (productSearchDto.getMissingImagesDto().isIsmock7()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("Mock7", true);
		}
		if (productSearchDto.getMissingImagesDto().isIsmock8()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("Mock8", true);
		}
		if (productSearchDto.getMissingImagesDto().isIsmock9()
				|| productSearchDto.getMissingImagesDto().isSelectAll()) {
			query.setParameter("Mock9", true);
		}
	}

	/**
	 * Common method for searching archived and non archived products
	 * 
	 * @param productSearchDto
	 * @param isPagination
	 * @return List<Object[]>
	 * @throws ImproException
	 */
	private List<Object[]> filterSearch(ProductSearchDto productSearchDto, boolean isPagination) throws ImproException {
		StringBuilder searchQuery = new StringBuilder();
		Session session = null;
		if (productSearchDto.getColumnName() == null || "".equals(productSearchDto.getColumnName())) {
			productSearchDto.setColumnName("ProductName");
		}
		if (productSearchDto.getColumnOrder() == null || "".equals(productSearchDto.getColumnOrder())) {
			productSearchDto.setColumnOrder("asc");
		}

		session = sessionFactory.openSession();
		if (productSearchDto.isActiveSearch())
			searchQuery.append(getSearchQuery() + " and p.archiveFlag=0");
		else
			searchQuery.append(getSearchQuery() + " and p.archiveFlag=1");

		// Set the filter conditions
		searchQuery = setFilter(productSearchDto, searchQuery);

		// set the missing image filter conditions
		if (productSearchDto.isMissingImages() && productSearchDto.isActiveSearch()) {
			searchQuery = setMissingImagesSearchFilter(productSearchDto, searchQuery);
		}

		// Set the pagination fetch
		if (isInEnumColName(productSearchDto.getColumnName(), COLUMNNAME.class)
				&& isInEnum(productSearchDto.getColumnOrder(), COLUMNORDER.class)) {
			searchQuery
					.append(" ORDER BY " + productSearchDto.getColumnName() + " " + productSearchDto.getColumnOrder());
		} else {
			throw new ImproException("Not a Valid ColumnName or ColumnOrder.");
		}

		// Pagination is used only main search.Export/Similar search/Products details
		// does not require that.
		if (isPagination) {
			searchQuery.append(" OFFSET " + productSearchDto.getPageDto().getRowNumber() + " ROWS " + "FETCH NEXT "
					+ productSearchDto.getPageDto().getRowSize() + " ROWS ONLY");
		}
		String sql = searchQuery.toString();
		if (LOG.isInfoEnabled()) {
			LOG.info("Filter Search query " + sql);
		}
		Query query = session.createSQLQuery(sql);

		// Set the filter data
		setFilterData(productSearchDto, query);
		if (productSearchDto.isMissingImages() && productSearchDto.isActiveSearch()) {
			setMissingImagesSearchFilterData(productSearchDto, query);
		}

		return (List<Object[]>) query.list();

	}

	/**
	 * Method returns the Search Query used to search for products.
	 * 
	 * @return String
	 */
	private String getSearchQuery() {
		return "SELECT p.ProductCode,p.ProductName,p.upc,p.DateMaintained,p.manufacturercode,"
				+ " m.ManufacturerName,p.brandcode,b.BrandName,b.ShortBrandName,p.packagecode,pk.packagename,pks.PackageSizeCode,pks.PackageSizeDescription,"
				+ " f.flavorcode,f.FlavorDescription,c.categorycode,c.CategoryDescription, p.subCategoryCode,sc.SubCategoryDescription,p.segmentCode,"
				+ " s.SegmentDescription,p.subsegmentcode,ss.SubSegmentDescription,p.archiveFlag,p.Extended_Desc_id,iepd.Long_Product_Desc,iepd.Short_Product_Desc, "
				+ " p.JPG,p.LowRes1,p.LowRes2,p.LowRes3,p.LowRes7,p.LowRes8,p.LowRes9,p.HiRes1,p.HiRes2,p.HiRes3,p.HiRes7,p.HiRes8,p.HiRes9,p.Mock1,p.Mock2,"
				+ " p.Mock3,p.Mock7,p.Mock8,p.Mock9,p.ProductId, "
				+ " p.shortProductName,p.upc12,p.ArchiveDate,p.DateAdded,mess.Delete_Message_Text,p.Consumption_Type,"
				+ " p.NielsenUPC,p.NielsenDesc,p.ACNCategory,p.ACNSubCategory,p.ACNSegment,p.ACNSubSegment,p.ACNBeverageType,p.ACNKeyManufacturer,"
				+ " p.ACNBrand,p.ACNKeyFlavor,p.ACNLongName,p.ACNConsumptionType "
				+ " FROM CCE_IMGPro.[dbo].products p INNER JOIN CCE_IMGPro.[dbo].Brands b "
				+ " ON b.BrandCode=p.BrandCode INNER JOIN CCE_IMGPro.[dbo].Manufacturers m ON m.ManufacturerCode=p.ManufacturerCode "
				+ " INNER JOIN CCE_IMGPro.[dbo].Packages pk ON pk.PackageCode=p.PackageCode INNER JOIN CCE_IMGPro.[dbo].PackageSize pks"
				+ " ON pks.PackageSizeCode=pk.PackageSizeCode INNER JOIN CCE_IMGPro.[dbo].Flavors f ON f.FlavorCode=p.FlavorCode INNER "
				+ " JOIN CCE_IMGPro.[dbo].ImgPro_Category_SubCategory cs ON cs.CategoryCode=p.CategoryCode "
				+ " AND cs.SubCategoryCode=p.SubCategoryCode  INNER JOIN CCE_IMGPro.[dbo].Category c ON c.CategoryCode=cs.CategoryCode "
				+ " INNER JOIN CCE_IMGPro.[dbo].SubCategory sc ON sc.SubCategoryCode=cs.SubCategoryCode "
				+ " INNER JOIN CCE_IMGPro.[dbo].ImgPro_Segment_Subsegment iss ON iss.SegmentCode=p.SegmentCode AND "
				+ " iss.SubSegmentCode=p.SubSegmentCode INNER JOIN CCE_IMGPro.[dbo].Segment s ON s.SegmentCode=iss.SegmentCode "
				+ " INNER JOIN CCE_IMGPro.[dbo].SubSegment ss ON ss.SubSegmentCode=iss.SubSegmentCode "
				+ " LEFT OUTER JOIN CCE_IMGPro.[dbo].ImgPro_Extended_Product_Desc iepd ON iepd.Extended_Desc_id=p.Extended_Desc_id "
				+ " LEFT OUTER JOIN CCE_IMGPro.[dbo].imgpro_Deletemessages_data mess ON p.ArchiveReason=mess.Delete_Message_ID WHERE 1=1";
	}

	@Override
	public List<ProductDto> searchProducts(ProductSearchDto productSearchDto, boolean isPagination)
			throws ImproException {
		LOG.info("ProductDaoImpl-->searchProduct()-->Begin");
		List<ProductDto> productList = null;
		ProductDto dto = null;
		@SuppressWarnings("unchecked")
		List<Object[]> list = filterSearch(productSearchDto, isPagination);
		if (list.size() > 0) {
			productList = new ArrayList<ProductDto>();
			for (Object[] rs : list) {
				dto = setProductListDto(rs, dto);
				productList.add(dto);
			}
		}
		LOG.info("ProductDaoImpl-->searchProduct()-->End");
		return productList;

	}

	@Override
	public List<ArchivedProductDto> searchArchivedProducts(ProductSearchDto productSearchDto, boolean isPagination)
			throws ImproException {
		LOG.info("ProductDaoImpl-->searchArchivedProducts()-->Begin");
		List<ArchivedProductDto> archProductList = null;
		ArchivedProductDto dto = null;
		@SuppressWarnings("unchecked")
		List<Object[]> list = filterSearch(productSearchDto, isPagination);
		if (list.size() > 0) {
			archProductList = new ArrayList<ArchivedProductDto>();
			for (Object[] rs : list) {
				dto = setArchiveProductListDto(rs, dto);
				archProductList.add(dto);
			}
		}
		LOG.info("ProductDaoImpl-->searchArchivedProducts()-->End");
		return archProductList;

	}

	/**
	 * This method will set additional product details into product dto
	 * 
	 * @param rs
	 * @param productDto
	 * @return ProductDto
	 */
	private ProductDto setProductDetailsDto(Object[] rs, ProductDto productDto) {
		PackageDto dto = new PackageDto();
		dto.setUnitHeight(ApplicationUtil.checkNull(rs[0]));
		dto.setUnitWidth(ApplicationUtil.checkNull(rs[1]));
		dto.setUnitDepth(ApplicationUtil.checkNull(rs[2]));
		dto.setPackageCode(productDto.getPkgdto().getPackageCode());
		dto.setPackageName(productDto.getPkgdto().getPackageName());

		ContainerTypeDto containerTypeDto = new ContainerTypeDto();
		containerTypeDto.setContainerTypeDescription(ApplicationUtil.checkNull(rs[3]));
		containerTypeDto.setContainerTypeCode(ApplicationUtil.checkNull(rs[4]));
		dto.setContainerTypeDto(containerTypeDto);

		PackageSizeDto packageSizeDto = new PackageSizeDto();
		packageSizeDto.setPackageSizeDescription(ApplicationUtil.checkNull(rs[5]));
		packageSizeDto.setPackageSizeCode(ApplicationUtil.checkNull(rs[6]));

		productDto.setPkgdto(dto);

		return productDto;

	}

	// Method for getting additional product details
	@Override
	public ProductDto getProductDetails(String id) throws ImproException {
		LOG.info("ProductDaoImpl-->getProductDetails()-->Begin");
		Session session = null;
		ProductDto productDto = new ProductDto();
		try {
			session = sessionFactory.openSession();

			/*
			 * The query will fetch additional details of a product
			 */
			ProductSearchDto productSearchDto = new ProductSearchDto();
			productSearchDto.setProductCode(id);
			productSearchDto.setActiveSearch(true);
			List<ProductDto> productDtoList = searchProducts(productSearchDto, false);
			productDto = productDtoList.get(0);

			String getProductQuery = "SELECT pk.UnitHeight,pk.UnitWidth,pk.UnitDepth,ct.ContainerTypeDescription,"
					+ "ct.ContainerTypeCode,ps.PackageSizeDescription,ps.PackageSizeCode FROM CCE_IMGPro.[dbo].products p "
					+ "INNER JOIN CCE_IMGPro.[dbo].Packages pk ON pk.PackageCode=p.PackageCode "
					+ "INNER JOIN CCE_IMGPro.[dbo].PackageSize ps ON ps.PackageSizeCode=pk.PackageSizeCode "
					+ "INNER JOIN CCE_IMGPro.[dbo].ContainerType ct ON ct.ContainerTypeCode=pk.ContainerType "
					+ "WHERE productcode=:productCode and ProductId=:ProductId";
			if (LOG.isInfoEnabled()) {
				LOG.info("Product details query " + getProductQuery);
			}
			Query query = session.createSQLQuery(getProductQuery);
			query.setParameter("productCode", productDto.getProductCode());
			query.setParameter("ProductId", productDto.getBitMapName());
			@SuppressWarnings("unchecked")
			List<Object[]> productDetails = (List<Object[]>) query.list();
			if (productDetails.size() > 0) {
				for (Object[] rs : productDetails) {
					productDto = setProductDetailsDto(rs, productDto);
				}
			}
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("ProductDaoImpl-->getProductDetails()-->Exception " + e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ProductDaoImpl-->getProductDetails()-->End");
		return productDto;

	}

	@Override
	public void editProduct(ProductDto productDto) throws ImproException {
		LOG.info("ProductDaoImpl-->editProductDetails()-->Begin");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			String editQuery = "Update Products p set p.bitmapName=:bitmapName,p.brandCode=:brandCode, p.manufacturerCode=:manufCode,p.flavorCode=:flavorCode, "
					+ "p.packageCode=:packageCode,p.categoryCode=:categoryCode,p.subCategoryCode=:subCategoryCode,p.segmentCode=:segmentCode, "
					+ "p.subSegmentCode=:subSegmentCode,p.productName=:productName,p.shortProductName=:shortProductName,p.upc=:upc, "
					+ "p.mock1=:mock1,p.mock2=:mock2,p.mock3=:mock3,p.mock7=:mock7, p.mock8=:mock8,p.mock9=:mock9,"
					+ "p.hiRes1=:hiRes1,p.hiRes2=:hiRes2,p.hiRes3=:hiRes3,p.hiRes7=:hiRes7, p.hiRes8=:hiRes8,p.hiRes9=:hiRes9,"
					+ "p.lowRes1=:LowRes1,p.lowRes2=:LowRes2,p.lowRes3=:LowRes3,p.lowRes7=:LowRes7, p.lowRes8=:LowRes8,p.lowRes9=:LowRes9,p.jpg=:jpg,"
					+ "p.upc12=:upc12,p.extendedDescId=:extendedDescId " + "WHERE p.productCode=:productCode ";

			Query query = session.createQuery(editQuery);
			LOG.info(editQuery);
			query.setParameter("bitmapName", productDto.getBitMapName());
			query.setParameter("brandCode", Integer.parseInt(productDto.getBrandDto().getBrandCode()));
			query.setParameter("manufCode", Integer.parseInt(productDto.getManufacturerDto().getManufacturerCode()));
			query.setParameter("flavorCode", Integer.parseInt(productDto.getFlavorDto().getFlavorCode()));
			query.setParameter("packageCode", Integer.parseInt(productDto.getPkgdto().getPackageCode()));
			query.setParameter("categoryCode", Integer.parseInt(productDto.getCategoryDto().getCategoryCode()));
			query.setParameter("subCategoryCode",Integer.parseInt(productDto.getSubCategoryDto().getSubCategoryCode()));
			query.setParameter("segmentCode", Integer.parseInt(productDto.getSegmentDto().getSegmentCode()));
			query.setParameter("subSegmentCode", Integer.parseInt(productDto.getSubSegmentDto().getSubSegmentCode()));
			query.setParameter("upc", productDto.getUpc());
			query.setParameter("productName", ApplicationUtil.generateLongProductName(productDto));
			query.setParameter("shortProductName", ApplicationUtil.generateShortProductName(productDto));
			query.setParameter("productCode", Integer.parseInt(productDto.getProductCode()));

			query.setParameter("upc12", productDto.getUpc12());
			query.setParameter("extendedDescId",
					Integer.parseInt(productDto.getExtendedProductDescDto().getExtendedDescId()));
			updateImageFlags(productDto,query);
			query.executeUpdate();
			auditDao.addLog(AppConstants.USER_NAME, AppConstants.TABLE_PRODUCTS,
					String.valueOf(productDto.getProductCode()), AppConstants.TRANSACTION_TYPE_CHANGE);

		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("ProductDaoImpl-->editProductDetails()-->Exception " + e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}

	}

	@Transactional(rollbackFor = ImproException.class)
	@Override
	public void addProduct(Products product) throws ImproException {
		LOG.info("ProductDaoImpl-->addProduct()-->BEGIN");
		try {
			entityMgr.persist(product);
			auditDao.addLog(AppConstants.USER_NAME, AppConstants.TABLE_PRODUCTS,
					String.valueOf(product.getProductCode()), AppConstants.TRANSACTION_TYPE_ADD);
		} catch (Exception ex) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ProductDaoImpl-->addProduct()--> Exception occured " + ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		}
		LOG.info("ProductDaoImpl-->addProduct()-->End");
	}

	@Override
	public boolean checkDuplicateProduct(String bitmapName) throws ImproException {
		return checkDuplicateProduct(bitmapName,false);
	}

	
	/**
	 * Duplicate check of bitMap Name during product Archive
	 * @param bitmapName
	 * @param isRestore
	 * @return boolean
	 * @throws ImproException
	 */
	private boolean checkDuplicateProduct(String bitmapName,boolean isRestore) throws ImproException {
		LOG.info("ProductDaoImpl-->checkDuplicateProduct()-->BEGIN");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			StringBuffer sqlQuery=new StringBuffer("select count(bitmapName) from Products p where p.bitmapName=:bitmapName");
			if(isRestore) {
				sqlQuery.append(" and archiveFlag=0");
			}
			Query query = session.createQuery(sqlQuery.toString());
			query.setParameter("bitmapName", bitmapName);
			if ((Long) query.uniqueResult() > 0) {
				LOG.info("ProductDaoImpl-->checkDuplicateProduct()-->End");
				return true;
			} else {
				LOG.info("ProductDaoImpl-->checkDuplicateProduct()-->End");
				return false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
	}

	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void multiEditProduct(ProductDto productdtosrc, List<ProductDto> productDtoList) throws ImproException {
		LOG.info("ProductDaoImpl-->multiEditProduct()-->BEGIN");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			String editquery = "Update Products p set p.brandCode=:brandCode, "
					+ "p.manufacturerCode=:manufCode,p.flavorCode=:flavorCode, "
					+ "p.packageCode=:packageCode,p.categoryCode=:categoryCode,p.subCategoryCode=:subCategoryCode,p.segmentCode=:segmentCode, "
					+ "p.subSegmentCode=:subSegmentCode,p.productName=:productName,p.shortProductName=:shortProductName,p.upc=:upc,p.extendedDescId=:extendedDescId, "
					+ "p.upc12=:upc12 " + "WHERE p.productCode=:productCode";
			LOG.info(editquery);
			Query query = session.createQuery(editquery);
			for (ProductDto productDto : productDtoList) {
				if (productdtosrc.getBrandDto().getBrandCode() == null
						|| "".equals(productdtosrc.getBrandDto().getBrandCode())) {
					query.setParameter("brandCode", Integer.parseInt(productDto.getBrandDto().getBrandCode()));
				} else {
					query.setParameter("brandCode", Integer.parseInt(productdtosrc.getBrandDto().getBrandCode()));
				}

				if (productdtosrc.getManufacturerDto().getManufacturerCode() == null
						|| "".equals(productdtosrc.getManufacturerDto().getManufacturerCode())) {
					query.setParameter("manufCode",
							Integer.parseInt(productDto.getManufacturerDto().getManufacturerCode()));
				} else {
					query.setParameter("manufCode",
							Integer.parseInt(productdtosrc.getManufacturerDto().getManufacturerCode()));
				}

				if (productdtosrc.getFlavorDto().getFlavorCode() == null
						|| "".equals(productdtosrc.getFlavorDto().getFlavorCode())) {
					query.setParameter("flavorCode", Integer.parseInt(productDto.getFlavorDto().getFlavorCode()));
				} else {
					query.setParameter("flavorCode", Integer.parseInt(productdtosrc.getFlavorDto().getFlavorCode()));
				}

				if (productdtosrc.getPkgdto().getPackageCode() == null
						|| "".equals(productdtosrc.getPkgdto().getPackageCode())) {
					query.setParameter("packageCode", Integer.parseInt(productDto.getPkgdto().getPackageCode()));
				} else {
					query.setParameter("packageCode", Integer.parseInt(productdtosrc.getPkgdto().getPackageCode()));
				}

				if (productdtosrc.getCategoryDto().getCategoryCode() == null
						|| "".equals(productdtosrc.getCategoryDto().getCategoryCode())) {
					query.setParameter("categoryCode", Integer.parseInt(productDto.getCategoryDto().getCategoryCode()));
				} else {
					query.setParameter("categoryCode",
							Integer.parseInt(productdtosrc.getCategoryDto().getCategoryCode()));
				}

				if (productdtosrc.getSubCategoryDto().getSubCategoryCode() == null
						|| "".equals(productdtosrc.getSubCategoryDto().getSubCategoryCode())) {
					query.setParameter("subCategoryCode", Integer.parseInt(productDto.getBrandDto().getBrandCode()));
				} else {
					query.setParameter("subCategoryCode", Integer.parseInt(productdtosrc.getBrandDto().getBrandCode()));
				}

				if (productdtosrc.getSegmentDto().getSegmentCode() == null
						|| "".equals(productdtosrc.getSegmentDto().getSegmentCode())) {
					query.setParameter("segmentCode", Integer.parseInt(productDto.getSegmentDto().getSegmentCode()));
				} else {
					query.setParameter("segmentCode", Integer.parseInt(productdtosrc.getSegmentDto().getSegmentCode()));
				}

				if (productdtosrc.getSubSegmentDto().getSubSegmentCode() == null
						|| "".equals(productdtosrc.getSubSegmentDto().getSubSegmentCode())) {
					query.setParameter("subSegmentCode",
							Integer.parseInt(productDto.getSubSegmentDto().getSubSegmentCode()));
				} else {
					query.setParameter("subSegmentCode",
							Integer.parseInt(productdtosrc.getSubSegmentDto().getSubSegmentCode()));
				}

				if (productdtosrc.getUpc() == null || "".equals(productdtosrc.getUpc())) {
					query.setParameter("upc", productDto.getUpc());
				} else {
					query.setParameter("upc", productdtosrc.getUpc());
				}

				if (productdtosrc.getBrandDto().getBrandCode() == null
						|| "".equals(productdtosrc.getBrandDto().getBrandCode())) {
					query.setParameter("brandCode", Integer.parseInt(productDto.getBrandDto().getBrandCode()));
				} else {
					query.setParameter("brandCode", Integer.parseInt(productdtosrc.getBrandDto().getBrandCode()));
				}

				query.setParameter("productName",
						ApplicationUtil.generateLongProductNameMultiEdit(productdtosrc, productDto));
				query.setParameter("shortProductName",
						ApplicationUtil.generateShortProductNameMultiEdit(productdtosrc, productDto));
				query.setParameter("productCode", Integer.parseInt(productDto.getProductCode()));

				if (productdtosrc.getUpc12() == null || "".equals(productdtosrc.getUpc12())) {
					query.setParameter("upc12", productDto.getUpc12());
				} else {
					query.setParameter("upc12", productdtosrc.getUpc12());
				}

				if (productdtosrc.getExtendedProductDescDto().getExtendedDescId() == null
						|| "".equals(productdtosrc.getExtendedProductDescDto().getExtendedDescId())) {
					query.setParameter("extendedDescId",
							Integer.parseInt(productDto.getExtendedProductDescDto().getExtendedDescId()));
				} else {
					query.setParameter("extendedDescId",
							Integer.parseInt(productdtosrc.getExtendedProductDescDto().getExtendedDescId()));
				}
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.TABLE_PRODUCTS,
						String.valueOf(productdtosrc.getProductCode()), AppConstants.TRANSACTION_TYPE_CHANGE);
			}

		} catch (Exception ex) {
			if (LOG.isErrorEnabled()) {
				LOG.error("ProductDaoImpl-->multiEditProduct()-->EXCPETION " + ex.getMessage());
			}
			throw new ImproException(ex);
		} finally {
			HibernateUtil.closeSession(session);
		}

		LOG.info("ProductDaoImpl-->multiEditProduct()-->END");

	}

	@Override
	public Integer getTotalCountForProducts(ProductSearchDto productSearchDto) throws ImproException {
		LOG.info("ProductDaoImpl-->getTotalCountForProducts()-->Begin");
		StringBuilder searchQuery = new StringBuilder();
		Session session = null;
		Integer count = 0;

		try {
			session = sessionFactory.openSession();
			if (productSearchDto.isActiveSearch())
				searchQuery.append(getTotalCountQuery() + " and p.archiveFlag=0");
			else
				searchQuery.append(getTotalCountQuery() + " and p.archiveFlag=1");

			searchQuery = setFilter(productSearchDto, searchQuery);

			// set the missing image filter conditions
			if (productSearchDto.isMissingImages() && productSearchDto.isActiveSearch()) {
				searchQuery = setMissingImagesSearchFilter(productSearchDto, searchQuery);
			}
			String sql = searchQuery.toString();
			LOG.info(sql);
			Query query = session.createSQLQuery(sql);
			setFilterData(productSearchDto, query);
			if (productSearchDto.isMissingImages() && productSearchDto.isActiveSearch()) {
				setMissingImagesSearchFilterData(productSearchDto, query);
			}
			count = (Integer) query.uniqueResult();
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("ProductDaoImpl-->getTotalCountForProducts()-->Exception" + e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}

		LOG.info("ProductDaoImpl-->getTotalCountForProducts()-->End");
		return count;
	}

	/**
	 * Method returns the query used to count the number of products
	 * @return String
	 */
	private String getTotalCountQuery() {
		return "SELECT count(*) " + " FROM CCE_IMGPro.[dbo].products p INNER JOIN CCE_IMGPro.[dbo].Brands b "
				+ "ON b.BrandCode=p.BrandCode INNER JOIN CCE_IMGPro.[dbo].Manufacturers m ON m.ManufacturerCode=p.ManufacturerCode "
				+ "INNER JOIN CCE_IMGPro.[dbo].Packages pk ON pk.PackageCode=p.PackageCode INNER JOIN CCE_IMGPro.[dbo].PackageSize pks "
				+ "ON pks.PackageSizeCode=pk.PackageSizeCode INNER JOIN CCE_IMGPro.[dbo].Flavors f ON f.FlavorCode=p.FlavorCode INNER "
				+ "JOIN CCE_IMGPro.[dbo].ImgPro_Category_SubCategory cs ON cs.CategoryCode=p.CategoryCode "
				+ "AND cs.SubCategoryCode=p.SubCategoryCode  INNER JOIN CCE_IMGPro.[dbo].Category c ON c.CategoryCode=cs.CategoryCode "
				+ "INNER JOIN CCE_IMGPro.[dbo].SubCategory sc ON sc.SubCategoryCode=cs.SubCategoryCode "
				+ "INNER JOIN CCE_IMGPro.[dbo].ImgPro_Segment_Subsegment iss ON iss.SegmentCode=p.SegmentCode AND "
				+ "iss.SubSegmentCode=p.SubSegmentCode INNER JOIN CCE_IMGPro.[dbo].Segment s ON s.SegmentCode=iss.SegmentCode "
				+ "INNER JOIN CCE_IMGPro.[dbo].SubSegment ss ON ss.SubSegmentCode=iss.SubSegmentCode "
				+ "LEFT OUTER JOIN CCE_IMGPro.[dbo].ImgPro_Extended_Product_Desc iepd ON iepd.Extended_Desc_id=p.Extended_Desc_id WHERE 1=1";
	}
	
	
	/**
	 * Update the images flags when image is added or deleted
	 * @param dto
	 * @param query
	 */
	private void updateImageFlags(ProductDto dto,Query query) {
		EditImageDto editImgDto=dto.getEditImageDto() ;
		if(editImgDto != null) {
			StdImageDto stdImgDto=editImgDto.getStdImageDto() ;
			HiResImageDto hiResImgDto=editImgDto.getHiResImageDto() ;
			MockImageDto mockImgDto=editImgDto.getMockImageDto() ;
			JpgImageDto jpgImgDto=editImgDto.getJpgImageDto() ;

			if(stdImgDto != null) {
				if(stdImgDto.isAddFLag1()) {
					query.setParameter("LowRes1", true);
				} else if(stdImgDto.isDeleteFLag1()) {
					query.setParameter("LowRes1", false);
				} else {
					query.setParameter("LowRes1", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes1()));

				}
				if(stdImgDto.isAddFLag2()) {
					query.setParameter("LowRes2", true);
				} else if(stdImgDto.isDeleteFLag2()) {
					query.setParameter("LowRes2", false);
				} else {
					query.setParameter("LowRes2", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes2()));

				}
				if(stdImgDto.isAddFLag3()) {
					query.setParameter("LowRes3", true);
				} else if(stdImgDto.isDeleteFLag3()) {
					query.setParameter("LowRes3", false);
				} else {
					query.setParameter("LowRes3", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes3()));
				}
				
				if(stdImgDto.isAddFLag7()) {
					query.setParameter("LowRes7", true);
				} else if(stdImgDto.isDeleteFLag7()) {
					query.setParameter("LowRes7", false);
				} else {
					query.setParameter("LowRes7", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes7()));
				}
				
				if(stdImgDto.isAddFLag8()) {
					query.setParameter("LowRes8", true);
				} else if(stdImgDto.isDeleteFLag8()) {
					query.setParameter("LowRes8", false);
				} else {
					query.setParameter("LowRes8", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes8()));

				}
				if(stdImgDto.isAddFLag9()) {
					query.setParameter("LowRes9", true);
				} else if(stdImgDto.isDeleteFLag9()) {
					query.setParameter("LowRes9", false);
				} else {
					query.setParameter("LowRes9", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes9()));
				}
			} else {
				query.setParameter("LowRes1", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes1()));
				query.setParameter("LowRes2", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes2()));
				query.setParameter("LowRes3", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes3()));
				query.setParameter("LowRes4", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes7()));
				query.setParameter("LowRes5", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes8()));
				query.setParameter("LowRes6", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes9()));
			}
			
			if(hiResImgDto != null) {
				if(hiResImgDto.isAddFLag1()) {
					query.setParameter("hiRes1", true);
				} else if(hiResImgDto.isDeleteFLag1()) {
					query.setParameter("hiRes1", false);
				} else {
					query.setParameter("hiRes1", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes1()));

				}
				if(hiResImgDto.isAddFLag2()) {
					query.setParameter("hiRes2", true);
				} else if(hiResImgDto.isDeleteFLag2()) {
					query.setParameter("hiRes2", false);
				} else {
					query.setParameter("hiRes2", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes2()));
				}
				
				if(hiResImgDto.isAddFLag3()) {
					query.setParameter("hiRes3", true);
				} else if(hiResImgDto.isDeleteFLag3()) {
					query.setParameter("hiRes3", false);
				} else {
					query.setParameter("hiRes3", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes3()));
				}
				
				if(hiResImgDto.isAddFLag7()) {
					query.setParameter("hiRes7", true);
				} else if(hiResImgDto.isDeleteFLag7()) {
					query.setParameter("hiRes7", false);
				} else {
					query.setParameter("hiRes7", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes7()));
				}
				if(hiResImgDto.isAddFLag8()) {
					query.setParameter("hiRes8", true);
				} else if(hiResImgDto.isDeleteFLag8()) {
					query.setParameter("hiRes8", false);
				}
				else {
					query.setParameter("hiRes8", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes8()));
				}
				if(hiResImgDto.isAddFLag9()) {
					query.setParameter("hiRes9", true);
				} else if(hiResImgDto.isDeleteFLag9()) {
					query.setParameter("hiRes9", false);
				}else {
					query.setParameter("hiRes9", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes9()));
				}

			} else {
				query.setParameter("hiRes1", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes1()));
				query.setParameter("hiRes2", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes2()));
				query.setParameter("hiRes3", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes3()));
				query.setParameter("hiRes7", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes7()));
				query.setParameter("hiRes8", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes8()));
				query.setParameter("hiRes9", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes9()));

			}
			if(mockImgDto != null) {
				if(mockImgDto.isAddFLag1()) {
					query.setParameter("mock1", true);
				} else if(mockImgDto.isDeleteFLag1()) {
					query.setParameter("mock1", false);
				} else {
					query.setParameter("mock1", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock1()));
				}
				
				if(mockImgDto.isAddFLag2()) {
					query.setParameter("mock2", true);
				} else if(mockImgDto.isDeleteFLag2()) {
					query.setParameter("mock2", false);
				}else {
					query.setParameter("mock2", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock2()));
				}
				
				if(mockImgDto.isAddFLag3()) {
					query.setParameter("mock3", true);
				} else if(mockImgDto.isDeleteFLag3()) {
					query.setParameter("mock3", false);
				}else {
					query.setParameter("mock3", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock3()));
				}
				
				if(mockImgDto.isAddFLag7()) {
					query.setParameter("mock7", true);
				} else if(mockImgDto.isDeleteFLag7()) {
					query.setParameter("mock7", false);
				} else {
					query.setParameter("mock7", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock7()));
				}
				
				if(mockImgDto.isAddFLag8()) {
					query.setParameter("mock8", true);
				} else if(mockImgDto.isDeleteFLag8()) {
					query.setParameter("mock8", false);
				} else {
					query.setParameter("mock8", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock8()));
				}
				
				if(mockImgDto.isAddFLag9()) {
					query.setParameter("mock9", true);
				} else if(mockImgDto.isDeleteFLag9()) {
					query.setParameter("mock9", false);
				} else {
					query.setParameter("mock9", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock9()));
				}

			} else {
				query.setParameter("mock1", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock1()));
				query.setParameter("mock2", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock2()));
				query.setParameter("mock3", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock3()));
				query.setParameter("mock7", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock7()));
				query.setParameter("mock8", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock8()));
				query.setParameter("mock9", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock9()));
			}
			if(jpgImgDto != null) {
				if(jpgImgDto.isAddFLag1()) {
					query.setParameter("jpg", true);
				} else if(jpgImgDto.isDeleteFLag1()) {
					query.setParameter("jpg", false);
				} else {
					query.setParameter("jpg", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getJpg()));
				}
			} else {
				query.setParameter("jpg", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getJpg()));
			}
		} else {
			query.setParameter("LowRes1", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes1()));
			query.setParameter("LowRes2", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes2()));
			query.setParameter("LowRes3", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes3()));
			query.setParameter("LowRes7", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes7()));
			query.setParameter("LowRes8", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes8()));
			query.setParameter("LowRes9", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getLowRes9()));
			query.setParameter("hiRes1", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes1()));
			query.setParameter("hiRes2", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes2()));
			query.setParameter("hiRes3", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes3()));
			query.setParameter("hiRes7", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes7()));
			query.setParameter("hiRes8", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes8()));
			query.setParameter("hiRes9", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getHiRes9()));
			query.setParameter("mock1", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock1()));
			query.setParameter("mock2", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock2()));
			query.setParameter("mock3", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock3()));
			query.setParameter("mock7", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock7()));
			query.setParameter("mock8", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock8()));
			query.setParameter("mock9", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getMock9()));
			
			query.setParameter("jpg", ApplicationUtil.setImageFlag(dto.getMissingImagesDto().getJpg()));

		}
	}
}
