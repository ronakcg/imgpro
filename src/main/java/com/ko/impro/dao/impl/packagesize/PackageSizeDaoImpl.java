package com.ko.impro.dao.impl.packagesize;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.packagesize.PackageSizeDao;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.packagesize.PackageSize;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author hlekshmy : 
 * This class is an implementation of PackageSizeDao Interface
 */
@Repository
public class PackageSizeDaoImpl extends BaseDaoImpl implements PackageSizeDao {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(PackageSizeDaoImpl.class);

	/**
	 * This is to calculate number of count of rows in product table
	 */
	private int count;
	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PackageSizeDto> loadPackageSize() throws ImproException {
		LOG.info("PackageSizeDaoImpl-->loadPackageSize()-->Begin");
		Session session=null;
		List<PackageSizeDto> packageSizeList=null;
		try {
			session = sessionFactory.openSession();		
			PackageSizeDto dto=null;
			Query query=null;
			query = session.createQuery("select p.packageSizeCode,p.packageSizeDescription,p.packageSizeOz from PackageSize p order by p.packageSizeDescription asc");
			
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				packageSizeList=new ArrayList<PackageSizeDto>();
				for (Object[] rs : list) {
					dto = setPackageSizeDto(rs, dto);
					packageSizeList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("PackageSizeDaoImpl-->loadPackageSize()-->End");

		return packageSizeList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PackageSizeDto> loadPackageSize(List<String> packageList) throws ImproException {
		LOG.info("PackageSizeDaoImpl-->loadPackageSize()-->Begin");
		Session session=null;
		List<PackageSizeDto> packageSizeList=null;
		try {
			session = sessionFactory.openSession();		
			PackageSizeDto dto=null;
			Query query=null;
			query=session.createSQLQuery("select packSize.packageSizeCode,packSize.packageSizeDescription,packSize.packageSizeOz from [CCE_IMGPro].[dbo].[PackageSize] packSize INNER JOIN" + 
					"  [CCE_IMGPro].[dbo].[Packages] pack on packsize.PackageSizeCode=pack.PackageSizeCode where pack.PackageCode in (:packageList) order by packSize.packageSizeDescription asc");
			query.setParameterList("packageList",packageList);
			List<Object[]> list = (List<Object[]>) query.list();
			if (list.size() > 0) {
				packageSizeList=new ArrayList<PackageSizeDto>();
				for (Object[] rs : list) {
					dto = setPackageSizeDto(rs, dto);
					packageSizeList.add(dto);
				}
			}
		}catch(Exception e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->loadPackageSize()-->Exception "+e.getMessage());
			}
		}
		LOG.info("PackageSizeDaoImpl-->loadPackageSize()-->End");
		return packageSizeList;
	}
	
	/**
	 * Set the packageSizeDto
	 * @param rs
	 * @param dto
	 * @return PackageSizeDto
	 */
	private PackageSizeDto setPackageSizeDto(Object[] resultSet, PackageSizeDto dto) {
		dto=new PackageSizeDto();
		dto.setPackageSizeCode(resultSet[0].toString());
		dto.setPackageSizeDescription(ApplicationUtil.checkNull(resultSet[1]));
		dto.setPackageSizeOz(ApplicationUtil.checkNull(resultSet[2]));
		return dto;
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addPackageSize(PackageSizeDto packageSizeDto) throws ImproException {
		LOG.info("PackageSizeDaoImpl-->addPackageSize()-->Begin");
		try {
			PackageSize packageSize = new PackageSize();
			packageSize.setPackageSizeDescription(packageSizeDto.getPackageSizeDescription());
			packageSize.setPackageSizeOz(Float.parseFloat(packageSizeDto.getPackageSizeOz()));
			
			entityMgr.persist(packageSize);
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PACKAGESIZE,String.valueOf(packageSize.getPackageSizeCode()),AppConstants.TRANSACTION_TYPE_ADD);
		} catch (PersistenceException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->addPackageSize()--> Exception occured " + e.getMessage());
			}
			throw new ImproException(e.getMessage());
		}
		LOG.info("PackageSizeDaoImpl-->addPackageSize()-->End");
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deletePackageSize(PackageSizeDto packageSizeDto) throws ImproException {
		LOG.info("PackageSizeDaoImpl-->deletePackageSize()-->Begin");
		Session session = null;
		Query query = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
			if (isPackageSizeActive(Integer.parseInt(packageSizeDto.getPackageSizeCode()))) {
				return "PackageSize-Package combination are currently in use on (" + count + ") Products ";
			} else {
				query = session.createQuery("Delete from PackageSize WHERE packageSizeCode=:packageSizeCode");
				query.setParameter("packageSizeCode",  Short.parseShort(packageSizeDto.getPackageSizeCode()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PACKAGESIZE,String.valueOf(packageSizeDto.getPackageSizeCode()),AppConstants.TRANSACTION_TYPE_DELETE);
				query = session.createQuery("Delete from Packages WHERE packageSize.packageSizeCode=:packageSizeCode");
				query.setParameter("packageSizeCode", Short.parseShort(packageSizeDto.getPackageSizeCode()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PACKAGES,String.valueOf(packageSizeDto.getPackageSizeCode()),AppConstants.TRANSACTION_TYPE_DELETE);
			}
			msg = AppConstants.DATA_DELETE_SUCC_MSG;
		} catch (HibernateException e) {
			msg = AppConstants.FAILURE_MSG;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->deletePackageSize()-->End with Error " + e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return msg;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editPackageSize(PackageSizeDto packageSizeDto) throws ImproException {
		LOG.info("PackageSizeDaoImpl-->editPackageSize()-->Begin");
		Session session = null;
		Query query = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
				query = session.createQuery("Update PackageSize set PackageSizeDescription=:PackageSizeDescription, packageSizeOz=:packageSizeOz"
						+ " WHERE packageSizeCode=:packageSizeCode");
				query.setParameter("PackageSizeDescription", packageSizeDto.getPackageSizeDescription());
				query.setParameter("packageSizeOz", Float.valueOf(packageSizeDto.getPackageSizeOz()));
				query.setParameter("packageSizeCode", (short) Integer.parseInt(packageSizeDto.getPackageSizeCode()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PACKAGESIZE,String.valueOf(packageSizeDto.getPackageSizeCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
			msg = AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch (HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->editPackageSize()-->Exception " + e.getMessage());
			}
			msg = AppConstants.FAILURE_MSG;
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return msg;
	}


	@Override
	public PackageSizeDto loadPackageSizeById(int packageSizeId) throws ImproException {
		LOG.info("PackageSizeDaoImpl-->loadPackageSizeById()-->Begin");
		Session session = null;
		PackageSizeDto dto = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery("select packageSizeCode,packageSizeDescription ,packageSizeOz from PackageSize "
							+ " where packageSizeCode=:packageSizeCode");
			query.setParameter("packageSizeCode", (short)packageSizeId);
			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				for (Object[] rs : list) {
					dto = setPackageSizeDto(rs, dto);
				}
			}
		}
		catch(HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->loadPackageSizeById()-->Exception " + e.getMessage());
			}
		}
		finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("PackageSizeDaoImpl-->loadPackageSizeById()-->End");
		return dto;
	}

	@Override
	public boolean checkDuplicatePackageSize(String name) throws ImproException {
		LOG.info("PackageSizeDaoImpl-->checkDuplicatePackageSize()-->BEGIN");
		Session session = null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery("select count(packageSizeDescription) from PackageSize where "
					+ " packageSizeDescription= :packageSizeDescription");
			query.setParameter("packageSizeDescription",name);
			if ((long) query.uniqueResult() > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		} catch(HibernateException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->checkDuplicatePackageSize()-->Exception" +ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("PackageSizeDaoImpl-->checkDuplicatePackageSize()-->End");
		return flag;
	}
	
	/**
	 * Check the presence of packageCode/packageSize in product
	 * @param packageSizeCode
	 * @return boolean 
	 */
	private boolean isPackageSizeActive(int packageSizeCode) {
		LOG.info("PackageSizeDaoImpl-->checkPackageSize()-->Begin");
		Session session = null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session
					.createSQLQuery("select count(*) from products p1"
							+ "  INNER JOIN Packages p2 ON p1.PackageCode=p2.PackageCode"
							+ "  INNER JOIN PackageSize p3 ON p2.PackageSizeCode=p3.PackageSizeCode"
							+ "  and p3.PackageSizeCode=:packageSizeCode")
					.setParameter("packageSizeCode", packageSizeCode).uniqueResult();
			if (count > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("PackageSizeDaoImpl-->checkPackageSize()-->End");
		return flag;
	}

}