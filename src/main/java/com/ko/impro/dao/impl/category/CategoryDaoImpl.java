package com.ko.impro.dao.impl.category;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.category.CategoryDao;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.category.Category;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 *	Description : This Class is implementation of CategoryDao Interface
 */
@Repository
public class CategoryDaoImpl extends BaseDaoImpl implements CategoryDao {
	

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(CategoryDaoImpl.class);

	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	
	/**
	 * This is to calculate number of count of rows in product table
	 */
	private long count;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;

	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CategoryDto> loadCategory() throws ImproException {
		LOG.info("CategoryDaoImpl-->loadCategory()-->Begin");
		Session session=null;
		List<CategoryDto> categoryList=null;
		try {
			session = sessionFactory.openSession();		
			CategoryDto dto = null;
			Query query=null;
			
			query = session.createQuery("SELECT c.categoryCode,c.categoryDescription FROM Category c ORDER BY  c.categoryDescription asc");
			
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				categoryList = new ArrayList<CategoryDto>();
				for (Object[] rs : list) {
	
					dto = setCategoryDto(rs, dto);
					categoryList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("CategoryDaoImpl-->loadCategory()-->End");
		return categoryList;
	}

	/**
	 * Set the CategoryDto
	 * @param rs
	 * @param dto
	 * @return CategoryDto
	 */
	private CategoryDto setCategoryDto(Object[] resultSet, CategoryDto categorydto) {
		categorydto = new CategoryDto();
		categorydto.setCategoryCode(resultSet[0].toString());
		categorydto.setCategoryDescription(ApplicationUtil.checkNull(resultSet[1]));
		return categorydto;
	}


	@SuppressWarnings("unchecked")
	@Override
	public CategoryDto loadCategory(int catId) throws ImproException {
		LOG.info("CategoryDaoImpl-->loadCategory()-->Begin");
		Session session=null;
		CategoryDto categoryDto = null;
		try {
			session=sessionFactory.openSession();
			Query query = session.createQuery("SELECT c.categoryCode,c.categoryDescription FROM Category c WHERE CategoryCode=:CategoryCode");
			query.setParameter("CategoryCode", catId);
	
			List<Object[]> list = (List<Object[]>) query.list();
			if (null!=list && !list.isEmpty()) {				
				for (Object[] rs : list) {
					categoryDto = setCategoryDto(rs, categoryDto);					
				}
			}
			
		} catch(HibernateException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("CategoryDaoImpl-->loadCategory()-->Exception "+ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}	
		
		LOG.info("CategoryDaoImpl-->loadCategory()-->End");
		return categoryDto;
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addCategory(CategoryDto categoryDto) throws ImproException { 
		LOG.info("CategoryDaoImpl-->addCategory()-->Begin");
		try {
    		Category category = new Category();
    		category.setCategoryDescription(categoryDto.getCategoryDescription());	
        	entityMgr.persist(category);
        	auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_CATEGORY,String.valueOf(category.getCategoryCode()),AppConstants.TRANSACTION_TYPE_ADD);
		} catch(PersistenceException ex) {
			throw new ImproException(ex.getMessage());
		} 
		LOG.info("CategoryDaoImpl-->addCategory()-->End");
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deleteCategory(CategoryDto categoryDto) throws ImproException {
		LOG.info("CategoryDaoImpl-->deleteCategory()-->Begin");
		Query query=null;
		Session session = null;
		String msg="";
        try {
        	session = sessionFactory.openSession();
    		if(checkPresenceInProduct(Integer.parseInt(categoryDto.getCategoryCode()))) {
				return "Category are currently in use on ("+count+") Products "; 
    		} else {
    			query=session.createSQLQuery("DELETE FROM [CCE_IMGPro].[dbo].[ImgPro_Category_SubCategory] WHERE CategoryCode=:CategoryCode");
    			query.setParameter("CategoryCode", Short.parseShort(categoryDto.getCategoryCode()));
				query.executeUpdate();
				
    			query=session.createQuery("DELETE FROM Category WHERE CategoryCode=:CategoryCode");
				query.setParameter("CategoryCode", Short.parseShort(categoryDto.getCategoryCode()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_CATEGORY,String.valueOf(categoryDto.getCategoryCode()),AppConstants.TRANSACTION_TYPE_DELETE);
	    		msg= AppConstants.DATA_DELETE_SUCC_MSG;
        	}
    		
		} catch(HibernateException ex) {
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
        LOG.info("CategoryDaoImpl-->deleteCategory()-->End");
        return msg;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editCategory(CategoryDto categoryDto) throws ImproException {
		LOG.info("CategoryDaoImpl-->editCategory()-->Begin");
		Query query=null;
		Session session = null;
		String msg="";
        try {
    		session = sessionFactory.openSession();	
    			query=session.createQuery("Update Category set CategoryDescription=:CategoryDescription WHERE CategoryCode=:CategoryCode");
    			query.setParameter("CategoryDescription", categoryDto.getCategoryDescription());
    			query.setParameter("CategoryCode", Short.parseShort(categoryDto.getCategoryCode()));
    			query.executeUpdate();
    			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_CATEGORY,String.valueOf(categoryDto.getCategoryCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
    			
			msg= AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch(HibernateException ex) {
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
        LOG.info("CategoryDaoImpl-->editCategory()-->End");
        return msg;
	}
	
	@Override
	public boolean checkDuplicateCategory(String categoryDesc) throws ImproException {
		Session session = null;
		Query query=null;
		boolean flag=false;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery("select count(categoryDescription) from Category where categoryDescription= :categoryDescription");
			query.setParameter("categoryDescription",categoryDesc);
			if((Integer) query.uniqueResult() > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		} catch(HibernateException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("CategoryDaoImpl-->checkDuplicateCategory()-->Exception" +ex.getMessage());
			}
			throw new ImproException(ex.getMessage()); // NOPMD by swshedge on 3/27/18 4:22 PM
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("CategoryDaoImpl-->checkDuplicateCategory()-->End");
		return flag;
	}
	
	@Override
	public boolean checkPresenceInProduct(int categoryCode) throws ImproException {
		LOG.info("CategoryDaoImpl-->checkPresenceInProduct()-->BEGIN");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session.createSQLQuery("SELECT COUNT(*) FROM products p1"
							+ "  INNER JOIN ImgPro_Category_SubCategory p2 ON p1.categoryCode=p2.categoryCode"
							+ "  AND p1.subCategoryCode=p2.subCategoryCode AND p2.categoryCode=:categoryCode")
							.setParameter("categoryCode", categoryCode)
							.uniqueResult();
			if (count > 0) {
				LOG.info("CategoryDaoImpl-->checkPresenceInProduct()-->End");
				return true;
			} else {				
				LOG.info("CategoryDaoImpl-->checkPresenceInProduct()-->End");
				return false;
			}
		} finally {
				HibernateUtil.closeSession(session);
		}
	}
	
}
