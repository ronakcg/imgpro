package com.ko.impro.dao.impl.brand;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.brand.BrandDao;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.brand.Brands;
import com.ko.impro.model.manufacturer.Manufacturers;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * Description : This class is an implementation of BrandDao Interface
 */
@Repository
public class BrandDaoImpl extends BaseDaoImpl implements BrandDao {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(BrandDaoImpl.class);
	
	/**
	 * This is to calculate number of count of rows in product table
	 */
	private int count;
	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	

	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<BrandDto> loadBrands() throws ImproException {
		Session session=null;
		List<BrandDto> brandList=null;
		try {
			session = sessionFactory.openSession();		
			BrandDto dto = null;
			Query query=null;
				query = session.createQuery("select b.brandCode,b.brandName from Brands b  order by b.brandName asc");
			
			List<Object[]> list = (List<Object[]>) query.list();
			
			if (!list.isEmpty()) {
				brandList=new ArrayList<BrandDto>();
				for (Object[] rs : list) {
					dto = setBrandDto(rs, dto);
					brandList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		return brandList; 

	}
	
	/**
	 * Set the brandDto with code and name
	 * @param resultSet
	 * @param dto
	 * @return BrandDto
	 */
	private BrandDto setBrandDto(Object[] resultSet, BrandDto dto) {
		dto = new BrandDto();
		dto.setBrandCode(resultSet[0].toString());
		dto.setBrandName(ApplicationUtil.checkNull(resultSet[1]));
		return dto;	
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<BrandDto> loadBrands(List<String> manufctIdList) throws ImproException {
		LOG.info("BrandDaoImpl-->loadBrands()-->Begin");
		Session session=null;
		List<Integer> manufctrIdIntList = manufctIdList.stream()
	            .map(Integer::valueOf).collect(Collectors.toList());
		List<BrandDto> brandList=null;
		try {
			session = sessionFactory.openSession();		
			BrandDto dto = null;
			List<Object[]> list = session.createQuery("select distinct b.brandCode,b.brandName from Brands b "
					+ " where b.manufacturers.manufacturerCode in (:manufacturerIdList) order by b.brandName ")
					.setParameterList("manufacturerIdList", manufctrIdIntList).list();

			if (!list.isEmpty()) {
				brandList=new ArrayList<BrandDto>();
				for (Object[] rs : list) {
					dto = setBrandDto(rs, dto);
					brandList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("BrandDaoImpl-->loadBrands()-->End");
		return brandList; 
	}
	
	@Override
	public BrandDto getBrandDetails(BrandDto brandDto) throws ImproException {
		LOG.info("BrandDaoImpl-->getBrandDetails()-->Begin");
		Session session=null;
		BrandDto bDto = new BrandDto();
		try {
			session=sessionFactory.openSession();
			@SuppressWarnings("unchecked")
			List<Object[]>list=session.createQuery("select distinct b.brandName,b.shortBrandName,b.viewingOrder  from Brands b where b.brandCode=:brandCode").
			setParameter("brandCode",Integer.parseInt(brandDto.getBrandCode())).list();
			if(!list.isEmpty()) {
				for(Object[]rs:list) {
					bDto=setBrandDetailsDto(rs, brandDto);
				}
			}
		} catch(HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("BrandDaoImpl-->getBrandDetails()-->Exception "+e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("BrandDaoImpl-->getBrandDetails()-->End");
		return bDto;
	}

	/**
	 * Set BrandDetailsDto
	 * @param rs
	 * @param brandDto
	 * @return BrandDto
	 */
	private BrandDto setBrandDetailsDto(Object [] resultSet,BrandDto brandDto) {
		brandDto.setBrandName(ApplicationUtil.checkNull(resultSet[0]));
		brandDto.setShortBrandName(ApplicationUtil.checkNull(resultSet[1]));
		brandDto.setViewingOrder(ApplicationUtil.checkNull(resultSet[2]));
		return brandDto;
	}

	@Transactional(rollbackFor = ImproException.class)
	@Override
	public void addBrand(BrandDto brandDto) throws ImproException {
		LOG.info("BrandDaoImpl-->addBrand()-->BEGIN");		
		try {				
			Brands dto=new Brands();
			Manufacturers manufacturers=new Manufacturers();			
			dto.setBrandName(brandDto.getBrandName());
			dto.setShortBrandName(brandDto.getShortBrandName());
			manufacturers.setManufacturerCode(Integer.parseInt(brandDto.getManufacturerDto().getManufacturerCode()));
			dto.setManufacturers(manufacturers);
			entityMgr.persist(dto);	
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_BRANDS,String.valueOf(dto.getBrandCode()),AppConstants.TRANSACTION_TYPE_ADD);
		} catch(PersistenceException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error(" BrandDaoImpl-->addBrand--> Exception occured "+ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		}
		LOG.info("BrandDaoImpl-->addBrand()-->End");		
	}
	
	@Transactional(rollbackFor = ImproException.class)	
	@Override
	public String deleteBrand(BrandDto dto) throws ImproException {
		LOG.info("BrandDaoImpl-->deleteBrand()-->Begin");
		Session session = null;		
		Query query=null;
		String msg="";
		try {
			session = sessionFactory.openSession();
			// Checking Brands in Products
			if(checkPresenceInProduct(Integer.parseInt(dto.getBrandCode()))) {							
				return "Brand are currently in use on ("+count+") Products ";
			} else {				
				query=session.createQuery("Delete from Brands WHERE brandCode=:brandCode");
				query.setParameter("brandCode", Integer.parseInt(dto.getBrandCode()));	
				updateBrandExtendedProdDesc(dto);
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_BRANDS,String.valueOf(dto.getBrandCode()),AppConstants.TRANSACTION_TYPE_DELETE);
			}
			msg= AppConstants.DATA_DELETE_SUCC_MSG;
		} catch(HibernateException e){
			msg=AppConstants.FAILURE_MSG;
			LOG.error("ManufacturerDaoImpl-->deleteBrand()-->End");
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ManufacturerDaoImpl-->deleteBrand()-->End");	
		return msg;		
	}
	
	@Transactional(rollbackFor = ImproException.class)
	@Override
	public String editBrand(BrandDto dto) throws ImproException {
		LOG.info("BrandDaoImpl-->editBrand()-->Begin");
		Session session = null;	
		Query query=null;
		String msg="";
		try {
			session = sessionFactory.openSession();
			
				if(dto.getOldValue()!=null) {
					updateProduct(dto);
				}
				query=session.createQuery("Update Brands set brandName=:brandName,shortBrandName=:shortBrandName, "
							+ "manufacturers.manufacturerCode=:manufacturerCode WHERE brandCode=:brandCode");
				query.setParameter("brandName", dto.getBrandName());
				query.setParameter("shortBrandName",dto.getShortBrandName());
				query.setParameter("manufacturerCode", Integer.parseInt(dto.getManufacturerDto().getManufacturerCode()));	
				query.setParameter("brandCode", Integer.parseInt(dto.getBrandCode()));
				query.executeUpdate();	
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_BRANDS,String.valueOf(dto.getBrandCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
			

			msg= AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch(HibernateException e){
			if(LOG.isErrorEnabled()) {
				LOG.error("BrandDaoImpl-->editBrand()-->Exception "+e.getMessage());
			}
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("BrandDaoImpl-->editBrand()-->End");		
		return msg;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BrandDto loadBrand(int brandId) throws ImproException {
		LOG.info("BrandDaoImpl-->loadBrand()-->Begin");
		Session session=null;
		BrandDto brandDto=new BrandDto();
		try {
			session=sessionFactory.openSession();			
			List<Object[]>list=session.createQuery("select distinct b.brandName,b.shortBrandName,b.viewingOrder,m.manufacturerCode,"
					+ " m.manufacturerName,b.brandCode from Brands b, Manufacturers m where b.brandCode=:brandCode"
					+ " and m.manufacturerCode=b.manufacturers.manufacturerCode").
					setParameter("brandCode",brandId).list();
			if(null!=list && !list.isEmpty()) {
				for(Object[]rs:list) {
					brandDto=setBrandDetailsObject(rs, brandDto);
				}
			}
		} catch(HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("BrandDaoImpl-->loadBrand()-->Exception "+e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("BrandDaoImpl-->loadBrand()-->End");
		return brandDto;
	}
	
	/**
	 * Set BrandDetailsDto
	 * @param resultSet
	 * @param brandDto
	 * @return BrandDto
	 */
	private BrandDto setBrandDetailsObject(Object [] resultSet,BrandDto brandDto) {
		brandDto.setBrandName(ApplicationUtil.checkNull(resultSet[0]));
		brandDto.setShortBrandName(ApplicationUtil.checkNull(resultSet[1]));
		brandDto.setViewingOrder(ApplicationUtil.checkNull(resultSet[2]));
		ManufacturerDto dto=new ManufacturerDto();
		dto.setManufacturerCode(resultSet[3].toString());
		dto.setManufacturerName(ApplicationUtil.checkNull(resultSet[4]));
		brandDto.setBrandCode(ApplicationUtil.checkNull(resultSet[5]));
		brandDto.setManufacturerDto(dto);
		return brandDto;
		
	}

	@Override
	public boolean checkDuplicateBrand(String name,boolean isLong) throws ImproException {
		LOG.info("BrandDaoImpl-->checkDuplicateBrand()-->BEGIN");
		Session session = null;
		StringBuilder searchQuery = new StringBuilder(60);
		boolean flag=false;
		
		try {
			session = sessionFactory.openSession();
			if(isLong)
				searchQuery.append("select count(brandName) from Brands where brandName='"+name+"'");	
			else
				searchQuery.append("select count(brandName) from Brands where shortBrandName='"+name+"'");
			String sql=searchQuery.toString();
			Query query = session.createSQLQuery(sql);
			if(((Integer) query.uniqueResult()) > 0) {
				flag= true;	
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("BrandDaoImpl-->checkDuplicateBrand()-->End");
		return flag;
	}
	
	@Override
	public boolean checkPresenceInProduct(int brandCode) throws ImproException {
		LOG.info("BrandDaoImpl-->checkPresenceInProduct()-->BEGIN");
		Session session = null;
		boolean flag=false;
		StringBuilder searchQuery = new StringBuilder();
		try {
			session = sessionFactory.openSession();
			searchQuery.append("select count(brandCode) from Products where brandCode="+brandCode+" "
					+ "and Extended_Desc_id in (select Extended_Desc_id FROM ImgPro_Brand_Extended_Desc where BrandCode="+brandCode+")");			
			String sql=searchQuery.toString();
			Query query = session.createSQLQuery(sql);
			
			count = (Integer) query.uniqueResult();
			if((count) > 0) {
				LOG.info("BrandDaoImpl-->checkPresenceInProduct()-->End");
			 	flag = true;	
			} else {
				LOG.info("BrandDaoImpl-->checkPresenceInProduct()-->End");
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}	
		return flag;
	}
	
	/**
	 * To verify any active products for given brand/old manufacturer combination
	 * @param manfCode
	 * @param brandCode
	 * @return int
	 * @throws ImproException
	 */
	private int validateInProduct(int manfCode,int brandCode) throws ImproException {
		Session session = null;
		StringBuilder searchQuery = new StringBuilder();
		try {
			session = sessionFactory.openSession();
			searchQuery.append("select count(ProductCode) from products where BrandCode="+brandCode+" and ManufacturerCode="+manfCode);			
			String sql=searchQuery.toString();
			Query query = session.createSQLQuery(sql);
			count = (Integer) query.uniqueResult();
		} catch (HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("BrandDaoImpl-->validateInProduct()-->Exception"+e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return count;
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)	
	public void updateProduct(BrandDto dto) throws ImproException {
		LOG.info("BrandDaoImpl-->updateProduct()-->Begin");
		Session session = null;		
		try {
			
			// If user wants to modify the brand along with Manufacuturer
			session = sessionFactory.openSession();
			Query query=session.createQuery("Update Products p set p.manufacturerCode=:manufCode where p.brandCode=:brandCode and "
					+ "p.manufacturerCode=:oldCode");
			query.setParameter("manufCode", Integer.parseInt(dto.getManufacturerDto().getManufacturerCode()));
			query.setParameter("brandCode",Integer.parseInt(dto.getBrandCode()));
			query.setParameter("oldCode", Integer.parseInt(dto.getOldValue()));
			query.executeUpdate();	
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PRODUCTS,String.valueOf(dto.getBrandCode()),AppConstants.TRANSACTION_TYPE_CHANGE);

			LOG.info("BrandDaoImpl-->updateProduct()-->End");
		} catch(HibernateException e){
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}		
	}

	@Override
	public boolean validateInProduct(BrandDto dto) throws ImproException {
		LOG.info("BrandDaoImpl-->validateInProduct()-->Begin");
		int count =validateInProduct(Integer.parseInt(dto.getOldValue()),Integer.parseInt(dto.getBrandCode()));
		boolean flag=false;
		if(count >0) {
			flag=true;
			dto.setCounter(count);
		}
		LOG.info("BrandDaoImpl-->validateInProduct()-->End");
		return flag;			
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void updateBrandExtendedProdDesc(BrandDto dto) throws ImproException {
		LOG.info("BrandDaoImpl-->updateBrandExtendedProdDesc()-->Begin");
		try {						
				entityMgr.createNativeQuery("Delete from [CCE_IMGPro].[dbo].[ImgPro_Brand_Extended_Desc] WHERE brandCode = ?")
				.setParameter(1, Integer.parseInt(dto.getBrandCode()))
				.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_BRANDS_EXTEN_DESC,String.valueOf(dto.getBrandCode()),AppConstants.TRANSACTION_TYPE_DELETE);
					
			LOG.info("BrandDaoImpl-->updateBrandExtendedProdDesc()-->End");
		} catch(PersistenceException e){
			throw new ImproException(e.getMessage());
		} 	
	}
	
	
}
