package com.ko.impro.dao.impl.gatekeeping;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.gatekeeping.ProductGateKeepingDao;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.dto.MissingImagesDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author Vilas
 * Data Access Object of ProductGateKeeping - Methods implementations
 */
@Repository
public class ProductGateKeepingDaoImpl extends BaseDaoImpl implements ProductGateKeepingDao {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ProductGateKeepingDaoImpl.class);

	@PersistenceContext
	private EntityManager entityMgr;

	@Autowired
	private  SessionFactory sessionFactory;


	/**
	 * Load the complete/incomplete records
	 * @param isInComplete
	 * @return List<ProductGateKeepingDto>
	 * @throws ImproException
	 */
	// Load the records if it is complete/incomplete
	@SuppressWarnings("unchecked")
	@Override
	public List<ProductGateKeepingDto> loadGateKeeping(boolean isInComplete) throws ImproException {
		LOG.info("GateKeepingDaoImpl-->loadGateKeeping()-->Begin");
		Session session = null;
		List<ProductGateKeepingDto> productList = null;
		try {
			session = sessionFactory.openSession();
			ProductGateKeepingDto dto = null;
			String sql  = ("SELECT g.ProductCode,g.ProductName,g.UPC,g.UPC12,g.Height,g.Width,g.Depth,g.PackageCode,g.PackageName,"
					+ "  g.ContainerTypeCode,g.ContainerTypeName,g.BrandCode,g.BrandName, "
					+ "  g.FlavorCode,g.FlavorName,g.CategoryCode,g.CategoryName,g.SubCategoryCode,g.SubCategoryName,"
					+ "  g.SegmentCode,g.SegmentName,g.SubSegmentCode,g.SubSegmentName, "
					+ "  g.ManufacturerCode,g.ManufacturerName,g.Extended_Desc_Id,g.PackageSizeCode,g.PackageSizeName,g.ProductId, "
					+ "  g.LowRes1,g.LowRes2,g.LowRes3,g.LowRes7,g.LowRes8,g.LowRes9,g.HiRes1,g.HiRes2,g.HiRes3,g.HiRes7,g.HiRes8,g.HiRes9 "
					+ "  FROM CCE_IMGPro.[dbo].GateKeeping g "
					+ "  where g.completeFlag=:status and  g.DeleteFlag=:DeleteFlag");

			Query query  = session.createSQLQuery(sql);
			if (LOG.isInfoEnabled()) {
				LOG.info("SQLQuery:" + sql);
			}
			query.setParameter("DeleteFlag", AppConstants.STATUS_FLAG_NO);

			if (isInComplete) {
				query.setParameter("status", AppConstants.STATUS_FLAG_NO);
			} else {
				query.setParameter("status", AppConstants.STATUS_FLAG_YES);
			}
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				productList = new ArrayList<ProductGateKeepingDto>();
				for (Object[] rs : list) {
					dto = setGateKeepingListDto(rs, dto);
					dto.setCompleteFlag(isInComplete);
					productList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("GateKeepingDaoImpl-->loadGateKeeping()-->End");
		return productList;
	}

	/**
	 * Set the GateKeepingDto
	 * 
	 * @param rsObject
	 * @param gatekeepingDto
	 * @return GateKeepingDto
	 */
	private ProductGateKeepingDto setGateKeepingListDto(Object[] rsObject, ProductGateKeepingDto gatekeepingDto) {
		gatekeepingDto = new ProductGateKeepingDto();

		gatekeepingDto.setProductCode(ApplicationUtil.checkNull(rsObject[0]));
		gatekeepingDto.setProductName(ApplicationUtil.checkNull(rsObject[1]));
		gatekeepingDto.setUpc(ApplicationUtil.checkNull(rsObject[2]));
		gatekeepingDto.setUpc12(ApplicationUtil.checkNull(rsObject[3]));

		PackageDto pdto = new PackageDto();
		pdto.setUnitHeight(ApplicationUtil.checkNull(rsObject[4]));
		pdto.setUnitWidth(ApplicationUtil.checkNull(rsObject[5]));
		pdto.setUnitDepth(ApplicationUtil.checkNull(rsObject[6]));
		pdto.setPackageName(ApplicationUtil.checkNull(rsObject));
		pdto.setPackageCode(ApplicationUtil.checkNull(rsObject[8]));
		gatekeepingDto.setPackageDto(pdto);

		ContainerTypeDto cdto = new ContainerTypeDto();
		cdto.setContainerTypeCode(ApplicationUtil.checkNull(rsObject[9]));
		cdto.setContainerTypeDescription(ApplicationUtil.checkNull(rsObject[10]));
		gatekeepingDto.setContainerTypeDto(cdto);

		BrandDto brandDto = new BrandDto();
		brandDto.setBrandCode(ApplicationUtil.checkNull(rsObject[11]));
		brandDto.setBrandName(ApplicationUtil.checkNull(rsObject[12]));
		gatekeepingDto.setBrandDto(brandDto);

		FlavorDto flavorDto = new FlavorDto();
		flavorDto.setFlavorCode(ApplicationUtil.checkNull(rsObject[13]));
		flavorDto.setFlavorDescription(ApplicationUtil.checkNull(rsObject[14]));
		gatekeepingDto.setFlavorDto(flavorDto);

		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setCategoryCode(ApplicationUtil.checkNull(rsObject[15]));
		categoryDto.setCategoryDescription(ApplicationUtil.checkNull(rsObject[16]));
		gatekeepingDto.setCategoryDto(categoryDto);

		SubCategoryDto subCategoryDto = new SubCategoryDto();
		subCategoryDto.setSubCategoryCode(ApplicationUtil.checkNull(rsObject[17]));
		subCategoryDto.setSubCategoryDescription(ApplicationUtil.checkNull(rsObject[18]));
		gatekeepingDto.setSubCategoryDto(subCategoryDto);

		SegmentDto segmentDto = new SegmentDto();
		segmentDto.setSegmentCode(ApplicationUtil.checkNull(rsObject[19]));
		segmentDto.setSegmentDescription(ApplicationUtil.checkNull(rsObject[20]));
		gatekeepingDto.setSegmentDto(segmentDto);

		SubSegmentDto subSegmentDto = new SubSegmentDto();
		subSegmentDto.setSubSegmentCode(ApplicationUtil.checkNull(rsObject[21]));
		subSegmentDto.setSubSegmentDescription(ApplicationUtil.checkNull(rsObject[22]));
		gatekeepingDto.setSubSegmentDto(subSegmentDto);

		ManufacturerDto manufacturerDto = new ManufacturerDto();
		manufacturerDto.setManufacturerCode(ApplicationUtil.checkNull(rsObject[23]));
		manufacturerDto.setManufacturerName(ApplicationUtil.checkNull(rsObject[24]));
		gatekeepingDto.setManufacturerDto(manufacturerDto);

		ExtendedProductDescDto extPrdDescDto = new ExtendedProductDescDto();
		extPrdDescDto.setExtendedDescId(ApplicationUtil.checkNull(rsObject[25]));
		gatekeepingDto.setExtPrdDescDto(extPrdDescDto);

		gatekeepingDto.setCompleteFlag(Boolean.valueOf(ApplicationUtil.checkNull(rsObject[26])));

		PackageSizeDto psDto = new PackageSizeDto();
		psDto.setPackageSizeCode(ApplicationUtil.checkNull(rsObject[26]));
		psDto.setPackageSizeDescription(ApplicationUtil.checkNull(rsObject[27]));
		gatekeepingDto.setPackageSizeDto(psDto);
		
		gatekeepingDto.setProductId(ApplicationUtil.checkNull(rsObject[28]));
		
		MissingImagesDto missingImagesDto = new MissingImagesDto();
		missingImagesDto.setLowRes1(ApplicationUtil.checkNull(rsObject[29]));
		missingImagesDto.setLowRes2(ApplicationUtil.checkNull(rsObject[30]));
		missingImagesDto.setLowRes3(ApplicationUtil.checkNull(rsObject[31]));
		missingImagesDto.setLowRes7(ApplicationUtil.checkNull(rsObject[32]));
		missingImagesDto.setLowRes8(ApplicationUtil.checkNull(rsObject[33]));
		missingImagesDto.setLowRes9(ApplicationUtil.checkNull(rsObject[34]));
		
		missingImagesDto.setHiRes1(ApplicationUtil.checkNull(rsObject[35]));
		missingImagesDto.setHiRes2(ApplicationUtil.checkNull(rsObject[36]));
		missingImagesDto.setHiRes3(ApplicationUtil.checkNull(rsObject[37]));
		missingImagesDto.setHiRes7(ApplicationUtil.checkNull(rsObject[38]));
		missingImagesDto.setHiRes8(ApplicationUtil.checkNull(rsObject[39]));
		missingImagesDto.setHiRes9(ApplicationUtil.checkNull(rsObject[40]));
		gatekeepingDto.setMissingImagesDto(missingImagesDto);

		return gatekeepingDto;
	}


	/**
	 * Pull the gateKeeping Details
	 * 
	 * @param gatekeepingId
	 * @return ProductGateKeepingDto
	 * @throws ImproException
	 */
	// Load the records, depends on productcode
	@Override
	public ProductGateKeepingDto loadGateKeeping(int prodCode) throws ImproException {
		LOG.info("GateKeepingDaoImpl-->loadGateKeeping()-->Begin");
		Session session = null;
		ProductGateKeepingDto dto = null;
		try {
			session = sessionFactory.openSession();
			String sql = ("SELECT g.ProductCode,g.ProductName,g.UPC,g.UPC12,g.Height,g.Width,g.Depth,g.PackageCode,g.PackageName,"
					+ "  g.ContainerTypeCode,g.ContainerTypeName,g.BrandCode,g.BrandName, "
					+ "  g.FlavorCode,g.FlavorName,g.CategoryCode,g.CategoryName,g.SubCategoryCode,"
					+ "  g.SubCategoryName,g.SegmentCode,g.SegmentName,g.SubSegmentCode,g.SubSegmentName, "
					+ "  g.ManufacturerCode,g.ManufacturerName,g.Extended_Desc_Id,g.PackageSizeCode,g.PackageSizeName,g.ProductId, "
					+ "  g.LowRes1,g.LowRes2,g.LowRes3,g.LowRes7,g.LowRes8,g.LowRes9,g.HiRes1,g.HiRes2,g.HiRes3,g.HiRes7,g.HiRes8,g.HiRes9 "
					+ "  FROM CCE_IMGPro.[dbo].GateKeeping g " + "  where g.productcode=:productcode ");
			Query query = session.createSQLQuery(sql);
			query.setParameter("productcode", prodCode);

			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				for (Object[] rs : list) {
					dto = setGateKeepingListDto(rs, dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("GateKeepingDaoImpl-->loadGateKeeping()-->End");

		return dto;
	}

	/**
	 * Method to trigger StoredProcedure [dbo].[ProductUpdateFromGatekeeping]
	 * 
	 * @param dto
	 * @return ResponseEntity<GateKeepingResponseDto>
	 */
	// push the products of ProductGateKeeping
	@Override
	public void pushProducts() throws ImproException {
		LOG.info("GateKeepingDaoImpl-->pushProducts()-->BEGIN");
		Session session = null;
		Transaction transaction = null;
		session = HibernateUtil.getSessionFactory(entityMgr).openSession();
		try {
			long lStartTime = System.nanoTime();
			transaction = session.beginTransaction();
			SQLQuery query = session.createSQLQuery("EXEC [dbo].[ProductUpdateFromGatekeeping]");
			query.executeUpdate();
			transaction.commit();
			long lEndTime = System.nanoTime();
			// time elapsed
			long output = lEndTime - lStartTime;
			if (LOG.isInfoEnabled()) {
				LOG.info("Elapsed time in milliseconds: " + output / 1000000);
			}
		} catch (Exception ex) {
			if (LOG.isErrorEnabled()) {
				LOG.error("GateKeepingDaoImpl-->pushProducts()-->EXCEPTION" + ex.getMessage());
			}
			transaction.rollback();
			throw new ImproException(ex);
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("GateKeepingDaoImpl-->purgeProductGateKeeping()-->END");

	}

	/**
	 * To purge a   productGatekeeping record
	 * @param int gatekeepingId
	 * @throws ImproException
	 */
	// purge the records of ProductGateKeeping
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void purgeProductGateKeeping(int gatekeepingId) throws ImproException {
		LOG.info("GateKeepingDaoImpl-->purgeProductGateKeeping()-->BEGIN");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			String purgequery = "Update GateKeeping p set p.deleteFlag=:deleteFlag,p.deleteDate=:deleteDate "
					+ "WHERE p.productCode=:productCode";
			LOG.info(purgequery);
			Query query = session.createQuery(purgequery);
			query.setParameter("deleteFlag", AppConstants.STATUS_FLAG_YES);
			query.setParameter("deleteDate", ApplicationUtil.getDateTime());
			query.setParameter("productCode", gatekeepingId);
			query.executeUpdate();
		} catch (Exception ex) {
			if (LOG.isErrorEnabled()) {
				LOG.error("GateKeepingDaoImpl-->purgeProductGateKeeping()-->EXCPETION " + ex.getMessage());
			}
			throw new ImproException(ex);
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("GateKeepingDaoImpl-->purgeProductGateKeeping()-->END");
	}

	/**
	 * Method to edit Gatekeeping
	 * 
	 * @param gatekeepingDto
	 * @throws ImproException
	 */
	// edit/update the required records of ProductGateKeeping
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void editGateKeeping(ProductGateKeepingDto gatekeepingDto) throws ImproException {
		LOG.info("ProductGateKeepingDaoImpl()-->editGateKeeping()-->Begin");
		Session session = null;
		Query query = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery("UPDATE [CCE_IMGPro].[dbo].[Gatekeeping]  SET productCode=:ProductCode, ProductId=:ProductId, "
					+ "productName=:ProductName,completeFlag=:CompleteFlag,shortProductName=:ShortProductName, "
					+ "height=:Height,width=:Width,depth=:Depth,"
					+ "packageCode=:PackageCode,packageName=:PackageName,packageSizeCode=:PackageSizeCode,packageSizeName=:PackageSizeName,"
					+ "containerTypeCode=:ContainerTypeCode, containerTypeName=:ContainerTypeName,"
					+ "brandCode=:BrandCode,brandName=:BrandName,upc=:UPC,flavorCode=:FlavorCode,"
					+ "flavorName=:FlavorName,categoryCode=:CategoryCode,"
					+ "categoryName=:CategoryName,subCategoryCode=:SubCategoryCode,subCategoryName=:SubCategoryName, "
					+ "segmentCode=:SegmentCode,"
					+ "segmentName=:SegmentName,subSegmentCode=:SubSegmentCode,subSegmentName=:SubSegmentName,"
					+ "manufacturerCode=:ManufacturerCode,"
					+ "manufacturerName=:ManufacturerName,extended_Desc_Id=:Extended_Desc_Id,upc12=:UPC12,DateMaintained=:DateMaintained,"
					+ "hiRes1=:hiRes1,hiRes2=:hiRes2,hiRes3=:hiRes3,hiRes7=:hiRes7, hiRes8=:hiRes8,hiRes9=:hiRes9,"
					+ "lowRes1=:LowRes1,lowRes2=:LowRes2,lowRes3=:LowRes3,lowRes7=:LowRes7, lowRes8=:LowRes8,lowRes9=:LowRes9"
					
					+ " WHERE productCode=:ProductCode ");
			if (LOG.isInfoEnabled()) {
				LOG.info(" SQLQuery:" + query);
			}
			query.setParameter("ProductCode", gatekeepingDto.getProductCode());
			query.setParameter("ProductId", gatekeepingDto.getProductId());

			query.setParameter("ProductName", ApplicationUtil.generateLongProductName(gatekeepingDto));
			query.setParameter("ShortProductName", ApplicationUtil.generateShortProductName(gatekeepingDto));
			if(gatekeepingDto.isApprovedFlag()) {
				query.setParameter("CompleteFlag", true);
			} else {
				query.setParameter("CompleteFlag", false);
			}
			query.setParameter("Height", gatekeepingDto.getPackageDto().getUnitHeight());
			query.setParameter("Width", gatekeepingDto.getPackageDto().getUnitWidth());
			query.setParameter("Depth", gatekeepingDto.getPackageDto().getUnitDepth());
			query.setParameter("PackageCode", gatekeepingDto.getPackageDto().getPackageCode());
			query.setParameter("PackageName", gatekeepingDto.getPackageDto().getPackageName());
			query.setParameter("PackageSizeName", gatekeepingDto.getPackageSizeDto().getPackageSizeDescription());
			query.setParameter("PackageSizeCode", gatekeepingDto.getPackageSizeDto().getPackageSizeCode());
			query.setParameter("ContainerTypeCode",
					Integer.parseInt(gatekeepingDto.getContainerTypeDto().getContainerTypeCode()));
			query.setParameter("ContainerTypeName", gatekeepingDto.getContainerTypeDto().getContainerTypeDescription());
			query.setParameter("BrandCode", Integer.parseInt(gatekeepingDto.getBrandDto().getBrandCode()));
			query.setParameter("BrandName", gatekeepingDto.getBrandDto().getBrandName());
			query.setParameter("UPC", gatekeepingDto.getUpc());
			query.setParameter("FlavorCode", Short.parseShort(gatekeepingDto.getFlavorDto().getFlavorCode()));
			query.setParameter("FlavorName", gatekeepingDto.getFlavorDto().getFlavorDescription());
			query.setParameter("CategoryCode", Short.parseShort(gatekeepingDto.getCategoryDto().getCategoryCode()));
			query.setParameter("CategoryName", gatekeepingDto.getCategoryDto().getCategoryDescription());
			query.setParameter("DateMaintained", ApplicationUtil.getDateTime());
			query.setParameter("SubCategoryCode",
					Short.parseShort(gatekeepingDto.getSubCategoryDto().getSubCategoryCode()));
			query.setParameter("SubCategoryName", gatekeepingDto.getSubCategoryDto().getSubCategoryDescription());
			query.setParameter("SegmentCode", Short.parseShort(gatekeepingDto.getSegmentDto().getSegmentCode()));
			query.setParameter("SegmentName", gatekeepingDto.getSegmentDto().getSegmentDescription());
			query.setParameter("SubSegmentCode",
					Short.parseShort(gatekeepingDto.getSubSegmentDto().getSubSegmentCode()));
			query.setParameter("SubSegmentName", gatekeepingDto.getSubSegmentDto().getSubSegmentDescription());
			query.setParameter("ManufacturerCode",
					Integer.parseInt(gatekeepingDto.getManufacturerDto().getManufacturerCode()));
			query.setParameter("ManufacturerName", gatekeepingDto.getManufacturerDto().getManufacturerName());
			query.setParameter("Extended_Desc_Id",
					Integer.parseInt(gatekeepingDto.getExtPrdDescDto().getExtendedDescId()));
			query.setParameter("UPC12", gatekeepingDto.getUpc12());
			updateImageFlags(gatekeepingDto,query);
			query.executeUpdate();
		} catch (Exception ex) {
			if (LOG.isErrorEnabled()) {
				LOG.error("ProductGateKeepingDaoImpl()-->editGateKeeping()-->Exception " + ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ProductGateKeepingDaoImpl()-->editGateKeeping()-->End");
	}
	
	/**
	 * Update the images flags when image is added or deleted
	 * @param dto
	 * @param query
	 */
	private void updateImageFlags(ProductGateKeepingDto prodGateDto,Query query) {
		if(prodGateDto.getMissingImagesDto().isIslowRes1() ) {
			query.setParameter("LowRes1", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIslowRes1()) {
			if(prodGateDto.getMissingImagesDto().getLowRes1Url() != null) {
				query.setParameter("LowRes1", "N");
			}
		} else {
			query.setParameter("LowRes1", "");
		}
		
		if(prodGateDto.getMissingImagesDto().isIslowRes2() ) {
			query.setParameter("LowRes2", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIslowRes2()) {
			if(prodGateDto.getMissingImagesDto().getLowRes2Url() != null) {
				query.setParameter("LowRes2", "N");
			}
		} else {
			query.setParameter("LowRes2", "");
		}
		
		if(prodGateDto.getMissingImagesDto().isIslowRes3() ) {
			query.setParameter("LowRes3", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIslowRes3()) {
			if(prodGateDto.getMissingImagesDto().getLowRes3Url() != null) {
				query.setParameter("LowRes3", "N");
			}
		} else {
			query.setParameter("LowRes3", "");
		}
		
		if(prodGateDto.getMissingImagesDto().isIslowRes7() ) {
			query.setParameter("LowRes7", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIslowRes7()) {
			if(prodGateDto.getMissingImagesDto().getLowRes7Url() != null) {
				query.setParameter("LowRes7", "N");
			}
		} else {
			query.setParameter("LowRes7", "");
		}
		
		if(prodGateDto.getMissingImagesDto().isIslowRes8() ) {
			query.setParameter("LowRes8", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIslowRes8()) {
			if(prodGateDto.getMissingImagesDto().getLowRes8Url() != null) {
				query.setParameter("LowRes8", "N");
			}
		} else {
			query.setParameter("LowRes8", "");
		}
		
		if(prodGateDto.getMissingImagesDto().isIslowRes9() ) {
			query.setParameter("LowRes9", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIslowRes9()) {
			if(prodGateDto.getMissingImagesDto().getLowRes9Url() != null) {
				query.setParameter("LowRes9", "N");
			}
		} else {
			query.setParameter("LowRes9", "");
		}
		
		
		if(prodGateDto.getMissingImagesDto().isIshiRes1() ) {
			query.setParameter("hiRes1", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIshiRes1()) {
			if(prodGateDto.getMissingImagesDto().getHiRes1Url() != null) {
				query.setParameter("hiRes1", "N");
			}
		} else {
			query.setParameter("hiRes1", "");
		}
		
		if(prodGateDto.getMissingImagesDto().isIshiRes2() ) {
			query.setParameter("hiRes2", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIshiRes2()) {
			if(prodGateDto.getMissingImagesDto().getHiRes2Url() != null) {
				query.setParameter("hiRes2", "N");
			}
		} else {
			query.setParameter("hiRes2", "");

		}
		if(prodGateDto.getMissingImagesDto().isIshiRes3() ) {
			query.setParameter("hiRes3", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIshiRes3()) {
			if(prodGateDto.getMissingImagesDto().getHiRes3Url() != null) {
				query.setParameter("hiRes3", "N");
			}
		} else {
			query.setParameter("hiRes3", "");

		}
		
		if(prodGateDto.getMissingImagesDto().isIshiRes7() ) {
			query.setParameter("hiRes7", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIshiRes7()) {
			if(prodGateDto.getMissingImagesDto().getHiRes7Url() != null) {
				query.setParameter("hiRes7", "N");
			}
		} else {
			query.setParameter("hiRes7", "");
		}
		if(prodGateDto.getMissingImagesDto().isIshiRes8() ) {
			query.setParameter("hiRes8", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIshiRes8()) {
			if(prodGateDto.getMissingImagesDto().getHiRes8Url() != null) {
				query.setParameter("hiRes8", "N");
			}
		} else {
			query.setParameter("hiRes8", "");
		}
		
		if(prodGateDto.getMissingImagesDto().isIshiRes9() ) {
			query.setParameter("hiRes9", "Y");
		} else if(prodGateDto.getProductDto() != null && prodGateDto.getProductDto().getMissingImagesDto().isIshiRes9()) {
			if(prodGateDto.getMissingImagesDto().getHiRes9Url() != null) {
				query.setParameter("hiRes9", "N");
			}
		} else {
			query.setParameter("hiRes9", "");

		}
		
	}

}
