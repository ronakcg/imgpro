package com.ko.impro.dao.impl.deletemessagedata;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.deletemessagedata.DeleteMessageDataDao;
import com.ko.impro.dto.DeleteMessagesDataDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.deletemessagedata.DeleteMessagesData;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * Description : This class is an implementation of DeleteMessageDataDao Interface
 */
@Repository
public class DeleteMessagesDataDaoImpl extends BaseDaoImpl implements DeleteMessageDataDao {
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(DeleteMessagesDataDaoImpl.class);
	
	/**
	 * This is to calculate number of count of rows in product table
	 */
	private int count;
	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<DeleteMessagesDataDto> loadDeleteMessagesData() throws ImproException {
		LOG.info("DeleteMessagesDataDaoImpl-->loadDeleteMessagesData()-->Begin");
		Session session=null;
		List<DeleteMessagesDataDto> conList=null;
		try {
			session = sessionFactory.openSession();		
			DeleteMessagesDataDto dto=null;
			Query query=null;
			
				query = session.createQuery("select c.deleteMessageId,c.deleteMessageText from DeleteMessagesData c "
						+ " order by c.deleteMessageText asc");
			
			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				conList=new ArrayList<DeleteMessagesDataDto>();
				for (Object[] rs : list) {
					dto = setDeleteMessagesDataDto(rs, dto);
					conList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("DeleteMessagesDataDaoImpl-->loadDeleteMessagesData()-->End");
		return conList; 
	}

	/**
	 * To set the DeleteMessagesDataDto
	 * @param rs
	 * @param dto
	 * @return DeleteMessagesDataDto
	 */
	private DeleteMessagesDataDto setDeleteMessagesDataDto(Object[] resultSet, DeleteMessagesDataDto dto) {
		dto=new DeleteMessagesDataDto();
		dto.setDeleteMessageId(resultSet[0].toString());
		dto.setDeleteMessageText(ApplicationUtil.checkNull(resultSet[1]));
		return dto;	
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addDeleteMessagesData(DeleteMessagesDataDto delMsgDataDto) throws ImproException {
		LOG.info("DeleteMessagesDataDaoImpl-->addDeleteMessagesData()-->BEGIN");
		try {				
			DeleteMessagesData delMsgData=new DeleteMessagesData();			
			delMsgData.setDeleteMessageText(delMsgDataDto.getDeleteMessageText());
			entityMgr.persist(delMsgData);
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_DELETE_MESSAGES_DATA,String.valueOf(delMsgData.getDeleteMessageId()),AppConstants.TRANSACTION_TYPE_ADD);
		} catch(PersistenceException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error(" DeleteMessagesDataDaoImpl-->addDeleteMessagesData--> Exception occured "+ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		}
		LOG.info("DeleteMessagesDataDaoImpl-->addDeleteMessagesData()-->End");		
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deleteDeleteMessagesData(DeleteMessagesDataDto dto) throws ImproException {
		LOG.info("DeleteMessagesDaoImpl-->deleteDeleteMessagesData()-->Begin");
		Session session = null;		
		Query query=null;
		String msg="";
		try {
			session = sessionFactory.openSession();
			if(checkDeleteMessagesDataInArchiveProducts(dto.getDeleteMessageText())) {							
				return "DeleteMessagesData are currently in use on ("+count+") Archive Products ";
			} else {				
				query=session.createQuery("Delete from DeleteMessagesData WHERE deleteMessageId=:deleteMessageId");
				//query.setParameter("DeleteMessagesDataDescription",dto.getDeleteMessageText());
			   query.setParameter("deleteMessageId", Integer.parseInt(dto.getDeleteMessageId()));
			}
					
			query.executeUpdate();
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_DELETE_MESSAGES_DATA,String.valueOf(dto.getDeleteMessageId()),AppConstants.TRANSACTION_TYPE_DELETE);
			msg= AppConstants.DATA_DELETE_SUCC_MSG;
		} catch(HibernateException e){
			msg=AppConstants.FAILURE_MSG;
			if(LOG.isErrorEnabled()) {
				LOG.error("DeleteMessagesDaoImpl-->deleteDeleteMessagesData()-->End");
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("DeleteMessagesDataDaoImpl-->deleteDeleteMessagesData()-->End");	
		return msg;	

	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editDeleteMessagesData(DeleteMessagesDataDto dto) throws ImproException {
		LOG.info("DeleteMessagesDataDaoImpl-->editDeleteMessagesData()-->BEGIN");
		Session session = null;	
		Query query=null;
		String msg="";
		try {
			session = sessionFactory.openSession();
			
				query=session.createQuery("Update DeleteMessagesData set deleteMessageText=:DeleteMessagesDataDescription  WHERE deleteMessageId=:deleteMessageId");
				query.setParameter("DeleteMessagesDataDescription", dto.getDeleteMessageText());
				query.setParameter("deleteMessageId", Integer.parseInt(dto.getDeleteMessageId()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_DELETE_MESSAGES_DATA,String.valueOf(dto.getDeleteMessageId()),AppConstants.TRANSACTION_TYPE_CHANGE);
			
			msg= AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch(HibernateException e){
			if(LOG.isErrorEnabled()) {
				LOG.error("DeleteMessagesDataDaoImpl-->editDeleteMessagesData()-->Exception "+e.getMessage());
			}
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return msg;

	}

	@Override
	public DeleteMessagesDataDto loadDeleteMessagesData(int messageID) throws ImproException {
		LOG.info("DeleteMessagesDataDaoImpl-->loadDeleteMessagesDatas(int id)-->Begin");
		Session session=null;
		DeleteMessagesDataDto dto=null;
		try {
			session = sessionFactory.openSession();		
			Query query = session.createQuery("select c.deleteMessageId,c.deleteMessageText from DeleteMessagesData c "
					+ " where c.deleteMessageId=:deleteMessageId");
			query.setParameter("deleteMessageId", messageID);
			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				for (Object[] rs : list) {
					dto = setDeleteMessagesDataDto(rs, dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("DeleteMessagesDataDaoImpl-->loadDeleteMessagesDatas(int id)-->End");
		return dto; 
	}

	@Override
	public boolean checkDuplicateDeleteMessagesData(String name) throws ImproException {
		LOG.info("DeleteMessagesDataDaoImpl-->checkDuplicateDeleteMessagesData()-->BEGIN");
		Session session = null;
		boolean flag= false;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery("select count(deleteMessageText) from DeleteMessagesData where deleteMessageText= :deleteMessageText");
			query.setParameter("deleteMessageText",name);
			if ((Long) query.uniqueResult() > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		} catch(HibernateException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("DeleteMessagesDataDaoImpl-->checkDuplicateDeleteMessagesData()-->Exception" +ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("DeleteMessagesDataDaoImpl-->checkDuplicateDeleteMessagesData()-->End");
		return flag;
	}
	
	/**
	 * Method to check whether delete reason  is used  in Archive Products
	 * @param reason
	 * @return boolean
	 * @throws ImproException
	 */
	private boolean checkDeleteMessagesDataInArchiveProducts(String  reason)  throws ImproException {
		LOG.info("DeleteMessagesDataDaoImpl-->checkDeleteMessagesDataInArchiveProducts()-->BEGIN");
		Session session=null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();		
			count=(Integer)session.createSQLQuery("select count(*)  from ImgPro_Deleted_Products p  where p.DELETE_REASON =:DELETE_REASON ")
			.setParameter("DELETE_REASON", reason).uniqueResult();
			if(count > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("DeleteMessagesDataDaoImpl-->checkDeleteMessagesDataInArchiveProducts()-->End");
		return flag;
	}


}