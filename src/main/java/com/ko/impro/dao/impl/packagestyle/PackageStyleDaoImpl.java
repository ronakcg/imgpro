package com.ko.impro.dao.impl.packagestyle;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.packagestyle.PackageStyleDao;
import com.ko.impro.dto.PackageStyleDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.packagestyle.PackageStyle;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author hlekshmy : 
 * This class is an implementation of PackageStyleDao Interface
 */
@Repository
public class PackageStyleDaoImpl extends BaseDaoImpl implements PackageStyleDao {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PackageStyleDaoImpl.class);

	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * This is to calculate number of count of rows in product table
	 */
	private int count;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired
	private AuditLogDao auditLog;

	@SuppressWarnings("unchecked")
	@Override
	public PackageStyleDto loadPackageStyle(int pkgStyId) throws ImproException {
		LOG.info("PackageStyleDaoImpl-->loadPackageStyle(int)-->Begin");
		Session session = null;
		List<PackageStyleDto> pkgStyleList = null;
		try {
			session = sessionFactory.openSession();
			PackageStyleDto dto = null;
			Query query = session
					.createQuery("select p.styleCode,p.styleDesc from PackageStyle p where p.styleCode=:styleCode");
			query.setParameter("styleCode", pkgStyId);
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				pkgStyleList = new ArrayList<PackageStyleDto>();
				for (Object[] rs : list) {
					dto = setPackageStyleDto(rs, dto);
					pkgStyleList.add(dto);
				}
			}
		} catch (HibernateException e) {
			LOG.error("PackageStyleDaoImpl-->loadPackageStyle(int)-->End");
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("PackageStyleDaoImpl-->loadPackageStyle(int)-->End");
		return pkgStyleList.get(0);
	}


	@Override
	public boolean checkDuplicatePackageStyle(String name) throws ImproException {
		LOG.info("PackageStyleDaoImpl-->checkDuplicatePackageStyle()-->BEGIN");
		Session session = null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery("select count(styleDesc) from PackageStyle where styleDesc= :name");
			query.setParameter("name", name);
			if ((Long) query.uniqueResult() > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("PackageStyleDaoImpl-->checkDuplicatePackageStyle()-->End");
		return flag;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addPackageStyle(PackageStyleDto packageStyleDto) throws ImproException {
		LOG.info("PackageStyleDaoImpl-->addPackageStyle()-->BEGIN");
		try {				
			PackageStyle packageStyle=new PackageStyle();			
			packageStyle.setStyleDesc(packageStyleDto.getStyleDesc());
			entityMgr.persist(packageStyle);
			auditLog.addLog(AppConstants.USER_NAME, AppConstants.TABLE_PACKAGESTYLE, packageStyleDto.getStyleDesc(), AppConstants.TRANSACTION_TYPE_ADD);
		} catch(PersistenceException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error(" PackageStyleDaoImpl-->addPackageStyle--> Exception occured "+ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		}
		LOG.info("PackageStyleDaoImpl-->addPackageStyle()-->End");	
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editPackageStyle(PackageStyleDto dto) throws ImproException {
		LOG.info("PackageStyleDaoImpl-->editPackageStyle()-->BEGIN");
		Session session = null;
		Query query = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
				query = session.createQuery("Update PackageStyle set styleDesc=:styleDesc WHERE styleCode=:styleCode");
				query.setParameter("styleCode", Integer.parseInt(dto.getStyleCode()));
				query.setParameter("styleDesc", dto.getStyleDesc());
				query.executeUpdate();
				auditLog.addLog(AppConstants.USER_NAME, AppConstants.TABLE_PACKAGESTYLE, dto.getStyleDesc(), AppConstants.TRANSACTION_TYPE_CHANGE);
			msg = AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch (HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageStyleDaoImpl-->editPackageStyle()-->Exception " + e.getMessage());
			}
			msg = AppConstants.FAILURE_MSG;
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return msg;
	}
	
	@Override
	public boolean checkPresenceInPackages(int packageStyle) throws ImproException {
		LOG.info("PackageStyleDaoImpl-->checkPresenceInPackages()-->BEGIN");
		Session session = null;
		count =0;
		boolean flag = false;
		StringBuilder searchQuery = new StringBuilder();
		try {
			session = sessionFactory.openSession();
			searchQuery.append("select count(packageStyle) from Packages where packageStyle="+packageStyle);
			String sql=searchQuery.toString();
			Query query = session.createSQLQuery(sql);
			count = (Integer) query.uniqueResult();
			if((count) > 0) {
				flag = true;	
			} else {
				flag = false;
			}
		}catch (HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageStyleDaoImpl-->checkPresenceInPackages()-->Exception"+e.getMessage());
			}
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}		
		LOG.info("PackageStyleDaoImpl-->checkPresenceInPackages()-->End");
		return flag;
	}
	
	/**
	 * Check the presence of packageCode/PackageStyle Code in product
	 * @param ContainerTypeCode
	 * @return boolean 
	 */
	private boolean isPackageStyleActive(int packageStyleCode) {
		LOG.info("ContainerTypeDaoImpl-->isContainerTypeActive()-->Begin");
		Session session = null;
		boolean flag = false;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session
					.createSQLQuery("select count(*) from products p1"
							+ "  INNER JOIN Packages p2 ON p1.PackageCode=p2.PackageCode"
							+ "  INNER JOIN PackageStyle p3 ON p2.packagestyle=p3.styleCode"
							+ "  and p3.styleCode=:packageStyleCode")
					.setParameter("packageStyleCode", packageStyleCode).uniqueResult();
			if (count > 0) {
				flag = true;
			} else {
				flag = false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("ContainerTypeDaoImpl-->isContainerTypeActive()-->End");
		return flag;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deletePackageStyle(PackageStyleDto dto) throws ImproException {
		LOG.info("PackageStyleDaoImpl-->deletePackageStyleDto()-->Begin");
		Session session = null;
		Query query = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
			if (isPackageStyleActive(Integer.parseInt(dto.getStyleCode()))) {
				return "Package-Style combination are currently active on (" + count + ") Products ";
			} else {
				query = session.createQuery("Delete from PackageStyle WHERE styleCode=:styleCode");
				query.setParameter("styleCode", Integer.parseInt(dto.getStyleCode()));
				query.executeUpdate();
				
				query = session.createQuery("Delete from Packages WHERE packageStyle.styleCode=:styleCode");
				query.setParameter("styleCode", Integer.parseInt(dto.getStyleCode()));
				query.executeUpdate();
			}
			query.executeUpdate();
			msg = AppConstants.DATA_DELETE_SUCC_MSG;
		} catch (HibernateException e) {
			msg = AppConstants.FAILURE_MSG;
			LOG.error("PackageStyleDaoImpl-->deletePackageStyleDto()-->End");
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("PackageStyleDaoImpl-->deletePackageStyleDto()-->End");
		return msg;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PackageStyleDto> loadPackageStyle() throws ImproException {
		Session session = null;
		List<PackageStyleDto> packageStyleList = null;
		try {
			session = sessionFactory.openSession();
			PackageStyleDto dto = null;
			Query query=null;
				query = session.createQuery("select p.styleCode,p.styleDesc  from PackageStyle p order by p.styleDesc asc");
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				packageStyleList = new ArrayList<PackageStyleDto>();
				for (Object[] rs : list) {
					dto = setPackageStyleDto(rs, dto);
					packageStyleList.add(dto);

				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		return packageStyleList;

	}

	/**
	 * Set package Style
	 * @param resultSet
	 * @param dto
	 * @return PackageStyleDto
	 */
	private PackageStyleDto setPackageStyleDto(Object[] resultSet, PackageStyleDto dto) {
		dto = new PackageStyleDto();
		dto.setStyleCode(ApplicationUtil.checkNull(resultSet[0]));
		dto.setStyleDesc(ApplicationUtil.checkNull(resultSet[1]));
		return dto;
	}

}
