package com.ko.impro.dao.impl.packages;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.packages.PackageDao;
import com.ko.impro.dto.*;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.containertype.ContainerType;
import com.ko.impro.model.packages.Packages;
import com.ko.impro.model.packagesize.PackageSize;
import com.ko.impro.model.packagestyle.PackageStyle;
import com.ko.impro.model.shape.Shapes;
import com.ko.impro.util.*;

/**
 * @author hlekshmy : 
 * This class is an implementation of PackageDao Interface
 */
@Repository
public class PackageDaoImpl extends BaseDaoImpl implements PackageDao {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(PackageDaoImpl.class);

	/**
	 * This is to calculate number of count of rows in product table
	 */
	private long count;


	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<PackageDto> loadPackages() throws ImproException {
		LOG.info("PackageDaoImpl-->loadPackages()-->Begin");
		Session session=null;
		List<PackageDto> packageList=null;
		session = sessionFactory.openSession();		
		PackageDto dto=null;
		Query query=null;
		try {
				query = session.createQuery("SELECT p.packageCode,p.packageName FROM Packages p order by p.packageName asc");
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				packageList=new ArrayList<PackageDto>();
				for (Object[] rs : list) {
					dto = setPackageDto(rs, dto);
					packageList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("PackageDaoImpl-->loadPackages()-->End");
		return packageList;
	}

	/**
	 * Set the PackageDto
	 * @param rs
	 * @param dto
	 * @return PackageDto
	 */
	private PackageDto setPackageDto(Object[] resultSet, PackageDto dto) {
		dto=new PackageDto();
		dto.setPackageCode(resultSet[0].toString());
		dto.setPackageName(ApplicationUtil.checkNull(resultSet[1]));
		return dto;
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addPackage(PackageDto packageDto) throws ImproException {
		LOG.info("PackageDaoImpl-->addPackage()-->Begin");
		try {
			Packages packages = new Packages();
			
			PackageSize packageSize = new PackageSize();
			ContainerType type = new ContainerType();
			PackageStyle style = new PackageStyle();
			Shapes shapes = new Shapes();

			type.setContainerTypeCode(Short.parseShort(packageDto.getContainerTypeDto().getContainerTypeCode()));
			packageSize.setPackageSizeCode(Short.parseShort(packageDto.getPackageSizeDto().getPackageSizeCode()));
			shapes.setShapeId(Integer.parseInt(packageDto.getShapesDto().getShapeId()));
			style.setStyleCode(Integer.parseInt(packageDto.getStyleDto().getStyleCode()));
			
			packages.setPackageName(packageDto.getPackageName());
			packages.setUnitPerTray(Short.parseShort(packageDto.getUnitPerTray()));
			packages.setLiquidVolumePerPkg(BigDecimal.valueOf(Double.parseDouble(packageDto.getLiquidVolumePerPkg())));
			packages.setTrayWidth(BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayWidth())));
			packages.setTrayHeight(BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayHeight())));
			packages.setTrayDepth(BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayDepth())));
			packages.setTrayWide(BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayWide())));
			packages.setTrayHigh(BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayHigh())));
			packages.setTrayDeep(BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayDeep())));
			packages.setUnitHeight(BigDecimal.valueOf(Double.parseDouble(packageDto.getUnitHeight())));
			packages.setUnitDepth(BigDecimal.valueOf(Double.parseDouble(packageDto.getUnitDepth())));
			packages.setUnitWidth(BigDecimal.valueOf(Double.parseDouble(packageDto.getUnitWidth())));
			packages.setPackageSize(packageSize);
			packages.setContainerType(type);
			packages.setPackageStyle(style);
			packages.setShapes(shapes);

			entityMgr.persist(packages);
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PACKAGES,String.valueOf(packages.getPackageCode()),AppConstants.TRANSACTION_TYPE_ADD);
		} catch(PersistenceException ex) {
			throw new ImproException(ex.getMessage());
		} 
		LOG.info("PackageDaoImpl-->addPackage()-->End");
	}	
		
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deletePackage(PackageDto packageDto) throws ImproException {
		LOG.info("PackageDaoImpl-->deletePackage()-->Begin");
		Query query=null;
		Session session = null;
		String msg="";
		try {
			session = sessionFactory.openSession();
				if(checkPresenceInProduct(Integer.parseInt(packageDto.getPackageCode()))) {
					return "Packages are currently in use on (" + count + ") Products ";
				} else {
					query=session.createQuery("DELETE FROM Packages WHERE packageCode=:packageCode");
					query.setParameter("packageCode", Integer.parseInt(packageDto.getPackageCode()));
					query.executeUpdate();
					auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PACKAGES,String.valueOf(packageDto.getPackageCode()),AppConstants.TRANSACTION_TYPE_DELETE);
	    			msg= AppConstants.DATA_DELETE_SUCC_MSG;
				}
			
		} catch(HibernateException ex) {
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("PackageDaoImpl-->deletePackage()-->End");
		return msg;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editPackage(PackageDto packageDto) throws ImproException {
		LOG.info("PackageDaoImpl-->editPackage()-->Begin");
		Query query=null;
		Session session = null;
		String msg="";
		try {
			session = sessionFactory.openSession();

			query=session.createQuery("UPDATE Packages SET packageName=:PackageName, unitPerTray=:UnitsPerTray, liquidVolumePerPkg=:LiquidVolumePerPkg, "
					+ "unitHeight=:UnitHeight, unitDepth=:UnitDepth, unitWidth=:UnitWidth, "
					+ "trayWidth=:TrayWidth, trayHeight=:TrayHeight, trayDepth=:TrayDepth, "
					+ "trayWide=:TrayWide, trayHigh=:TrayHigh, trayDeep=:TrayDeep, "
					+ "packageSize.packageSizeCode=:PackageSizeCode, shapes.shapeId=:ShapeId, containerType.containerTypeCode=:ContainerType, packageStyle.styleCode=:PackageStyle "
					+ "WHERE packageCode=:PackageCode");
				query.setParameter("PackageCode", Integer.parseInt(packageDto.getPackageCode()));
				query.setParameter("PackageName", packageDto.getPackageName());
				query.setParameter("UnitsPerTray", Short.parseShort(packageDto.getUnitPerTray()));
				query.setParameter("LiquidVolumePerPkg", BigDecimal.valueOf(Double.parseDouble(packageDto.getLiquidVolumePerPkg())));
				query.setParameter("UnitHeight", BigDecimal.valueOf(Double.parseDouble(packageDto.getUnitHeight())));
				query.setParameter("UnitDepth", BigDecimal.valueOf(Double.parseDouble(packageDto.getUnitDepth())));
				query.setParameter("UnitWidth", BigDecimal.valueOf(Double.parseDouble(packageDto.getUnitWidth())));
				query.setParameter("TrayWidth", BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayWidth())));
				query.setParameter("TrayHeight", BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayHeight())));
				query.setParameter("TrayDepth", BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayDepth())));
				query.setParameter("TrayWide", BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayWide())));
				query.setParameter("TrayHigh", BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayHigh())));
				query.setParameter("TrayDeep", BigDecimal.valueOf(Double.parseDouble(packageDto.getTrayDeep())));
				query.setParameter("PackageSizeCode", Short.parseShort(packageDto.getPackageSizeDto().getPackageSizeCode()));
				query.setParameter("ShapeId", Integer.parseInt(packageDto.getShapesDto().getShapeId()));
				query.setParameter("ContainerType", Short.parseShort(packageDto.getContainerTypeDto().getContainerTypeCode()));
				query.setParameter("PackageStyle", Integer.parseInt(packageDto.getStyleDto().getStyleCode()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PACKAGES,String.valueOf(packageDto.getPackageCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
			
			msg= AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch(HibernateException ex) {
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("PackageDaoImpl-->editPackage()-->End");
		return msg;
	}

	/**
	 * To check whether given package present in products table or not
	 * @param int packageCode
	 * @return boolean flag
	 */
	private boolean checkPresenceInProduct(int packageCode) throws ImproException {
		LOG.info("PackageDaoImpl-->checkPresenceInProduct()-->BEGIN");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			count = (Long) session.createQuery("select count(packageCode) from Products where packageCode=:packageCode")
					.setParameter("packageCode",packageCode)
					.uniqueResult();
			if (count > 0) {
				LOG.info("PackageDaoImpl-->checkPresenceInProduct()-->End");
				return 	true;	
			} else {
				LOG.info("PackageDaoImpl-->checkPresenceInProduct()-->End");
				return false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public boolean checkDuplicatePackageName(String packageName) throws ImproException {
		LOG.info("PackageDaoImpl-->checkDuplicatePackageName()-->Begin");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery("select count(PackageName) from Packages where PackageName= :packageName");
			query.setParameter("packageName", packageName);
			if ((Long) query.uniqueResult() > 0) {
				LOG.info("PackageDaoImpl-->checkDuplicatePackageName()-->End");
				return 	true;	
			} else {
				LOG.info("PackageDaoImpl-->checkDuplicatePackageName()-->End");
				return false;
			}
		} catch(HibernateException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageDaoImpl-->checkDuplicatePackageName()-->Exception" +ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public PackageDto loadPackage(int packageId) throws ImproException {		
		LOG.info("PackageDaoImpl-->loadPackage(int id)-->Begin");
		Session session=null;
		PackageDto dto = new PackageDto();
		try {
			session=sessionFactory.openSession(); // change select query names after DB changes
			List<Object[]>list=session.createQuery("SELECT DISTINCT p.packageCode, p.packageName, p.unitPerTray, p.liquidVolumePerPkg,"
					+ " p.unitHeight, p.unitDepth, p.unitWidth,"
					+ " p.trayHeight, p.trayDepth, p.trayWidth,"
					+ " p.trayDeep, p.trayHigh, p.trayWide,"
					+ " ps.styleCode, ps.styleDesc,"
					+ " ct.containerTypeCode, ct.containerTypeDescription,"
					+ " si.shapeId, si.shapeCode,"
					+ " pks.packageSizeCode, pks.packageSizeDescription"
					+ " FROM Packages p, PackageSize pks, PackageStyle ps, ContainerType ct, Shapes si"
					+ " WHERE p.packageCode=:packageCode  AND ps.styleCode=p.packageStyle.styleCode AND "
					+ " ct.containerTypeCode=p.containerType.containerTypeCode"
					+ " AND si.shapeId=p.shapes.shapeId AND pks.packageSizeCode=p.packageSize.packageSizeCode")
			.setParameter("packageCode",packageId).list();
			if(null!=list && !list.isEmpty()) {
				for(Object[]rs:list) {
					dto=setPackageDtoObject(rs, dto);
				}
			}
		}
		catch(HibernateException e) {
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageDaoImpl-->loadPackage(int id)-->Exception "+e.getMessage());
			}
			throw new ImproException(e.getMessage());
		}
		LOG.info("PackageDaoImpl-->loadPackage(int id)-->End");
		return dto;
	}
	
	/**
	 * Set the PackageDto
	 * @param rs
	 * @param dto
	 * @return PackageDto
	 */
	private PackageDto setPackageDtoObject(Object[] resultSet, PackageDto dto) {
		dto=new PackageDto();
		dto.setPackageCode(ApplicationUtil.checkNull(resultSet[0]).toString());
		dto.setPackageName(ApplicationUtil.checkNull(resultSet[1]).toString());
		dto.setUnitPerTray(ApplicationUtil.checkNull(resultSet[2]).toString());
		dto.setLiquidVolumePerPkg(ApplicationUtil.checkNull(resultSet[3]).toString());
		dto.setUnitHeight(ApplicationUtil.checkNull(resultSet[4]).toString());
		dto.setUnitDepth(ApplicationUtil.checkNull(resultSet[5]).toString());
		dto.setUnitWidth(ApplicationUtil.checkNull(resultSet[6]).toString());
		dto.setTrayHeight(ApplicationUtil.checkNull(resultSet[7]).toString());
		dto.setTrayDepth(ApplicationUtil.checkNull(resultSet[8]).toString());
		dto.setTrayWidth(ApplicationUtil.checkNull(resultSet[9]).toString());
		dto.setTrayDeep(ApplicationUtil.checkNull(resultSet[10]).toString());
		dto.setTrayHigh(ApplicationUtil.checkNull(resultSet[11]).toString());
		dto.setTrayWide(ApplicationUtil.checkNull(resultSet[12]).toString());
		PackageStyleDto pstDto = new PackageStyleDto();
		pstDto.setStyleCode(ApplicationUtil.checkNull(resultSet[13]).toString());
		pstDto.setStyleDesc(ApplicationUtil.checkNull(resultSet[14]).toString());
		ContainerTypeDto ctDto = new ContainerTypeDto();
		ctDto.setContainerTypeCode(ApplicationUtil.checkNull(resultSet[15]).toString());
		ctDto.setContainerTypeDescription(ApplicationUtil.checkNull(resultSet[16]).toString());
		ShapeDto siDto = new ShapeDto();
		siDto.setShapeId(ApplicationUtil.checkNull(resultSet[17]).toString());
		siDto.setShapeCode(ApplicationUtil.checkNull(resultSet[18]).toString());
		PackageSizeDto psDto = new PackageSizeDto();
		psDto.setPackageSizeCode(ApplicationUtil.checkNull(resultSet[19]).toString());
		psDto.setPackageSizeDescription(ApplicationUtil.checkNull(resultSet[20]).toString());
		dto.setPackageSizeDto(psDto);
		dto.setContainerTypeDto(ctDto);
		dto.setShapesDto(siDto);
		dto.setStyleDto(pstDto);
		return dto;
	}
}
