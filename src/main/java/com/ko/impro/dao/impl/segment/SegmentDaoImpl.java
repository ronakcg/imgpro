package com.ko.impro.dao.impl.segment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.segment.SegmentDao;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.segment.Segment;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author hlekshmy : 
 * This class is an implementation of SegmentDao Interface
 */

@Repository
public class SegmentDaoImpl extends BaseDaoImpl implements SegmentDao {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SegmentDaoImpl.class);

	/**
	 * This is to calculate number of count of rows in product table
	 */
	private int count;
	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SegmentDto> loadSegment() throws ImproException {
		LOG.info("SegmentDaoImpl()-->loadSegment()-->Begin");
		Session session = null;
		List<SegmentDto> segmentList = null;
		try {
			session = sessionFactory.openSession();
			SegmentDto dto = null;
			Query query = null;
			query = session.createQuery("SELECT s.segmentCode,s.segmentDescription FROM Segment s ORDER BY s.segmentDescription asc");

			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				segmentList = new ArrayList<SegmentDto>();
				for (Object[] rs : list) {
					dto = setSegmentDto(rs);
					segmentList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SegmentDaoImpl()-->loadSegment()-->End");
		return segmentList;
	}

	@Override
	public List<SegmentDto> loadSegment(List<String> subCategoryList) throws ImproException {
		LOG.info("SegmentDaoImpl()-->loadSegment(List<String> subCategoryList)()-->Begin");
		Session session = null;
		List<SegmentDto> segmentList = null;
		List<Short> subCatShortList = subCategoryList.stream().map(Short::valueOf).collect(Collectors.toList());
		try {
			session = sessionFactory.openSession();
			SegmentDto dto = null;
			String sql = "SELECT DISTINCT seg.segmentCode,seg.segmentDescription FROM Segment seg join seg.subCategories subcat "
					+ " where subcat.subCategoryCode in(:subCategoryList) order by seg.segmentDescription asc";
			Query query = session.createQuery(sql);
			query.setParameterList("subCategoryList", subCatShortList);

			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				segmentList = new ArrayList<SegmentDto>();
				for (Object[] rs : list) {
					dto = setSegmentDto(rs);
					segmentList.add(dto);

				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SegmentDaoImpl()-->loadSegment(List<String> subCategoryList)()-->End");
		return segmentList;
	}

	/**
	 * Set the SegmentDto
	 * @param resultSet
	 * @param dto
	 * @return SegmentDto
	 */
	private SegmentDto setSegmentDto(Object[] resultSet) {
		SegmentDto dto = new SegmentDto();
		dto.setSegmentCode(resultSet[0].toString());
		dto.setSegmentDescription(ApplicationUtil.checkNull(resultSet[1]));
		return dto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SegmentDto loadSegment(int segmentId) throws ImproException {
		LOG.info("SegmentDaoImpl-->loadSegment()-->Begin");
		Session session = null;
		SegmentDto segmentDto = null;
		try {
			session = sessionFactory.openSession();
			Query query=session.createSQLQuery("select s.SegmentCode,s.SegmentDescription,sc.subCategoryCode,sc.SubCategoryDescription  "
					+ "from [CCE_IMGPro].[dbo].[SubCategory] sc " + 
					"INNER JOIN [CCE_IMGPro].[dbo].[ImgPro_Segment_SubCategory] ssc ON sc.SubCategoryCode=ssc.SubCategoryCode " + 
					"INNER JOIN [CCE_IMGPro].[dbo].[Segment] s on s.SegmentCode=ssc.SegmentCode " + 
					"where s.SegmentCode=:SegmentCode");
			query.setParameter("SegmentCode", segmentId);
			List<Object[]> list = (List<Object[]>) query.list();
			if (null != list && !list.isEmpty()) {
				for (Object[] rs : list) {
					segmentDto = new SegmentDto();
					segmentDto.setSegmentCode(ApplicationUtil.checkNull(rs[0]));
					segmentDto.setSegmentDescription(ApplicationUtil.checkNull(rs[1]));
					
					SubCategoryDto subCategoryDto= new SubCategoryDto();
					subCategoryDto.setSubCategoryCode(ApplicationUtil.checkNull(rs[2]));
					subCategoryDto.setSubCategoryDescription(ApplicationUtil.checkNull(rs[3]));
					segmentDto.setSubCategoryDto(subCategoryDto);
				}
			}
		} catch (HibernateException ex) { 
			if(LOG.isErrorEnabled()) {
				LOG.error("SegmentDaoImpl-->loadSegment()-->Exception " + ex.getMessage());	
			}
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}

		LOG.info("SegmentDaoImpl-->loadSegment()-->End");
		return segmentDto;
	}



	@Override
	@Transactional(rollbackFor = ImproException.class)
	public int addSegment(SegmentDto segmentDto) throws ImproException {
		int genSegmntCode = 0;
		LOG.info("SegmentDaoImpl()-->-addSegment()-->Begin");
		try {
			Segment segment = new Segment();
			segment.setSegmentDescription(segmentDto.getSegmentDescription());
			entityMgr.persist(segment);
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_SEGMENT,String.valueOf(segment.getSegmentCode()),AppConstants.TRANSACTION_TYPE_ADD);
			genSegmntCode = segment.getSegmentCode(); 
			LOG.info("SegmentDaoImpl()-->-addSegment()-->End");
			return genSegmntCode;
		} catch (PersistenceException ex) {
			throw new ImproException(ex.getMessage());
		}

	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deleteSegment(SegmentDto segmentDto) throws ImproException {
		LOG.info("SegmentDaoImpl()-->deleteSegment()-->Begin");
		Query query;
		Session session = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
			
				if (checkPresenceInProduct_SubCategory(Integer.parseInt(segmentDto.getSegmentCode()))) {
					return "Segment-SubCategory are currently in use on (" + count + ") Products ";
				} else if (checkPresenceInProduct_SubSegment(Integer.parseInt(segmentDto.getSegmentCode()))) {
					return "Segment-SubSegment are currently in use on (" + count + ") Products ";
				} else {
					query = session.createQuery("DELETE FROM [CCE_IMGPro].[dbo].[ImgPro_Segment_Subsegment] WHERE SegmentCode=:SegmentCode");
					query.setParameter("SegmentCode", Short.parseShort(segmentDto.getSegmentCode()));
					query.executeUpdate();
					
					query = session.createQuery("DELETE FROM [CCE_IMGPro].[dbo].[ImgPro_Segment_SubCategory] WHERE SegmentCode=:SegmentCode");
					query.setParameter("SegmentCode", Short.parseShort(segmentDto.getSegmentCode()));
					query.executeUpdate();
					
					query = session.createQuery("DELETE FROM Segment WHERE SegmentCode=:SegmentCode");
					query.setParameter("SegmentCode", Short.parseShort(segmentDto.getSegmentCode()));
					query.executeUpdate();
					auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_SEGMENT,String.valueOf(segmentDto.getSegmentCode()),AppConstants.TRANSACTION_TYPE_DELETE);
					msg = AppConstants.DATA_DELETE_SUCC_MSG;
				}
			
		} catch (HibernateException ex) {
			msg = AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SegmentDaoImpl()-->deleteSegment()-->End");
		return msg;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editSegment(SegmentDto segmentDto) throws ImproException {
		LOG.info("SegmentDaoImpl()-->editSegment()-->Begin");
		Query query = null;
		Session session = null;
		String msg = "";
		try {
			session = sessionFactory.openSession();
			
			if (segmentDto.getOldValue() != null) {	
				updateProduct(segmentDto);
				query = session.createQuery("UPDATE [CCE_IMGPro].[dbo].[ImgPro_Segment_SubCategory] SET subCategoryCode =: subCategoryCode WHERE SegmentCode =: SegmentCode ");
				query.setParameter("subCategoryCode", Short.parseShort(segmentDto.getSubCategoryDto().getSubCategoryCode()));
				query.setParameter("SegmentCode", Short.parseShort(segmentDto.getSegmentCode()));
				query.executeUpdate();
			}
				
			query = session.createQuery("Update Segment set segmentDescription=:SegmentDescription WHERE segmentCode=:SegmentCode");
			query.setParameter("SegmentDescription", segmentDto.getSegmentDescription());
			query.setParameter("SegmentCode", Short.parseShort(segmentDto.getSegmentCode()));
			query.executeUpdate();
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_SEGMENT,String.valueOf(segmentDto.getSegmentCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
			
			msg = AppConstants.DATA_UPDATE_SUCC_MSG;
		} catch (PersistenceException | HibernateException ex) {
			msg = AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());

		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SegmentDaoImpl()-->editSegment()-->End");
		return msg;
	}

	@Transactional(rollbackFor = ImproException.class)
	public void updateProduct(SegmentDto segmentDto) throws ImproException {
		LOG.info("SegmentDaoImpl()-->updateProduct()-->Begin");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery("Update Products p set p.SubCategoryCode=:SubCategoryCode where p.SegmentCode=:SegmentCode and "
							+ "p.SubCategoryCode=:oldCode");
			query.setParameter("SubCategoryCode",Integer.parseInt(segmentDto.getSubCategoryDto().getSubCategoryCode()));
			query.setParameter("SegmentCode", Integer.parseInt(segmentDto.getSegmentCode()));
			query.setParameter("oldCode", Integer.parseInt(segmentDto.getOldValue()));
			query.executeUpdate();
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PRODUCTS,String.valueOf(segmentDto.getSegmentCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
			LOG.info("SegmentDaoImpl()-->updateProduct()-->End");
		} catch (HibernateException e) {
			throw new ImproException(e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addSegmentSubCategoryMapping(SegmentDto segmentDto, int genSegCode) throws ImproException {
		LOG.info("SegmentDaoImpl()-->addSegmentSubCategoryMapping()-->Begin");
		try {
			entityMgr.createNativeQuery(
					"INSERT INTO [CCE_IMGPro].[dbo].[ImgPro_Segment_SubCategory] ([SegmentCode] ,[SubCategoryCode]) VALUES(?,?)")
					.setParameter(1, Short.parseShort(String.valueOf(genSegCode)))
					.setParameter(2, Short.parseShort(segmentDto.getSubCategoryDto().getSubCategoryCode()))
					.executeUpdate();
		} catch (PersistenceException ex) {
			throw new ImproException(ex.getMessage());
		}
		LOG.info("SegmentDaoImpl()-->addSegmentSubCategoryMapping()-->End");
	}

	@Override
	public boolean checkDuplicateSegment(String segmentDesc) throws ImproException {
		LOG.info("SubSegmentDaoImpl-->checkDuplicateSubSegment()-->Begin");
		Session session = null;
		boolean validate=false;
		try {
			session = sessionFactory.openSession();
			Query query =session.createQuery("select count(segmentDescription) from Segment where segmentDescription= :segmentDescription");
			query.setParameter("segmentDescription", segmentDesc);
			if ((Long) query.uniqueResult() > 0) {
				validate= true;
			} else {
				validate= false;
			}
		}  finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SubSegmentDaoImpl-->checkDuplicateSubSegment()-->End");
		return validate;

	}

		/**
	 * Validate the products table for the combination of segment
	 * and old SubCategory Code during editing
	 * @param subCategoryCode
	 * @param segmentCode
	 * @return int
	 * @throws ImproException
	 */
	private int validateInProduct(int subCategoryCode, int segmentCode) throws ImproException {
		Session session = null;
		StringBuilder searchQuery = new StringBuilder();
		try {
			session = sessionFactory.openSession();
			searchQuery.append("select count(ProductCode) from products where SegmentCode=" + segmentCode
					+ " and SubCategoryCode=" + subCategoryCode);
			final String sql = searchQuery.toString();
			Query query = session.createSQLQuery(sql);
			count = (Integer) query.uniqueResult();
		}  finally {
			HibernateUtil.closeSession(session);
		}
		return count;
	}

	@Override
	public boolean validateInProduct(SegmentDto segmentDto) throws ImproException {
		LOG.info("SegmentDaoImpl()-->validateInProduct()-->Begin");
		boolean flag = false;
		if (validateInProduct(Integer.parseInt(segmentDto.getOldValue()),
				Integer.parseInt(segmentDto.getSegmentCode())) > 0) {
			flag = true;
			segmentDto.setCounter(count);
		}
		LOG.info("SegmentDaoImpl()-->validateInProduct()-->End");
		return flag;
	}
	
	/**
	 * Validate in products table for Segment/SubSegement or segment/SubCategory
	 * @param segmentCode
	 * @return
	 * @return boolean
	 */
	private boolean checkPresenceInProduct_SubCategory(int segmentCode) {
		LOG.info("SegmentDaoImpl-->checkPresenceInProduct_SubCategory()-->Begin");
		Session session = null;
		boolean validate;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session
					.createSQLQuery("select count(*) from products p1"
							+ "  INNER JOIN ImgPro_Segment_SubCategory p2 ON p1.SegmentCode=p2.SegmentCode"
							+ "  and p1.subCategoryCode=p2.subCategoryCode"
							+ "  WHERE p2.SegmentCode=:SegmentCode")
					.setParameter("SegmentCode", segmentCode).uniqueResult();
			if (count > 0) {
				validate= true;
			} else {
				validate= false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SegmentDaoImpl-->checkPresenceInProduct_SubCategory()-->End");
		return validate;

	}

	/**
	 * Validate in products table for Segment/SubSegement or segment/SubCategory
	 * @param segmentCode
	 * @return boolean
	 */
	private boolean checkPresenceInProduct_SubSegment(int segmentCode) {
		LOG.info("SegmentDaoImpl-->checkPresenceInProduct_SubSegment()-->Begin");
		Session session = null;
		boolean validate=false;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session
					.createSQLQuery("select count(*) from products p1"
							+ "  INNER JOIN ImgPro_Segment_Subsegment p2 ON p1.SegmentCode=p2.SegmentCode"
							+ "  and p1.subSegmentCode=p2.subSegmentCode"
							+ "  WHERE p2.SegmentCode=:SegmentCode")
					.setParameter("SegmentCode", segmentCode).uniqueResult();
			if (count > 0) {
				validate= true;
			} else {
				validate= false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("SegmentDaoImpl-->checkPresenceInProduct_SubSegment()-->End");
		return validate;

	}

}
