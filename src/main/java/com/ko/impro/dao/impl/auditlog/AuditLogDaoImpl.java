package com.ko.impro.dao.impl.auditlog;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dto.AuditLogDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.auditlog.AuditLogs;
import com.ko.impro.util.ApplicationUtil;

/**
 * Description : This class is an implementation of AuditLogDao Interface
 *
 */
@Repository
public class AuditLogDaoImpl extends BaseDaoImpl implements AuditLogDao {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AuditLogDaoImpl.class);

	
	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;

	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<AuditLogDto> loadAuditLog() throws ImproException {
		LOG.info("AuditLogDaoImpl-->loadAuditLog()-->BEGIN");
		Session session = null;
		List<AuditLogDto> auditLogDtoList = null;
		try {
			session = sessionFactory.openSession();
			AuditLogDto dto = null;
			Query query = session.createQuery("select a.transactionDate, a.transactionUser, a.tableUpdated,a.recordUpdated,"
					+ " a.transactionType from AuditLogs a order by a.transactionDate");

			List<Object[]> list = (List<Object[]>) query.list();
			if (!list.isEmpty()) {
				auditLogDtoList = new ArrayList<AuditLogDto>();
				for (Object[] resultSet : list) {
					dto = setAuditLogDto(resultSet,dto);
					auditLogDtoList.add(dto);

				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("AuditLogDaoImpl-->loadAuditLog()-->END");
		return auditLogDtoList;

	}

	/**
	 * Set the AuditLogDto
	 * @param rs
	 * @param dto
	 * @return ShapeDto
	 */
	private AuditLogDto setAuditLogDto(Object[] resultSet, AuditLogDto dto) {
		dto = new AuditLogDto();
		dto.setTransactionDate(ApplicationUtil.getformattedDate(ApplicationUtil.checkNull(resultSet[0])));
		dto.setTransactionUser(ApplicationUtil.checkNull(resultSet[1]));
		dto.setTableUpdated(ApplicationUtil.checkNull(resultSet[2]));
		dto.setRecordUpdated(ApplicationUtil.checkNull(resultSet[3]));
		dto.setTransactionType(ApplicationUtil.checkNull(resultSet[4]));
		return dto;
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addLog(String transactionUser, String tableUpdated, String recordUpdated, String transactionType)
			throws ImproException {
		LOG.info("AuditLogDaoImpl-->addLog()-->BEGIN");
		try {
			AuditLogs auditLogs = new AuditLogs();
			auditLogs.setTransactionDate(ApplicationUtil.getDateTime());
			auditLogs.setTransactionUser(transactionUser);
			auditLogs.setTableUpdated(tableUpdated);
			auditLogs.setRecordUpdated(recordUpdated);
			auditLogs.setTransactionType(transactionType);
			entityMgr.persist(auditLogs);
		} catch (PersistenceException ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error(" AuditLogDaoImpl-->addLog--> Exception occured " + ex.getMessage());	
			}
			throw new ImproException(ex.getMessage());
		}
		LOG.info("AuditLogDaoImpl-->addLog()-->End");
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void saveLog(String transactionUser, String tableUpdated, String recordUpdated, String transactionType)
			throws ImproException {
		LOG.info("AuditLogDaoImpl-->saveLog()-->BEGIN");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			String logQuery="INSERT INTO [dbo].[Audit_Log] ([TransactionDate],[TransactionUser],[TableUpdated],[RecordUpdated],[TransactionType]) VALUES  " + 
					"   (GETDATE(),:TransactionUser,:TableData,:RecordedUpdated,:TransactionType) ";
			
			LOG.info(logQuery);
    		SQLQuery query = session.createSQLQuery(logQuery);
			query.setParameter("TransactionUser",transactionUser);
			query.setParameter("TableData",tableUpdated);
			query.setParameter("RecordedUpdated",recordUpdated);
			query.setParameter("TransactionType",transactionType);
			query.executeUpdate();
			
		} catch (HibernateException ex) {
			LOG.error(" AuditLogDaoImpl-->saveLog--> Exception occured " + ex.getMessage());
			throw new ImproException(ex.getMessage());
		}
		LOG.info("AuditLogDaoImpl-->saveLog()-->End");
	}

}
