package com.ko.impro.dao.impl.flavour;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.config.HibernateUtil;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dao.base.BaseDaoImpl;
import com.ko.impro.dao.flavour.FlavorDao;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.flavour.Flavors;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author hlekshmy : 
 * This class is an implementation of FlavorDao Interface
 */
@Repository
public class FlavorDaoImpl  extends BaseDaoImpl implements FlavorDao {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(FlavorDaoImpl.class);
	
	/**
	 * This is to calculate number of count of rows in product table
	 */
	private long count=0;

	/**
	 * Reference of EntityManager
	 */
	@PersistenceContext
	private EntityManager entityMgr;
	

	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;
	
	/**
	 * Reference of SessionFactory
	 */
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<FlavorDto> loadFlavor() throws ImproException {
		LOG.info("FlavorDaoImpl-->loadFlavor()-->Begin");

		Session session=null;
		List<FlavorDto> flavorList=null;
		try {
			session = sessionFactory.openSession();		
			FlavorDto dto=null;
			Query query =null;
			
				query = session.createQuery("select f.flavorCode,f.flavorDescription from Flavors f order by f.flavorDescription asc");
	
			List<Object[]> list = (List<Object[]>) query.list();
			if (list.size() > 0) {
				flavorList=new ArrayList<FlavorDto>();
				for (Object[] rs : list) {
					dto = setFlavorDto(rs, dto);
					flavorList.add(dto);
	
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("FlavorDaoImpl-->loadFlavor()-->End");

		return flavorList;
	}


	/**
	 * Set the Flavor DTO
	 * @param rs
	 * @param dto
	 * @return FlavorDto
	 */
	private FlavorDto setFlavorDto(Object[] resultSet, FlavorDto dto) {
		dto=new FlavorDto();
		dto.setFlavorCode(resultSet[0].toString());
		dto.setFlavorDescription(ApplicationUtil.checkNull(resultSet[1]));
		return dto;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<FlavorDto> loadFlavor(List<String> subCategoryList) throws ImproException {
		LOG.info("FlavorDaoImpl-->loadFlavor(List<String> subCategoryList)-->Begin");

		Session session=null;
		List<FlavorDto> flavorList=null;
		List<Short> flavShortList = subCategoryList.stream()
	            .map(Short::valueOf).collect(Collectors.toList());
		try {
			session = sessionFactory.openSession();		
			FlavorDto dto=null;
			String sql="select distinct flav.flavorCode, flav.flavorDescription FROM Flavors flav join flav.subCategories subcat " +
			          " where subcat.subCategoryCode in(:subCategoryList)"
			          + " order by flav.flavorDescription asc";
			Query query = session.createQuery(sql);
			query.setParameterList("subCategoryList", flavShortList);
			List<Object[]> list = (List<Object[]>) query.list();
			if (list.size() > 0) {
				flavorList=new ArrayList<FlavorDto>();
				for (Object[] rs : list) {
					dto = setFlavorDto(rs, dto);
					flavorList.add(dto);
				}
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("FlavorDaoImpl-->loadFlavor(List<String> subCategoryList)-->End");
		return flavorList; 
	}

	@Override
	public FlavorDto loadFlavor(int flavorId) throws ImproException {
		LOG.info("FlavorDaoImpl-->loadFlavor(int id)-->Begin");
		int count=0;
		Session session = null;
		List<SubCategoryDto> subCatList = null;
		FlavorDto flavorDto = new FlavorDto();
		
		try {
			session = sessionFactory.openSession();
			@SuppressWarnings("unchecked")
			List<Object[]> list = session.createSQLQuery("SELECT f.FlavorCode,f.FlavorDescription,"
					+ " s.SubCategoryCode,s.SubCategoryDescription"
					+ " FROM [CCE_IMGPro].[dbo].[SubCategory] s"
					+ " INNER JOIN [CCE_IMGPro].[dbo].[ImgPro_Flavor_SubCategory] ifs ON s.SubCategoryCode=ifs.SubCategoryCode"
					+ " INNER JOIN [CCE_IMGPro].[dbo].[Flavors] f ON f.FlavorCode=ifs.FlavorCode"
					+ " WHERE f.FlavorCode=:FlavorCode")
				.setParameter("FlavorCode", flavorId).list();
			
			if (null!=list && !list.isEmpty()) {
				subCatList= new ArrayList<SubCategoryDto>();
				for (Object[] rs : list) {
					if(count==0) {
						flavorDto.setFlavorCode(rs[0].toString());
						flavorDto.setFlavorDescription(ApplicationUtil.checkNull(rs[1]));
						SubCategoryDto subCatDto= new SubCategoryDto();
						subCatDto.setSubCategoryCode(rs[2].toString());
						subCatDto.setSubCategoryDescription(ApplicationUtil.checkNull(rs[3]));
						subCatList.add(subCatDto);
						count++;
					} else {
						SubCategoryDto subCatDto= new SubCategoryDto();
						subCatDto.setSubCategoryCode(rs[2].toString());
						subCatDto.setSubCategoryDescription(ApplicationUtil.checkNull(rs[3]));
						subCatList.add(subCatDto);
					}
					flavorDto.setSubCategoryList(subCatList);
				}
			}
		} catch(Exception ex) {
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		LOG.info("FlavorDaoImpl-->loadFlavor(int id)-->End");
		return flavorDto; 
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public int addFlavor(FlavorDto flavorDto) throws ImproException {
		LOG.info("FlavorsDaoImpl-->addFlavor()-->Begin");	
		try {				
			Flavors flavor=new Flavors();			
			flavor.setFlavorDescription(flavorDto.getFlavorDescription());
			entityMgr.persist(flavor);
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_FLAVOURS,String.valueOf(flavor.getFlavorCode()),AppConstants.TRANSACTION_TYPE_ADD);
			LOG.info("FlavorsDaoImpl-->addFlavor()-->End");	
			return flavor.getFlavorCode();
		} catch(Exception ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error(" FlavorsDaoImpl-->addFlavor-> Exception occured "+ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		}
	}
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addFlavorSubCategory(FlavorDto flavorDto) throws ImproException {
		LOG.info("FlavorDaoImpl-->addFlavorSubCategory()-->BEGIN");
		try {
			for(SubCategoryDto dto:flavorDto.getSubCategoryList()) {
				
				String sqlQuery="INSERT INTO [CCE_IMGPro].[dbo].[ImgPro_Flavor_SubCategory] ([FlavorCode] ,[SubCategoryCode]) VALUES(?,?)";
				javax.persistence.Query query =entityMgr.createNativeQuery(sqlQuery);
				
				query.setParameter(1,flavorDto.getFlavorCode());
				query.setParameter(2,dto.getSubCategoryCode());
				query.executeUpdate();
			}
			
		} catch(Exception ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("FlavorDaoImpl-->addFlavorSubCategory()-->Exception" +ex.getMessage());
			}
			throw new ImproException(ex.getMessage());
		}	
		
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String editFlavor(FlavorDto dto) throws ImproException {
		LOG.info("FlavorDaoImpl-->editFlavor()-->Begin");
		String msg="";
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			// if No activity happened on inactive flag
				query=session.createQuery("UPDATE Flavors SET FlavorDescription=:FlavorDescription WHERE FlavorCode=:FlavorCode");
				query.setParameter("FlavorDescription",dto.getFlavorDescription());
				query.setParameter("FlavorCode", Short.parseShort(dto.getFlavorCode()));
				query.executeUpdate();
				auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_FLAVOURS,String.valueOf(dto.getFlavorCode()),AppConstants.TRANSACTION_TYPE_CHANGE);
			
			
			if(dto.getAddSubCatList() != null) {		// To add New SubCategory to Flavor. It will reflect on Join table
				for(SubCategoryDto subCategoryDto : dto.getAddSubCatList()) {
					entityMgr.createNativeQuery("INSERT INTO  [CCE_IMGPro].[dbo].[ImgPro_Flavor_SubCategory] "
							+ "([FlavorCode] ,[SubCategoryCode]) VALUES (?,?)")
					.setParameter(1, Short.parseShort(dto.getFlavorCode()))
					.setParameter(2, Short.parseShort(subCategoryDto.getSubCategoryCode()))
					.executeUpdate();
				}
			}
			
			if(dto.getRemoveSubCatList() != null  && dto.getRemoveSubCatList().size() > 0) {
				List <String> removeSubCategoryList=new ArrayList<String>();
				for(SubCategoryDto subCategoryDto : dto.getRemoveSubCatList()) {	
					removeSubCategoryList.add(subCategoryDto.getSubCategoryCode());
				}
				List<Short> subCatShortList = removeSubCategoryList.stream().map(Short::valueOf).collect(Collectors.toList());
				if(checkPresenceInProduct(removeSubCategoryList, Integer.parseInt(dto.getFlavorCode()))) {
					return "Flavor are currently in use on ("+count+") Products ";
				} else {
					query=session.createSQLQuery("DELETE FROM [CCE_IMGPro].[dbo].[ImgPro_Flavor_SubCategory]"
							+ " WHERE SubCategoryCode in(:removeSubCategoryList)  AND FlavorCode=:FlavorCode");
					query.setParameterList("removeSubCategoryList", subCatShortList);
					query.setParameter("FlavorCode", dto.getFlavorCode());
					query.executeUpdate();
				}
			}
		msg= AppConstants.DATA_UPDATE_SUCC_MSG;
			
		} catch(Exception ex) {
			msg=AppConstants.FAILURE_MSG;
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
        LOG.info("FlavorDaoImpl-->editFlavor()-->End");
        return msg;	
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public String deleteFlavor(FlavorDto flavorDto) throws ImproException {
		LOG.info("FlavorDaoImpl-->deleteFlavor()-->Begin");
		Query query;
		Session session = null;
		String msg="";
		
		try {
			session = sessionFactory.openSession();
			
				if(checkPresenceInProductForFlavor(Integer.parseInt(flavorDto.getFlavorCode()))) {
					return "Flavor are currently in use on ("+count+") Products ";
				} else {
					query=session.createSQLQuery("Delete from [CCE_IMGPro].[dbo].[ImgPro_Flavor_SubCategory] WHERE FlavorCode=:FlavorCode");
					query.setParameter("FlavorCode", flavorDto.getFlavorCode());
					query.executeUpdate();
				
					query=session.createQuery("Delete from Flavors WHERE FlavorCode=:FlavorCode");
					query.setParameter("FlavorCode", Short.parseShort(flavorDto.getFlavorCode()));
					query.executeUpdate();
					auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_FLAVOURS,String.valueOf(flavorDto.getFlavorCode()),AppConstants.TRANSACTION_TYPE_DELETE);
			
					msg= AppConstants.DATA_DELETE_SUCC_MSG;
				}
			
		} catch(Exception ex) {
			msg=AppConstants.FAILURE_MSG;
			ex.printStackTrace();
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
			
		LOG.info("FlavorDaoImpl-->deleteFlavor()-->End");
        return msg;
	}
	
	@Override
	public boolean checkDuplicateFlavor(String flavorDescription) throws ImproException {
		LOG.info("FlavorDaoImpl-->checkDuplicateFlavor()-->BEGIN");
		Session session = null;
		Query query=null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery("select count(flavorDescription) from Flavors where flavorDescription= :flavorDescription");
			query.setParameter("flavorDescription",flavorDescription);
			
			if((Integer) query.uniqueResult() > 0) {
				LOG.info("FlavorDaoImpl-->checkDuplicateFlavor()-->End");
				return 	true;	
			} else {
				LOG.info("FlavorDaoImpl-->checkDuplicateFlavor()-->End");
				return false;
			}
		} catch(Exception ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("FlavorDaoImpl-->checkDuplicateFlavor()-->Exception" +ex.getMessage());
			}	
			throw new ImproException(ex.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
	}

	@Override
	public boolean checkPresenceInProductForFlavor(int flavorCode) throws ImproException {
		LOG.info("FlavorDaoImpl-->checkPresenceInProductForFlavor()-->BEGIN");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			count = (Integer) session.createSQLQuery("SELECT COUNT(*) FROM products p1"
							+ "  INNER JOIN ImgPro_Flavor_SubCategory p2 ON p1.FlavorCode=p2.FlavorCode"
							+ "  AND p1.subCategoryCode=p2.subCategoryCode AND p2.flavorCode=:flavorCode ")
							.setParameter("flavorCode", flavorCode).uniqueResult();
			if (count > 0) {
				LOG.info("FlavorDaoImpl-->checkPresenceInProductForFlavor()-->End");
				return true;
			} else {
				LOG.info("FlavorDaoImpl-->checkPresenceInProductForFlavor()-->End");
				return false;
			}
		} finally {
			HibernateUtil.closeSession(session);
		}
	}

	@Override
	public boolean checkPresenceInProduct(List<String> subCatCodeRmvList, int flavorCode) throws ImproException {
		LOG.info("FlavorDaoImpl-->checkPresenceInProduct()-->BEGIN");
		Session session = null;
		List<Short> subCatShortList = subCatCodeRmvList.stream().map(Short::valueOf).collect(Collectors.toList());
		try {
			session = sessionFactory.openSession();
			count = (Integer) session.createSQLQuery("SELECT COUNT(*) FROM Products "
					+ " WHERE subCategoryCode in(:subCatShortList) AND flavorCode=:flavorCode")
					.setParameterList("subCatShortList",subCatShortList)
					.setParameter("flavorCode",flavorCode)
					.uniqueResult();
			if(count > 0) {
				LOG.info("FlavorDaoImpl-->checkPresenceInProduct()-->End");
				return 	true;	
			} else {
				LOG.info("FlavorDaoImpl-->checkPresenceInProduct()-->End");
				return false;
			}
		}finally {
		HibernateUtil.closeSession(session);
		}
	}
}
