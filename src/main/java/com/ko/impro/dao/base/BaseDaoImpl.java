package com.ko.impro.dao.base;

/**
 *
 * Description : BaseDAO class for handling CRUD operation for all Dao Classes
 * 
 */
public class BaseDaoImpl {

	/**
	 * Check if string belongs to enum
	 * 
	 * @param value
	 * @param enumClass
	 * @return
	 */
	public static <E extends Enum<E>> boolean isInEnum(String value, Class<E> enumClass) {
		for (E e : enumClass.getEnumConstants()) {
			if (e.name().equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if string belongs to enumColName
	 * 
	 * @param value
	 * @param enumClass
	 * @return
	 */
	public static <E extends Enum<E>> boolean isInEnumColName(String value, Class<E> enumClass) {
		for (E e : enumClass.getEnumConstants()) {
			if (e.name().equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

}
