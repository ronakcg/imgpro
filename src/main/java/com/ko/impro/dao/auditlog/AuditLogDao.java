package com.ko.impro.dao.auditlog;

import java.util.List;

import com.ko.impro.dto.AuditLogDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Dao class for handling CRUD operation for Auditing
 * 
 */
public interface AuditLogDao {

	/**
	 * Exports the audit log
	 * @return List<AuditLogDto>
	 * @throws ImproException
	 */
	List<AuditLogDto> loadAuditLog() throws ImproException;

	/**
	 * Add the change log information to audit table
	 * @param transactionUser
	 * @param tableUpdated
	 * @param recordUpdated
	 * @param transactionType
	 * @throws ImproException
	 */
	void addLog(String transactionUser, String tableUpdated, String recordUpdated, String transactionType)
			throws ImproException;

	/**
	 * Add the change log information to audit table using the nativeSQL
	 * @param transactionUser
	 * @param tableUpdated
	 * @param recordUpdated
	 * @param transactionType
	 * @throws ImproException
	 */
	void saveLog(String transactionUser, String tableUpdated, String recordUpdated, String transactionType) throws ImproException;

}
