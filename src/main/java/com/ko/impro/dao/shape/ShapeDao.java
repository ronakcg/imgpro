package com.ko.impro.dao.shape;

import java.util.List;

import com.ko.impro.dto.ShapeDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * ShapeDao to handle the  CRUD operations for Shape
 */
public interface ShapeDao {

	/**
	 * Load the Shapes
	 * @return List<ShapeDto>
	 * @throws ImproException
	 */
	List<ShapeDto> loadShapes() throws ImproException;

	/**
	 * Load the Shape with id
	 * @param shapeId
	 * @return shapeDto
	 * @throws ImproException
	 */
	ShapeDto loadshapeID(int shapeId) throws ImproException;

	/**
	 * Checking duplicate Shape name present or not
	 * @param name
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicateShape(String name) throws ImproException;


	/** Delete the shape
	 * @param dto
	 * @return 
	 * @throws ImproException
	 */
	String deleteShapes(ShapeDto dto) throws ImproException;

	/**
	 * Add the Shape
	 * @param shapeDto
	 * @throws ImproException
	 */
	void addShapes(ShapeDto shapeDto) throws ImproException;

	/**
	 * Edit the Shape
	 * @param shapedto
	 * @return String
	 * @throws ImproException
	 */
	String editShapes(ShapeDto dto) throws ImproException;

		
}
