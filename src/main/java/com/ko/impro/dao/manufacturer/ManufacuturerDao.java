package com.ko.impro.dao.manufacturer;

import java.util.List;

import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * ManufacuturerDao to handle the  CRUD operations for Manufacturer
 * 
*/
public interface ManufacuturerDao {

	/**
	 * Load the Manufacturer
	 * @return List<ManufacturerDto>
	 * @throws ImproException
	 */
	List<ManufacturerDto> loadManufacuturer() throws ImproException;
	
	/**
	 * Add the Manufacturer
	 * @param manufacturerDto
	 * @throws ImproException
	 */
	void addManufacuturer(ManufacturerDto manufacturerDto) throws ImproException;
	
	/**
	 * Delete the Manufacturer
	 * @param id
	 * @throws ImproException
	 */
	String deleteManufacuturer(ManufacturerDto dto) throws ImproException;
	
	/**
	 * Edit the Manufacturer
	 * @param dto
	 * @return String
	 * @throws ImproException
	 */
	String editManufacuturer(ManufacturerDto dto) throws ImproException;
	
	/**
	 * Load the Manufacturer with id
	 * @param manufactureId
	 * @return ManufacturerDto
	 * @throws ImproException
	 */
	ManufacturerDto loadManufacuturer(int manufactureId) throws ImproException;
	
	/**
	 * Checking duplicate Manufacturer name present or not
	 * @param name
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicateManufacuturer(String name) throws ImproException;
	
	/**
	 * Checking presence in Product of Manufacturer
	 * @param manufacturerCode
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkPresenceInProduct(int manufacturerCode) throws ImproException ;
	
	
	

}
