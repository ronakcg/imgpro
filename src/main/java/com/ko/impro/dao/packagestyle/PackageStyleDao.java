package com.ko.impro.dao.packagestyle;

import java.util.List;

import com.ko.impro.dto.PackageStyleDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * PackageStyleDao to handle the  CRUD operations for PackageStyle
 */
public interface PackageStyleDao {

	/**
	 * Loads the package Styles
	 * @param isMaintainence
	 * @return List<PackageStyleDto>
	 * @throws ImproException
	 */
	List<PackageStyleDto> loadPackageStyle() throws ImproException;

	/**
	 * Edit the PackageStyle
	 * @param dto
	 * @return String
	 * @throws ImproException
	 */
	String editPackageStyle(PackageStyleDto packageStyleDto) throws ImproException;

	/**
	 * Add the PackageStyle
	 * @param PackageStyleDto
	 * @throws ImproException
	 */
	void addPackageStyle(PackageStyleDto packageStyleDto) throws ImproException;

	/**
	 * Checking duplicate PackageStyle name present or not
	 * @param name
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicatePackageStyle(String name) throws ImproException;

	/**
	 * Delete the PackageStyle
	 * @param id
	 * @throws ImproException
	 */
	String deletePackageStyle(PackageStyleDto dto) throws ImproException;

	/**
	 * Load the PackageStyle with id
	 * @param packageStyleId
	 * @return PackageStyleDto
	 * @throws ImproException
	 */
	PackageStyleDto loadPackageStyle(int packageStyleId) throws ImproException;

	/**
	 * @param packageStyle
	 * @return
	 * @throws ImproException
	 */
	boolean checkPresenceInPackages(int packageStyle) throws ImproException;


}