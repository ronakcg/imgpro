package com.ko.impro.dao.containerType;

import java.util.List;

import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.exception.ImproException;

/**
 *  @author hlekshmy
 * ConatainerTypeDao to handle the  CRUD operations for ContainerType
 */
public interface ContainerTypeDao {

	/**
	 * Load the ContainerType details
	 * @param boolean isMaintainence
	 * @return List<ContainerTypeDto>
	 * @throws ImproException
	 */
	List<ContainerTypeDto> loadContainerTypes() throws ImproException;
	
	/**
	 * @param containerTypeDto
	 * @throws ImproException
	 */
	void addContainerType(ContainerTypeDto containerTypeDto) throws ImproException;

	/**
	 * Method to delete the ContainerType
	 * @param ContainerTypeDto containerTypeDto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteContainerType(ContainerTypeDto containerTypeDto) throws ImproException;

	/**
	 * Method to edit ContainerType
	 * @param ContainerTypeDto 
	 * @return String message
	 * @throws ImproException
	 */
	String editContainerType(ContainerTypeDto dto) throws ImproException;
	
	/**
	 * Method to load ContainerType based on containerTypeCode
	 * @param containerTypId
	 * @return ContainerTypeDto
	 * @throws ImproException
	 */
	ContainerTypeDto loadContainerType(int containerTypId) throws ImproException;
	
	/**
	 * Method to check duplicate ContainerType Description
	 * @param name
	 * @return boolean 
	 * @throws ImproException
	 */
	boolean checkDuplicateContainerType(String name) throws ImproException;
	
	
}
