package com.ko.impro.dao.product;

import java.util.List;

import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Interface for ArchiveProductDao API
 */
public interface ArchiveProductDao {

	/** 
	 * T Method to archive the products to the product table
	 @param List<ProductDto> dtoList
	 * @param String deleteReason
	 * @param String userId
	 * @throws ImproException
	 */
	void archiveProducts(List<ProductDto> prodDtoList, String deleteReason, String userId) throws ImproException;
			
	
	/**
	 * This method will return additional details of a particular archived product
	 * @param ArchivedProductDto deletedProductDto
	 * @return ArchivedProductDto
	 * @throws ImproException
	 */
	ArchivedProductDto getArchivedProductDetails(String id) throws ImproException;
	
	
	/**
	 * Method used to purge all the selected archived products
	 * @param List<ArchivedProductDto> archiProdDtoList
	 * @throws ImproException
	 */
	 void purgeArchivedProducts(List<ArchivedProductDto> archiProdDtoList) throws ImproException;

	
	
}





