package com.ko.impro.dao.product;

import java.util.List;

import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.product.Products;

/**
 * @author hlekshmy
 * Interface for Product Dao API
 *
 */
public interface ProductDao {

	/**
	 * The method to restore the archived products from the product table
	 * @param ArchivedProductDto dto
	 * @param boolean
	 * @throws ImproException
	 */
	boolean restoreProduct(ArchivedProductDto prodDto) throws ImproException;
	
	/**
	 * This method will search products based on the filter input 
	 * @param ProductSearchDto productSearchDto
	 * @return ProductResponseDto
	 * @throws ImproException
	 */
	List<ProductDto> searchProducts(ProductSearchDto productSearchDto,boolean isPagination) throws ImproException;
	

	/**
	 * This method will search Archived products based on the filter input 
	 * @param productSearchDto
	 * @param isPagination
	 * @return ArchivedProductDto
	 * @throws ImproException
	 */
	List<ArchivedProductDto> searchArchivedProducts(ProductSearchDto productSearchDto,boolean isPagination) throws ImproException;
	
	/**
	 * To fetch total record count
	 * @param ProductSearchDto productSearchDto
	 * @return Integer
	 * @throws ImproException
	 */
	Integer getTotalCountForProducts(ProductSearchDto productSearchDto) throws ImproException ;
		
	/**
	 * This method will fetch additional product information and 
	 * store in the same object
	 * @param ProductDto productDto
	 * @return ProductDto
	 * @throws ImproException
	 */
	ProductDto getProductDetails(String id) throws ImproException;
	
	/**
	 * Method to edit product details
	 * @param ProductDto productDto
	 * @throws ImproException
	 */
	void editProduct(ProductDto productDto) throws ImproException;
	
	/**
	 * Method to add/clone a product
	 * @param product
	 * @throws ImproException
	 */
	void addProduct(Products product) throws ImproException;
	
	/**
	 * Method to check duplicate product
	 * @param bitmapName
	 * @throws ImproException
	 */
	boolean checkDuplicateProduct(String upc) throws ImproException;
	
	
	/**
	 * Method to edit multiple products based on common parameters
	 * @param productdto
	 * @param productDtoList
	 * @throws ImproException
	 */
	void multiEditProduct(ProductDto productdto,List<ProductDto> productDtoList) throws ImproException;
	

	

}





