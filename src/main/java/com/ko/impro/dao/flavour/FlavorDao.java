package com.ko.impro.dao.flavour;

import java.util.List;

import com.ko.impro.dto.FlavorDto;
import com.ko.impro.exception.ImproException;

/**  
 * @author hlekshmy
 * 
 *  FlavorDao to handle the  CRUD operations for Flavor
 */

public interface FlavorDao {

	/**
	 * Load the Flavors
	 * @param boolean isMaintainence
	 * @return List<FlavorDto>
	 * @throws ImproException
	 */
	List<FlavorDto> loadFlavor() throws ImproException;

	/**
	 * Load the Flavors based on SubCategoryList
	 * @param SubCategoryList
	 * @return List<FlavorDto>
	 * @throws ImproException
	 */
	List<FlavorDto> loadFlavor(List<String> SubCategoryList) throws ImproException;

	/**
	 * Method to add the flavor
	 * @param FlavorDto
	 * @return int
	 * @throws ImproException
	 */
	int addFlavor(FlavorDto flavorDto) throws ImproException;
	
	/**
	 * Method to add the flavor,subCategory entries in FlavorSubCategory Join table
	 * @param FlavorDto
	 * @throws ImproException
	 */
	void addFlavorSubCategory(FlavorDto flavorDto) throws ImproException;

	/**
	 * Method to delete the Flavor
	 * @param FlavorDto FlavorDto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteFlavor(FlavorDto flavorDto) throws ImproException;

	/**
	 * Method to edit Flavor
	 * @param FlavorDto dto
	 * @return String message
	 * @throws ImproException
	 */
	String editFlavor(FlavorDto dto) throws ImproException;
	
	/**
	 * Method to load Flavor based on flavorCode
	 * @param id
	 * @return FlavorDto
	 * @throws ImproException
	 */
	FlavorDto loadFlavor(int flavorid) throws ImproException;
	
	/**
	 * Method to check duplicate Flavor Description
	 * @param name
	 * @return boolean 
	 * @throws ImproException
	 */
	boolean checkDuplicateFlavor(String name) throws ImproException;
	
	/**
	 * To check Combination of SubcateogryCode and FlavorCode in products table 
	 * @param flavorCode
	 * @return boolean value
	 * @throws ImproException
	 */
	boolean checkPresenceInProductForFlavor(int flavorCode) throws ImproException;
	
	/**
	 * To check Combination of removeSubCateogryList 
	 * and specific FlavorCode in products table 
	 * @param subCatCodeRmvList,subCategoryCode
	 * @return boolean value
	 * @throws ImproException
	 */
	boolean checkPresenceInProduct(List<String> subCatCodeRmvList,int flavorCode) throws ImproException;

}
