package com.ko.impro.dao.segment;

import java.util.List;

import com.ko.impro.dto.SegmentDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * SegmentDao to handle the  CRUD operations for Segment
 */
public interface SegmentDao {

	/**
	 * Load the Segments
	 * @return List<SegmentDto>
	 * @throws ImproException
	 */
	List<SegmentDto> loadSegment() throws ImproException;

	/**
	 * Load the Segments based on subCategoryList
	 * 
	 * @param subCategoryList
	 * @return List<SegmentDto>
	 * @throws ImproException
	 */
	List<SegmentDto> loadSegment(List<String> subCategoryList) throws ImproException;

	/**
	 * Method to add the Segment
	 * @param segmentDto
	 * @return short
	 * @throws ImproException
	 */
	int addSegment(SegmentDto segmentDto) throws ImproException;

	/**
	 * Method to delete the Segment
	 * @param segmentDto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteSegment(SegmentDto segmentDto) throws ImproException;

	/**
	 * Method to edit the Segment
	 * @param segmentDto
	 * @return String message
	 * @throws ImproException
	 */
	String editSegment(SegmentDto segmentDto) throws ImproException;

	/**
	 * Method to add the segment-SubCategory entries in the join table
	 * 
	 * @param segmentDto
	 * @param genSegCode
	 * @throws ImproException
	 */
	void addSegmentSubCategoryMapping(SegmentDto segmentDto, int genSegCode) throws ImproException;

	/**
	 * Method to check the duplicate Segment Desc
	 * 
	 * @param segmentDesc
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicateSegment(String segmentDesc) throws ImproException;

	/**
	 * Method to validate the segment/old-Subcagtegory in products table 
	 * if the subcategory is changed to new value.
	 * @param segmentDto
	 * @return boolean
	 * @throws ImproException
	 */
	boolean validateInProduct(SegmentDto segmentDto) throws ImproException;

	/**
	 * Load Segment details for a given segmentCode
	 * @param segmentid
	 * @return SegmentDto
	 * @throws ImproException
	 */
	SegmentDto loadSegment(int segmentid) throws ImproException;

	/**
	 * Update the products with new subcatgegory,if the subcategory is changed
	 * @param segmentDto
	 * @throws ImproException
	 */
	void updateProduct(SegmentDto segmentDto) throws ImproException;

	

}
