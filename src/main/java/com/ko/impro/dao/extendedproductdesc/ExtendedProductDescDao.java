package com.ko.impro.dao.extendedproductdesc;

import java.util.List;

import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Dao class for ExtendedProdDesc 
 *
 */
public interface ExtendedProductDescDao {
	
	/**
	 * Load the ExtendedProduct
	 * @param boolean isMaintainence
	 * @return List<ExtendedProductDto>
	 * @throws ImproException
	 */
	List<ExtendedProductDescDto> loadExtendedProductDesc() throws ImproException;
	

	/**
	 * Load ExtendedProductDesc based on brandCode
	 * @param brandCode
	 * @return ExtendedProductDescDto
	 * @throws ImproException
	 */
	List<ExtendedProductDescDto> loadExtendedProductDesc(List<String> brandList) throws ImproException;
	
	
	/**
	 * Method to add the ExtendedProdDesc
	 * @param extProductDescDto
	 * @throws ImproException
	 */
	void addExtendedProductDesc(ExtendedProductDescDto extProductDescDto) throws ImproException;
	
	/**
	 * Method to delete the  ExtendedProdDesc
	 * @param dto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteExtendedProductDesc(ExtendedProductDescDto dto) throws ImproException;

	/**
	 * Method to edit  the ExtendedProdDesc
	 * @param dto
	 * @return String
	 * @throws ImproException
	 */
	String editExtendedProductDesc(ExtendedProductDescDto dto) throws ImproException;
	
	/**
	 * Method to load the  ExtendedProdDesc based on extendedProdDescCode
	 * @param extProdId
	 * @return  ExtendedProductDescDto
	 * @throws ImproException
	 */
	ExtendedProductDescDto loadExtendedProductDesc(int extProdId) throws ImproException;
	
	/**
	 * Method to duplicate check the ExtendedProdDesc
	 * @param name
	 * @param boolean isLongDesc
	 * @return boolean 
	 * @throws ImproException
	 */
	boolean checkDuplicateExtendedProductDesc(String name,boolean isLongDesc) throws ImproException;

}
