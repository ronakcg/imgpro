package com.ko.impro.dao.subcategory;

import java.util.List;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * SubCategoryDao to handle the  CRUD operations for SubCategory
 */
public interface SubCategoryDao {

	/**
	 * Load the SubCategory
	 * @return List<SubCategoryDto>
	 * @throws ImproException
	 */
	List<SubCategoryDto> loadSubCategory() throws ImproException;

	/**
	 * Load the SubCategory based on categoryList
	 * @param categoryList
	 * @return List<SubCategoryDto>
	 * @throws ImproException
	 */
	List<SubCategoryDto> loadSubCategory(List<String> categoryList) throws ImproException;

	/**
	 * Method to add SubCategory
	 * @param subcategoryDto
	 * @throws ImproException
	 */
	int addSubCategory(SubCategoryDto subcategoryDto) throws ImproException;

	/**
	 * Method to delete SubCategory
	 * @param subcategoryDto
	 * @return String
	 * @throws ImproException
	 */
	String deleteSubCategory(SubCategoryDto subcategoryDto) throws ImproException;

	/**
	 * Method to edit SubCategory
	 * @param subcategoryDto
	 * @return String 
	 * @throws ImproException
	 */
	String editSubCategory(SubCategoryDto subcategoryDto) throws ImproException;
	
	/**
	 * Method to Check/Validate Duplication of SubCategory desc 
	 * at the time of adding new category
	 * @param String
	 * @return boolean 
	 * @throws ImproException
	 */
	boolean checkDuplicateSubCategory(String subCategoryDesc) throws ImproException;
	
	/**
	 * Method to Map SubCategory to one or multiple Category.
	 * @param subcategoryDto 
	 * @param genSubCatCode 
	 * @throws ImproException
	 */
	void addCategoyMapping(SubCategoryDto subcategoryDto,int genSubCatCode) throws ImproException;

	/**
	 * Load the SubCategory
	 * @param subCatId
	 * @return SubCategoryDto
	 * @throws ImproException
	 */
	SubCategoryDto loadSubCategory(int subCatId) throws ImproException;
	
	/**
	 * To check Combination of removeCategoryList 
	 * and specific SubcateogryCode in products table 
	 * @param catCodeRmvList 
	 * @param subCatCode
	 * @return boolean value
	 * @throws ImproException
	 */
	boolean checkPresenceInProduct(List<String> catCodeRmvList,int subCatCode) throws ImproException;
	
	/**
	 * To check Combination of SubcateogryCode and CategoryCode in products table 
	 * @param subCategoryCode
	 * @return boolean value
	 * @throws ImproException
	 */
	boolean checkPresenceInProductForCategory(int subCategoryCode) throws ImproException;
	

	/**
	 * To check Combination of SubcateogryCode and FlavorCode in products table 
	 * @param subCategoryCode
	 * @return boolean value
	 * @throws ImproException
	 */
	boolean checkPresenceInProductForFlavor(int subCategoryCode) throws ImproException;
	

	/**
	 * To check Combination of SubcateogryCode and SegmentCode in products table 
	 * @param subCategoryCode
	 * @return boolean value
	 * @throws ImproException
	 */
	boolean checkPresenceInProductForSegment(int subCategoryCode) throws ImproException;
}
