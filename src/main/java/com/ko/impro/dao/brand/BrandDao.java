package com.ko.impro.dao.brand;

import java.util.List;

import com.ko.impro.dto.BrandDto;
import com.ko.impro.exception.ImproException;

/**
 *  @author hlekshmy
 * BrandDao to handle the  CRUD operations for Brand
 */

public interface BrandDao {
	 
	/**
	 * Load the Brands
	 * @return List<BrandDto>
	 * @throws ImproException
	 */
	List<BrandDto> loadBrands() throws ImproException;
	
	/**
	 * Load the brands based on manufacturerIdList
	 * @param manufactIdList
	 * @return List<BrandDto>
	 * @throws ImproException
	 */
	List<BrandDto> loadBrands(List<String> manufactIdList) throws ImproException;
	
	/**
	 * This method returns a particular brand details based on the brandcode
	 * @param brandCode
	 * @return BrandDto
	 * @throws ImproException
	 */
	BrandDto getBrandDetails(BrandDto brandDto) throws ImproException;

	
	/**
	 * Add the Brand
	 * @param brandDto
	 * @throws ImproException
	 */
	void addBrand(BrandDto brandDto) throws ImproException;
	
	/**
	 * Delete the Brand
	 * @throws ImproException
	 */
	String deleteBrand(BrandDto brandDto) throws ImproException;
	 
	/**
	 * Edit the Brand
	 * @param brandDto
	 * @throws ImproException
	 */
	String editBrand(BrandDto brandDto) throws ImproException;
	
	/**
	 * Checking Duplicate Brand
	 * @param name
	 * @return BrandDto
	 * @throws ImproException
	 */
	boolean checkDuplicateBrand(String name,boolean isLong) throws ImproException;	
	
	/**
	 * Load the Brand
	 * @param brandID
	 * @return BrandDto
	 * @throws ImproException
	 */
	BrandDto loadBrand(int brandID) throws ImproException;
	
	/**
	 * update the product which contains the manufacturer and brand
	 * @param dto
	 * @throws ImproException
	 */
	 void updateProduct(BrandDto dto) throws ImproException;
		
	/**
	 * check presence in product
	 * @param brandCode
	 * @return boolean
	 * @throws ImproException
	 */
	 boolean checkPresenceInProduct(int brandCode) throws ImproException;

	/**
	 * Validate active products for the given brand/old-manufacturer combination
	 * @param dto
	 * @return boolean
	 * @throws ImproException
	 */
	boolean validateInProduct(BrandDto dto) throws ImproException;
	
	/**
	 * Update the brand extendedProdDesc  relation ship table when extendedDesc is 
	 * inActivated/activated/deleted
	 * @param dto
	 * @param flag
	 * @throws ImproException
	 */
	 void updateBrandExtendedProdDesc(BrandDto dto) throws ImproException;
}
