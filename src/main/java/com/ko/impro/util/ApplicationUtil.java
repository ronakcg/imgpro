package com.ko.impro.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;

import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.EditImageDto;
import com.ko.impro.dto.HiResImageDto;
import com.ko.impro.dto.JpgImageDto;
import com.ko.impro.dto.MockImageDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.StdImageDto;
import com.ko.impro.model.product.Products;

/**
 * @author nvohra
 * Class for utility methods.
 */
public class ApplicationUtil {

	/**
	 * Check for object Null.
	 * @param value
	 * @return String
	 */
	public static String checkNull(Object value) {
		if (value == null)
			return "";
		else
			return value.toString();
	}
	
	/**
	 * Returns Date and Time
	 * @return
	 */
	public static String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
		Calendar cal = Calendar.getInstance(); 		
		return dateFormat.format(cal.getTime());
		
	}

	/**
	 * This method is used to convert the date from mm/dd/yyyy(UI) to yyyy-MM-dd
	 * @param date
	 * @return
	 */
	public static String setFormattedDate(final String date) {
		String formDate=null;
		if (!"".equals(date)) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat simpleRetrieve = new SimpleDateFormat("yyyy-MM-dd");
			try {
				formDate= simpleRetrieve.format(simpleDateFormat.parse(date));
			} catch (ParseException e) {
				formDate= null;
			}
		} else {
			formDate= null;
		}
		return formDate;

	}

	/**
	 * This method is used to convert the date from yyyy-mm-dd to mm-dd-yyyy format
	 * @param date
	 * @return
	 */
	public static String getformattedDate(String date) {
		if (!"".equals(date)) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat simpleDateFormatDisplay = new SimpleDateFormat("MM/dd/yyyy");
			try {
				return simpleDateFormatDisplay.format(simpleDateFormat.parse(date));
			} catch (ParseException e) {
				return null;
			}
		} else {
			return null;
		}

	}
	
	/**
	 * Convert Object to int
	 * @param value
	 * @return int
	 */
	public static int convertInteger(Object value) {
		if (value == null)
			return 0;
		else
			return Integer.parseInt(value.toString());

	}

	/**
	 * Get the boolean values based on 1/0
	 * @param archiveflag
	 * @return boolean
	 */
	public static boolean setBoolean(String archiveflag) {
		boolean res = false;
		if (StringUtils.isNotEmpty(archiveflag)) {
			if ("0".equals(archiveflag)) {
				res = false;
			} else {
				res = true;
			}
		}
		return res;
	}
	
	/**
	 * Get the boolean values based on N/Y/""
	 * @param iamgeFlag
	 * @return boolean
	 */
	public static boolean setImageFlag(String imageFlag) {
		boolean res = false;
		if (StringUtils.isNotEmpty(imageFlag)) {
			if ("Y".equals(imageFlag)) {
				res = true;
			} else {
				res = false;
			}
		}
		return res;
	}
	
	/**
	 * Get the boolean values based on N/Y/""
	 * @param iamgeFlag
	 * @return boolean
	 */
	public static String setImageFlag(Object imageFlag) {
		String res = "N";
		if (imageFlag == null) {
			res="N";
		} else if("1".equals(imageFlag.toString())) {
			res="Y";
		} else {
			res="N";
		}
		return res;

	}
	
	/**
	 * Convert object from productDto to Product Model 
	 * @param dto
	 * @return Products
	 */
	public static Products convertToProduct(ProductDto dto) {
		
		Products products = new Products();
		products.setBitmapName(dto.getBitMapName());
		products.setArchiveFlag((byte) 0);
		products.setProductName(generateLongProductName(dto));
		products.setShortProductName(generateShortProductName(dto));
		products.setPackageCode(Integer.parseInt(dto.getPkgdto().getPackageCode()));
		products.setBrandCode(Integer.parseInt(dto.getBrandDto().getBrandCode()));
		products.setUpc(dto.getUpc());
		products.setFlavorCode(Integer.parseInt(dto.getFlavorDto().getFlavorCode()));
		products.setCategoryCode(Integer.parseInt(dto.getCategoryDto().getCategoryCode()));
		products.setDateMaintained(getDateTime());
		products.setSubCategoryCode(Integer.parseInt(dto.getSubCategoryDto().getSubCategoryCode()));
		products.setSegmentCode(Integer.parseInt(dto.getSegmentDto().getSegmentCode()));
		products.setSubSegmentCode(Integer.parseInt(dto.getSubSegmentDto().getSubSegmentCode()));
		products.setManufacturerCode(Integer.parseInt(dto.getManufacturerDto().getManufacturerCode()));
		
		products.setNielsenUPC(dto.getNielsenUpc());
		products.setNielsenDesc(dto.getNielsenDesc());
		products.setExtendedDescId(Integer.parseInt(dto.getExtendedProductDescDto().getExtendedDescId()));
		products.setConsumptionType(dto.getConsumptionType());
		products.setUpc12(dto.getUpc12());
		products.setDateAdded(getDateTime());
		EditImageDto editImgDto=dto.getEditImageDto() ;
		if(editImgDto != null) {
			StdImageDto stdImgDto=editImgDto.getStdImageDto() ;
			HiResImageDto hiResImgDto=editImgDto.getHiResImageDto() ;
			MockImageDto mockImgDto=editImgDto.getMockImageDto() ;
			JpgImageDto jpgImgDto=editImgDto.getJpgImageDto() ;

			if(stdImgDto != null) {
				if(stdImgDto.getFile1() != null ) {
					products.setLowRes1(true);
				} else {
					products.setLowRes1(false);
				}
				if(stdImgDto.getFile2() != null ) {
					products.setLowRes2(true);
				} else {
					products.setLowRes2(false);
				}
				if(stdImgDto.getFile3() != null ) {
					products.setLowRes3(true);
				} else {
					products.setLowRes3(false);
				}
				if(stdImgDto.getFile7() != null ) {
					products.setLowRes7(true);
				} else {
					products.setLowRes7(false);
				}
				if(stdImgDto.getFile8() != null ) {
					products.setLowRes8(true);
				} else {
					products.setLowRes8(false);
				}
				if(stdImgDto.getFile9() != null ) {
					products.setLowRes9(true);
				} else {
					products.setLowRes9(false);
				}
			}
			if(hiResImgDto != null) {
	
				if(hiResImgDto.getFile1() != null ) {
					products.setHiRes1(true);
				} else {
					products.setHiRes1(false);
				}
				if(hiResImgDto.getFile2() != null ) {
					products.setHiRes2(true);
				} else {
					products.setHiRes2(false);
				}
				if(hiResImgDto.getFile3() != null ) {
					products.setHiRes3(true);
				} else {
					products.setHiRes3(false);
				}
				if(hiResImgDto.getFile7() != null ) {
					products.setHiRes7(true);
				} else {
					products.setHiRes7(false);
				}
				if(hiResImgDto.getFile8() != null ) {
					products.setHiRes8(true);
				} else {
					products.setHiRes8(false);
				}
				if(hiResImgDto.getFile9() != null ) {
					products.setHiRes9(true);
				} else {
					products.setHiRes9(false);
				}
			}
			if(mockImgDto != null) {

				if(mockImgDto.getFile1() != null ) {
					products.setMock1(true);
				} else {
					products.setMock1(false);
				}
				if(mockImgDto.getFile2() != null ) {
					products.setMock2(true);
				} else {
					products.setMock2(false);
				}
				if(mockImgDto.getFile3() != null ) {
					products.setMock3(true);
				} else {
					products.setMock3(false);
				}
				if(mockImgDto.getFile7() != null ) {
					products.setMock7(true);
				} else {
					products.setMock7(false);
				}
				if(mockImgDto.getFile8() != null ) {
					products.setMock8(true);
				} else {
					products.setMock8(false);
				}
				if(mockImgDto.getFile9() != null ) {
					products.setMock9(true);
				} else {
					products.setMock9(false);
				}
			}
			if(jpgImgDto != null) {

				if(jpgImgDto.getFile1() != null ) {
					products.setJpg(true);
				} else {
					products.setJpg(false);
				}
			}
		}
				
		return products;
	}
	

	/**
	 * Set the long productName
	 * @param productDto
	 * @return String
	 */
	public static String generateLongProductName(Object product) {
		String longProductName=null;
		ProductDto productDto=null;
		ArchivedProductDto archivedProductDto=null;
		if(product instanceof ProductDto) {
				productDto=(ProductDto)product;
			longProductName=generateLongProductName(productDto.getBrandDto().getBrandName(),productDto.getExtendedProductDescDto().getExtendedDescId(),productDto.getPackageSizeDto().getPackageSizeDescription(),productDto.getPkgdto().getContainerTypeDto().getContainerTypeDescription());
			return longProductName;
		} else if(product instanceof ArchivedProductDto){
			archivedProductDto=(ArchivedProductDto)product;
			longProductName=generateLongProductName(archivedProductDto.getBrandDto().getBrandName(),archivedProductDto.getExtendedProductDescDto().getExtendedDescId(),archivedProductDto.getPackageSizeDto().getPackageSizeDescription(),archivedProductDto.getPkgdto().getContainerTypeDto().getContainerTypeDescription());
			return longProductName;
		} else {
			return null;
		}
	}
	
	/**
	 * Set the long productName
	 * @param brandName
	 * @param longExtendedDesc
	 * @param packageSizeName
	 * @param containerTypeDesc
	 * @return
	 */
	private static String generateLongProductName(String brandName,String longExtendedDesc, String packageSizeName, String containerTypeDesc) {
		if(null==longExtendedDesc) {
			return  brandName+" "+packageSizeName+" "+containerTypeDesc;
		} else {
			return  brandName+" "+longExtendedDesc+" "+packageSizeName+" "+containerTypeDesc;	
		}
		
	}
	
	/**
	 * Set the Short productName
	 * @param productDto
	 * @return String
	 */
	public static String generateShortProductName(Object product) {
		String shortProductName=null;
		ProductDto productDto=null;
		ArchivedProductDto archivedProductDto=null;
		if(product instanceof ProductDto) {
			productDto=(ProductDto)product;
			shortProductName=generateShortProductName(productDto.getBrandDto().getBrandName(),productDto.getExtendedProductDescDto().getExtendedDescId(),productDto.getPackageSizeDto().getPackageSizeDescription(),productDto.getPkgdto().getContainerTypeDto().getContainerTypeDescription());
			return shortProductName;
		} else if(product instanceof ArchivedProductDto){
			archivedProductDto=(ArchivedProductDto)product;
			shortProductName=generateShortProductName(archivedProductDto.getBrandDto().getBrandName(),archivedProductDto.getExtendedProductDescDto().getExtendedDescId(),archivedProductDto.getPackageSizeDto().getPackageSizeDescription(),archivedProductDto.getPkgdto().getContainerTypeDto().getContainerTypeDescription());
			return shortProductName;
		} else {
			return null;
		}
	}
	
	/**
	 * Set the Short productName
	 * @param brandName
	 * @param shortExtendedDesc
	 * @param packageSizeName
	 * @param containerTypeDesc
	 * @return String
	 */
	private static String generateShortProductName(String brandName,String shortExtendedDesc,String packageSizeName, String containerTypeDesc) {
		if(null==shortExtendedDesc) 
			return  brandName+" "+packageSizeName+" "+containerTypeDesc;
		else
			return  brandName+" "+shortExtendedDesc+" "+packageSizeName+" "+containerTypeDesc;
	}
	
	/**
	 * Generate the long productName during multi edit
	 * @param productdtosrc
	 * @param productDto
	 * @return String
	 */
	public static String generateLongProductNameMultiEdit(ProductDto productdtosrc, ProductDto productDto) {
		String brandName = null;
		String longExt = null;
		String packageName = null;
		String containerTypeDesc=null;
		
		if(productdtosrc.getBrandDto().getBrandName() == null || "".equals(productdtosrc.getBrandDto().getBrandName())) {
			brandName = productDto.getBrandDto().getBrandName();
		} else {
			brandName =  productdtosrc.getBrandDto().getBrandName();
		}
		
		if(productdtosrc.getExtendedProductDescDto().getLongProductDesc() == null || "".equals(productdtosrc.getExtendedProductDescDto().getLongProductDesc())) {
			longExt = productDto.getExtendedProductDescDto().getLongProductDesc();
		} else {
			longExt =  productdtosrc.getExtendedProductDescDto().getLongProductDesc();
		}
		
		if(productdtosrc.getPackageSizeDto().getPackageSizeDescription() == null || "".equals(productdtosrc.getPackageSizeDto().getPackageSizeDescription())) {
			packageName =productDto.getPackageSizeDto().getPackageSizeDescription();
		} else {
			packageName =  productdtosrc.getPackageSizeDto().getPackageSizeDescription();
		}
		
		if(productdtosrc.getPkgdto().getContainerTypeDto().getContainerTypeDescription() == null || "".equals(productdtosrc.getPkgdto().getContainerTypeDto().getContainerTypeDescription())) {
			containerTypeDesc =productDto.getPkgdto().getContainerTypeDto().getContainerTypeDescription();
		} else {
			containerTypeDesc =  productdtosrc.getPkgdto().getContainerTypeDto().getContainerTypeDescription();
		}
		
		if(null==longExt) 
		{
			return  brandName+" "+packageName+" "+containerTypeDesc;
		}
		else
		{
			return  brandName+" "+longExt+" "+packageName+" "+containerTypeDesc;
		}

	}
	
	/**
	 * Generate the Short productName during multiEdit
	 * @param productDto
	 * @return String
	 */
	public static String generateShortProductNameMultiEdit(ProductDto productdtosrc,ProductDto productDto) {
		String brandName = null;
		String shortExt = null;
		String packageName = null;
		String containerTypeDesc=null;
		
		if(productdtosrc.getBrandDto().getBrandName() == null || "".equals(productdtosrc.getBrandDto().getBrandName())) 
			brandName = productDto.getBrandDto().getBrandName();
		else 
			brandName =  productdtosrc.getBrandDto().getBrandName();
		
		
		if(productdtosrc.getExtendedProductDescDto().getShortProductDesc() == null || "".equals(productdtosrc.getExtendedProductDescDto().getShortProductDesc())) {
			shortExt = productDto.getExtendedProductDescDto().getShortProductDesc();
		} else {
			shortExt =  productdtosrc.getExtendedProductDescDto().getLongProductDesc();
		}
		
		if(productdtosrc.getPackageSizeDto().getPackageSizeDescription() == null || "".equals(productdtosrc.getPackageSizeDto().getPackageSizeDescription())) {
			packageName =productDto.getPackageSizeDto().getPackageSizeDescription();
		} else {
			packageName =  productdtosrc.getPackageSizeDto().getPackageSizeDescription();
		}
		
		if(productdtosrc.getPkgdto().getContainerTypeDto().getContainerTypeDescription() == null || "".equals(productdtosrc.getPkgdto().getContainerTypeDto().getContainerTypeDescription())) {
			containerTypeDesc =productDto.getPkgdto().getContainerTypeDto().getContainerTypeDescription();
		} else {
			containerTypeDesc =  productdtosrc.getPkgdto().getContainerTypeDto().getContainerTypeDescription();
		}
		
		if(null==shortExt) 
			return  brandName+" "+packageName+" "+containerTypeDesc;
		else
			return  brandName+" "+shortExt+" "+packageName+" "+containerTypeDesc;
		
	}
	
	
	

}