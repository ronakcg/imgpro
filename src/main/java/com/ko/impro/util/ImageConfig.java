package com.ko.impro.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.services.s3.AmazonS3;
import com.ko.impro.dao.auditlog.AuditLogDao;

/**
 * @author nvohra
 * Interface for constants
 */
public class ImageConfig {
		
	/**
	 * s3client Reference
	 */
	@Autowired
	protected AmazonS3 s3client;
	
	@Autowired(required = true)
	protected AuditLogDao auditDao;
	
	/**
	 * bucketName value for the given key in propetryfile
	 */
	@Value("${s3.bucket.product}")
	protected String bucketName;
	
	/**
	 * url value for the given key in propetryFile
	 */
	@Value("${s3.bucket.staticUrl}")
	protected String url;
	
	/**
	 * activeStdFolNm value for the given key in propetryFile
	 */
	@Value("${s3.active.std.folderName}")
	protected String activeStdFolNm;
	
	/**
	 * activeHiReFolNm value for the given key in propetryFile
	 */
	@Value("${s3.active.hiRes.folderName}")
	protected String activeHiReFolNm;
	
	/**
	 * activeMockFolNm value for the given key in propetryFile
	 */
	@Value("${s3.active.mock.folderName}")
	protected String activeMockFolNm;
	
	
	/**
	 * archiveStdFolNm value for the given key in propetryFile
	 */
	@Value("${s3.archive.std.folderName}")
	protected String archiveStdFolNm;
	
	/**
	 * archiveHiReFolNm value for the given key in propetryFile
	 */
	@Value("${s3.archive.hiRes.folderName}")
	protected String archiveHiReFolNm;
	
	/**
	 * archiveMockFolNm value for the given key in propetryFile
	 */
	@Value("${s3.archive.mock.folderName}")
	protected String archiveMockFolNm;
	
	/**
	 * activeJpgFolNm value for the given key in propetryFile
	 */
	@Value("${s3.active.jpg.folderName}")
	protected String activeJpgFolNm;
	
	/**
	 * activeJpgFolNm value for the given key in propetryFile
	 */
	@Value("${s3.imagearchive.std.folderName}")
	private String imagearchiveStdFolNm;

	/**
	 * activeJpgFolNm value for the given key in propetryFile
	 */
	@Value("${s3.imagearchive.hiRes.folderName}")
	private String imagearchiveHiReFolNm;

	/**
	 * activeJpgFolNm value for the given key in propetryFile
	 */
	@Value("${s3.imagearchive.mock.folderName}")
	private String imagearchiveMocFolNm;

	/**
	 * activeJpgFolNm value for the given key in propetryFile
	 */
	@Value("${s3.imagearchive.jpg.folderName}")
	private String imagearchiveJpgFolNm;
	
	
	/**
	 * gatekeepingStdFolNm value for the given key in propetryFile
	 */
	@Value("${s3.gatekeeping.std.folderName}")
	protected String gatekeepingStdFolNm;
	
	/**
	 * gatekeepingHiFolNm value for the given key in propetryFile
	 */
	@Value("${s3.gatekeeping.hiRes.folderName}")
	protected String gatekeepingHiFolNm;
	
	/**
	 * gatekeepingMockFolNm value for the given key in propetryFile
	 */
	@Value("${s3.gatekeeping.mock.folderName}")
	protected String gatekeepingMockFolNm;
	
	/**
	 * gateImgArchStdFolNm value for the given key in propetryFile
	 */
	@Value("${s3.gateImgarchive.std.folderName}")
	protected String gateImgArchStdFolNm;
	
	/**
	 * gateImgArchHiFolNm value for the given key in propetryFile
	 */
	@Value("${s3.gateImgarchive.hiRes.folderName}")
	protected String gateImgArchHiFolNm;
	
	/**
	 * gateImgArchMockFolNm value for the given key in propetryFile
	 */
	@Value("${s3.gateImgarchive.mock.folderName}")
	protected String gateImgArchMockFolNm;
	
	/**
	 * gateimagearchiveJpgFolNm value for the given key in propetryFile
	 */
	@Value("${s3.gateImgarchive.jpg.folderName}")
	protected String gateImgArchJpgFolNm;

}