package com.ko.impro.util;

/**
 * @author nvohra
 * Interface for constants
 */
public class AppConstants {
		
	public static final String JWT_SECRET_KEY = "carsda081029017";
	public static String TOKEN_HEADER = "x-auth-token";	
	public static final String XLSX_EXTENSION = "xlsx";
	
	
	// MSG CONTANTS
	public static final String SUCCESS_CODE="200";
	public static final String DUPLICATE_CODE="202";

	public static final String FAILURE_CODE="422";
	public static final String NOT_FOUND_CODE="201";
	public static final String FAILURE_MSG="Application encountered an error. Please try again.";
	public static final String NOT_FOUND_MSG="No records found.";
	
	
	//PRODUCTS CONSTANTS
	public static final String PRODUCT_UPDATE_SUCC_MSG="Product updated successfully.";
	public static final String MULTI_PRODUCT_UPDATE_SUCC_MSG="Product(s) updated successfully.";

	public static final String PRODUCT_DUPL_MSG="You have tried to create a duplicate value.  Please update with a unique object.";
	public static final String ARCHIVE_PRODUCT_PURGE_MSG="Selected Archived Products purged successfully.";
	public static final String PRODUCT_ARCHIVE_MSG="Product(s) have been archived successfully.";

	public static final String PRODUCT_ADD_SUCC_MSG="Product added successfully";
	public static final String PRODUCT_RESTRORE_SUCC_MSG="Product has been restored successfully.";
	public static final String PRODUCT_RESTRORE_DUPL_MSG="Already an active record exists with same Id.";

	
	public static final String PRODUCT_SHEETNAME = "Products";
	public static final String PRODUCT_FILENAME="ProductList";
	
	
	public static final String MISSING_FLAG_YES="Y";
	public static final String MISSING_FLAG_NO="N";
    
    //MAINTAINENCE CONST
	public static final String DATA_UPDATE_SUCC_MSG="Data updated successfully.";
    public static final String DATA_DELETE_SUCC_MSG="Data deleted successfully.";
    public static final String DATA_ADD_SUCC_MSG="Data added successfully.";
	
	
	//AUDIT CONSTANTS
	public static final String TRANSACTION_TYPE_ADD="Add";
	public static final String TRANSACTION_TYPE_CHANGE="Update";
	public static final String TRANSACTION_TYPE_ARCHIVE="Archive";
	public static final String TRANSACTION_TYPE_DELETE="Delete";
	public static final String TRANSACTION_TYPE_RESTORE="Restore";
	public static final String TRANSACTION_TYPE_PURGE="Purge";
	
	public static final String STD_IMAGE="Std Image";
	public static final String HIRES_IMAGE="HiRes Image";
	public static final String MOCK_IMAGE="Mock Image";

	
	public static final String TABLE_PRODUCTS="Products";
	public static final String TABLE_BRANDS="Brands";
	public static final String TABLE_CATEGORY="Category";
	public static final String TABLE_CONTAINERTYPE="ContainerType";
	public static final String TABLE_FLAVOURS="Flavours";
	public static final String TABLE_MANUFACTURER="Manufacturer";
	public static final String TABLE_PACKAGESTYLE="PackageStyle";
	public static final String TABLE_PACKAGESIZE="PackageSize";
	public static final String TABLE_SEGMENT="Segment";
	public static final String TABLE_SHAPES="Shapes";
	public static final String TABLE_SUBCATEGORY="SubCategory";
	public static final String TABLE_SUBSEGEMENT="SubSegment";
	public static final String TABLE_PACKAGES="Packages";
	public static final String TABLE_SUPPLIER="Supplier";
	
	public static final String TABLE_DELETE_MESSAGES_DATA="DeleteMessagesData";
	public static final String TABLE_EXTENDED_PRODUCT_DESCRIPTION="ExtendedDesc";
	public static final String TABLE_DELETED_PRODUCTS="Deleted_Products";
	public static final String TABLE_BRANDS_EXTEN_DESC="Brand_Extended_Desc";

	public static final String USER_NAME="Nickerson";
	public static final String AUDIT_FILENAME = "Change_log.xlsx";
	public static final String AUDIT_SHEETNAME = "Change Log";

	//GATEKEEPING CONSTANTS
	public static final String GATEKEEPING_SHEETNAME = "GateKeeping Products";
	public static final String GATEKEEPING_FILENAME="Gatekeeping ProductList";
	public static final boolean STATUS_FLAG_YES=true;
	public static final boolean STATUS_FLAG_NO=false;
	public static final String GATEKEEPING_PUSH_PRODUCTS_SUCC_MSG = "Products(s) pushed successfully";
	public static final String GATEKEEPING_PRODUCT_PURGE_SUCC_MSG="Product purged successfully.";
	public static final String GATEKEEPING_PRODUCT_UPDATE_SUCC_MSG="Product(s) updated successfully.";
	
	// IMAGE CONSTANTS
	public static final String IMAGE_VIEW1 = ".1";
	public static final String IMAGE_VIEW2 = ".2";
	public static final String IMAGE_VIEW3 = ".3";
	public static final String IMAGE_VIEW7 = ".7";
	public static final String IMAGE_VIEW8 = ".8";
	public static final String IMAGE_VIEW9 = ".9";
	
	public static final String IMAGE_TRANSACTION_TYPE_STD_ADD1="Add Std_Image View:1";
	public static final String IMAGE_TRANSACTION_TYPE_STD_ADD2="Add Std_Image View:2";
	public static final String IMAGE_TRANSACTION_TYPE_STD_ADD3="Add Std_Image View:3";
	public static final String IMAGE_TRANSACTION_TYPE_STD_ADD7="Add Std_Image View:7";
	public static final String IMAGE_TRANSACTION_TYPE_STD_ADD8="Add Std_Image View:8";
	public static final String IMAGE_TRANSACTION_TYPE_STD_ADD9="Add Std_Image View:9";
	
	public static final String IMAGE_TRANSACTION_TYPE_STD_EDIT1="Edit Std_Image View:1";
	public static final String IMAGE_TRANSACTION_TYPE_STD_EDIT2="Edit Std_Image View:2";
	public static final String IMAGE_TRANSACTION_TYPE_STD_EDIT3="Edit Std_Image View:3";
	public static final String IMAGE_TRANSACTION_TYPE_STD_EDIT7="Edit Std_Image View:7";
	public static final String IMAGE_TRANSACTION_TYPE_STD_EDIT8="Edit Std_Image View:8";
	public static final String IMAGE_TRANSACTION_TYPE_STD_EDIT9="Edit Std_Image View:9";
	
	public static final String IMAGE_TRANSACTION_TYPE_STD_DELETE1="Delete Std_Image View:1";
	public static final String IMAGE_TRANSACTION_TYPE_STD_DELETE2="Delete Std_Image View:2";
	public static final String IMAGE_TRANSACTION_TYPE_STD_DELETE3="Delete Std_Image View:3";
	public static final String IMAGE_TRANSACTION_TYPE_STD_DELETE7="Delete Std_Image View:7";
	public static final String IMAGE_TRANSACTION_TYPE_STD_DELETE8="Delete Std_Image View:8";
	public static final String IMAGE_TRANSACTION_TYPE_STD_DELETE9="Delete Std_Image View:9";

	public static final String IMAGE_TRANSACTION_TYPE_HIRES_ADD1="Add HiRes_Image View:1";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_ADD2="Add HiRes_Image View:2";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_ADD3="Add HiRes_Image View:3";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_ADD7="Add HiRes_Image View:7";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_ADD8="Add HiRes_Image View:8";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_ADD9="Add HiRes_Image View:9";
	
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_EDIT1="Edit HiRes_Image View:1";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_EDIT2="Edit HiRes_Image View:2";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_EDIT3="Edit HiRes_Image View:3";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_EDIT7="Edit HiRes_Image View:7";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_EDIT8="Edit HiRes_Image View:8";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_EDIT9="Edit HiRes_Image View:9";
	
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_DELETE1="Delete HiRes_Image View:1";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_DELETE2="Delete HiRes_Image View:2";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_DELETE3="Delete HiRes_Image View:3";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_DELETE7="Delete HiRes_Image View:7";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_DELETE8="Delete HiRes_Image View:8";
	public static final String IMAGE_TRANSACTION_TYPE_HIRES_DELETE9="Delete HiRes_Image View:9";
	
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_ADD1="Add Mock_Image View:1";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_ADD2="Add Mock_Image View:2";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_ADD3="Add Mock_Image View:3";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_ADD7="Add Mock_Image View:7";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_ADD8="Add Mock_Image View:8";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_ADD9="Add Mock_Image View:9";
	
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_EDIT1="Edit Mock_Image View:1";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_EDIT2="Edit Mock_Image View:2";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_EDIT3="Edit Mock_Image View:3";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_EDIT7="Edit Mock_Image View:7";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_EDIT8="Edit Mock_Image View:8";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_EDIT9="Edit Mock_Image View:9";
	
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_DELETE1="Delete Mock_Image View:1";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_DELETE2="Delete Mock_Image View:2";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_DELETE3="Delete Mock_Image View:3";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_DELETE7="Delete Mock_Image View:7";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_DELETE8="Delete Mock_Image View:8";
	public static final String IMAGE_TRANSACTION_TYPE_MOCK_DELETE9="Delete Mock_Image View:9";

	public static final String IMAGE_TRANSACTION_TYPE_JPG_ADD="Add Jpg_Image";
	public static final String IMAGE_TRANSACTION_TYPE_JPG_EDIT="Edit Jpg_Image";
	public static final String IMAGE_TRANSACTION_TYPE_JPG_DELETE="Delete Jpg_Image";

	
	
	//public static final String IMAGE_TRANSACTION_TYPE_STD_DELETE1="Delete Std_Image View:1";
	//public static final String IMAGE_TRANSACTION_TYPE_EDIT_DELETE1="Edit Std_Image View:1";
	
	public static final String AWS_IMAGES="AWS S3 Bucket";

	public static final String TO_REMOVE1="https://s3.amazonaws.com/";
	public static final String TO_REMOVE2="active";
	public static final String TO_REPLACE="imagearchive";
	public static final String TO_REMOVEGATEKEEPING="gatekeeping";
	public static final String TO_REPLACEGATEKEEPING="gatearchive";	

	public static final String IMAGE_TYPE_STD="Std_Image";
	public static final String IMAGE_TYPE_HIRES="HiRes_Image";
	public static final String IMAGE_TYPE_MOCK="Mock_Image";
	public static final String IMAGE_TYPE_JPG="JPG_Image";
	public static final String IMAGE_TYPE_STD_MOCK="Std_Mock_Image";

	public static final String IMAGE_TRANSACTION_ADD="Add ";
	public static final String IMAGE_TRANSACTION_EDIT="Edit ";
	public static final String IMAGE_TRANSACTION_DELETE="Delete ";
	

}