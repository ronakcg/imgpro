package com.ko.impro.util;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dto.EditImageDto;
import com.ko.impro.dto.HiResImageDto;
import com.ko.impro.dto.MockImageDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.dto.StdImageDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy Class for image helper methods.
 */
public class ImageHelper {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ImageHelper.class);

	/**
	 * @param byteDate
	 * @param keyName
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	public static void addImage(String byteDate, String keyName, AmazonS3 s3client, String bucketName) {

		String fileType = getImageType(byteDate);
		keyName = keyName + "." + fileType;
		byte[] bI = getImageByte(byteDate);
		ByteArrayInputStream fis = new ByteArrayInputStream(bI);
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(bI.length);
		s3client.putObject(bucketName, keyName, fis, metadata);
		s3client.setObjectAcl(bucketName, keyName, CannedAccessControlList.Private);
	}

	/**
	 * Get the fileType
	 * 
	 * @param byteDate
	 * @return
	 */
	public static String getImageType(String byteDate) {
		return byteDate.substring(byteDate.indexOf("/") + 1, byteDate.indexOf(";"));
	}

	/**
	 * Get the image Byte[]
	 * 
	 * @param byteDate
	 * @return byte[]
	 */
	public static byte[] getImageByte(String byteDate) {
		return org.apache.commons.codec.binary.Base64
				.decodeBase64((byteDate.substring(byteDate.indexOf(",") + 1)).getBytes());

	}

	/**
	 * Parse the imageFolder name from UPC
	 * 
	 * @param productID
	 * @return imageFolderName
	 */
	public static String getImageFolderName(final String productID) {
		String folderName = null;
		if (StringUtils.isNotEmpty(productID)) {
			folderName = productID.substring(0, 5);
		}
		return folderName;
	}

	/**
	 * Parse the image name from UPC and upcSuffix
	 * 
	 * @param productID
	 * @return imageFolderName
	 */
	public static String getImageName(final String productID) {
		String imageName = null;
		if (StringUtils.isNotEmpty(productID)) {
			imageName = productID.substring(5, productID.length());

		}
		return imageName;
	}

	/**
	 * 
	 * @param url
	 * @param bucketName
	 * @param sourcePathName
	 * @param sourceImgFolName
	 * @return
	 */
	public static Map<String, String> getImageMap(AmazonS3 s3client, String url, String bucketName,
			String sourcePathName, String sourceImgFolName) {
		List<S3ObjectSummary> fileList = s3client.listObjects(bucketName, sourcePathName + sourceImgFolName)
				.getObjectSummaries();
		Map<String, String> map = new HashMap<String, String>();
		for (S3ObjectSummary file : fileList) {
			if (!file.getKey().endsWith("/")) {
				String name = file.getKey().substring(file.getKey().lastIndexOf("/") + 1, file.getKey().length());
				map.put(name.substring(0, name.lastIndexOf(".")), url + bucketName + "/" + file.getKey());
			}
		}
		return map;
	}

	/**
	 * Delete the S3 image folder and the images in it.
	 * 
	 * @param pathFolderName
	 * @param imageFolderName
	 * @return boolean
	 */
	public static boolean deleteFolder(String pathFolderName, String bitMapName, AmazonS3 s3client, String bucketName) {
		boolean isDeleted = false;
		String imageFolderName=ImageHelper.getImageFolderName(bitMapName);
		String imageName=ImageHelper.getImageName(bitMapName);		
		List<S3ObjectSummary> fileList = s3client.listObjects(bucketName, pathFolderName + imageFolderName).getObjectSummaries();
		for (S3ObjectSummary file : fileList) {			
			String fileName = file.getKey().substring(file.getKey().lastIndexOf("/") + 1, file.getKey().length());
			String name=fileName.substring(0,fileName.lastIndexOf("."));
			if(name.equals(imageName + AppConstants.IMAGE_VIEW1) || name.equals(imageName + AppConstants.IMAGE_VIEW2) 
			|| name.equals(imageName + AppConstants.IMAGE_VIEW3) || name.equals(imageName + AppConstants.IMAGE_VIEW7)
			|| name.equals(imageName + AppConstants.IMAGE_VIEW8) || name.equals(imageName + AppConstants.IMAGE_VIEW9)
			|| name.equals(imageName)) {
				s3client.deleteObject(bucketName, file.getKey());				
			}		
		}
		fileList = s3client.listObjects(bucketName, pathFolderName + imageFolderName).getObjectSummaries();
		if(fileList!=null) {
			if(fileList.isEmpty() && fileList.size()==0) {
				s3client.deleteObject(bucketName, imageFolderName);
				isDeleted = true;
			}
		}		
		return isDeleted;
	}

	/**
	 * Copy the folder name with Images to destination path
	 * 
	 * @param pathFolderName
	 * @param sourceImageFolderName
	 */
	public static void copyFolder(String sourcePathName, String destPathName, String bitMapName, AmazonS3 s3client, String bucketName) {
		String sourceImgFolName=ImageHelper.getImageFolderName(bitMapName);
		String imageName=ImageHelper.getImageName(bitMapName);
		List<S3ObjectSummary> fileList = s3client.listObjects(bucketName, sourcePathName + sourceImgFolName).getObjectSummaries();
		for (S3ObjectSummary file : fileList) {			 
			String fileName = file.getKey().substring(file.getKey().lastIndexOf("/"), file.getKey().length());
			String name=fileName.substring(1,fileName.lastIndexOf("."));
			if(name.equals(imageName + AppConstants.IMAGE_VIEW1) || name.equals(imageName + AppConstants.IMAGE_VIEW2) 
			|| name.equals(imageName + AppConstants.IMAGE_VIEW3) || name.equals(imageName + AppConstants.IMAGE_VIEW7)
			|| name.equals(imageName + AppConstants.IMAGE_VIEW8) || name.equals(imageName + AppConstants.IMAGE_VIEW9)
			|| name.equals(imageName)) {
				CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, file.getKey(), bucketName,destPathName + sourceImgFolName + fileName);
				s3client.copyObject(copyObjRequest);			
			}
		}
	}
	
	/**
	 * @param prod
	 * @param mapStd
	 * @param mapHiRes
	 * @param mapMock
	 */
	public static void getProductImagesById(ProductDto prod, Map<String, String> mapStd, Map<String, String> mapHiRes,
			Map<String, String> mapMock) {
		String imageName = ImageHelper.getImageName(prod.getBitMapName());
		if (mapStd != null && !mapStd.isEmpty()) {
			prod.getMissingImagesDto().setLowRes1Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW1));
			prod.getMissingImagesDto().setLowRes2Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW2));
			prod.getMissingImagesDto().setLowRes3Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW3));
			prod.getMissingImagesDto().setLowRes7Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW7));
			prod.getMissingImagesDto().setLowRes8Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW8));
			prod.getMissingImagesDto().setLowRes9Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW9));
			prod.getMissingImagesDto().setJpgUrl(mapStd.get(imageName));
		}
		if (mapHiRes != null && !mapHiRes.isEmpty()) {
			prod.getMissingImagesDto().setHiRes1Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW1));
			prod.getMissingImagesDto().setHiRes2Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW2));
			prod.getMissingImagesDto().setHiRes3Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW3));
			prod.getMissingImagesDto().setHiRes7Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW7));
			prod.getMissingImagesDto().setHiRes8Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW8));
			prod.getMissingImagesDto().setHiRes9Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW9));
		}
		if (mapMock != null && !mapMock.isEmpty()) {
			prod.getMissingImagesDto().setMock1Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW1));
			prod.getMissingImagesDto().setMock2Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW2));
			prod.getMissingImagesDto().setMock3Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW3));
			prod.getMissingImagesDto().setMock7Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW7));
			prod.getMissingImagesDto().setMock8Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW8));
			prod.getMissingImagesDto().setMock9Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW9));
		}
	}

	/**
	 * @param prod
	 * @param mapStd
	 * @param mapHiRes
	 * @param mapMock
	 */
	public static void getProductImagesById(ProductGateKeepingDto prod, Map<String, String> mapStd,
			Map<String, String> mapHiRes) {
		String imageName = ImageHelper.getImageName(prod.getBitMapName());
		if (mapStd != null && !mapStd.isEmpty()) {
			prod.getMissingImagesDto().setLowRes1Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW1));
			prod.getMissingImagesDto().setLowRes2Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW2));
			prod.getMissingImagesDto().setLowRes3Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW3));
			prod.getMissingImagesDto().setLowRes7Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW7));
			prod.getMissingImagesDto().setLowRes8Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW8));
			prod.getMissingImagesDto().setLowRes9Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW9));
			prod.getMissingImagesDto().setJpgUrl(mapStd.get(imageName));
		}
		if (mapHiRes != null && !mapHiRes.isEmpty()) {
			prod.getMissingImagesDto().setHiRes1Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW1));
			prod.getMissingImagesDto().setHiRes2Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW2));
			prod.getMissingImagesDto().setHiRes3Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW3));
			prod.getMissingImagesDto().setHiRes7Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW7));
			prod.getMissingImagesDto().setHiRes8Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW8));
			prod.getMissingImagesDto().setHiRes9Url(mapHiRes.get(imageName + AppConstants.IMAGE_VIEW9));
		}
/*		if (mapMock != null && !mapMock.isEmpty()) {
			prod.getMissingImagesDto().setMock1Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW1));
			prod.getMissingImagesDto().setMock2Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW2));
			prod.getMissingImagesDto().setMock3Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW3));
			prod.getMissingImagesDto().setMock7Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW7));
			prod.getMissingImagesDto().setMock8Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW8));
			prod.getMissingImagesDto().setMock9Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW9));
		}*/
	}

	/**
	 * Method to do edit functionalities of StdImage1
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage1(ProductDto dto, AmazonS3 s3client, String bucketName, String activeStdFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());
		try {
			if ((!dto.getEditImageDto().getStdImageDto().getFile1().isEmpty())) { // Image from UI
				if (dto.getEditImageDto().getStdImageDto().isAddFLag1()) {
					// Previous image not present && add flag(ADDImage)
					ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile1(),
							activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
					auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, dto.getBitMapName(),
							AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD1);

				} else if (dto.getEditImageDto().getStdImageDto().isEditFLag1()) {
					// Previous image present && add flag(EDITImage)
					archiveImage(dto.getMissingImagesDto().getLowRes1Url(), s3client, bucketName);
					ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile1(),
							activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
					auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES,
							String.valueOf(dto.getBitMapName()), AppConstants.IMAGE_TRANSACTION_TYPE_STD_EDIT1);

				} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag1()) {// delete flag
					archiveImage(dto.getMissingImagesDto().getLowRes1Url(), s3client, bucketName);
					deleteImage(dto.getMissingImagesDto().getLowRes1Url(), s3client, bucketName);
					auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES,
							String.valueOf(dto.getBitMapName()), AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

				}
			}
		} catch (AmazonServiceException ase) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl editImage() AmazonServiceException " + ase.getMessage(), s3client,
						bucketName);
			}
			throw new ImproException(ase.getMessage());
		} catch (AmazonClientException ace) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl editImage() AmazonClientException " + ace.getMessage(), s3client,
						bucketName);
			}
			throw new ImproException(ace.getMessage());

		}
	}

	/**
	 * Method to do edit functionalities of StdImage2
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage2(ProductDto dto, AmazonS3 s3client, String bucketName, String activeStdFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getStdImageDto().getFile2().isEmpty())) {

			if (dto.getEditImageDto().getStdImageDto().isAddFLag2()) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile2(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD2);

			} else if (dto.getEditImageDto().getStdImageDto().isEditFLag2()) {
				archiveImage(dto.getMissingImagesDto().getLowRes2Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile2(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_EDIT2);

			} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag2()) {
				archiveImage(dto.getMissingImagesDto().getLowRes2Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getLowRes2Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE2);

			}
		}
	}

	/**
	 * Method to do edit functionalities of StdImage3
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage3(ProductDto dto, AmazonS3 s3client, String bucketName, String activeStdFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getStdImageDto().getFile3().isEmpty())) {

			if (dto.getEditImageDto().getStdImageDto().isAddFLag3()) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile3(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD3);

			} else if (dto.getEditImageDto().getStdImageDto().isEditFLag3()) {
				archiveImage(dto.getMissingImagesDto().getLowRes3Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile3(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_EDIT3);

			} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag3()) {
				archiveImage(dto.getMissingImagesDto().getLowRes3Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getLowRes3Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE3);

			}
		}
	}

	/**
	 * Method to do edit functionalities of StdImage7
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage7(ProductDto dto, AmazonS3 s3client, String bucketName, String activeStdFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());
		if ((!dto.getEditImageDto().getStdImageDto().getFile7().isEmpty())) {
			if (dto.getEditImageDto().getStdImageDto().isAddFLag7()) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile7(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD7);

			} else if (dto.getEditImageDto().getStdImageDto().isEditFLag7()) {
				archiveImage(dto.getMissingImagesDto().getLowRes7Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile7(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_EDIT7);

			} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag7()) {
				archiveImage(dto.getMissingImagesDto().getLowRes7Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getLowRes7Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE7);

			}
		}
	}

	/**
	 * Method to do edit functionalities of StdImage8
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage8(ProductDto dto, AmazonS3 s3client, String bucketName, String activeStdFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getStdImageDto().getFile8().isEmpty())) {

			if (dto.getEditImageDto().getStdImageDto().isAddFLag8()) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile8(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD8);

			} else if (dto.getEditImageDto().getStdImageDto().isEditFLag8()) {
				archiveImage(dto.getMissingImagesDto().getLowRes8Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile8(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_EDIT8);

			} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag8()) {
				archiveImage(dto.getMissingImagesDto().getLowRes8Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getLowRes8Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE8);

			}
		}
	}

	/**
	 * Method to do edit functionalities of editStdImage9
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage9(ProductDto dto, AmazonS3 s3client, String bucketName, String activeStdFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getStdImageDto().getFile9().isEmpty())) {

			if (dto.getEditImageDto().getStdImageDto().isAddFLag9()) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile9(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD9);

			} else if (dto.getEditImageDto().getStdImageDto().isEditFLag9()) {
				archiveImage(dto.getMissingImagesDto().getLowRes9Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile9(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_EDIT9);

			} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag9()) {
				archiveImage(dto.getMissingImagesDto().getLowRes9Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getLowRes9Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE9);

			}
		}
	}

	/**
	 * Method to do edit functionalities of HiResImage1
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage1(ProductDto dto, AmazonS3 s3client, String bucketName, String activeHiReFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile1().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag1()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile1(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD1);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag1()) {
				archiveImage(dto.getMissingImagesDto().getHiRes1Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile1(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_EDIT1);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag1()) {
				archiveImage(dto.getMissingImagesDto().getHiRes1Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes1Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of HiResImage2
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage2(ProductDto dto, AmazonS3 s3client, String bucketName, String activeHiReFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile2().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag2()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile2(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD2);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag2()) {
				archiveImage(dto.getMissingImagesDto().getHiRes2Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile2(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_EDIT2);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag2()) {
				archiveImage(dto.getMissingImagesDto().getHiRes2Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes2Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_DELETE2);

			}
		}
	}

	/**
	 * Method to do edit functionalities of HiResImage3
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage3(ProductDto dto, AmazonS3 s3client, String bucketName, String activeHiReFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile3().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag3()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile3(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD3);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag3()) {
				archiveImage(dto.getMissingImagesDto().getHiRes3Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile3(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_EDIT3);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag3()) {
				archiveImage(dto.getMissingImagesDto().getHiRes3Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes3Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_DELETE3);

			}
		}
	}

	/**
	 * Method to do edit functionalities of HiResImage7
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage7(ProductDto dto, AmazonS3 s3client, String bucketName, String activeHiReFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile7().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag7()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile7(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD7);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag7()) {
				archiveImage(dto.getMissingImagesDto().getHiRes7Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile7(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_EDIT7);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag7()) {
				archiveImage(dto.getMissingImagesDto().getHiRes7Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes7Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_DELETE7);

			}
		}
	}

	/**
	 * Method to do edit functionalities of HiResImage8
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage8(ProductDto dto, AmazonS3 s3client, String bucketName, String activeHiReFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile8().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag8()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile8(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD8);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag8()) {
				archiveImage(dto.getMissingImagesDto().getHiRes8Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile8(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_EDIT8);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag8()) {
				archiveImage(dto.getMissingImagesDto().getHiRes8Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes8Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_DELETE8);

			}
		}
	}

	/**
	 * Method to do edit functionalities of HiResImage9
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage9(ProductDto dto, AmazonS3 s3client, String bucketName, String activeHiReFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile9().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag9()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile9(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD9);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag9()) {
				archiveImage(dto.getMissingImagesDto().getHiRes9Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile9(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_EDIT9);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag9()) {
				archiveImage(dto.getMissingImagesDto().getHiRes9Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes9Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_DELETE9);

			}
		}
	}

	/**
	 * Method to do edit functionalities of MockImage1
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage1(ProductDto dto, AmazonS3 s3client, String bucketName, String activeMockFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile1().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag1()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile1(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD1);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag1()) {
				archiveImage(dto.getMissingImagesDto().getMock1Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile1(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_EDIT1);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag1()) {
				archiveImage(dto.getMissingImagesDto().getMock1Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock1Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of MockImage2
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage2(ProductDto dto, AmazonS3 s3client, String bucketName, String activeMockFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile2().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag2()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile2(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD2);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag2()) {
				archiveImage(dto.getMissingImagesDto().getMock2Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile2(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_EDIT2);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag2()) {
				archiveImage(dto.getMissingImagesDto().getMock2Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock2Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE2);

			}
		}
	}

	/**
	 * Method to do edit functionalities of MockImage3
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage3(ProductDto dto, AmazonS3 s3client, String bucketName, String activeMockFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile3().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag3()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile3(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD3);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag3()) {
				archiveImage(dto.getMissingImagesDto().getMock3Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile3(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_EDIT3);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag3()) {
				archiveImage(dto.getMissingImagesDto().getMock3Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock3Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_DELETE3);

			}
		}
	}

	/**
	 * Method to do edit functionalities of MockImage7
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage7(ProductDto dto, AmazonS3 s3client, String bucketName, String activeMockFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile7().isEmpty())) {
			if (dto.getEditImageDto().getMockImageDto().isAddFLag7()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile7(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD7);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag7()) {
				archiveImage(dto.getMissingImagesDto().getMock7Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile7(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_EDIT7);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag7()) {
				archiveImage(dto.getMissingImagesDto().getMock7Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock7Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_DELETE7);

			}
		}
	}

	/**
	 * Method to do edit functionalities of MockImage8
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage8(ProductDto dto, AmazonS3 s3client, String bucketName, String activeMockFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile8().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag8()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile8(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD8);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag8()) {
				archiveImage(dto.getMissingImagesDto().getMock8Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile8(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_EDIT8);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag8()) {
				archiveImage(dto.getMissingImagesDto().getMock8Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock8Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_DELETE8);

			}
		}
	}

	/**
	 * Method to do edit functionalities of MockImage9
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage9(ProductDto dto, AmazonS3 s3client, String bucketName, String activeMockFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile9().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag9()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile9(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD9);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag9()) {
				archiveImage(dto.getMissingImagesDto().getMock9Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile9(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_EDIT9);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag9()) {
				archiveImage(dto.getMissingImagesDto().getMock9Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock9Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_DELETE9);

			}
		}
	}

	/**
	 * Method to do edit functionalities of image JPG1
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editJpgImage1(ProductDto dto, AmazonS3 s3client, String bucketName, String activeJpgFolNm,
			AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getJpgImageDto().getFile1().isEmpty())) {

			if (dto.getEditImageDto().getJpgImageDto().isAddFLag1()) {
				ImageHelper.addImage(dto.getEditImageDto().getJpgImageDto().getFile1(),
						activeJpgFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_JPG_ADD);

			} else if (dto.getEditImageDto().getJpgImageDto().isEditFLag1()) {
				archiveImage(dto.getMissingImagesDto().getJpgUrl(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getJpgImageDto().getFile1(),
						activeJpgFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_JPG_EDIT);

			} else if (dto.getEditImageDto().getJpgImageDto().isDeleteFLag1()) {
				archiveImage(dto.getMissingImagesDto().getJpgUrl(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getJpgUrl(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_JPG_DELETE);

			}
		}
	}

	/**
	 * Method to delete image.
	 * 
	 * @param masterURL
	 * @param s3client
	 * @param bucketName
	 * @throws ImproException
	 */
	public static void deleteImage(String masterURL, AmazonS3 s3client, String bucketName) throws ImproException {
		String srcURL = masterURL.replace(AppConstants.TO_REMOVE1 + bucketName + "/", "");
		try {
			s3client.deleteObject(bucketName, srcURL);
		} catch (AmazonClientException ace) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageHelper deleteImage() AmazonClientException " + ace.getMessage(), s3client, bucketName);
			}
			throw new ImproException(ace.getMessage());

		}
	}

	/**
	 * Copy the folder name with Images to destination path
	 * 
	 * @param pathFolderName
	 * @param sourceImageFolderName
	 */
	public static void archiveImage(String masterURL, AmazonS3 s3client, String bucketName) {
		String srcURL = masterURL.replace(AppConstants.TO_REMOVE1 + bucketName + "/", "");
		String destURL = srcURL.replace(AppConstants.TO_REMOVE2, AppConstants.TO_REPLACE);
		CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, srcURL, bucketName, destURL);
		s3client.copyObject(copyObjRequest);
	}

	/**
	 * Get the newPath for image archival
	 * 
	 * @param byteDate
	 * @return
	 */
	public static String getNewPath(String oldPath, String newFolder, String newFileName) {
		int folderNameStart = StringUtils.ordinalIndexOf(oldPath, "/", 2);
		int folderNameEnd = StringUtils.ordinalIndexOf(oldPath, "/", 3);
		oldPath = oldPath.substring(0, folderNameStart + 1) + newFolder
				+ oldPath.substring(folderNameEnd, oldPath.length());
		int fileNameStart = StringUtils.ordinalIndexOf(oldPath, "/", 3);
		int fileNameEnd = StringUtils.ordinalIndexOf(oldPath, ".", 1);
		String newPath = oldPath.substring(0, fileNameStart + 1) + newFileName
				+ oldPath.substring(fileNameEnd, oldPath.length());
		return newPath;
	}

	// OverLoading above Methods for GateKeeping

	/**
	 * Method to do edit functionalities of GateKeeping StdImage1
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage1(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingStdFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		try {

			if ((!dto.getEditImageDto().getStdImageDto().getFile1().isEmpty())) { // Image from UI

				if (dto.getEditImageDto().getStdImageDto().isAddFLag1()) {
					// Previous image not present && add flag(ADDImage)
					ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile1(),
							gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client,
							bucketName);
					auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, dto.getBitMapName(),
							AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD1);

				} else if (dto.getEditImageDto().getStdImageDto().isEditFLag1()) {
					// Previous image present && add flag(EDITImage)
					archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes1Url(), s3client, bucketName);
					ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile1(),
							gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client,
							bucketName);
					auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES,
							String.valueOf(dto.getBitMapName()), AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD1);

				} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag1()) {// delete flag
					archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes1Url(), s3client, bucketName);
					deleteImage(dto.getMissingImagesDto().getLowRes1Url(), s3client, bucketName);
					auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES,
							String.valueOf(dto.getBitMapName()), AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

				}
			}
		} catch (AmazonServiceException ase) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl editImage() AmazonServiceException " + ase.getMessage(), s3client,
						bucketName);
			}
			throw new ImproException(ase.getMessage());
		} catch (AmazonClientException ace) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl editImage() AmazonClientException " + ace.getMessage(), s3client,
						bucketName);
			}
			throw new ImproException(ace.getMessage());

		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping StdImage2
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage2(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingStdFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getStdImageDto().getFile2().isEmpty())) {

			if (dto.getEditImageDto().getStdImageDto().isAddFLag2()) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile2(),
						gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD2);

			} else if (dto.getEditImageDto().getStdImageDto().isEditFLag2()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes2Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile2(),
						gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD2);

			} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag2()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes2Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getLowRes2Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping StdImage3
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage3(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingStdFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getStdImageDto().getFile3().isEmpty())) {

			if (dto.getEditImageDto().getStdImageDto().isAddFLag3()) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile3(),
						gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD3);

			} else if (dto.getEditImageDto().getStdImageDto().isEditFLag3()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes3Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile3(),
						gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD3);

			} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag3()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes3Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getLowRes3Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping StdImage7
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage7(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingStdFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getStdImageDto().getFile7().isEmpty())) {

			if (dto.getEditImageDto().getStdImageDto().isAddFLag7()) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile7(),
						gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD7);

			} else if (dto.getEditImageDto().getStdImageDto().isEditFLag7()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes7Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile7(),
						gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD7);

			} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag7()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes7Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getLowRes7Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping StdImage8
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage8(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingStdFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getStdImageDto().getFile8().isEmpty())) {

			if (dto.getEditImageDto().getStdImageDto().isAddFLag8()) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile8(),
						gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD8);

			} else if (dto.getEditImageDto().getStdImageDto().isEditFLag8()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes8Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile8(),
						gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD8);

			} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag8()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes8Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getLowRes8Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of HiResImage9
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editStdImage9(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingStdFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getStdImageDto().getFile9().isEmpty())) {

			if (dto.getEditImageDto().getStdImageDto().isAddFLag9()) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile9(),
						gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD9);

			} else if (dto.getEditImageDto().getStdImageDto().isEditFLag9()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes9Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile9(),
						gatekeepingStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD9);

			} else if (dto.getEditImageDto().getStdImageDto().isDeleteFLag9()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getLowRes9Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getLowRes9Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping HiResImage1
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage1(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingHiFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());
		if ((!dto.getEditImageDto().getHiResImageDto().getFile1().isEmpty())) {
			if (dto.getEditImageDto().getHiResImageDto().isAddFLag1()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile1(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD1);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag1()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes1Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile1(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD1);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag1()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes1Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes1Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);
			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping HiResImage2
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage2(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingHiFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile2().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag2()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile2(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD2);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag2()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes2Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile2(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD2);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag2()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes2Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes2Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of HiResImage3
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage3(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingHiFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile3().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag3()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile3(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD3);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag3()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes3Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile3(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD3);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag3()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes3Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes3Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping HiResImage7
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage7(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingHiFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile7().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag7()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile7(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD7);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag7()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes7Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile7(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD7);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag7()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes7Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes7Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping HiResImage8
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage8(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingHiFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile8().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag8()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile8(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD8);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag8()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes8Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile8(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD8);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag8()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes8Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes8Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping HiResImage9
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editHiResImage9(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingHiFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getHiResImageDto().getFile9().isEmpty())) {

			if (dto.getEditImageDto().getHiResImageDto().isAddFLag9()) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile9(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD9);

			} else if (dto.getEditImageDto().getHiResImageDto().isEditFLag9()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes9Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile9(),
						gatekeepingHiFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD9);

			} else if (dto.getEditImageDto().getHiResImageDto().isDeleteFLag9()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getHiRes9Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getHiRes9Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping MockImage1
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage1(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingMockFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile1().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag1()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile1(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD1);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag1()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock1Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile1(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD1);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag1()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock1Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock1Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping MockImage2
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage2(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingMockFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile2().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag2()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile2(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD2);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag2()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock2Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile2(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD2);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag2()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock2Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock2Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping MockImage3
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage3(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingMockFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile3().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag3()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile3(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD3);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag3()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock3Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile3(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD3);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag3()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock3Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock3Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping MockImage7
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage7(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingMockFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile7().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag7()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile7(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD7);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag7()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock7Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile7(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD7);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag7()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock7Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock7Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping MockImage8
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage8(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingMockFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile8().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag8()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile8(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD8);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag8()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock8Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile8(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD8);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag8()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock8Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock8Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Method to do edit functionalities of GateKeeping MockImage9
	 * 
	 * @param dto
	 * @param s3client
	 * @param bucketName
	 * @param activeJpgFolNm
	 * @param auditDao
	 * @throws ImproException
	 */
	public static void editMockImage9(ProductGateKeepingDto dto, AmazonS3 s3client, String bucketName,
			String gatekeepingMockFolNm, AuditLogDao auditDao) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String fileName = ImageHelper.getImageName(dto.getBitMapName());

		if ((!dto.getEditImageDto().getMockImageDto().getFile9().isEmpty())) {

			if (dto.getEditImageDto().getMockImageDto().isAddFLag9()) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile9(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD9);

			} else if (dto.getEditImageDto().getMockImageDto().isEditFLag9()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock9Url(), s3client, bucketName);
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile9(),
						gatekeepingMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client,
						bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD9);

			} else if (dto.getEditImageDto().getMockImageDto().isDeleteFLag9()) {
				archiveGateKeepingImage(dto.getMissingImagesDto().getMock9Url(), s3client, bucketName);
				deleteImage(dto.getMissingImagesDto().getMock9Url(), s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_DELETE1);

			}
		}
	}

	/**
	 * Copy the folder name with Images to destination path
	 * @param pathFolderName
	 * @param sourceImageFolderName
	 */
	public static void archiveGateKeepingImage(String masterURL, AmazonS3 s3client, String bucketName) {
		String srcURL = masterURL.replace(AppConstants.TO_REMOVE1 + bucketName + "/", "");
		String destURL = srcURL.replace(AppConstants.TO_REMOVEGATEKEEPING, AppConstants.TO_REPLACEGATEKEEPING);
		CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, srcURL, bucketName, destURL);
		s3client.copyObject(copyObjRequest);

	}
	
	/**
	 * Get the newPathURL for Gatekeeping images archival
	 * @param oldPath
	 * @return newPathURL
	 */
	public static String getNewImagePath(String oldPath) {
		String newPathURL = oldPath.replace(AppConstants.TO_REMOVEGATEKEEPING, AppConstants.TO_REMOVE2);  
		return newPathURL;
	}
	
	public static String getBase64String(AmazonS3 s3client,final String bucketName, final String key) {
		com.amazonaws.services.s3.model.S3Object s3object = s3client.getObject(bucketName, key);
		String base64Str="";
		StringBuilder sb = new StringBuilder();
		try {
			java.io.InputStream is = s3object.getObjectContent();
			java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
	        byte[] buffer = new byte[1024];
	        int read = 0;
	        while ((read = is.read(buffer, 0, buffer.length)) != -1) {
	            baos.write(buffer, 0, read);
	        }
	        baos.flush();
	       
	        base64Str=new Base64().encodeToString(baos.toByteArray());
	        // check content type of the file
	        java.io.File file = new java.io.File(key);
	        String contentType = java.nio.file.Files.probeContentType(file.toPath());
	        // cretate "data URI"	        
	        sb.append("data:"); 	        
	        sb.append(contentType);
	        sb.append(";base64,");
	        sb.append(base64Str);
	        
		LOG.info("Base64 "+sb.toString());	
		}catch(Exception e) {
			LOG.error("ImageHelper-->getBase64String "+e.getMessage());
		}
		return sb.toString();
	}
	
	
	/**
	 * 
	 * @param s3client
	 * @param prod
	 * @param bucketName
	 * @param sourcePathName
	 * @param sourceImgFolName
	 */
	public static void getBase64ImagesById(AmazonS3 s3client,ProductDto prod, String bucketName,String sourcePathName, String sourceImgFolName) {
		List<S3ObjectSummary> fileList = s3client.listObjects(bucketName, sourcePathName + sourceImgFolName).getObjectSummaries();
		Map<String, String> map = new HashMap<String, String>();
		for (S3ObjectSummary file : fileList) {
			if (!file.getKey().endsWith("/")) {
				String name = file.getKey().substring(file.getKey().lastIndexOf("/") + 1, file.getKey().length());
				map.put(name.substring(0, name.lastIndexOf(".")), file.getKey());
			}
		}
		
		String imageName = ImageHelper.getImageName(prod.getBitMapName());		
		EditImageDto editImageDto=new EditImageDto();
		prod.setEditImageDto(editImageDto);		
		
		if (!map.isEmpty()) {
			if(prod.getImageType().equals(AppConstants.IMAGE_TYPE_STD)) {
				StdImageDto stdImageDto=new StdImageDto();
				editImageDto.setStdImageDto(stdImageDto);
				if(map.get(imageName + AppConstants.IMAGE_VIEW1)!=null) 
					stdImageDto.setFile1(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW1)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW2)!=null) 
					stdImageDto.setFile2(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW2)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW3)!=null) 
					stdImageDto.setFile3(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW3)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW7)!=null) 
					stdImageDto.setFile7(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW7)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW8)!=null) 
					stdImageDto.setFile8(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW8)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW9)!=null) 
					stdImageDto.setFile9(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW9)));				
			}else if(prod.getImageType().equals(AppConstants.IMAGE_TYPE_HIRES)) {
				HiResImageDto dto =new HiResImageDto();
				editImageDto.setHiResImageDto(dto);
				if(map.get(imageName + AppConstants.IMAGE_VIEW1)!=null) 
					dto.setFile1(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW1)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW2)!=null) 
					dto.setFile2(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW2)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW3)!=null) 
					dto.setFile3(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW3)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW7)!=null) 
					dto.setFile7(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW7)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW8)!=null) 
					dto.setFile8(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW8)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW9)!=null) 
					dto.setFile9(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW9)));
			}else if(prod.getImageType().equals(AppConstants.IMAGE_TYPE_MOCK)) {
				MockImageDto dto=new MockImageDto();
				editImageDto.setMockImageDto(dto);
				if(map.get(imageName + AppConstants.IMAGE_VIEW1)!=null) 
					dto.setFile1(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW1)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW2)!=null) 
					dto.setFile2(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW2)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW3)!=null) 
					dto.setFile3(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW3)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW7)!=null) 
					dto.setFile7(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW7)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW8)!=null) 
					dto.setFile8(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW8)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW9)!=null) 
					dto.setFile9(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW9)));
			}
			else if(prod.getImageType().equals(AppConstants.IMAGE_TYPE_STD_MOCK)) {
				StdImageDto stdImageDto=new StdImageDto();
				editImageDto.setStdImageDto(stdImageDto);
				if(map.get(imageName + AppConstants.IMAGE_VIEW1)!=null) 
					stdImageDto.setFile1(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW1)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW2)!=null) 
					stdImageDto.setFile2(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW2)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW3)!=null) 
					stdImageDto.setFile3(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW3)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW7)!=null) 
					stdImageDto.setFile7(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW7)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW8)!=null) 
					stdImageDto.setFile8(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW8)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW9)!=null) 
					stdImageDto.setFile9(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW9)));
				
				MockImageDto dto=new MockImageDto();
				editImageDto.setMockImageDto(dto);
				if(map.get(imageName + AppConstants.IMAGE_VIEW1)!=null) 
					dto.setFile1(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW1)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW2)!=null) 
					dto.setFile2(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW2)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW3)!=null) 
					dto.setFile3(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW3)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW7)!=null) 
					dto.setFile7(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW7)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW8)!=null) 
					dto.setFile8(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW8)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW9)!=null) 
					dto.setFile9(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW9)));
			}
		}
	}
	
	/**
	 * 
	 * @param s3client
	 * @param gatekeepingDto
	 * @param bucketName
	 * @param sourcePathName
	 * @param sourceImgFolName
	 */
	public static void getBase64ImagesById(AmazonS3 s3client, ProductGateKeepingDto gatekeepingDto, String bucketName,String sourcePathName, String sourceImgFolName) {
		List<S3ObjectSummary> fileList = s3client.listObjects(bucketName, sourcePathName + sourceImgFolName).getObjectSummaries();
		Map<String, String> map = new HashMap<String, String>();
		for (S3ObjectSummary file : fileList) {
			if (!file.getKey().endsWith("/")) {
				String name = file.getKey().substring(file.getKey().lastIndexOf("/") + 1, file.getKey().length());
				map.put(name.substring(0, name.lastIndexOf(".")), file.getKey());
			}
		}
		
		String imageName = ImageHelper.getImageName(gatekeepingDto.getProductId());		
		EditImageDto editImageDto=new EditImageDto();
		if (!map.isEmpty()) {
			if(gatekeepingDto.getImageType().equals(AppConstants.IMAGE_TYPE_STD)) {
				StdImageDto stdImageDto=new StdImageDto();
				if(map.get(imageName + AppConstants.IMAGE_VIEW1)!=null) 
					stdImageDto.setFile1(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW1)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW2)!=null) 
					stdImageDto.setFile2(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW2)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW3)!=null) 
					stdImageDto.setFile3(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW3)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW7)!=null) 
					stdImageDto.setFile7(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW7)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW8)!=null) 
					stdImageDto.setFile8(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW8)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW9)!=null) { 
					stdImageDto.setFile9(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW9)));
				}
				editImageDto.setStdImageDto(stdImageDto);
			}else if(gatekeepingDto.getImageType().equals(AppConstants.IMAGE_TYPE_HIRES)) {
				HiResImageDto dto =new HiResImageDto();
				if(map.get(imageName + AppConstants.IMAGE_VIEW1)!=null) 
					dto.setFile1(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW1)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW2)!=null) 
					dto.setFile2(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW2)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW3)!=null) 
					dto.setFile3(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW3)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW7)!=null) 
					dto.setFile7(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW7)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW8)!=null) 
					dto.setFile8(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW8)));
				if(map.get(imageName + AppConstants.IMAGE_VIEW9)!=null) { 
					dto.setFile9(getBase64String(s3client,bucketName,map.get(imageName + AppConstants.IMAGE_VIEW9)));
				}
				editImageDto.setHiResImageDto(dto);
			}					
		}
		gatekeepingDto.setEditImageDto(editImageDto);
	}
}











