package com.ko.impro.service.category;

import java.util.List;

import com.ko.impro.dto.CategoryDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * CategoryService class for handling CRUD operation for Category
 * 
 */
public interface CategoryService {

	/**
	 * Load the all the categories
	 * @param boolean isMaintainence
	 * @return List<CategoryDto>
	 * @throws ImproException
	 */
	List<CategoryDto> loadCategory() throws ImproException;

	/**
	 * Method to add category
	 * @param categoryDto
	 * @throws ImproException
	 */
	void addCategory(CategoryDto categoryDto) throws ImproException;

	/**
	 * Method to delete category
	 * @param categoryDto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteCategory(CategoryDto categoryDto) throws ImproException;

	/**
	 * Method to edit category
	 * @param categoryDto
	 * @return String message
	 * @throws ImproException
	 */
	String editCategory(CategoryDto categoryDto) throws ImproException;
	
	/**
	 * Method to validate duplicate category
	 * @param categoryDesc
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicateCategory(String categoryDesc) throws ImproException;
	/**
	 * Load the Category
	 * @param catId
	 * @return CategoryDto
	 * @throws ImproException
	 */
	CategoryDto loadCategory(int catId) throws ImproException;

}
