package com.ko.impro.service.shape;

import java.util.List;

import com.ko.impro.dto.ShapeDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Service class for doing CRUD operation for Shapes
 *
 */
public interface ShapeService {

	/**
	 * Load the Shapes
	 * @return List<ShapeDto>
	 * @throws ImproException
	 */
	List<ShapeDto> loadShapes() throws ImproException;
	
	/***
	 * Load the Shapes
	 * @param shapeId
	 * @return ShapesDto
	 * @throws ImproException
	 */
	ShapeDto loadShapeID(int shapeId) throws ImproException;

	/**
	 * Add the Shape
	 * @param shapeDto
	 * @throws ImproException
	 */
	void addShapes(ShapeDto shapeDto) throws ImproException;

	/**
	 * Delete the Shape
	 * @param shapedto
	 * @throws ImproException
	 */
	String deleteShapes(ShapeDto shapeDto) throws ImproException;

	/**
	 * Checking the duplicate of Shape
	 * @param name
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicateShape(String name) throws ImproException;

	/**
	 * Edit the Shape
	 * @param shapeDto
	 * @return String
	 * @throws ImproException
	 */
	String editShapes(ShapeDto dto) throws ImproException;


}
