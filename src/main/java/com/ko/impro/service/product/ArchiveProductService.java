package com.ko.impro.service.product;

import java.util.List;

import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.ArchivedProductResponseDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.exception.ImproException;


/**
 * @author hlekshmy
 * Interface for ArchiveProduct Service API
 */
public interface ArchiveProductService {

	/**
	 * Method to archive the products to the product table
	 * @param List<ProductDto> dtoList
	 * @param String deleteReason
	 * @param String userId
	 * @throws ImproException
	 */
	void archiveProducts(List<ProductDto> dtoList, String deleteReason, String userId) throws ImproException;
	
	
	/**
	 * This method will return archived product list based on the input 
	 * and displays total record count
	 * @param ProductSearchDto productSearchDto
	 * @return ArchivedProductResponseDto
	 * @throws ImproException
	 */
	ArchivedProductResponseDto searchArchivedProducts(ProductSearchDto productSearchDto) throws ImproException;	
	
	/**
	 * This method will return additional details of a particular archived product
	 * @param ArchivedProductDto deletedProductDto
	 * @return ArchivedProductDto
	 * @throws ImproException
	 */
	ArchivedProductDto getArchivedProductDetails(String id) throws ImproException;
	
	/**
	 * This method will return similar product
	 * @param ProductSearchDto productSearchDto
	 * @return List<ArchivedProductDto>
	 * @throws ImproException
	 */
	List<ArchivedProductDto> getSimilarArchivedProducts(ProductSearchDto productSearchDto) throws ImproException;	
	
	/**
	 * Method used to export all Archived product details when the user clicks 
	 * the export button directly
	 * without selecting any check boxes.
	 * @param ProductSearchDto productSearchDto
	 * @return List<ArchivedProductDto>
	 * @throws ImproException
	 */
	List<ArchivedProductDto> exportArchivedAllProducts(ProductSearchDto productSearchDto) throws ImproException;
	
	/**
	 * Method used to purge all the selected archived products
	 * @param List<ArchivedProductDto> archivedProdDtoList
	 * @throws ImproException
	 */
	void purgeArchivedProducts(List<ArchivedProductDto> archivedProdDtoList) throws ImproException;


}


	

	


