package com.ko.impro.service.product;

import java.util.List;

import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductResponseDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Interface for Product Service API
 */
public interface ProductService {
	
	/**
	 * The method to restore the archived products from the product table
	 * @param ArchivedProductDto dto
	 * @param boolean
	 * @throws ImproException
	 */
	boolean restoreProducts(ArchivedProductDto dto) throws ImproException;
		
	/**
	 * This method will search products based on the filter input 
	 * @param ProductSearchDto productSearchDto
	 * @return ProductResponseDto
	 * @throws ImproException
	 */
	ProductResponseDto searchProducts(ProductSearchDto productSearchDto) throws ImproException;	
	
	/**
	 * This method will fetch additional product information and store in the same object
	 * @param ProductDto productDto
	 * @return ProductDto
	 * @throws ImproException
	 */
	ProductDto getProductDetails(String id) throws ImproException;
	
	/**
	 * This method will fetch similar products based on the filters
	 * @param ProductSearchDto productSearchDto
	 * @return ProductDto
	 * @throws ImproException
	 */
	List<ProductDto> getSimilarProductDetails(ProductSearchDto productSearchDto) throws ImproException;

	/**
	 * Method to update product details
	 * @param ProductDto productDto
	 * @throws ImproException
	 */
	void editProduct(ProductDto productDto) throws ImproException;
	
	/**
	 * Method used to add/clone a product
	 * @param ProductDto dto
	 * @throws ImproException
	 * @throws PersistenceException
	 */
	void addProduct(ProductDto dto) throws ImproException;
	
	/**
	 * Method to check duplicate product
	 * @param upc
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicateProduct(String upc) throws ImproException;
	
	/**
	 * Method used to export all product details when the users clicks  exportButton without selecting any check boxes.
	 * @param ProductSearchDto productSearchDto
	 * @return List<ProductDto>
	 * @throws ImproException
	 */
	List<ProductDto> exportAllProducts(ProductSearchDto productSearchDto) throws ImproException;
	
	/**
	 * This method will save multiple products
	 * @param ProductDto
	 * @param List<ProductDto> 
	 * @throws ImproException
	 */	
	void multiEditProduct(ProductDto productDto,List<ProductDto> dtoList) throws ImproException;


}






