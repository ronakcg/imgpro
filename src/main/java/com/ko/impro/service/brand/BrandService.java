package com.ko.impro.service.brand;

import java.util.List;

import com.ko.impro.dto.BrandDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * BrandService class for handling CRUD operation for Brand
 * 
 */
public interface BrandService {

	/**
	 * Load Brands based 
	 * @param isMaintainence 
	 * @return List<BrandDto>
	 * @throws ImproException
	 */
	List<BrandDto> loadBrands() throws ImproException;

	/**
	 * Load Brands based on manufacturerIdList
	 * @param manufactIdList
	 * @return List<BrandDto>
	 * @throws ImproException
	 */
	List<BrandDto> loadBrands(List<String> manufactIdList) throws ImproException;
	
	/**
	 * This method returns a particular brand details based on the brandcode
	 * @param brandCode
	 * @return BrandDto
	 * @throws ImproException
	 */
	BrandDto getBrandDetails(BrandDto brandDto) throws ImproException;

	/**
	 * Add the Brand
	 * @param brandDto
	 * @throws ImproException
	 */
	void addBrand(BrandDto brandDto) throws ImproException;

	/**
	 * Deelete the brand
	 * @param id
	 * @throws ImproException
	 */
	String deleteBrand(BrandDto dto) throws ImproException;

	/**
	 * Edit the brand
	 * @param brandDto
	 * @return String
	 * @throws ImproException
	 */
	String editBrand(BrandDto brandDto) throws ImproException;
	
	/**
	 * Load the brand
	 * @param brandId
	 * @return BrandDto
	 * @throws ImproException
	 */
	BrandDto loadBrand(int brandId) throws ImproException;
	
	/**
	 * Checking duplicate brand
	 * @param name
	 * @param isLong
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicateBrand(String name, boolean isLong) throws ImproException;
	
	/**
	 * Checking old manufacturer with brand present in product or not
	 * @param brandDto
	 * @return boolean
	 * @throws ImproException
	 */
	boolean validateInProduct(BrandDto brandDto) throws ImproException ;

	

}
