package com.ko.impro.service.image;

import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.exception.ImproException;

/**
 * @author swshedge
 * Service Class for GateKeepingImage Interface
 *
 */
public interface ImageGatekeepingService {
	
	
	/**
	 * Method to fetch new and existing images URL
	 * @param prodGateDto
	 */
	void getImagesById(ProductGateKeepingDto prodGateDto) throws ImproException;
	
	/**
	 * Method to edit image
	 * @param prodGateDto
	 * @throws ImproException
	 */
	void editImage(ProductGateKeepingDto prodGateDto) throws ImproException;
	
	/**
	 * Method to push Images Manual into destination path (Gatekeeping images archival)
	 * @param dto
	 */
	void pushImagesManual(ProductGateKeepingDto dto) throws ImproException;
	
	
	/**
	 * Method to get the base24 format of the images
	 * @param gatekeepingDto
	 * @throws ImproException
	 */
	void getAllBase64String(ProductGateKeepingDto gatekeepingDto) throws ImproException;


}
