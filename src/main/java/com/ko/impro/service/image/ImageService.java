package com.ko.impro.service.image;

import java.io.File;
import java.util.List;

import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy ImageServiceImpl class for handling operation related to
 *         image manipulation
 * 
 */
public interface ImageService {

	/**
	 * Method used to add Image
	 * 
	 * @param keyName
	 * @param image
	 */
	void addImage(String keyName, File image);

	/**
	 * Method to get the url path of .1 Std/or .1 Mock images to display in grid
	 * view
	 * 
	 * @param prodList
	 */
	void getGridViewImages(List<ProductDto> prodList);

	/**
	 * Method to get the url path of all images to display in view only mode
	 * 
	 * @param prod
	 */
	void getProductImagesById(ProductDto prod);

	/**
	 * Method to archive images.Images from active folder will be moved to archive
	 * folder
	 * 
	 * @param prodList
	 * @throws ImproException
	 */
	void archiveImages(List<ProductDto> prodList) throws ImproException;

	/**
	 * Method to restore images.Image from archive folder will be moved to active
	 * folder
	 * 
	 * @param prod
	 */
	void restoreImages(ArchivedProductDto prod);

	/**
	 * Method to purge Images.The images and image folder in the archived folder
	 * will be deleted permanently
	 * 
	 * @param archProdList
	 */
	void purgeImages(List<ArchivedProductDto> archProdList);

	
	/**Method to add image using productDto
	 * @param dto
	 * @throws ImproException
	 */
	void addImage(ProductDto dto) throws ImproException;

	/**Method to edit images
	 * @param dto
	 * @throws ImproException
	 */
	void editImage(ProductDto dto) throws ImproException;

	/**Method to rename images when bitmapname is changed
	 * @param dto
	 * @throws ImproException
	 */
	void bitmapChange(ProductDto dto) throws ImproException;
	
	void getAllBase64String(ProductDto productDto)throws ImproException;
}
