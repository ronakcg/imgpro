package com.ko.impro.service.manufacturer;

import java.util.List;

import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Service class for doing CRUD operation for Manufacuturer
 *
 */
public interface ManufacuturerService {

	/**
	 * Load the Manufacuturers
	 * @param isMaintainenance 
	 * @return List<ManufacturerDto>
	 * @throws ImproException
	 */
	List<ManufacturerDto> loadManufacuturer() throws ImproException;
	
	/**
	 * Add the Manufacuturer
	 * @param manufacturerDto
	 * @throws ImproException
	 */
	void addManufacuturer(ManufacturerDto manufacturerDto) throws ImproException;

	/**
	 * Delete the Manufacuturer
	 * @param id
	 * @throws ImproException
	 */
	String deleteManufacuturer(ManufacturerDto dto) throws ImproException;

	/**
	 * Edit the Manufacuturer
	 * @param manufacturerDto
	 * @return String
	 * @throws ImproException
	 */
	String editManufacuturer(ManufacturerDto manufacturerDto) throws ImproException;
	
	/**
	 * Load the ManufacturerDto
	 * @param manufCode
	 * @return ManufacturerDto
	 * @throws ImproException
	 */
	ManufacturerDto loadManufacuturer(int manufCode) throws ImproException;
	
	/**
	 * Checking the duplicate Manufacuturer
	 * @param name
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicateManufacuturer(String name) throws ImproException;

}
