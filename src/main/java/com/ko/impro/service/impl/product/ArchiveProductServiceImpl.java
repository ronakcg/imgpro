package com.ko.impro.service.impl.product;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.dao.product.ArchiveProductDao;
import com.ko.impro.dao.product.ProductDao;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.ArchivedProductResponseDto;
import com.ko.impro.dto.PaginationDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.product.ArchiveProductService;

/**
 * @author nvohra
 * Implementation of ArchiveProduct methods at Service Layer.
 */
@Service
public class ArchiveProductServiceImpl implements ArchiveProductService {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(ArchiveProductServiceImpl.class);
	
	@Autowired
	private  ArchiveProductDao archiveProdDao;
	
	@Autowired
	private  ProductDao prodDao;
	
	
	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void archiveProducts(List<ProductDto> dtoList,String deleteReason,String userId) throws ImproException {
		LOG.info("ProductServiceImpl-->archiveProducts()-->Begin");
		archiveProdDao.archiveProducts(dtoList, deleteReason, userId);
		
		//TODO
		//Archive the images
		//Arhive DAO logic modifications for record update/modifications?
		
		LOG.info("ProductServiceImpl-->archiveProducts()-->End");
	}
	
	@Override
	public ArchivedProductResponseDto searchArchivedProducts(ProductSearchDto productSearchDto) throws ImproException {
		LOG.info("ProductServiceImpl-->searchArchivedProducts()-->Begin");
		ArchivedProductResponseDto obj= new ArchivedProductResponseDto();
		productSearchDto.setActiveSearch(false);
		final int rowRowNumber=productSearchDto.getPageDto().getRowNumber();
		if(rowRowNumber == 0) {
			PaginationDto pageObj=new PaginationDto();
			pageObj.setTotalNoOfRecords(prodDao.getTotalCountForProducts(productSearchDto));	
			obj.setPageDto(pageObj);
		}
		List<ArchivedProductDto> archProdDtoList=prodDao.searchArchivedProducts(productSearchDto,true);
		obj.setArchiveProductDtoList(archProdDtoList);
		
		//TODO
		//Archive the image to temp archive folder
		//Search should pull latest record?
		LOG.info("ProductServiceImpl-->searchArchivedProducts()-->End");
		return obj;
	}
	
	
	@Override
	public ArchivedProductDto getArchivedProductDetails(String id) throws ImproException {
		return archiveProdDao.getArchivedProductDetails(id);
	}

	
	@Override
	public List<ArchivedProductDto> getSimilarArchivedProducts(ProductSearchDto productSearchDto)
			throws ImproException {
		productSearchDto.setActiveSearch(false);
		return prodDao.searchArchivedProducts(productSearchDto, false);
	}

	@Override
	public List<ArchivedProductDto> exportArchivedAllProducts(ProductSearchDto productSearchDto) throws ImproException{
		LOG.info("ProductServicempl-->exportArchivedAllProducts()-->Begin");
		productSearchDto.setActiveSearch(false);
		List<ArchivedProductDto> prodList=prodDao.searchArchivedProducts(productSearchDto, false);
		LOG.info("ProductServicempl-->exportArchivedAllProducts()-->End");
		return prodList;

	}

	@Override
	public void purgeArchivedProducts(List<ArchivedProductDto> archiProdDtoList) throws ImproException {
		LOG.info("ArchviedProductServicempl-->purgeArchivedProducts()-->Begin");
		archiveProdDao.purgeArchivedProducts(archiProdDtoList);
		
		//TODO
		//Delete the archived images also
		LOG.info("ArchviedProductServicempl-->purgeArchivedProducts()-->End");
	}



}
