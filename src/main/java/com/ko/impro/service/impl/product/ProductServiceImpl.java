package com.ko.impro.service.impl.product;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ko.impro.dao.product.ProductDao;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.PaginationDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductResponseDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.model.product.Products;
import com.ko.impro.service.product.ProductService;
import com.ko.impro.util.ApplicationUtil;

/**
 * @author nvohra
 * Implementation of Product methods at Service Layer.
 */
@Service
public class ProductServiceImpl implements ProductService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);

	/**
	 * Product Dao Reference
	 */
	@Autowired
	private ProductDao productDao;

	@Override
	public boolean restoreProducts(ArchivedProductDto dto) throws ImproException {
		LOG.info("ProductServiceImpl-->restoreProducts()-->Begin");
		boolean isDuplicate=productDao.restoreProduct(dto);
		
		//TODO 
		//restore the images
		//Validate the record before restore
		//if invalid in the UI correct it and save.
		//re write the restore logic
		LOG.info("ProductServiceImpl-->restoreProducts()-->End");
		return isDuplicate;
	}

	@Override
	public ProductResponseDto searchProducts(ProductSearchDto productSearchDto) throws ImproException {
		LOG.info("ProductServiceImpl-->searchProducts()-->Begin");
		ProductResponseDto obj = new ProductResponseDto();
		productSearchDto.setActiveSearch(true);
		int rowRowNumber=productSearchDto.getPageDto().getRowNumber();
		if(rowRowNumber == 0) {
			PaginationDto pageObj = new PaginationDto();
			int count = productDao.getTotalCountForProducts(productSearchDto);
			pageObj.setTotalNoOfRecords(count);
			obj.setPageDto(pageObj);
		} 
		
		List<ProductDto> productDtoList = productDao.searchProducts(productSearchDto, true);
		obj.setProductDtoList(productDtoList);
		if(productSearchDto.isGridView()) {
			//TODO
			//.1 Standard image or if that does not exist, then the .1 Mock image
		}
		LOG.info("ProductServiceImpl-->searchProducts()-->End");
		return obj;

	}

	@Override
	public ProductDto getProductDetails(String id) throws ImproException {
		return productDao.getProductDetails(id);
	}

	@Override
	public List<ProductDto> getSimilarProductDetails(ProductSearchDto productSearchDto) throws ImproException {
		productSearchDto.setActiveSearch(true);
		return productDao.searchProducts(productSearchDto, false);
	}

	@Override
	@Transactional(rollbackFor = ImproException.class)
	public void addProduct(ProductDto dto) throws ImproException {
		LOG.info("ProductServicempl-->addProduct()-->Begin");
		Products product = ApplicationUtil.convertToProduct(dto);
		productDao.addProduct(product);

		LOG.info("ProductServicempl-->addProduct()-->END");

	}

	@Override
	public void editProduct(ProductDto productDto) throws ImproException {
		productDao.editProduct(productDto);
	}
	
	@Override
	public List<ProductDto> exportAllProducts(ProductSearchDto productSearchDto) throws ImproException {
		LOG.info("ProductServicempl-->exportAllProducts()-->Begin");
		productSearchDto.setActiveSearch(true);
		List<ProductDto> prodList=productDao.searchProducts(productSearchDto, false);
		LOG.info("ProductServicempl-->exportAllProducts()-->End");
		return prodList;

	}

	@Override
	public boolean checkDuplicateProduct(String upc) throws ImproException {
		return productDao.checkDuplicateProduct(upc);
	}

	@Override
	@Transactional(rollbackFor=ImproException.class)
	public void multiEditProduct(ProductDto productdto, List<ProductDto> productDtoList)throws ImproException {
		LOG.info("ProductServicempl-->multiEditProduct()-->Begin");
		productDao.multiEditProduct(productdto,productDtoList);
		LOG.info("ProductServicempl-->multiEditProduct()-->END");
	}
}



