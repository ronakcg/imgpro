package com.ko.impro.service.impl.packagestyle;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.packagestyle.PackageStyleDao;
import com.ko.impro.dto.PackageStyleDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.packagestyle.PackageStyleService;

/**
 * @author hlekshmy : 
 * This class is an implementation of PackageStyleService Interface
 */
@Service
public class PackageStyleServiceImpl implements PackageStyleService{

	/** Reference of PackageStyleDao */
	@Autowired
	private  PackageStyleDao pkgStyleDao;
	
	@Override
	public List<PackageStyleDto> loadPackageStyle() throws ImproException {
		return pkgStyleDao.loadPackageStyle();
	}

	@Override
	public void addPackageStyle(PackageStyleDto packageStyleDto) throws ImproException {
		pkgStyleDao.addPackageStyle(packageStyleDto);
	}

	@Override
	public String deletePackageStyle(PackageStyleDto packageStyleDto) throws ImproException {
		return pkgStyleDao.deletePackageStyle(packageStyleDto);
	}

	@Override
	public String editPackageStyle(PackageStyleDto dto) throws ImproException {
		return pkgStyleDao.editPackageStyle(dto);
	}
	
	
	@Override
	public PackageStyleDto loadPackageStyle(int pkgStyleid) throws ImproException {
		return pkgStyleDao.loadPackageStyle(pkgStyleid);
	}


	@Override
	public boolean checkDuplicatePackageStyle(String name) throws ImproException {
		return pkgStyleDao.checkDuplicatePackageStyle(name);
	}
}