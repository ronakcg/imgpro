package com.ko.impro.service.impl.flavour;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.flavour.FlavorDao;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.flavour.FlavorService;

/**
 * @author hlekshmy
 * Implementation Class for FlavorService
 */
@Service
public class FlavorServiceImpl implements FlavorService {

	/** Reference of flavorDao */
	@Autowired
	private FlavorDao flavorDao;
	
	@Override
	public List<FlavorDto> loadFlavor() throws ImproException {
		return flavorDao.loadFlavor();
	}

	@Override
	public List<FlavorDto> loadFlavor(List<String> subCategoryList) throws ImproException {
		return flavorDao.loadFlavor(subCategoryList);
	}

	@Override
	public int addFlavor(FlavorDto dto) throws ImproException {
		 return flavorDao.addFlavor(dto);
	}

	@Override
	public void addFlavorSubCategory(FlavorDto flavorDto) throws ImproException {
		 flavorDao.addFlavorSubCategory(flavorDto);
	}

	
	@Override
	public String deleteFlavor(FlavorDto dto) throws ImproException {
		return flavorDao.deleteFlavor(dto);
	}

	@Override
	public String editFlavor(FlavorDto dto) throws ImproException {
		return flavorDao.editFlavor(dto);
	}

	@Override
	public FlavorDto loadFlavor(int flavorId) throws ImproException {
		return flavorDao.loadFlavor(flavorId);
	}

	@Override
	public boolean checkDuplicateFlavor(String name) throws ImproException {
		return flavorDao.checkDuplicateFlavor(name);
	}
	
}
