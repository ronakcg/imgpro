package com.ko.impro.service.impl.extendedproductdesc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.extendedproductdesc.ExtendedProductDescDao;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.extendedproductdesc.ExtendedProductDescService;
/**
 * @author hlekshmy : 
 * This class is an implementation of ExtendedProductDescService Interface
 */

@Service
public class ExtendedProductDescServiceImpl implements ExtendedProductDescService {
	
	/** Reference of extProdDao */
	@Autowired
	private  ExtendedProductDescDao extProdDao;
	
	@Override
	public List<ExtendedProductDescDto> loadExtendedProductDesc() throws ImproException {
		return extProdDao.loadExtendedProductDesc();
	}

	@Override
	public List<ExtendedProductDescDto> loadExtendedProductDesc(List<String> brandList) throws ImproException {
		return extProdDao.loadExtendedProductDesc(brandList);

	}

	@Override
	public void addExtendedProductDesc(ExtendedProductDescDto ExtdProdDescDto) throws ImproException {
		 extProdDao.addExtendedProductDesc(ExtdProdDescDto);
		
	}
	
	@Override
	public String deleteExtendedProductDesc(ExtendedProductDescDto ExtdProdDescDto) throws ImproException {
		 return extProdDao.deleteExtendedProductDesc(ExtdProdDescDto);
	}

	@Override
	public String editExtendedProductDesc(ExtendedProductDescDto dto) throws ImproException {
		 return extProdDao.editExtendedProductDesc(dto);

	}

	@Override
	public ExtendedProductDescDto loadExtendedProductDesc(int ExtdProdDescId) throws ImproException {
		 return extProdDao.loadExtendedProductDesc(ExtdProdDescId);

	}

	@Override
	public boolean checkDuplicateExtendedProductDesc(String name,boolean isLongDesc) throws ImproException {
		 return extProdDao.checkDuplicateExtendedProductDesc(name,isLongDesc);

	}

	

	
}
