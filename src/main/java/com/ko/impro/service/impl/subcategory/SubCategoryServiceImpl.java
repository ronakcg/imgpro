package com.ko.impro.service.impl.subcategory;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.subcategory.SubCategoryDao;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.subcategory.SubCategoryService;

/**
 * @author hlekshmy : 
 * This class is an implementation of SubCategoryService Interface
 */
@Service
public class SubCategoryServiceImpl implements SubCategoryService {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(SubCategoryServiceImpl.class);

	/** Reference of SubCategoryDao */
	@Autowired
	private SubCategoryDao subCategoryDao;

	@Override
	public List<SubCategoryDto> loadSubCategory() throws ImproException {
		return subCategoryDao.loadSubCategory();
	}

	@Override
	public List<SubCategoryDto> loadSubCategory(List<String> categoryList) throws ImproException {
		return subCategoryDao.loadSubCategory(categoryList);

	}

	@Override
	public int addSubCategory(SubCategoryDto subcategoryDto) throws ImproException {
		LOG.info("Sub-CategoryServiceImpl-->addSubCategory()-->Begin");
		int code = subCategoryDao.addSubCategory(subcategoryDto);
		LOG.info("Sub-CategoryServiceImpl-->addSubCategory()-->End");
		return code;
	}

	@Override
	public String deleteSubCategory(SubCategoryDto subcategoryDto) throws ImproException {
		LOG.info("Sub-CategoryServiceImpl-->deleteSubCategory()-->Begin");
		String msg = subCategoryDao.deleteSubCategory(subcategoryDto);
		LOG.info("Sub-CategoryServiceImpl-->deleteSubCategory()-->End");
		return msg;
	}

	@Override
	public String editSubCategory(SubCategoryDto subcategoryDto) throws ImproException {
		LOG.info("Sub-CategoryServiceImpl-->editSubCategory()-->Begin");
		String msg = subCategoryDao.editSubCategory(subcategoryDto);
		LOG.info("Sub-CategoryServiceImpl-->editSubCategory()-->End");
		return msg;
	}

	@Override
	public boolean checkDuplicateSubCategory(String subCategoryDesc) throws ImproException {
		LOG.info("Sub-CategoryServiceImpl-->checkDuplicateSubCategory()-->Begin");
		boolean flag = subCategoryDao.checkDuplicateSubCategory(subCategoryDesc);
		LOG.info("Sub-CategoryServiceImpl-->checkDuplicateSubCategory()-->End");
		return flag;
	}

	@Override
	public void addCategoyMapping(SubCategoryDto subcategoryDto, int genSubCatCode) throws ImproException {
		LOG.info("Sub-CategoryServiceImpl-->addCategoyMapping()-->Begin");
		subCategoryDao.addCategoyMapping(subcategoryDto,genSubCatCode);
		LOG.info("Sub-CategoryServiceImpl-->addCategoyMapping()-->End");
	}

	@Override
	public SubCategoryDto loadSubCategory(int subCatCode) throws ImproException {
		LOG.info("Sub-CategoryServiceImpl-->loadSubCategory()-->Begin");
		SubCategoryDto dto = subCategoryDao.loadSubCategory(subCatCode);
		LOG.info("Sub-CategoryServiceImpl-->loadSubCategory()-->End");
		return dto;
	}

}
