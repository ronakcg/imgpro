package com.ko.impro.service.impl.manufacturer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.manufacturer.ManufacuturerDao;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.manufacturer.ManufacuturerService;

/**
 * @author hlekshmy : 
 * This class is an implementation of ManufacuturerService Interface
 */
@Service
public class ManufacturerServiceImpl  implements ManufacuturerService{
	
	/** Reference of Logger */
	private static final Logger LOG = LoggerFactory.getLogger(ManufacturerServiceImpl.class);
	
	/** Reference of ManufacuturerDao */
	@Autowired
	private  ManufacuturerDao manufDao;

	@Override
	public List<ManufacturerDto> loadManufacuturer() throws ImproException {
		return manufDao.loadManufacuturer();
	}

	@Override
	public void addManufacuturer(ManufacturerDto manufacturerDto) throws ImproException {
		LOG.info("ManufacturerServiceImpl-->addManufacuturer()-->Begin");
		manufDao.addManufacuturer(manufacturerDto);
		LOG.info("ManufacturerServiceImpl-->addManufacuturer()-->End");		
	}

	@Override
	public String deleteManufacuturer(ManufacturerDto dto) throws ImproException {		
		return manufDao.deleteManufacuturer(dto);
	}

	@Override
	public String editManufacuturer(ManufacturerDto manufacturerDto) throws ImproException {
		LOG.info("ManufacturerServiceImpl-->editManufacuturer()-->Begin");
		String msg= manufDao.editManufacuturer(manufacturerDto);
		LOG.info("ManufacturerServiceImpl-->editManufacuturer()-->End");
		return msg;
	}

	@Override
	public ManufacturerDto loadManufacuturer(int manufacturId) throws ImproException {
		LOG.info("ManufacturerServiceImpl-->loadManufacuturer()-->Begin");
		ManufacturerDto dto=manufDao.loadManufacuturer(manufacturId);
		LOG.info("ManufacturerServiceImpl-->loadManufacuturer()-->End");
		return dto;
	}

	@Override
	public boolean checkDuplicateManufacuturer(String name) throws ImproException {
		LOG.info("ManufacturerServiceImpl-->checkDuplicateManufacuturer()-->Begin");
		boolean flag=manufDao.checkDuplicateManufacuturer(name);
		LOG.info("ManufacturerServiceImpl-->checkDuplicateManufacuturer()-->End");
		return flag;
	}

}
