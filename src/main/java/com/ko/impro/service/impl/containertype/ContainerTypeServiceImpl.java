package com.ko.impro.service.impl.containertype;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.containerType.ContainerTypeDao;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.containertype.ContainerTypeService;
/**
 * @author hlekshmy : 
 * This class is an implementation of ContainerTypeService Interface
 */

@Service
public class ContainerTypeServiceImpl implements ContainerTypeService {
	
	/** Reference of containerTypeDao */
	@Autowired
	private  ContainerTypeDao containerTypeDao;

	@Override
	public List<ContainerTypeDto> loadContainerTypes() throws ImproException {
		return containerTypeDao.loadContainerTypes();
	}

	@Override
	public void addContainerType(ContainerTypeDto containerTypeDto) throws ImproException {
		containerTypeDao.addContainerType(containerTypeDto);
	}

	@Override
	public String deleteContainerType(ContainerTypeDto containerTypeDto) throws ImproException {
		return containerTypeDao.deleteContainerType(containerTypeDto);
	}

	@Override
	public String editContainerType(ContainerTypeDto dto) throws ImproException {
		return containerTypeDao.editContainerType(dto);
	}

	@Override
	public ContainerTypeDto loadContainerType(int ContTypId) throws ImproException {
		return containerTypeDao.loadContainerType(ContTypId);
	}

	@Override
	public boolean checkDuplicateContainerType(String name) throws ImproException {
		return containerTypeDao.checkDuplicateContainerType(name);
	}

	
}
