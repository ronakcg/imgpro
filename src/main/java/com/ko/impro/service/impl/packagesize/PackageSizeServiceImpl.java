package com.ko.impro.service.impl.packagesize;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.packagesize.PackageSizeDao;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.packagesize.PackageSizeService;
/**
 * @author hlekshmy : 
 * This class is an implementation of PackageSizeService Interface
 */
@Service
public class PackageSizeServiceImpl implements PackageSizeService {
	
	/** Reference of PackageSizeDao */
	@Autowired
	private  PackageSizeDao pkgSizeDao;

	@Override
	public List<PackageSizeDto> loadPackageSize() throws ImproException {
		return pkgSizeDao.loadPackageSize();
	}

	@Override
	public List<PackageSizeDto> loadPackageSize(List<String> packageList) throws ImproException {
		return pkgSizeDao.loadPackageSize(packageList);
	}
	
	@Override
	public void addPackageSize(PackageSizeDto packageSizeDto) throws ImproException {
		 pkgSizeDao.addPackageSize(packageSizeDto);
	}

	@Override
	public String deletePackageSize(PackageSizeDto packageSizeDto) throws ImproException {
		 return pkgSizeDao.deletePackageSize(packageSizeDto);
	}

	@Override
	public String editPackageSize(PackageSizeDto packageSizeDto) throws ImproException {
		return  pkgSizeDao.editPackageSize(packageSizeDto);
	}

	@Override
	public PackageSizeDto loadPackageSizeById(int pkgSizeId) throws ImproException {
		 return  pkgSizeDao.loadPackageSizeById(pkgSizeId);
	}

	@Override
	public boolean checkDuplicatePackageSize(String name) throws ImproException {
		 return  pkgSizeDao.checkDuplicatePackageSize(name);
	}
}
