package com.ko.impro.service.impl.segment;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.segment.SegmentDao;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.segment.SegmentService;

/**
 * @author hlekshmy : 
 * This class is an implementation of AuditLogService Interface
 */

@Service
public class SegmentServiceImpl implements SegmentService {

	/** The Constant Log */
	private static final Logger LOG = LoggerFactory.getLogger(SegmentServiceImpl.class);

	/** Reference of SegmentDao */
	@Autowired
	private SegmentDao segmentDao;

	@Override
	public List<SegmentDto> loadSegment() throws ImproException {
		return segmentDao.loadSegment();
	}

	@Override
	public List<SegmentDto> loadSegment(List<String> subCategoryList) throws ImproException {
		return segmentDao.loadSegment(subCategoryList);
	}

	@Override
	public int addSegment(SegmentDto segmentDto) throws ImproException {
		LOG.info("SegmentServiceImpl()-->addSegment()-->Begin");
		int code = segmentDao.addSegment(segmentDto);
		LOG.info("SegmentServiceImpl()-->addSegment()-->End-->");
		return code;
	}

	@Override
	public String deleteSegment(SegmentDto segmentDto) throws ImproException {
		LOG.info("SegmentServiceImpl()-->deleteSegment()-->Begin");
		String msg = segmentDao.deleteSegment(segmentDto);
		LOG.info("SegmentServiceImpl()-->deleteSegment()-->End-->");
		return msg;
	}

	@Override
	public String editSegment(SegmentDto segmentDto) throws ImproException {
		LOG.info("SegmentServiceImpl()-->editSegment()-->Begin");
		String msg = segmentDao.editSegment(segmentDto);
		LOG.info("SegmentServiceImpl()-->editSegment()-->End-->");
		return msg;
	}

	@Override
	public void addSegmentSubCategoryMapping(SegmentDto segmentDto, int genSegCode) throws ImproException {
		LOG.info("SegmentServiceImpl()-->addSegmentSubCategoryMapping()-->Begin");
		segmentDao.addSegmentSubCategoryMapping(segmentDto, genSegCode);
		LOG.info("SegmentServiceImpl()-->addSegmentSubCategoryMapping()-->End-->");
	}

	@Override
	public boolean checkDuplicateSegment(String subSegmentDesc) throws ImproException {
		LOG.info("SegmentServiceImpl()-->checkDuplicateSegment()-->Begin");
		boolean flag = segmentDao.checkDuplicateSegment(subSegmentDesc);
		LOG.info("SegmentServiceImpl()-->checkDuplicateSegment()-->End-->");
		return flag;
	}

	@Override
	public boolean validateInProduct(SegmentDto segmentDto) throws ImproException {
		return segmentDao.validateInProduct(segmentDto);
	}

	@Override
	public SegmentDto loadSegment(int segmentCode) throws ImproException {
		return segmentDao.loadSegment(segmentCode);
	}

}

