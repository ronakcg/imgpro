package com.ko.impro.service.impl.gatekeeping;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.gatekeeping.ProductGateKeepingDao;
import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.gatekeeping.ProductGateKeepingService;

/**
 * @author hlekshmy
 * Service implementations of ProductGateKeeping
 */
@Service
public class ProductGateKeepingServiceImpl implements ProductGateKeepingService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ProductGateKeepingServiceImpl.class);

	@Autowired
	private transient ProductGateKeepingDao dao;

	/**
	 * Load the complete/incomplete records
	 * 
	 * @param isInComplete
	 * @return List<ProductGateKeepingDto>
	 * @throws ImproException
	 */
	@Override
	public List<ProductGateKeepingDto> loadGateKeeping(boolean isInComplete) throws ImproException {
		return dao.loadGateKeeping(isInComplete);
	}

	/**
	 * Pull the gateKeeping Details
	 * 
	 * @param gatekeepingId
	 * @return ProductGateKeepingDto
	 * @throws ImproException
	 */
	@Override
	public ProductGateKeepingDto loadGateKeeping(int gatekeepingId) throws ImproException {
		return dao.loadGateKeeping(gatekeepingId);

	}

	
	/**
	 * To purge  a  productGatekeeping record
	 * @param productCode
	 * @throws ImproException
	 */
	@Override
	public void purgeProductGateKeeping(int  productCode) throws ImproException {
		dao.purgeProductGateKeeping(productCode);

	}

	/**
	 * Method to trigger StoredProcedure [dbo].[ProductUpdateFromGatekeeping]
	 * @return ResponseEntity<GateKeepingResponseDto>
	 */
	@Override
	public void pushProducts() throws ImproException {
		List<ProductGateKeepingDto> list=dao.loadGateKeeping(false);
		dao.pushProducts();
		
		//Call the image related push from here
		for(ProductGateKeepingDto dto:list) {
			
		}
	}

	/**
	 * Method to edit Gate keeping
	 * @param gatekeepingDto
	 * @throws ImproException
	 */
	@Override
	public void editGateKeeping(ProductGateKeepingDto gatekeepingDto) throws ImproException {
		dao.editGateKeeping(gatekeepingDto);
	}

	

}
