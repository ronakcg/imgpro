package com.ko.impro.service.impl.brand;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.brand.BrandDao;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.brand.BrandService;
/**
 * @author hlekshmy : 
 * This class is an implementation of BrandService Interface
 */
@Service
public class BrandServiceImpl implements BrandService {
	private static final Logger LOG = LoggerFactory.getLogger(BrandServiceImpl.class);
	
	/** Reference of brandDao */
	@Autowired
	private  BrandDao brandDao;

	@Override
	public List<BrandDto> loadBrands() throws ImproException {
		return brandDao.loadBrands();
	}

	@Override
	public List<BrandDto> loadBrands(List<String> manufctrIdList) throws ImproException {
		return brandDao.loadBrands(manufctrIdList);

	}
	
	@Override
	public BrandDto getBrandDetails(BrandDto brandDto) throws ImproException {
		return brandDao.getBrandDetails(brandDto);
	}

	@Override
	public void addBrand(BrandDto brandDto) throws ImproException {
		LOG.info("BrandServiceImpl-->addBrand()-->Begin");
		brandDao.addBrand(brandDto);
		LOG.info("BrandServiceImpl-->addBrand()-->End");		
	}

	@Override
	public String deleteBrand(BrandDto dto) throws ImproException {
		return brandDao.deleteBrand(dto);
		
	}

	@Override
	public String editBrand(BrandDto brandDto) throws ImproException {
		return brandDao.editBrand(brandDto);		
	}

	@Override
	public BrandDto loadBrand(int manufctrId) throws ImproException {		
		return brandDao.loadBrand(manufctrId);
	}

	@Override
	public boolean checkDuplicateBrand(String name,boolean isLong) throws ImproException {		
		return brandDao.checkDuplicateBrand(name,isLong);
	}
	
	@Override
	public boolean validateInProduct(BrandDto brandDto) throws ImproException {		
		return brandDao.validateInProduct(brandDto);
	}

}
