package com.ko.impro.service.impl.category;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.category.CategoryDao;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.category.CategoryService;

/**
 * @author hlekshmy : 
 * This class is an implementation of CategoryService Interface
 */
@Service
public class CategoryServiceImpl implements CategoryService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(CategoryServiceImpl.class);
	
	/** Reference of categoryDao */
	@Autowired
	private CategoryDao categoryDao;
	
	@Override
	public List<CategoryDto> loadCategory() throws ImproException {
		return categoryDao.loadCategory();
	}
	
	@Override
	public void addCategory(CategoryDto categoryDto) throws ImproException {
		LOG.info("CategoryServiceImpl-->addCategory()-->Begin");
		categoryDao.addCategory(categoryDto);
		LOG.info("CategoryServiceImpl-->addCategory()-->End");
	}

	@Override
	public String deleteCategory(CategoryDto categoryDto) throws ImproException {
		LOG.info("CategoryServiceImpl-->deleteCategory()-->Begin");
		LOG.info("CategoryServiceImpl-->deleteCategory()-->End");
		return categoryDao.deleteCategory(categoryDto);
		
	}

	@Override
	public String editCategory(CategoryDto categoryDto) throws ImproException {
		LOG.info("CategoryServiceImpl-->editCategory()-->Begin");
		LOG.info("CategoryServiceImpl-->editCategory()-->End");
		return categoryDao.editCategory(categoryDto);
	}
	
	@Override
	public boolean checkDuplicateCategory(String categoryDesc) throws ImproException{
		LOG.info("CategoryServiceImpl-->checkDuplicateCategory()-->Begin");
		LOG.info("CategoryServiceImpl-->checkDuplicateCategory()-->End");
		return categoryDao.checkDuplicateCategory(categoryDesc);
	}

	@Override
	public CategoryDto loadCategory(int catId) throws ImproException {
		LOG.info("CategoryServiceImpl-->loadCategory()-->Begin");
		LOG.info("CategoryServiceImpl-->loadCategory()-->End");
		return categoryDao.loadCategory(catId);
	}

}
