package com.ko.impro.service.impl.image;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dto.EditImageDto;
import com.ko.impro.dto.HiResImageDto;
import com.ko.impro.dto.MockImageDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.dto.StdImageDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.image.ImageGatekeepingService;
import com.ko.impro.util.ImageConfig;
import com.ko.impro.util.ImageHelper;

/**
 * @author swshedge
 * ImageGatekeepingServiceImpl is implementation of ImageGateKeeping Interface
 *
 */
@Service
public class ImageGatekeepingServiceImpl extends ImageConfig implements ImageGatekeepingService {


	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ImageServiceImpl.class);
	
	/**
	 * auditDao Reference
	 */
	@Autowired(required = true)
	AuditLogDao auditDao;

	
	@Override
	public void getImagesById(ProductGateKeepingDto prodGateDto) {
		getNewImagesById(prodGateDto);
		ProductDto dto=new ProductDto();
		dto.setBitMapName(prodGateDto.getBitMapName());
		getExistingImagesById(dto);
		prodGateDto.setProductDto(dto);
		setImageSelection(prodGateDto);
	}
	
	private void getExistingImagesById(ProductDto prod) {
		LOG.info("ImageGatekeepingServiceImpl>> getExistingImagesById>> Begin");
		String imageFolderName = ImageHelper.getImageFolderName(prod.getBitMapName());
		Map<String,String> mapStd=ImageHelper.getImageMap(s3client,url,bucketName,activeStdFolNm,imageFolderName);
		Map<String,String> mapHiRes=ImageHelper.getImageMap(s3client,url,bucketName,activeHiReFolNm,imageFolderName);
		Map<String,String> mapMock=ImageHelper.getImageMap(s3client,url,bucketName,activeMockFolNm,imageFolderName);
		ImageHelper.getProductImagesById(prod, mapStd, mapHiRes, mapMock);
		LOG.info("ImageGatekeepingServiceImpl>> getExistingImagesById>> End");
	}
	
	private void getNewImagesById(ProductGateKeepingDto prodGateDto) {
		LOG.info("ImageGatekeepingServiceImpl>> getNewImagesById>> Begin");
		String imageFolderName = ImageHelper.getImageFolderName(prodGateDto.getBitMapName());
		Map<String,String> mapStd=ImageHelper.getImageMap(s3client,url,bucketName,gatekeepingStdFolNm,imageFolderName);
		Map<String,String> mapHiRes=ImageHelper.getImageMap(s3client,url,bucketName,gatekeepingHiFolNm,imageFolderName);
		//Map<String,String> mapMock=ImageHelper.getImageMap(s3client,url,bucketName,gatekeepingMockFolNm,imageFolderName);
		ImageHelper.getProductImagesById(prodGateDto, mapStd, mapHiRes);
		LOG.info("ImageGatekeepingServiceImpl>> getNewImagesById>> End");
	}
	
	/**
	 * Set the image selection in the edit screen
	 * If Gatekeeping flags are Y set the check box as ticked for gatekeeping images
	 * if Gatekeeping flags are N or empty, check if corresponding img/urls 
	 * for existing images exists then tick mark the existing images
	 * @param prodGateDto
	 */
	private void setImageSelection(ProductGateKeepingDto prodGateDto) {

		if("Y".equals(prodGateDto.getMissingImagesDto().getHiRes1())) {
			prodGateDto.getMissingImagesDto().setIshiRes1(true);
		} else {	
			if(prodGateDto.getProductDto().getMissingImagesDto().getHiRes1Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIshiRes1(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getHiRes2())) {
			prodGateDto.getMissingImagesDto().setIshiRes2(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getHiRes2Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIshiRes2(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getHiRes3())) {
			prodGateDto.getMissingImagesDto().setIshiRes3(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getHiRes3Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIshiRes3(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getHiRes7())) {
			prodGateDto.getMissingImagesDto().setIshiRes7(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getHiRes7Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIshiRes7(true);
			}
		}
				
		if("Y".equals(prodGateDto.getMissingImagesDto().getHiRes8())) {
			prodGateDto.getMissingImagesDto().setIshiRes8(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getHiRes8Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIshiRes8(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getHiRes9())) {
			prodGateDto.getMissingImagesDto().setIshiRes9(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getHiRes9Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIshiRes9(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getLowRes1())) {
			prodGateDto.getMissingImagesDto().setIslowRes1(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getLowRes1Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIslowRes1(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getLowRes2())) {
			prodGateDto.getMissingImagesDto().setIslowRes2(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getLowRes2Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIslowRes2(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getLowRes3())) {
			prodGateDto.getMissingImagesDto().setIslowRes3(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getLowRes3Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIslowRes3(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getLowRes7())) {
			prodGateDto.getMissingImagesDto().setIslowRes7(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getLowRes7Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIslowRes7(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getLowRes8())) {
			prodGateDto.getMissingImagesDto().setIslowRes8(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getLowRes8Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIslowRes8(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getLowRes9())) {
			prodGateDto.getMissingImagesDto().setIslowRes9(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getLowRes9Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIslowRes9(true);
			}
		}
		
		/*if("Y".equals(prodGateDto.getMissingImagesDto().getMock1())) {
			prodGateDto.getMissingImagesDto().setIsmock1(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getMock1Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIsmock1(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getMock2())) {
			prodGateDto.getMissingImagesDto().setIsmock2(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getMock2Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIsmock2(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getMock3())) {
			prodGateDto.getMissingImagesDto().setIsmock3(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getMock3Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIsmock3(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getMock7())) {
			prodGateDto.getMissingImagesDto().setIsmock7(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getMock7Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIsmock7(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getMock8())) {
			prodGateDto.getMissingImagesDto().setIsmock8(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getMock8Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIsmock8(true);
			}
		}
		
		if("Y".equals(prodGateDto.getMissingImagesDto().getMock9())) {
			prodGateDto.getMissingImagesDto().setIsmock9(true);
		} else {
			if(prodGateDto.getProductDto().getMissingImagesDto().getMock9Url() != null) {
				prodGateDto.getProductDto().getMissingImagesDto().setIsmock9(true);
			}
		}
*/	}

	@Override
	public void editImage(ProductGateKeepingDto prodGateDto) throws ImproException {
		ProductDto dto = new ProductDto();
		dto = prodGateDto.getProductDto();
		try {
			EditImageDto editImgDto=dto.getEditImageDto() ;
			if(editImgDto != null) {
				StdImageDto stdImgDto=editImgDto.getStdImageDto() ;
				HiResImageDto hiResImgDto=editImgDto.getHiResImageDto() ;
				MockImageDto mockImgDto=editImgDto.getMockImageDto() ;
			
				// For Existing GateKeeping Images Edit
				if(stdImgDto != null) {
					//For Standard Images
					ImageHelper.editStdImage1(dto,s3client,bucketName,activeStdFolNm,auditDao);
					ImageHelper.editStdImage2(dto,s3client,bucketName,activeStdFolNm,auditDao);
					ImageHelper.editStdImage3(dto,s3client,bucketName,activeStdFolNm,auditDao);
					ImageHelper.editStdImage7(dto,s3client,bucketName,activeStdFolNm,auditDao);
					ImageHelper.editStdImage8(dto,s3client,bucketName,activeStdFolNm,auditDao);
					ImageHelper.editStdImage9(dto,s3client,bucketName,activeStdFolNm,auditDao);
				}
				if(hiResImgDto != null) {
					//For HiRes Images			
					ImageHelper.editHiResImage1(dto,s3client,bucketName,activeHiReFolNm,auditDao);
					ImageHelper.editHiResImage2(dto,s3client,bucketName,activeHiReFolNm,auditDao);
					ImageHelper.editHiResImage3(dto,s3client,bucketName,activeHiReFolNm,auditDao);
					ImageHelper.editHiResImage7(dto,s3client,bucketName,activeHiReFolNm,auditDao);
					ImageHelper.editHiResImage8(dto,s3client,bucketName,activeHiReFolNm,auditDao);
					ImageHelper.editHiResImage9(dto,s3client,bucketName,activeHiReFolNm,auditDao);
				}
				if(mockImgDto != null) {
					//For Mock Images
					ImageHelper.editMockImage1(dto,s3client,bucketName,activeMockFolNm,auditDao);
					/*ImageHelper.editMockImage2(dto,s3client,bucketName,activeMockFolNm,auditDao);
					ImageHelper.editMockImage3(dto,s3client,bucketName,activeMockFolNm,auditDao);
					ImageHelper.editMockImage7(dto,s3client,bucketName,activeMockFolNm,auditDao);
					ImageHelper.editMockImage8(dto,s3client,bucketName,activeMockFolNm,auditDao);
					ImageHelper.editMockImage9(dto,s3client,bucketName,activeMockFolNm,auditDao);*/
				}
			}
			
			EditImageDto gateEditImgDto=prodGateDto.getEditImageDto() ;
			if(gateEditImgDto != null) {
				StdImageDto stdImgDto=gateEditImgDto.getStdImageDto() ;
				HiResImageDto hiResImgDto=gateEditImgDto.getHiResImageDto() ;
				//MockImageDto mockImgDto=gateEditImgDto.getMockImageDto() ;
			
				// For New GateKeeping Images Edit
				if(stdImgDto != null) {
					//For Standard Images
					ImageHelper.editStdImage1(prodGateDto,s3client,bucketName,gatekeepingStdFolNm,auditDao);
					ImageHelper.editStdImage2(prodGateDto,s3client,bucketName,gatekeepingStdFolNm,auditDao);
					ImageHelper.editStdImage3(prodGateDto,s3client,bucketName,gatekeepingStdFolNm,auditDao);
					ImageHelper.editStdImage7(prodGateDto,s3client,bucketName,gatekeepingStdFolNm,auditDao);
					ImageHelper.editStdImage8(prodGateDto,s3client,bucketName,gatekeepingStdFolNm,auditDao);
					ImageHelper.editStdImage9(prodGateDto,s3client,bucketName,gatekeepingStdFolNm,auditDao);
				}
				if(hiResImgDto != null) {
					//For HiRes Images			
					ImageHelper.editHiResImage1(prodGateDto,s3client,bucketName,gatekeepingHiFolNm,auditDao);
					ImageHelper.editHiResImage2(prodGateDto,s3client,bucketName,gatekeepingHiFolNm,auditDao);
					ImageHelper.editHiResImage3(prodGateDto,s3client,bucketName,gatekeepingHiFolNm,auditDao);
					ImageHelper.editHiResImage7(prodGateDto,s3client,bucketName,gatekeepingHiFolNm,auditDao);
					ImageHelper.editHiResImage8(prodGateDto,s3client,bucketName,gatekeepingHiFolNm,auditDao);
					ImageHelper.editHiResImage9(prodGateDto,s3client,bucketName,gatekeepingHiFolNm,auditDao);
				}
				/*if(mockImgDto != null) {
					//For Mock Images
					ImageHelper.editMockImage1(prodGateDto,s3client,bucketName,gatekeepingMockFolNm,auditDao);
					ImageHelper.editMockImage2(prodGateDto,s3client,bucketName,gatekeepingMockFolNm,auditDao);
					ImageHelper.editMockImage3(prodGateDto,s3client,bucketName,gatekeepingMockFolNm,auditDao);
					ImageHelper.editMockImage7(prodGateDto,s3client,bucketName,gatekeepingMockFolNm,auditDao);
					ImageHelper.editMockImage8(prodGateDto,s3client,bucketName,gatekeepingMockFolNm,auditDao);
					ImageHelper.editMockImage9(prodGateDto,s3client,bucketName,gatekeepingMockFolNm,auditDao);
				}*/
			}
			
		} catch (AmazonServiceException ase) {
			if(LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl addImageByte() AmazonServiceException "+ase.getMessage(), s3client,bucketName);
			}
			throw new ImproException(ase.getMessage());
		} catch (AmazonClientException ace) {
			if(LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl addImageByte() AmazonClientException "+ace.getMessage(), s3client,bucketName);
			}
			throw new ImproException(ace.getMessage());
		}
	}
	
	/**
	 * Movement of images during push images manually
	 * @param dto
	 * @throws ImproException
	 */	
	@Override
	public void pushImagesManual(ProductGateKeepingDto dto) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
		List<String> listUrl = new ArrayList<String>();
		listUrl.add(gatekeepingStdFolNm + folder);
		listUrl.add(gatekeepingHiFolNm + folder);
		try {
			ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName);
			ListObjectsV2Result listing = null;
			for (String prefix : listUrl) {
				req.withPrefix(prefix);
				listing = s3client.listObjectsV2(req);
				for (S3ObjectSummary summary : listing.getObjectSummaries()) {
					String oldPath = summary.getKey();
					if("Y".equals(dto.getMissingImagesDto().getHiRes1())   || "Y".equals(dto.getMissingImagesDto().getHiRes2())  || "Y".equals(dto.getMissingImagesDto().getHiRes3()) ||
						"Y".equals(dto.getMissingImagesDto().getHiRes7())  || "Y".equals(dto.getMissingImagesDto().getHiRes8())  || "Y".equals(dto.getMissingImagesDto().getHiRes9()) ||
						"Y".equals(dto.getMissingImagesDto().getLowRes1()) || "Y".equals(dto.getMissingImagesDto().getLowRes2()) || "Y".equals(dto.getMissingImagesDto().getLowRes3()) ||
						"Y".equals(dto.getMissingImagesDto().getLowRes7()) || "Y".equals(dto.getMissingImagesDto().getLowRes8()) || "Y".equals(dto.getMissingImagesDto().getLowRes9())) {
						String newPath = ImageHelper.getNewImagePath(oldPath);
						archiveImage(oldPath, newPath);
					}
					ImageHelper.deleteImage(oldPath, s3client, bucketName);  
				}
			}
		} catch (AmazonClientException ace) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageGatekeepingServiceImpl>> pushImagesManual()>> AmazonClientException " + ace.getMessage(), s3client,
						bucketName);
			}
			throw new ImproException(ace.getMessage());
		}
	}
	
	/**
	 * Archive Image
	 * @param srcURL
	 * @param destURL
	 */	
	private void archiveImage(String srcURL, String destURL) {
		CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, srcURL, bucketName, destURL);
		s3client.copyObject(copyObjRequest);
	}

	@Override
	public void getAllBase64String(ProductGateKeepingDto gatekeepingDto) throws ImproException {
		LOG.info("ImageGatekeepingServiceImpl-->getAllBase64String-->Begin");
		
		String imageFolderName = ImageHelper.getImageFolderName(gatekeepingDto.getProductId());
		ImageHelper.getBase64ImagesById(s3client,gatekeepingDto, bucketName, gatekeepingStdFolNm, imageFolderName);
		
		ProductDto prod = new ProductDto();
		prod = gatekeepingDto.getProductDto();
		ImageHelper.getBase64ImagesById(s3client, prod, bucketName, activeStdFolNm, imageFolderName);
		LOG.info("ImageGatekeepingServiceImpl-->getAllBase64String-->End");
	}	
}
