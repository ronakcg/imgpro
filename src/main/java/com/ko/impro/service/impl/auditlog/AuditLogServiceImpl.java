package com.ko.impro.service.impl.auditlog;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dto.AuditLogDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.auditlog.AuditLogService;
/**
 * @author hlekshmy : 
 * This class is an implementation of AuditLogService Interface
 */
@Service
public class AuditLogServiceImpl implements AuditLogService {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AuditLogServiceImpl.class);

	/** Reference of AuditLogDao */
	@Autowired
	private AuditLogDao auditDao; 
	
	@Override
	public List<AuditLogDto> exportAuditLog() throws ImproException {
		LOG.info("AuditLogServiceImpl-->exportAuditLog()-->Begin");
		List<AuditLogDto> auditList=auditDao.loadAuditLog();
		LOG.info("AuditLogServiceImpl-->exportAuditLog()-->End");
		return auditList;
	}

	@Override
	public List<AuditLogDto> loadAuditLog() throws ImproException {
		return auditDao.loadAuditLog();

	}

}
