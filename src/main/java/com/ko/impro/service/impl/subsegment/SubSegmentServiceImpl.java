package com.ko.impro.service.impl.subsegment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ko.impro.dao.subsegment.SubSegmentDao;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.subsegment.SubSegmentService;

/**
 * @author hlekshmy : 
 * This class is an implementation of AuditLogService Interface
 */
@Service
public class SubSegmentServiceImpl implements SubSegmentService {
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(SubSegmentServiceImpl.class);
	
	/** Reference of AuditLogDao */
	@Autowired
	private SubSegmentDao subSegmentDao;

	@Override
	public List<SubSegmentDto> loadSubSegment() throws ImproException {
		return subSegmentDao.loadSubSegment();
	}

	@Override
	public List<SubSegmentDto> loadSubSegment(List<String> segmentList) throws ImproException {
		return subSegmentDao.loadSubSegment(segmentList);

	}

	@Override
	public int addSubSegment(SubSegmentDto subSegmentDto) throws ImproException {
		LOG.info("SubSegmentServiceImpl()-->addSubSegment()-->Begin");
		int code = subSegmentDao.addSubSegment(subSegmentDto);
		LOG.info("SubSegmentServiceImpl()-->addSubSegment()-->End");
		return code;
	}

	@Override
	public String deleteSubSegment(SubSegmentDto subSegmentDto) throws ImproException {
		LOG.info("SubSegmentServiceImpl()-->deleteSubSegment()-->Begin");
		String msg = subSegmentDao.deleteSubSegment(subSegmentDto);
		LOG.info("SubSegmentServiceImpl()-->deleteSubSegment()-->End");
		return msg;
	}
	
	@Override
	public String editSubSegment(SubSegmentDto subSegmentDto) throws ImproException {
		LOG.info("SubSegmentServiceImpl()-->editSubSegment()-->Begin");
		String msg = subSegmentDao.editSubSegment(subSegmentDto);
		LOG.info("SubSegmentServiceImpl()-->editSubSegment()-->End");
		return msg;
	}

	@Override
	public void addSegmentSubSegmentMapping(SubSegmentDto subSegmentDto, int genSubSegCode) throws ImproException {
		LOG.info("SubSegmentServiceImpl()-->addSegmentSubSegmentMapping()-->Begin");
		subSegmentDao.addSegmentSubSegmentMapping(subSegmentDto, genSubSegCode);
		LOG.info("SubSegmentServiceImpl()-->addSegmentSubSegmentMapping()-->End");
	}
	
	@Override
	public boolean checkDuplicateSubSegment(String subSegmentDesc) throws ImproException {
		LOG.info("SubSegmentServiceImpl()-->checkDuplicateSubSegment()-->Begin");
		boolean flag = subSegmentDao.checkDuplicateSubSegment(subSegmentDesc);
		LOG.info("SubSegmentServiceImpl()-->checkDuplicateSubSegment()-->End");
		return flag;
	}

	@Override
	public SubSegmentDto loadSubSegment(int segmentId) throws ImproException {
		return subSegmentDao.loadSubSegment(segmentId);
	}


}
