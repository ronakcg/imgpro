package com.ko.impro.service.impl.image;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.EditImageDto;
import com.ko.impro.dto.HiResImageDto;
import com.ko.impro.dto.JpgImageDto;
import com.ko.impro.dto.MockImageDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.StdImageDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.image.ImageService;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ImageConfig;
import com.ko.impro.util.ImageHelper;

/**
 * @author hlekshmy ImageServiceImpl class for handling operation related to
 *         image manipulation
 * 
 */
@Service
public class ImageServiceImpl extends ImageConfig implements ImageService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ImageServiceImpl.class);

	

	@Override
	public void addImage(String keyName, File image) {
		try {
			s3client.putObject(new PutObjectRequest(bucketName, keyName, image));
		} catch (AmazonServiceException ase) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl addImage() AmazonServiceException " + ase.getMessage());
			}
		} catch (AmazonClientException ace) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl addImage() AmazonClientException " + ace.getMessage());
			}
		}
	}

	/**
	 * Get the GridViewImages
	 * 
	 * @param List<ProductDto>
	 * @param prodList
	 * @return String
	 */
	@Override
	public void getGridViewImages(List<ProductDto> prodList) {
		for (ProductDto prod : prodList) {
			String imageFolderName = ImageHelper.getImageFolderName(prod.getBitMapName());
			String imageName = ImageHelper.getImageName(prod.getBitMapName());

			Map<String, String> mapStd = null;
			Map<String, String> mapMock = null;

			LOG.info("========== Standard/Mock ImageUrl ");
			mapStd = ImageHelper.getImageMap(s3client, url, bucketName, activeStdFolNm, imageFolderName);
			if (null != mapStd.get(imageName + AppConstants.IMAGE_VIEW1)) {
				prod.getMissingImagesDto().setLowRes1Url(mapStd.get(imageName + AppConstants.IMAGE_VIEW1));
			} else {
				mapMock = ImageHelper.getImageMap(s3client, url, bucketName, activeMockFolNm, imageFolderName);
				prod.getMissingImagesDto().setMock1Url(mapMock.get(imageName + AppConstants.IMAGE_VIEW1));
			}
		}
	}

	@Override
	public void archiveImages(List<ProductDto> prodList) throws ImproException {
		for (ProductDto prod : prodList) {
			ImageHelper.copyFolder(activeStdFolNm, archiveStdFolNm, prod.getBitMapName(), s3client, bucketName);
			ImageHelper.copyFolder(activeHiReFolNm, archiveHiReFolNm,prod.getBitMapName(), s3client, bucketName);
			ImageHelper.copyFolder(activeMockFolNm, archiveMockFolNm,prod.getBitMapName(), s3client, bucketName);

			boolean stdDeleteFlag = ImageHelper.deleteFolder(activeStdFolNm,prod.getBitMapName(), s3client, bucketName);
			if (stdDeleteFlag) {
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.STD_IMAGE, String.valueOf(prod.getProductCode()),
						AppConstants.TRANSACTION_TYPE_ARCHIVE);
			}

			boolean hiResDeleteFlag = ImageHelper.deleteFolder(activeHiReFolNm,prod.getBitMapName(), s3client, bucketName);
			if (hiResDeleteFlag) {
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.HIRES_IMAGE, String.valueOf(prod.getProductCode()),
						AppConstants.TRANSACTION_TYPE_ARCHIVE);
			}

			boolean mockDeleteFlag = ImageHelper.deleteFolder(activeMockFolNm,prod.getBitMapName(), s3client, bucketName);
			if (mockDeleteFlag) {
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.MOCK_IMAGE, String.valueOf(prod.getProductCode()),
						AppConstants.TRANSACTION_TYPE_ARCHIVE);
			}
		}
	}

	@Override
	public void restoreImages(ArchivedProductDto prod) {
		ImageHelper.copyFolder(archiveStdFolNm, activeStdFolNm, prod.getBitMapName(), s3client, bucketName);
		ImageHelper.copyFolder(archiveHiReFolNm, activeHiReFolNm, prod.getBitMapName(), s3client, bucketName);
		ImageHelper.copyFolder(archiveMockFolNm, activeMockFolNm, prod.getBitMapName(), s3client, bucketName);

		ImageHelper.deleteFolder(archiveStdFolNm, prod.getBitMapName(), s3client, bucketName);
		ImageHelper.deleteFolder(archiveHiReFolNm, prod.getBitMapName(), s3client, bucketName);
		ImageHelper.deleteFolder(archiveMockFolNm, prod.getBitMapName(), s3client, bucketName);

	}

	@Override
	public void purgeImages(List<ArchivedProductDto> archProdList) {
		for (ArchivedProductDto prod : archProdList) {
			ImageHelper.deleteFolder(archiveStdFolNm, prod.getBitMapName(), s3client, bucketName);
			ImageHelper.deleteFolder(archiveHiReFolNm, prod.getBitMapName(), s3client, bucketName);
			ImageHelper.deleteFolder(archiveMockFolNm, prod.getBitMapName(), s3client, bucketName);
		}
	}

	@Override
	public void getProductImagesById(ProductDto prod) {
		String imageFolderName = ImageHelper.getImageFolderName(prod.getBitMapName());
		Map<String, String> mapStd = ImageHelper.getImageMap(s3client, url, bucketName, activeStdFolNm,
				imageFolderName);
		Map<String, String> mapHiRes = ImageHelper.getImageMap(s3client, url, bucketName, activeHiReFolNm,
				imageFolderName);
		Map<String, String> mapMock = ImageHelper.getImageMap(s3client, url, bucketName, activeMockFolNm,
				imageFolderName);
		ImageHelper.getProductImagesById(prod, mapStd, mapHiRes, mapMock);

	}

	@Override
	public void addImage(ProductDto dto) throws ImproException {
		try {
			String folder = ImageHelper.getImageFolderName(dto.getBitMapName());
			String fileName = ImageHelper.getImageName(dto.getBitMapName());

			if (!dto.getMissingImagesDto().isIslowRes1()
					&& (!dto.getEditImageDto().getStdImageDto().getFile1().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile1(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD1);
			}

			if (!dto.getMissingImagesDto().isIslowRes2()
					&& (!dto.getEditImageDto().getStdImageDto().getFile2().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile2(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD2);
			}

			if (!dto.getMissingImagesDto().isIslowRes3()
					&& (!dto.getEditImageDto().getStdImageDto().getFile3().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile3(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD3);
			}

			if (!dto.getMissingImagesDto().isIslowRes7()
					&& (!dto.getEditImageDto().getStdImageDto().getFile7().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile7(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD7);
			}

			if (!dto.getMissingImagesDto().isIslowRes8()
					&& (!dto.getEditImageDto().getStdImageDto().getFile8().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile8(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD8);
			}

			if (!dto.getMissingImagesDto().isIslowRes9()
					&& (!dto.getEditImageDto().getStdImageDto().getFile9().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getStdImageDto().getFile9(),
						activeStdFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_STD_ADD9);
			}

			// For Hi Res Images
			if (!dto.getMissingImagesDto().isIshiRes1()
					&& (!dto.getEditImageDto().getHiResImageDto().getFile1().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile1(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD1);
			}

			if (!dto.getMissingImagesDto().isIshiRes2()
					&& (!dto.getEditImageDto().getHiResImageDto().getFile2().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile2(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD2);
			}

			if (!dto.getMissingImagesDto().isIshiRes3()
					&& (!dto.getEditImageDto().getHiResImageDto().getFile3().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile3(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD3);
			}

			if (!dto.getMissingImagesDto().isIshiRes7()
					&& (!dto.getEditImageDto().getHiResImageDto().getFile7().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile7(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD7);
			}

			if (!dto.getMissingImagesDto().isIshiRes8()
					&& (!dto.getEditImageDto().getHiResImageDto().getFile8().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile8(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD8);
			}

			if (!dto.getMissingImagesDto().isIshiRes9()
					&& (!dto.getEditImageDto().getHiResImageDto().getFile9().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getHiResImageDto().getFile9(),
						activeHiReFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_HIRES_ADD9);
			}

			// For Mock Images
			if (!dto.getMissingImagesDto().isIsmock1()
					&& (!dto.getEditImageDto().getMockImageDto().getFile1().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile1(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD1);
			}

			if (!dto.getMissingImagesDto().isIsmock2()
					&& (!dto.getEditImageDto().getMockImageDto().getFile2().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile2(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW2, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD2);
			}

			if (!dto.getMissingImagesDto().isIsmock1()
					&& (!dto.getEditImageDto().getMockImageDto().getFile3().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile1(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW3, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD3);
			}

			if (!dto.getMissingImagesDto().isIsmock7()) {
				if (!dto.getEditImageDto().getMockImageDto().getFile7().isEmpty()) {
					ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile7(),
							activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW7, s3client, bucketName);
					auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES,
							String.valueOf(dto.getBitMapName()), AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD7);
				}
			}

			if (!dto.getMissingImagesDto().isIsmock8()
					&& (!dto.getEditImageDto().getMockImageDto().getFile8().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile8(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW8, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD8);
			}

			if (!dto.getMissingImagesDto().isIsmock9()
					&& (!dto.getEditImageDto().getMockImageDto().getFile9().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getMockImageDto().getFile9(),
						activeMockFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW9, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_MOCK_ADD9);
			}

			// Check for jpeg images
			if (!dto.getMissingImagesDto().isIsjpg()
					&& (!dto.getEditImageDto().getJpgImageDto().getFile1().isEmpty())) {
				ImageHelper.addImage(dto.getEditImageDto().getJpgImageDto().getFile1(),
						activeJpgFolNm + folder + "/" + fileName + AppConstants.IMAGE_VIEW1, s3client, bucketName);
				auditDao.addLog(AppConstants.USER_NAME, AppConstants.AWS_IMAGES, String.valueOf(dto.getBitMapName()),
						AppConstants.IMAGE_TRANSACTION_TYPE_JPG_ADD);
			}

		} catch (AmazonServiceException ase) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl addImageByte() AmazonServiceException " + ase.getMessage(), s3client,
						bucketName);
			}
			throw new ImproException(ase.getMessage());
		} catch (AmazonClientException ace) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl addImageByte() AmazonClientException " + ace.getMessage(), s3client,
						bucketName);
			}
			throw new ImproException(ace.getMessage());

		}
	}

	@Override
	public void editImage(ProductDto dto) throws ImproException {
		try {
			EditImageDto editImgDto=dto.getEditImageDto() ;
			if(editImgDto != null) {
				StdImageDto stdImgDto=editImgDto.getStdImageDto() ;
				HiResImageDto hiResImgDto=editImgDto.getHiResImageDto() ;
				MockImageDto mockImgDto=editImgDto.getMockImageDto() ;
				JpgImageDto jpgImgDto=editImgDto.getJpgImageDto() ;

				if(stdImgDto != null) {
					// For Standard Images
					ImageHelper.editStdImage1(dto, s3client, bucketName, activeStdFolNm, auditDao);
					ImageHelper.editStdImage2(dto, s3client, bucketName, activeStdFolNm, auditDao);
					ImageHelper.editStdImage3(dto, s3client, bucketName, activeStdFolNm, auditDao);
					ImageHelper.editStdImage7(dto, s3client, bucketName, activeStdFolNm, auditDao);
					ImageHelper.editStdImage8(dto, s3client, bucketName, activeStdFolNm, auditDao);
					ImageHelper.editStdImage9(dto, s3client, bucketName, activeStdFolNm, auditDao);
				}
				if(hiResImgDto != null) {

					// For HiRes Images
					ImageHelper.editHiResImage1(dto, s3client, bucketName, activeHiReFolNm, auditDao);
					ImageHelper.editHiResImage2(dto, s3client, bucketName, activeHiReFolNm, auditDao);
					ImageHelper.editHiResImage3(dto, s3client, bucketName, activeHiReFolNm, auditDao);
					ImageHelper.editHiResImage7(dto, s3client, bucketName, activeHiReFolNm, auditDao);
					ImageHelper.editHiResImage8(dto, s3client, bucketName, activeHiReFolNm, auditDao);
					ImageHelper.editHiResImage9(dto, s3client, bucketName, activeHiReFolNm, auditDao);
				}
				if(mockImgDto != null) {
		
					// For Mock Images
					ImageHelper.editMockImage1(dto, s3client, bucketName, activeMockFolNm, auditDao);
					ImageHelper.editMockImage2(dto, s3client, bucketName, activeMockFolNm, auditDao);
					ImageHelper.editMockImage3(dto, s3client, bucketName, activeMockFolNm, auditDao);
					ImageHelper.editMockImage7(dto, s3client, bucketName, activeMockFolNm, auditDao);
					ImageHelper.editMockImage8(dto, s3client, bucketName, activeMockFolNm, auditDao);
					ImageHelper.editMockImage9(dto, s3client, bucketName, activeMockFolNm, auditDao);
				}
				if(jpgImgDto != null) {

					// For JPG Images
					ImageHelper.editJpgImage1(dto, s3client, bucketName, activeJpgFolNm, auditDao);
				}
			}

		} catch (ImproException ase) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl editImage() AmazonServiceException " + ase.getMessage(), s3client,
						bucketName);
			}
			throw new ImproException(ase.getMessage());
		} catch (AmazonClientException ace) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl editImage() AmazonClientException " + ace.getMessage(), s3client,
						bucketName);
			}
			throw new ImproException(ace.getMessage());

		}
	}

	@Override
	public void bitmapChange(ProductDto dto) throws ImproException {
		String folder = ImageHelper.getImageFolderName(dto.getOldBitMapName());
		String newFolder = ImageHelper.getImageFolderName(dto.getBitMapName());
		String newFileName = ImageHelper.getImageName(dto.getBitMapName());

		List<String> listUrl = new ArrayList<String>();
		listUrl.add(activeStdFolNm + folder);
		listUrl.add(activeHiReFolNm + folder);
		listUrl.add(activeMockFolNm + folder);
		listUrl.add(activeJpgFolNm + folder);
		try {
			ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName);
			ListObjectsV2Result listing = null;

			for (String prefix : listUrl) {

				req.withPrefix(prefix);
				listing = s3client.listObjectsV2(req);

				for (S3ObjectSummary summary : listing.getObjectSummaries()) {

					String oldPath = summary.getKey();
					String newPath = ImageHelper.getNewPath(oldPath, newFolder, newFileName);

					archiveImage(oldPath, newPath);
					ImageHelper.deleteImage(oldPath, s3client, bucketName);
				}
			}
		} catch (AmazonClientException ace) {
			if (LOG.isErrorEnabled()) {
				LOG.error(" ImageServiceImpl addImageByte() AmazonClientException " + ace.getMessage(), s3client,
						bucketName);
			}
			throw new ImproException(ace.getMessage());

		}
	}

	/**
	 * Method to archive images
	 * 
	 * @param srcURL
	 * @param destURL
	 */
	private void archiveImage(String srcURL, String destURL) {

		CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, srcURL, bucketName, destURL);
		s3client.copyObject(copyObjRequest);

	}
	
	@Override
	public void getAllBase64String(ProductDto prod) throws ImproException {
		String imageFolderName = ImageHelper.getImageFolderName(prod.getBitMapName());
		ImageHelper.getBase64ImagesById(s3client,prod, bucketName, activeStdFolNm, imageFolderName);		
	}	

}