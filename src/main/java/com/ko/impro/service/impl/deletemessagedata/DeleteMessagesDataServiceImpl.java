package com.ko.impro.service.impl.deletemessagedata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.deletemessagedata.DeleteMessageDataDao;
import com.ko.impro.dto.DeleteMessagesDataDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.deletemessagedata.DeleteMessageDataService;
/**
 * @author hlekshmy : 
 * This class is an implementation of DeleteMessageDataService Interface
 */
@Service
public class DeleteMessagesDataServiceImpl implements DeleteMessageDataService {
	
	/** Reference of deleteMessagesDao */
	@Autowired
	private  DeleteMessageDataDao deleteMessagesDao;

	@Override
	public List<DeleteMessagesDataDto> loadDeleteMessagesDatas() throws ImproException {
		return deleteMessagesDao.loadDeleteMessagesData();
	}

	@Override
	public void addDeleteMessagesData(DeleteMessagesDataDto deltMsgDataDto) throws ImproException {
		deleteMessagesDao.addDeleteMessagesData(deltMsgDataDto);
	}

	@Override
	public String deleteDeleteMessagesData(DeleteMessagesDataDto deltMsgDataDto) throws ImproException {
		return deleteMessagesDao.deleteDeleteMessagesData(deltMsgDataDto);
	}

	@Override
	public String editDeleteMessagesData(DeleteMessagesDataDto dto) throws ImproException {
		return deleteMessagesDao.editDeleteMessagesData(dto);
	}

	@Override
	public DeleteMessagesDataDto loadDeleteMessagesData(int deltMsgDataid) throws ImproException {
		return deleteMessagesDao.loadDeleteMessagesData(deltMsgDataid);

	}

	@Override
	public boolean checkDuplicateDeleteMessagesData(String name) throws ImproException {
		return deleteMessagesDao.checkDuplicateDeleteMessagesData(name);

	}

	
}
