package com.ko.impro.service.impl.shape;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.shape.ShapeDao;
import com.ko.impro.dto.ShapeDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.shape.ShapeService;
/**
 * @author hlekshmy : 
 * This class is an implementation of ShapeService Interface
 */
@Service
public class ShapeServiceImpl implements ShapeService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ShapeServiceImpl.class);
		
	/** Reference of ShapeDao */
	@Autowired
	private  ShapeDao shapeidDao;
	
	@Override
	public List<ShapeDto> loadShapes() throws ImproException {
		return shapeidDao.loadShapes();
	}
	
	@Override
	public ShapeDto loadShapeID(int shapeId) throws ImproException {
		LOG.info("ShapeServiceImpl-->loadShape()-->Begin");
		LOG.info("ShapeServiceImpl-->loadShape()-->END");
		return shapeidDao.loadshapeID(shapeId);
	}
	
	@Override
	public boolean checkDuplicateShape(String name) throws ImproException {
		LOG.info("ShapeServiceImpl-->checkDuplicateShape()-->Begin");
		LOG.info("ShapeServiceImpl-->checkDuplicateShape()-->END");
		return shapeidDao.checkDuplicateShape(name);
	}
	
	
	@Override
	public String editShapes(ShapeDto dto) throws ImproException {
		return shapeidDao.editShapes(dto);
	}

	@Override
	public void addShapes(ShapeDto shapeDto) throws ImproException {
		shapeidDao.addShapes(shapeDto);
	}

	@Override
	public String deleteShapes(ShapeDto shapeDto) throws ImproException {
		return shapeidDao.deleteShapes(shapeDto);
	}	
}
