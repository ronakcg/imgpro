package com.ko.impro.service.impl.packages;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ko.impro.dao.packages.PackageDao;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.packages.PackageService;

/**
 * @author hlekshmy : 
 * This class is an implementation of PackageService Interface
 */
@Service
public class PackageServiceImpl implements PackageService {
	
	/** Reference of PackageDao */
	@Autowired
	private  PackageDao pkgDao;

	@Override
	public List<PackageDto> loadPackages() throws ImproException {
		return pkgDao.loadPackages();
	}

	@Override
	public void addPackage(PackageDto packageDto) throws ImproException {
		pkgDao.addPackage(packageDto);
	}

	@Override
	public String deletePackage(PackageDto packageDto) throws ImproException {
		return pkgDao.deletePackage(packageDto);
		
	}

	@Override
	public String editPackage(PackageDto packageDto) throws ImproException {
		return pkgDao.editPackage(packageDto);
		
	}

	@Override
	public boolean checkDuplicatePackageName(String packageName) throws ImproException {
		return pkgDao.checkDuplicatePackageName(packageName);
				
	}

	@Override
	public PackageDto loadPackage(int packageId) throws ImproException {
		return pkgDao.loadPackage(packageId);
		
	}
	
}
