package com.ko.impro.service.containertype;

import java.util.List;

import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.exception.ImproException;
/**
 * @author hlekshmy
 * ContainerTypeService class for handling CRUD 
 * operation for ContainerType
 * 
 */
public interface ContainerTypeService {

	/**
	 * Load the ContainerType details
	 * @param  boolean isMaintainence
	 * @return List<ContainerTypeDto>
	 * @throws ImproException
	 */
	List<ContainerTypeDto> loadContainerTypes() throws ImproException;

	
	/**
	 * Method to add Container Type
	 * @param containerTypeDto
	 * @throws ImproException
	 */
	void addContainerType(ContainerTypeDto containerTypeDto) throws ImproException;

	/**
	 * Method to delete the ContainerType
	 * @param ContainerTypeDto containerTypeDto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteContainerType(ContainerTypeDto containerTypeDto) throws ImproException;

	/**
	 * Method to edit ContainerType
	 * @param ContainerTypeDto dto
	 * @return String message
	 * @throws ImproException
	 */
	String editContainerType(ContainerTypeDto dto) throws ImproException;
	
	/**
	 * Method to load ContainerType based on containerTypeCode
	 * @param ContainerTypId
	 * @return ContainerTypeDto
	 * @throws ImproException
	 */
	ContainerTypeDto loadContainerType(int ContainerTypId) throws ImproException;
	
	/**
	 * Method to check duplicate ContainerType Description
	 * @param name
	 * @return boolean 
	 * @throws ImproException
	 */
	boolean checkDuplicateContainerType(String name) throws ImproException;

}
