package com.ko.impro.service.gatekeeping;

import java.util.List;

import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.exception.ImproException;

/**
 * @author Vilas
 * Services provided for ProductGateKeeping
 */
public interface ProductGateKeepingService {

	/**
	 * Load the complete/incomplete records
	 * 
	 * @param isInComplete
	 * @return List<ProductGateKeepingDto>
	 * @throws ImproException
	 */
	List<ProductGateKeepingDto> loadGateKeeping(boolean isInComplete) throws ImproException;

	/**
	 * Pull the gateKeeping Details
	 * @param gatekeepingId
	 * @return ProductGateKeepingDto
	 * @throws ImproException
	 */
	ProductGateKeepingDto loadGateKeeping(int gatekeepingId) throws ImproException;

	/**
	 * To purge  a  productGatekeeping record
	 * @param productCode
	 * @throws ImproException
	 */
	void purgeProductGateKeeping(int  productCode) throws ImproException;

	/**
	 * Method to trigger StoredProcedure [dbo].[ProductUpdateFromGatekeeping]
	 * @return ResponseEntity<GateKeepingResponseDto>
	 */
	void pushProducts() throws ImproException;

	/**
	 * Method to edit Gate keeping
	 * @param gatekeepingDto
	 * @throws ImproException
	 */
	void editGateKeeping(ProductGateKeepingDto gatekeepingDto) throws ImproException;

	

}
