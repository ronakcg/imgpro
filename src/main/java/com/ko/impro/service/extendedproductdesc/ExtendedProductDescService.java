package com.ko.impro.service.extendedproductdesc;

import java.util.List;

import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.exception.ImproException;
/**
 * @author hlekshmy
 * ExtendedProductDescService class for handling 
 * CRUD operation for ExtendedProductDesc
 * 
 */
public interface ExtendedProductDescService{
	
	/**
	 * Load the ExtendedProduct
	 * @param  boolean isMaintainence
	 * @return List<ExtendedProductDto>
	 * @throws ImproException
	 */
	List<ExtendedProductDescDto> loadExtendedProductDesc() throws ImproException;
	

	/**
	 * Load ExtendedProductDesc based on brandCode
	 * @param brandCode
	 * @return ExtendedProductDescDto
	 * @throws ImproException
	 */
	List<ExtendedProductDescDto> loadExtendedProductDesc(List<String> brandList) throws ImproException;
	
	
	/**
	 * Method to add the ExtendedProdDesc
	 * @param ExtdProdDescDto
	 * @throws ImproException
	 */
	void addExtendedProductDesc(ExtendedProductDescDto ExtdProdDescDto) throws ImproException;
	
	/**
	 * Method to delete the  ExtendedProdDesc
	 * @param dto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteExtendedProductDesc(ExtendedProductDescDto dto) throws ImproException;

	/**
	 * Method to edit  the ExtendedProdDesc
	 * @param dto
	 * @return String
	 * @throws ImproException
	 */
	String editExtendedProductDesc(ExtendedProductDescDto dto) throws ImproException;
	
	/**
	 * Method to load the  ExtendedProdDesc based on extendedProdDescCode
	 * @param ExtdProdDesId
	 * @return  ExtendedProductDescDto
	 * @throws ImproException
	 */
	ExtendedProductDescDto loadExtendedProductDesc(int ExtdProdDesId) throws ImproException;
	
	/**
	 * Method to duplicate check the ExtendedProdDesc
	 * @param name
	 * @param boolean isLongDesc
	 * @return boolean 
	 * @throws ImproException
	 */
	boolean checkDuplicateExtendedProductDesc(String name,boolean isLongDesc) throws ImproException;

}
