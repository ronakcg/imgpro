package com.ko.impro.service.segment;


import java.util.List;

import com.ko.impro.dto.SegmentDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Service class for doing CRUD operation for Segment
 *
 */
public interface SegmentService {

	/**
	 * Load the Segments
	 * @return List<SegmentDto> 
	 * @throws ImproException
	 */
	List<SegmentDto> loadSegment() throws ImproException;

	/**
	 * Load the Segments based on  subCategoryList
	 * @param subCategoryList
	 * @return List<SegmentDto>
	 * @throws ImproException
	 */
	List<SegmentDto> loadSegment(List<String> subCategoryList) throws ImproException;
	
	/**
	 * Method to add the Segment
	 * @param segmentDto
	 * @return int
	 * @throws ImproException
	 */
	int addSegment(SegmentDto segmentDto) throws ImproException;
	
	/**
	 * Method to add the segment-SubCategory entries in the join table
	 * @param SegmentDto segmentDto
	 * @param genSegmentCode
	 * @throws ImproException
	 */
	void addSegmentSubCategoryMapping(SegmentDto segmentDto, int genSegmentCode) throws ImproException; 
	
	/**
	 * Method to delete the Segment
	 * @param segmentDto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteSegment(SegmentDto segmentDto) throws ImproException;

	/**
	 * Method to edit the Segment
	 * @param segmentDto
	 * @return String message
	 * @throws ImproException
	 */
	String editSegment(SegmentDto segmentDto) throws ImproException;

	/**
	 * Method to check the duplicate Segment Desc
	 * @param segmentDesc
	 * @return boolean 
	 * @throws ImproException
	 */
	boolean checkDuplicateSegment(String  segmentDesc)  throws ImproException;
	
	/**
	 * Method to validate the segment/old-SubCagtegory in products table if the 
	 * subCategory is changed to new value.
	 * @param segmentDto
	 * @return boolean
	 * @throws ImproException
	 */
	 boolean validateInProduct(SegmentDto segmentDto) throws ImproException;

	/**
	 * Load Segment details for a given segmentCode
	 * @param segmentID
	 * @return SegmentDto
	 * @throws ImproException
	 */
	SegmentDto loadSegment(int segmentID) throws ImproException;

}


