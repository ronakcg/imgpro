package com.ko.impro.service.packagestyle;

import java.util.List;

import com.ko.impro.dto.PackageStyleDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Service class for doing CRUD operation for PackageStyle
 *
 */
public interface PackageStyleService {
	
	/**
	 * Loads the package Styles
	 * @param isMaintainence
	 * @return List<PackageStyleDto>
	 * @throws ImproException
	 */
	List<PackageStyleDto> loadPackageStyle() throws ImproException;

	/**
	 * Edit the PackageStyle
	 * @param PackageStyleDto
	 * @return String
	 * @throws ImproException
	 */
	String editPackageStyle(PackageStyleDto dto) throws ImproException;

	/**
	 * Add the PackageStyle
	 * @param PackageStyleDto
	 * @throws ImproException
	 */
	void addPackageStyle(PackageStyleDto packageStyleDto) throws ImproException;

	/**
	 * Delete the PackageStyle
	 * @param dto
	 * @throws ImproException
	 */
	String deletePackageStyle(PackageStyleDto packageStyleDto) throws ImproException;

	/**
	 * Load the PackageStyleDto with id
	 * @param pkgStyleCode
	 * @return PackageStyleDto
	 * @throws ImproException
	 */
	PackageStyleDto loadPackageStyle(int pkgStyleCode) throws ImproException;

	/**
	 * Checking the duplicate PackageStyle
	 * @param name
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicatePackageStyle(String name)  throws ImproException;

}