package com.ko.impro.service.deletemessagedata;

import java.util.List;

import com.ko.impro.dto.DeleteMessagesDataDto;
import com.ko.impro.exception.ImproException;
/**
 * @author hlekshmy
 * DeleteMessageDataService class for handling 
 * CRUD operation for DeleteMessageData
 * 
 */
public interface DeleteMessageDataService {

	/**
	 * Load the DeleteMessagesDataDao details
	 * @param boolean isMaintainence 
	 * @return List<DeleteMessagesDataDto>
	 * @throws ImproException
	 */
	List<DeleteMessagesDataDto> loadDeleteMessagesDatas() throws ImproException;

	
	/**
	 * Method to add DeleteMessagesDataDao
	 * @param DeltMsgDataDto
	 * @throws ImproException
	 */
	void addDeleteMessagesData(DeleteMessagesDataDto DeltMsgDataDto) throws ImproException;

	/**
	 * Method to delete the DeleteMessagesDataDao
	 * @param DeltMsgDataDto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteDeleteMessagesData(DeleteMessagesDataDto DeltMsgDataDto) throws ImproException;

	/**
	 * Method to edit DeleteMessagesDataDao
	 * @param DeleteMessagesDataDto dto
	 * @return String message
	 * @throws ImproException
	 */
	String editDeleteMessagesData(DeleteMessagesDataDto dto) throws ImproException;
	
	/**
	 * Method to load DeleteMessagesDataDao based on DeleteMessagesDataCode
	 * @param DeltMsgDataId
	 * @return DeleteMessagesDataDto
	 * @throws ImproException
	 */
	DeleteMessagesDataDto loadDeleteMessagesData(int DeltMsgDataId) throws ImproException;
	
	/**
	 * Method to check duplicate DeleteMessagesDataDao Description
	 * @param name
	 * @return boolean 
	 * @throws ImproException
	 */
	boolean checkDuplicateDeleteMessagesData(String name) throws ImproException;

}
