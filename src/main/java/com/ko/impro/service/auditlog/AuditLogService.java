package com.ko.impro.service.auditlog;

import java.util.List;

import com.ko.impro.dto.AuditLogDto;
import com.ko.impro.exception.ImproException;

/**
 * @author Vilas
 * AuditLogService class for handling CRUD operation for Auditing
 * 
 */
public interface AuditLogService {

	/**
	 * Exports the audit logs
	 * @return List<AuditLogDto>
	 * @throws ImproException
	 */
	List<AuditLogDto> exportAuditLog() throws ImproException;

	/**
	 * Load the audit log details
	 * @return List<AuditLogDto>
	 * @throws ImproException
	 */
	List<AuditLogDto> loadAuditLog() throws ImproException;

}
