package com.ko.impro.service.packagesize;

import java.util.List;

import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Service class for doing CRUD operation for PackageSize
 *
 */
public interface PackageSizeService {

	/**
	 * Load the PackageSize
	 * @param boolean isMaintainence
	 * @return List<PackageSizeDto>
	 * @throws ImproException
	 */
	List<PackageSizeDto> loadPackageSize() throws ImproException;
	
	/**
	 * Load the PackageSize based on PackageCode
	 * @param List<String> packageList
	 * @return List<PackageSizeDto>
	 * @throws ImproException
	 */
	List<PackageSizeDto> loadPackageSize(List<String> packageList) throws ImproException;


	/**
	 * Method to add the package
	 * @param packageSizeDto
	 * @throws ImproException
	 */
	void addPackageSize(PackageSizeDto packageSizeDto) throws ImproException;

	/**
	 * @param packageSizeDto
	 * @return String
	 * @throws ImproException
	 */
	String deletePackageSize(PackageSizeDto packageSizeDto) throws ImproException;

	/**
	 * Method to edit the package
	 * @param packageSizeDto
	 * @return String
	 * @throws ImproException
	 */
	String editPackageSize(PackageSizeDto packageSizeDto) throws ImproException;
	
	/**
	 * Load the package size by id
	 * @param pkgSizeCode
	 * @return PackageSizeDto
	 * @throws ImproException
	 */
	PackageSizeDto loadPackageSizeById(int pkgSizeCode) throws ImproException;
	
	/**
	 * Method to check duplicate package size dedsc
	 * @param name
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicatePackageSize(String name) throws ImproException;

}
