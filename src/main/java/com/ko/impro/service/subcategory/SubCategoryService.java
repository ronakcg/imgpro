package com.ko.impro.service.subcategory;

import java.util.List;

import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.exception.ImproException;

/**
 * @author hlekshmy
 * Service class for doing CRUD operation for SubCategory
 *
 */
public interface SubCategoryService {

	/**
	 * Load the SubCategory
	 * @return List<SubCategoryDto>
	 * @throws ImproException
	 */
	List<SubCategoryDto> loadSubCategory() throws ImproException;

	/**
	 * Load the SubCategory based on categoryList
	 * @param categoryList
	 * @return List<SubCategoryDto>
	 * @throws ImproException
	 */
	List<SubCategoryDto> loadSubCategory(List<String> categoryList) throws ImproException;

	/**
	 * Method to add SubCategory
	 * @param subcategoryDto
	 * @throws ImproException
	 * @return int
	 */
	int addSubCategory(SubCategoryDto subcategoryDto) throws ImproException;

	/**
	 * Method to delete SubCategory
	 * @param subcategoryDto
	 * @return String
	 * @throws ImproException
	 */
	String deleteSubCategory(SubCategoryDto subcategoryDto) throws ImproException;

	/**
	 * Method to edit SubCategory
	 * @param subcategoryDto
	 * @return String 
	 * @throws ImproException
	 */
	
	String editSubCategory(SubCategoryDto subcategoryDto) throws ImproException;
	
	/**
	 * Method to Check/Validate Duplication of SubCategory at the time of 
	 * adding new category
	 * @param subCategoryDesc
	 * @return boolean 
	 * @throws ImproException
	 */
	boolean checkDuplicateSubCategory(String subCategoryDesc) throws ImproException;
	
	/**
	 * Method to Map SubCategory to one or multiple Category.
	 * @param subcategoryDto
	 * @throws ImproException
	 */
	void addCategoyMapping(SubCategoryDto subcategoryDto,int generatSubCatCode) throws ImproException;
	
	/**
	 * Load the SubCategory
	 * @param subCatID
	 * @return SubCategoryDto
	 * @throws ImproException
	 */
	SubCategoryDto loadSubCategory(int subCatID) throws ImproException;

}
