package com.ko.impro.service.subsegment;

import java.util.List;

import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;
/**
 * @author hlekshmy
 * Service class for doing CRUD operation for SubSegment
 */
public interface SubSegmentService {

	/**
	 * Load the SubSegments
	 * @return List<SubSegmentDto>
	 * @throws ImproException
	 */
	List<SubSegmentDto> loadSubSegment() throws ImproException;

	/**
	 * Load the SubSegments based on segmentList
	 * @param segmentList
	 * @return List<SubSegmentDto>
	 * @throws ImproException
	 */
	List<SubSegmentDto> loadSubSegment(List<String> segmentList) throws ImproException;

	/**
	 * Method to add SubSegment
	 * @param subSegmentDto
	 * @return int
	 * @throws ImproException
	 */
	int addSubSegment(SubSegmentDto subSegmentDto) throws ImproException;

	/**
	 * Method to add Segment-SubSegment info in join table
	 * @param subSegmentDto
	 * @param generSubSegCode
	 * @throws ImproException
	 */
	void addSegmentSubSegmentMapping(SubSegmentDto subSegmentDto, int generSubSegCode) throws ImproException;

	/**
	 * Method to edit SubSegment
	 * @param subSegmentDto
	 * @return String
	 * @throws ImproException
	 */
	String editSubSegment(SubSegmentDto subSegmentDto) throws ImproException;

	/**
	 * Method to delete SubSegment
	 * @param subSegmentDto
	 * @return String message
	 * @throws ImproException
	 */
	String deleteSubSegment(SubSegmentDto subSegmentDto) throws ImproException;

	/**
	 * Check the duplicate SubSegment desc
	 * @param subSegmentDesc
	 * @return boolean
	 * @throws ImproException
	 */
	boolean checkDuplicateSubSegment(String subSegmentDesc) throws ImproException;
	
	/**
	 * Load the SubSegment Information for a given subsegment Code
	 * @param subSegmentId
	 * @return SubSegmentDto
	 * @throws ImproException
	 */
	SubSegmentDto loadSubSegment(int subSegmentId) throws ImproException;
	
	
}
