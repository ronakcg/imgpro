package com.ko.impro.controller.base;

/**
 * @author swshedge
 * BaseController to define type for consume and produce
 *
 */
public class BaseController {
	
	/**Type of Consume/Produce */
	public static final String type="application/json";
}
