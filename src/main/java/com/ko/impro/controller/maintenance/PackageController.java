package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.packages.PackageService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * PackageController to handle the 
 * CRUD operations of Package
 *
 */
@RestController
@RequestMapping(path = "/package")
public class PackageController extends BaseController {

	/**
	 * Reference of ManufacuturerService to call the implementation method
	 */
	@Autowired
	private PackageService packageService;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PackageController.class);
	
	/**	 
	 * To Show Package details of specific package code  
	 * @param PackageDto packageDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewPackage(@RequestParam (value="packageCode") int packageCode) {
		LOG.info("PackageController-->viewPackage()-->Begin");

		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			PackageDto obj = packageService.loadPackage(packageCode);
			if(obj==null) {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}
		} catch(ImproException e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageController-->viewPackage()-->Exception " + e.getMessage());
			}
		}
		LOG.info("PackageController-->viewPackage()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
		}
	
	/**	 
	 * To add new Package will throw Error message if the Package already exist  
	 * @param PackageDto packageDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)	
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> addPackage(@RequestBody PackageDto packageDto) {
		LOG.info("PackageController-->addPackage()-->Begin");

		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			packageService.addPackage(packageDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);	
		} catch(ImproException e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageController-->addPackage()-->Exception " + e.getMessage());
			}
		}
		LOG.info("PackageController-->addPackage()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * To Edit the existing package
	 * @param PackageDto packageDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(path = "/edit",method = RequestMethod.POST)
	@Produces(type)	
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> editPackage(@RequestBody PackageDto packageDto) {
		LOG.info("PackageController-->editPackage()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			String msg = packageService.editPackage(packageDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageController-->editPackage()-->Exception " + e.getMessage());
			}
		}
		LOG.info("PackageController-->editPackage()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * To Delete package If no dependency available in product
	 * @param PackageDto packageDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> deletePackage(@RequestBody PackageDto packageDto) {
		LOG.info("PackageController-->deletePackage()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg = packageService.deletePackage(packageDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageController-->deletePackage()-->Exception " + e.getMessage());
			}
		}
		LOG.info("PackageController-->deletePackage()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * To check the duplicate packageName
	 * @param PackageDto packageDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@GET
	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicatePackage(@RequestParam (value="packageName") String packageName) {		LOG.info("PackageController-->checkDuplicatePackage()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated=packageService.checkDuplicatePackageName(packageName);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.SUCCESS_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageController-->checkDuplicatePackage()-->Exception " + e.getMessage());
			}
		}
		LOG.info("PackageController-->checkDuplicatePackage()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
}
