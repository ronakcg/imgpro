package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.service.brand.BrandService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * BrandController to handle CURD operations of Brand
 *
 */
@RestController
@RequestMapping(path = "/brand")
public class BrandController extends BaseController{
	
	/**
	 * Reference of BrandService to call the implementation method
	 */
	@Autowired
	private BrandService service;
	
	/**
	 * Logger object to log information 
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BrandController.class);
	
	/**
	 * To edit the Brand
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto> 
	 */

	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> editBrand(@RequestBody BrandDto dto) {
		LOG.info("BrandController-->editBrand()-->Begin");
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		String msg="";

		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		try {
			//Checking for edit, if user submit the yes to modify the 
			//manufacturer forcefully or trying to edit
			if(dto.getOldValue() == null && dto.isYesButton()) {
				msg=service.editBrand(dto);	
			} else {
				service.validateInProduct(dto);
				dto.setYesButton(true);
			}
			response.setResponseObject(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
						
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("BrandController-->editBrand()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("BrandController-->editBrand()-->End");
		 returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		 return returnEntity;
	}
	
	/**
	 * To add the Brand
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto> 
	 */
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> addBrand(@RequestBody BrandDto dto) {
		LOG.info("BrandController-->addBrand()-->Begin");
		
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			service.addBrand(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("BrandController-->addBrand()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("BrandController-->addBrand()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * To Delete the Brand
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto> 
	 */

	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> deleteBrand(@RequestBody BrandDto dto) {
		LOG.info("BrandController-->deleteBrand()-->Begin");
		
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.deleteBrand(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("BrandController-->deleteBrand()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("BrandController-->deleteBrand()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}	
	
	/**
	 * To view the Brand
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto> 
	 */

	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewBrand(@RequestParam (value="brandCode") String brandCode) {
		LOG.info("BrandController-->viewBrand()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			BrandDto obj=service.loadBrand(Integer.parseInt(brandCode));
			if(null==obj) {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}			
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("BrandController-->viewBrand()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("BrandController-->viewBrand()-->End");

		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Checking duplicate brand
	 * @param name
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */ 

	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateBrand(@RequestParam (value = "brandName") String name,@RequestParam (value = "isLong") boolean isLong) {
		LOG.info("BrandController-->checkDuplicateBrand()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated=service.checkDuplicateBrand(name,isLong);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("BrandController-->checkDuplicateBrand()-->Exception " + ex.getMessage());
			}
		}
		LOG.info("BrandController-->checkDuplicateBrand()-->End");

		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
