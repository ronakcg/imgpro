package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.service.packagesize.PackageSizeService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * PackageSizeController to handle the 
 * CRUD operations of PackageSize
 *
 */
@RestController
@RequestMapping(path ="/packageSize")
public class PackageSizeController extends BaseController {

	/**
	 * Reference of PackageSizeService to call the implementation method
	 */
	@Autowired
	private PackageSizeService service;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PackageSizeController.class);
	
	/**	 
	 * To add the packageSize 
	 * @param packageSizeDto 
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> addPackageSize(@RequestBody PackageSizeDto packageSizeDto){
		LOG.info("PackageSizeDaoImpl-->addPackageSize()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			service.addPackageSize(packageSizeDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->addPackageSize()-->Exception " + e.getMessage());
			}
			
		}
		LOG.info("PackageSizeDaoImpl-->addPackageSize()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	

	/**	 
	 * To delete the packageSize 
	 * @param packageSizeDto 
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value="/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> deletePackageSize(@RequestBody PackageSizeDto packageSizeDto) {
		LOG.info("PackageSizeDaoImpl-->deletePackageSize()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.deletePackageSize(packageSizeDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->deletePackageSize()-->Exception " + e.getMessage());
			}
			
		}		
		LOG.info("PackageSizeDaoImpl-->deletePackageSize()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**	 
	 * To edit the packageSize 
	 * @param packageSizeDto 
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value="/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> editPackageSize(@RequestBody PackageSizeDto packageSizeDto){
		LOG.info("PackageSizeDaoImpl-->editPackageSize()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.editPackageSize(packageSizeDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->editPackageSize()-->Exception " + e.getMessage());
			}
		}
		LOG.info("PackageSizeDaoImpl-->editPackageSize()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	

	/**	 
	 * To view all the packageSize OR based on packagesizeID 
	 * @param pkgizeId 
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value="/view",method = RequestMethod.GET)
	@Produces(type)
	protected ResponseEntity<MaintenanceResponseDto> viewPackageSize(@RequestParam (value = "packageSizeCode") int pkgizeId){
		LOG.info("PackageSizeDaoImpl-->viewPackageSize()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			PackageSizeDto dto=service.loadPackageSizeById(pkgizeId);
			if(dto==null) {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}else {
				response.setResponseObject(dto);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->viewPackageSize()-->Exception " + e.getMessage());
			}
		}
		LOG.info("PackageSizeDaoImpl-->viewPackageSize()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**	 
	 * To check duplicate PackageSize Description 
	 * @param name 
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value="/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	protected ResponseEntity<MaintenanceResponseDto> checkDuplicatePackageSizeDesc(@RequestParam (value = "packageSizeDescription") String name) {
		LOG.info("PackageSizeDaoImpl-->checkDuplicatePackageSizeDesc()-->BEGIN");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			boolean isDuplicated=service.checkDuplicatePackageSize(name);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageSizeDaoImpl-->checkDuplicatePackageSizeDesc()-->Exception " + e.getMessage());
			}
		}
		LOG.info("PackageSizeDaoImpl-->checkDuplicatePackageSizeDesc()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
