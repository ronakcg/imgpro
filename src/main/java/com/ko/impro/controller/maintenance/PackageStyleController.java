package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.dto.PackageStyleDto;
import com.ko.impro.service.packagestyle.PackageStyleService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * PackageStyleController to handle the 
 * CRUD operations of PackageStyle
 *
 */
@RestController
@RequestMapping(path = "/packageStyle")
public class PackageStyleController extends BaseController {
	
	/**
	 * Reference of PackageSizeService to call the implementation method
	 */
	@Autowired
	private PackageStyleService pkgStyleService;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PackageStyleController.class);
	
	
	/**
	 * Method to add PackageStyle
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> addPackageStyle(@RequestBody PackageStyleDto dto) {
		LOG.info("PackageStyleController-->addPackageStyle()-->Begin");

		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			pkgStyleService.addPackageStyle(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageStyleController-->addPackageStyle()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("PackageStyleController-->addPackageStyle()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	
	/**
	 * Method to delete PackageStyle
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> deletePackageStyle(@RequestBody PackageStyleDto dto) {
		LOG.info("PackageStyleController-->deletePackageStyle()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=pkgStyleService.deletePackageStyle(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageStyleController-->deletePackageStyle()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("PackageStyleController-->deletePackageStyle()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**View PackageStyle By ID
	 * @param pkgDtylId
	 * @return MaintenanceResponseDto
	 */
	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewPackageStyleById(@RequestParam (value = "styleCode") int pkgDtylId) {
		LOG.info("PackageStyleController-->viewPackageStyleById()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			PackageStyleDto obj=pkgStyleService.loadPackageStyle(pkgDtylId);
			if(obj == null) {
				response.setCode(AppConstants.FAILURE_CODE);
				response.setMessage(AppConstants.FAILURE_MSG);
			}else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}			
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageStyleController-->viewPackageStyleById()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("PackageStyleController-->viewPackageStyleById()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	
	/**Check duplicate value of packageStyle
	 * @param name
	 * @return MaintenanceResponseDto
	 */
	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicatePackageStyle(@RequestParam (value = "styleDesc") String name) {
		LOG.info("PackageStyleController-->checkDuplicatePackageStyle()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated=pkgStyleService.checkDuplicatePackageStyle(name);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageStyleController-->checkDuplicatePackageStyle()-->Exception " + e.getMessage());
			}
		}
		LOG.info("PackageStyleController-->checkDuplicatePackageStyle()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**To edit PackageStyle
	 * @param dto
	 * @return MaintenanceResponseDto
	 */
	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> editPackageStyle(@RequestBody PackageStyleDto dto) {
		LOG.info("PackageStyleController-->editPackageStyle()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=pkgStyleService.editPackageStyle(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("PackageStyleController-->editPackageStyle()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("PackageStyleController-->editPackageStyle()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
