package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.service.segment.SegmentService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * SegmentController to handle the 
 * CRUD operations of Segment
 *
 */
@RestController
@RequestMapping(path = "/segment")
public class SegmentController extends BaseController {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SegmentController.class);

	/**
	 * Reference of SegmentService to call the implementation method
	 */
	@Autowired
	private SegmentService segmentService;

	/**
	 * Method to add new Segment 
	 * @param SegmentDto segmentDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> addSegment(@RequestBody SegmentDto segmentDto) {
		LOG.info("SegmentController()-->addSegment()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		int genSegCode = 0;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			genSegCode = segmentService.addSegment(segmentDto);
			if (genSegCode != 0) {
				segmentService.addSegmentSubCategoryMapping(segmentDto, genSegCode);
				response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SegmentController-->addSegment()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SegmentController()-->addSegment()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}

	/**
	 * Method to Edit the existing Segment and will throw Error message if updated
	 * Segment already exist
	 * @param SegmentDto segmentDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> editSegment(@RequestBody SegmentDto segmentDto) {
		LOG.info("SegmentController()-->editSegment()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response = new MaintenanceResponseDto();
		String message = "";
		try {
			if (segmentDto.getOldValue() == null && segmentDto.isYesButton()) {
				message = segmentService.editSegment(segmentDto);
			} else {
				segmentService.validateInProduct(segmentDto);
				segmentDto.setYesButton(true);
			}
			response.setResponseObject(segmentDto);
			response.setMessage(message);
			response.setCode(AppConstants.SUCCESS_CODE);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SegmentController-->editSegment()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SegmentController()-->editSegment()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}

	/**
	 * Method to delete Segment If no dependency available
	 * @param SegmentDto segmentDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> deleteSegment(@RequestBody SegmentDto segmentDto) {
		LOG.info("SegmentController()-->deleteSegment()-->End-->");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		String msg;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			msg = segmentService.deleteSegment(segmentDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SegmentController-->deleteSegment()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SegmentController()-->deleteSegment()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}

	/**
	 * Method to Check Duplication of Segment Description.
	 * @param SSegmentDto segmentDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	@Consumes(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateSegment(@RequestParam (value="segmentDesc") String desc) {
		LOG.info("SegmentController()-->checkDuplicateSegment()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated = segmentService.checkDuplicateSegment(desc);
			if (isDuplicated) {
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SegmentController-->checkDuplicateSegment()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SegmentController()-->checkDuplicateSegment()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	
	/**
	 * To view the Segment
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto> 
	 */
	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewSegment(@RequestParam (value="segmentCode") int segmentCode) {
		LOG.info("SegmentController-->viewSegment()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			SegmentDto obj=segmentService.loadSegment(segmentCode);
			if(obj == null) {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}			
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SegmentController-->viewSegment()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("SegmentController-->viewSegment()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}