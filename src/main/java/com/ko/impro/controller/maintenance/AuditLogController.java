package com.ko.impro.controller.maintenance;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.AuditLogDto;
import com.ko.impro.dto.AuditLogRequestDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.report.AuditLogExcelReportGenerator;
import com.ko.impro.report.ExcelGeneratorFactory;
import com.ko.impro.service.auditlog.AuditLogService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * AuditLogController to handle audit view and export operations
 *
 */
@RestController
@RequestMapping(path = "/auditLog")
public class AuditLogController  extends BaseController  {

	/**
	 * Reference of AuditLogService to call the implementation method
	 */
	@Autowired
	private AuditLogService logService;
	
	/**
	 * Logger object to log information
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AuditLogController.class);
	
	/** 
	 * Method to create excel for Audit Log Export
	 * @return AuditExcelReportGenerator
	 * @throws IOException
	 */
	public AuditLogExcelReportGenerator getExcelGenerator() throws IOException {
		return ExcelGeneratorFactory.getInstance().getAuditLogExcelReportGenerator(AppConstants.AUDIT_FILENAME,
				AppConstants.AUDIT_SHEETNAME);
	}
	
	/**
	 * Load all Shapes GET
	 * @return List<ShapeDto>
	 * @throws ImproException
	 */

	@RequestMapping(path = "/view",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<AuditLogDto>> loadAuditLog() throws ImproException {
		LOG.info("AuditLogController-->loadAuditLog()-->Begin");
		List<AuditLogDto> auditList =  logService.loadAuditLog();
		LOG.info("AuditLogController-->loadAuditLog()-->End");
		return new ResponseEntity<List<AuditLogDto>>(auditList, HttpStatus.OK);
	}	
	
	/**
	 * Method used to export all product details when the users clicks exportButton
	 * without selecting any check boxes.
	 * @return ResponseEntity<byte[]>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/exportAuditLog",method = RequestMethod.GET)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<byte[]> exportAuditLog()  {
		LOG.info("AuditLogController-->exportAuditLog()-->Begin");
		byte[] bytes = null;
		
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<byte[]> returnEntity;
		HttpStatus httpStatus=HttpStatus.OK;
		
		List<AuditLogDto> auditLogDtoList = null;
		try {
			auditLogDtoList = logService.exportAuditLog(); 
			AuditLogExcelReportGenerator excelGenerator = getExcelGenerator(); 
			XSSFWorkbook workBook = excelGenerator.generateReport(auditLogDtoList);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			workBook.write(bos);
			bos.close();
			bytes = bos.toByteArray();
			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.set("charset", "utf-8");
			headers.set("Content-Disposition", "attachment; filename=" + "audit");
		} catch (Exception ex) {
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("AuditLogController-->exportAuditLog()-->Exception" + ex.getMessage());
			}
		}
		returnEntity = new ResponseEntity<byte[]>(bytes, headers, httpStatus);
		LOG.info("AuditLogController-->exportAuditLog()-->End");
		return returnEntity;
	}	
}
