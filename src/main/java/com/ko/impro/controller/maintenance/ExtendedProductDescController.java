package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.service.extendedproductdesc.ExtendedProductDescService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * ExtendedProductDescController to handle the 
 * CRUD operations of ExtendedProductDes
 *
 */
@RestController
@RequestMapping(path = "/extendedProductDesc")
public class ExtendedProductDescController extends BaseController {
	
	/**
	 * Reference of ExtendedProductDescService to call the implementation method
	 */
	@Autowired
	private ExtendedProductDescService service;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ExtendedProductDescController.class);
	
	/**
	 * Method to edit the ExtendedProdDesc
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> editExtendedProductDesc(@RequestBody ExtendedProductDescDto dto) {
		LOG.info("ExtendedProductDescController-->editExtendedProductDesc()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.editExtendedProductDesc(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ExtendedProductDescController-->editExtendedProductDesc()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ExtendedProductDescController-->editExtendedProductDesc()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 *  Method to add the ExtendedProdDesc
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> addExtendedProductDesc(@RequestBody ExtendedProductDescDto dto) {
		LOG.info("ExtendedProductDescController-->addExtendedProductDesc()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			service.addExtendedProductDesc(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ExtendedProductDescController-->addExtendedProductDesc()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ExtendedProductDescController-->addExtendedProductDesc()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to delete the  ExtendedProdDesc
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> deleteExtendedProductDesc(@RequestBody ExtendedProductDescDto dto) {
		LOG.info("ExtendedProductDescController-->deleteExtendedProductDesc()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.deleteExtendedProductDesc(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ExtendedProductDescController-->deleteExtendedProductDesc()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ManufacuturerController-->deleteExtendedProductDesc()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to view  the ExtendedProdDesc
	 * @param extedProdDescId
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewExtendedProductDesc(@RequestParam (value = "extendedProdDescCode") int extedProdDescId) {
		LOG.info("ExtendedProductDescController-->viewExtendedProductDesc()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			ExtendedProductDescDto obj=service.loadExtendedProductDesc(extedProdDescId);
			if(obj==null) {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ExtendedProductDescController-->viewExtendedProductDesc()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ManufacuturerController-->viewExtendedProductDesc()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * to validate the duplicate ExtendedProdDesc
	 * @param name
	 * @param isLongValue
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateExtendedProdDesc(@RequestParam (value = "name") String name,@RequestParam (value = "isLongValue") boolean isLongValue) {
		LOG.info("ExtendedProductDescController-->checkDuplicateExtendedProdDesc()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated=service.checkDuplicateExtendedProductDesc(name,isLongValue);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ExtendedProductDescController-->checkDuplicateExtendedProdDesc()-->Exception " + e.getMessage());
			}
		}
		LOG.info("ExtendedProductDescController-->checkDuplicateExtendedProdDesc()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
