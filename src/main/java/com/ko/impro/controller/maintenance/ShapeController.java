package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.dto.ShapeDto;
import com.ko.impro.service.shape.ShapeService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * ShapeController to handle the 
 * CRUD operations of Shape
 *
 */
@RestController
@RequestMapping(path = "/shape")
public class ShapeController extends BaseController {

	/**
	 * Reference of ShapeService to call the implementation method
	 */
	@Autowired
	private ShapeService shapeService;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ShapeController.class);
	
	
	/**Load Shapes by ShapeID
	 * @param shapeId
	 * @return
	 */
	@GET
	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewShape(@RequestParam (value = "shapeId") int shapeId) {
		LOG.info("ShapeController-->viewShape()-->Begin");

		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			ShapeDto obj=shapeService.loadShapeID(shapeId);
			if(null==obj) {
				response.setCode(AppConstants.FAILURE_CODE);
				response.setMessage(AppConstants.FAILURE_MSG);
			}else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}			
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ShapeController-->viewShape()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ShapeController-->viewShape()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	
	/**To check duplicate shape
	 * @param name
	 * @return MaintenanceResponseDto
	 */
	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateShape(@RequestParam (value = "shapeName") final String name) {
		LOG.info("ShapeController-->checkDuplicateShape()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		
		try {
			if(shapeService.checkDuplicateShape(name)){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ShapeController-->checkDuplicateShape()-->Exception " + e.getMessage());
			}
		}
		LOG.info("ShapeController-->checkDuplicateShape()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to add Shape
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> addShapes(@RequestBody ShapeDto dto) {
		LOG.info("ShapeController-->addShapes()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			shapeService.addShapes(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ShapeController-->addShapes()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ShapeController-->addShapes()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**Edit the Shape
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> editShapes(@RequestBody ShapeDto dto) {
		LOG.info("ShapeController-->editShapes()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=shapeService.editShapes(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ShapeController-->editShapes()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ShapeController-->editShapes()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}	
	
	/**
	 * Method to delete Shapes
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> deleteShapes(@RequestBody ShapeDto dto) {
		LOG.info("ShapeController-->deleteShapes()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=shapeService.deleteShapes(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ShapeController-->deleteShapes()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ShapeController-->deleteShapes()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
