package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.service.subcategory.SubCategoryService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * SubCategoryController to handle the 
 * CRUD operations of SubCategory
 *
 */
@RestController
@RequestMapping(path = "/subCategory")
public class SubCategoryController extends BaseController{
	/**
	 * Reference of SubCategoryService to call the implementation method
	 */
	@Autowired 
	private SubCategoryService subcatService;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SubCategoryController.class);
	

	/**
	 * Method to Display SubCategory and category associated with it for specific Id 
	 * @param SubCategoryDto subcategoryDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewSubCategory(@RequestParam (value="subCategoryCode") int subCategoryCode) {
		LOG.info("SubCategoryController-->viewSubCategory()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			SubCategoryDto obj=subcatService.loadSubCategory(subCategoryCode);
			if(obj == null) {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			} else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}						
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SubCategoryController-->viewSubCategory()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("SubCategoryController-->viewSubCategory()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	
	/**
	 * Method to add new SubCategory  
	 * @param SubCategoryDto subcategoryDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> addSubCategory(@RequestBody SubCategoryDto subcategoryDto) {
		LOG.info("SubCategoryController-->addSubCategory()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		int genSubCatCode=0;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			genSubCatCode =subcatService.addSubCategory(subcategoryDto);
			if(genSubCatCode!=0) {
				subcatService.addCategoyMapping(subcategoryDto,genSubCatCode);
				response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SubCategoryController-->addSubCategory()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SubCategoryController-->addSubCategory()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to Edit the existing SubCategory  
	 * @param SubCategoryDto subcategoryDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> editSubCategory(@RequestBody SubCategoryDto subcategoryDto) {
		LOG.info("SubCategoryController-->editSubCategory()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();		
		try {
			String message = subcatService.editSubCategory(subcategoryDto);
			response.setMessage(message);
			response.setCode(AppConstants.SUCCESS_CODE);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SubCategoryController-->editSubCategory()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SubCategoryController-->editSubCategory()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to Delete SubCategory If no dependency available 
	 * @param SubCategoryDto subcategoryDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> deleteSubCategory(@RequestBody SubCategoryDto subcategoryDto) {
		LOG.info("SubCategoryController-->deleteSubCategory()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		String msg;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			msg = subcatService.deleteSubCategory(subcategoryDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SubCategoryController-->deleteSubCategory()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SubCategoryController-->deleteSubCategory()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to Check Duplication of SubCategory Description. 
	 * @param SubCategoryDto subcategoryDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateSubCategory(@RequestParam (value="subCategoryDescription") String subCategoryDescription)	 {
		LOG.info("SubCategoryController-->checkDuplicateSubCategory()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated=subcatService.checkDuplicateSubCategory(subCategoryDescription);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.SUCCESS_CODE);
			} else {
				response.setMessage("Duplicate Not Present");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SubCategoryController-->checkDuplicateSubCategory()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SubCategoryController-->checkDuplicateSubCategory()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
