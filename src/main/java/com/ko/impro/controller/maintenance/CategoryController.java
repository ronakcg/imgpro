package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.service.category.CategoryService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * CategoryController to handle CURD operations of Category
 *
 */
@RestController
@RequestMapping(path = "/category")
public class CategoryController extends BaseController{

	/**
	 * Reference of CategoryService to call the implementation method
	 */
	@Autowired 
	private CategoryService categoryService;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(CategoryController.class);

	/**
	 * Method to display Category information for specific ID 
	 * @param CategoryDto categoryDto
	 * @return ResponseEntity<ProductResponseDto>
	 */

	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewCategory(@RequestParam (value="categoryCode") int categoryCode) {
	LOG.info("CategoryController-->viewCategory()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			CategoryDto obj=categoryService.loadCategory(categoryCode);
			if(null!=obj) {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("CategoryController-->viewCategory()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("CategoryController-->viewCategory()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}

	/**
	 * Method to add new category will throw Error message 
	 * if the category already exist 
	 * @param CategoryDto categoryDto
	 * @return ResponseEntity<ProductResponseDto>
	 */

	@RequestMapping(path = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> addCategory(@RequestBody CategoryDto categoryDto) {
		LOG.info("CategoryController-->addCategory()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			categoryService.addCategory(categoryDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("CategoryController-->addCategory()-->Exception " + e.getMessage());
			}
		}
		LOG.info("CategoryController-->addCategory()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to Edit the existing category  
	 * @param CategoryDto categoryDto
	 * @return ResponseEntity<ProductResponseDto>
	 */

	@RequestMapping(path = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> editCategory(@RequestBody CategoryDto categoryDto) {
		LOG.info("CategoryController-->editCategory()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();		
		try {
			String msg=categoryService.editCategory(categoryDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("CategoryController-->editCategory()-->Exception " + e.getMessage());
			}
		}
		LOG.info("CategoryController-->editCategory()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to Delete category If no dependency available 
	 * @param CategoryDto categoryDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> deleteCategory(@RequestBody CategoryDto categoryDto) {
		LOG.info("CategoryController-->deleteCategory()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		String msg;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			msg =categoryService.deleteCategory(categoryDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("CategoryController-->deleteCategory()-->Exception " + e.getMessage());
			}
		}
		LOG.info("CategoryController-->deleteCategory()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * To check the duplicate category
	 * @param categoryDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateCategory(@RequestParam (value="categoryDescription") String categoryDescription) {
		LOG.info("CategoryController-->checkDuplicateCategory()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated=categoryService.checkDuplicateCategory(categoryDescription);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.SUCCESS_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("CategoryController-->checkDuplicateCategory()-->Exception " + e.getMessage());
			}
		}
		LOG.info("CategoryController-->checkDuplicateCategory()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
