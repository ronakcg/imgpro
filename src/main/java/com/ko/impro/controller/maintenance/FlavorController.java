package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.service.flavour.FlavorService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * FlavorController to handle the 
 * CRUD operations of Flavor
 *
 */
@RestController
@RequestMapping(path = "/flavor")
public class FlavorController extends BaseController{
	
	/**
	 * Reference of FlavorService to call the implementation method
	 */
	@Autowired
	private FlavorService service;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(FlavorController.class);
	
	/**
	 * Method to edit Flavor
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> editFlavor(@RequestBody FlavorDto dto) {
		LOG.info("FlavorController-->editFlavor()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.editFlavor(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("FlavorController-->editFlavor()-->Exception " + e.getMessage());
			}
		}	
			
		LOG.info("FlavorController-->editFlavor()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to add the flavor,subCategory entries in FlavorSubCategory Join table
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> addFlavor(@RequestBody FlavorDto dto) {
		LOG.info("FlavorController-->addFlavor()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			dto.setFlavorCode(String.valueOf(service.addFlavor(dto)));
			service.addFlavorSubCategory(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("FlavorController-->addFlavor()-->Exception " + e.getMessage());
			}
		}	
		LOG.info("FlavorController-->addFlavor()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to delete the Flavor
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> deleteFlavor(@RequestBody FlavorDto dto) {
		LOG.info("FlavorController-->deleteFlavor()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
		
				String msg=service.deleteFlavor(dto);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage(msg);		
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("FlavorController-->deleteFlavor()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("FlavorController-->deleteFlavor()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to view the all flavors OR based on flavorID
	 * @param flavorId
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewFlavor(@RequestParam (value = "flavorCode") int flavorId) {
		LOG.info("FlavorController-->viewFlavor()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			FlavorDto obj=service.loadFlavor(flavorId);
			if(obj==null) {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("FlavorController-->viewFlavor()-->Exception " + e.getMessage());
			}
		}
				
		LOG.info("FlavorController-->viewFlavor()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to check duplicate flavors names
	 * @param name
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateFlavorDesc(@RequestParam (value = "flavorDesc") String name) {
		LOG.info("FlavorController-->checkDuplicateFlavorDesc()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated=service.checkDuplicateFlavor(name);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("FlavorController-->checkDuplicateFlavorDesc()-->Exception " + e.getMessage());
			}
		}
		LOG.info("FlavorController-->checkDuplicateFlavorDesc()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
