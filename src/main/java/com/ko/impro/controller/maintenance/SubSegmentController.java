package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.service.subsegment.SubSegmentService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * SubSegmentController to handle the 
 * CRUD operations of SubSegment
 *
 */
@RestController
@RequestMapping(path = "/subSegment")
public class SubSegmentController extends BaseController {


	/**
	 * Reference of SubCategoryService to call the implementation method
	 */
	@Autowired
	private SubSegmentService subSegmentService;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SubSegmentController.class);
	
	/**
	 * Method to add new SubSegment 
	 * @param SubSegmentDto subSegmentDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> addSubSegment(@RequestBody SubSegmentDto subSegmentDto) {
		LOG.info("SubSegmentController()-->addSubSegment-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			final int genSubSegmntCode = subSegmentService.addSubSegment(subSegmentDto);
			if (genSubSegmntCode != 0) {
				subSegmentService.addSegmentSubSegmentMapping(subSegmentDto, genSubSegmntCode);
				response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SubSegmentController-->addSubSegment()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SubSegmentController()-->addSubSegment()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}

	/**
	 * Method to Edit the existing SubSegment and will throw Error message if
	 * updated SubSegment already exist
	 * @param SubSegmentDto subSegmentDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> editSubSegment(@RequestBody SubSegmentDto subSegmentDto) {
		LOG.info("SubSegmentController()-->editSubSegment-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			String message = subSegmentService.editSubSegment(subSegmentDto);
			response.setMessage(message);
			response.setCode(AppConstants.SUCCESS_CODE);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SubSegmentController-->editSubSegment()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SubSegmentController()-->editSubSegment()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}

	/**
	 * Method to Delete SubSegment If no dependency available
	 * @param SubSegmentDto subSegmentDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<MaintenanceResponseDto> deleteSubSegment(@RequestBody SubSegmentDto subSegmentDto) {
		LOG.info("SubSegmentController()-->deleteSubSegment begins...!");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		String msg;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			msg = subSegmentService.deleteSubSegment(subSegmentDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SubSegmentController-->deleteSubSegment()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SubSegmentController()-->deleteSubSegment()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}

	/**
	 * Method to Check Duplication of SubSegment Description.
	 * @param SubSegmentDto subSegmentDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	@Consumes(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateSubSegment(@RequestParam (value="subSegmentCode") String subSegmentDesc) {
		LOG.info("SubSegmentController()-->checkDuplicateSubSegment-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated = subSegmentService.checkDuplicateSubSegment(subSegmentDesc);
			if (isDuplicated) {
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("SubSegmentController-->checkDuplicateSubSegment()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SubSegmentController()-->checkDuplicateSubSegment()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to display SubSegment information for specific ID
	 * @param SubSegmentDto  subSegmentDto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@GET
	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)
	protected ResponseEntity<MaintenanceResponseDto> viewSubSegment(@RequestParam (value="subSegmentCode") int subSegmentCode) {
		LOG.info("SubSegmentController-->viewSubSegment()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			SubSegmentDto obj = subSegmentService.loadSubSegment(subSegmentCode);
			if (null != obj) {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}
		} catch (Exception e) {
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			if(LOG.isErrorEnabled()) {
				LOG.error("SubSegmentController-->viewSubSegment()-->Exception " + e.getMessage());
			}
		}
		LOG.info("SubSegmentController-->viewSubSegment()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}