package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.service.manufacturer.ManufacuturerService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * ManufacuturerController to handle the 
 * CRUD operations of Manufacuturer
 *
 */
@RestController
@RequestMapping(path = "/manufacuturer")
public class ManufacuturerController extends BaseController{
	
	/**
	 * Reference of ManufacuturerService to call the implementation method
	 */
	@Autowired
	private ManufacuturerService service;
	
	/** The Constant LOG. */	
	private static final Logger LOG = LoggerFactory.getLogger(ManufacuturerController.class);
	
	/** 
	 * To edit the Manufacuturer
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> editManufacuturer(@RequestBody ManufacturerDto dto) {
		LOG.info("ManufacuturerController-->editManufacuturer()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.editManufacuturer(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ManufacuturerController-->editManufacuturer()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ManufacuturerController-->editManufacuturer()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * To add the Manufacuturer
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> addManufacuturer(@RequestBody ManufacturerDto dto) {
		LOG.info("ManufacuturerController-->addManufacuturer()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			service.addManufacuturer(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ManufacuturerController-->addManufacuturer()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ManufacuturerController-->addManufacuturer()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * To delete the Manufacuturer
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> deleteManufacuturer(@RequestBody ManufacturerDto dto) {
		LOG.info("ManufacuturerController-->deleteManufacuturer()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.deleteManufacuturer(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ManufacuturerController-->deleteManufacuturer()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ManufacuturerController-->deleteManufacuturer()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		
		return returnEntity; 
	}
	
	/**
	 * To view the Manufacuturer
	 * @param manufterId
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewManufacuturer(@RequestParam (value="manufacturerCode") int manufterId) {
		LOG.info("ManufacuturerController-->viewManufacuturer()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			ManufacturerDto obj=service.loadManufacuturer(Integer.valueOf(manufterId));
			if(obj==null) {
				response.setCode(AppConstants.FAILURE_CODE);
				response.setMessage(AppConstants.FAILURE_MSG);
			}else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}			
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ManufacuturerController-->viewManufacuturer()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ManufacuturerController-->viewManufacuturer()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * To check the duplicate
	 * @param String
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	
	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateManufacuturer(@RequestParam (value="manufacturerName") String name) { 
		LOG.info("ManufacuturerController-->checkDuplicateManufacuturer()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated=service.checkDuplicateManufacuturer(name);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ManufacuturerController-->checkDuplicateManufacuturer()-->Exception " + e.getMessage());
			}
		}
		LOG.info("ManufacuturerController-->checkDuplicateManufacuturer()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
