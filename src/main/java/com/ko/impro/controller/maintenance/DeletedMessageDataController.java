package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.DeleteMessagesDataDto;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.service.deletemessagedata.DeleteMessageDataService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * DeletedMessageDataController to handle the 
 * CRUD operations of DeletedMessageData
 *
 */
@RestController
@RequestMapping(path = "/deleteMessagesData")
public class DeletedMessageDataController extends BaseController{
	
	/**
	 * Reference of DeleteMessageDataService to call the implementation method
	 */
	@Autowired
	private DeleteMessageDataService service;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DeletedMessageDataController.class);
	
	
	/**
	 * Method to edit  the  messages
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> editDeleteMessagesData(@RequestBody DeleteMessagesDataDto dto) {
		LOG.info("DeletedMessageDataController-->editDeleteMessagesData()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
				String msg=service.editDeleteMessagesData(dto);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage(msg);
			} catch (Exception e) {
				response.setCode(AppConstants.FAILURE_CODE);
				response.setMessage(AppConstants.FAILURE_MSG);
				httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
				if(LOG.isErrorEnabled()) {
					LOG.error("DeletedMessageDataController-->editDeleteMessagesData()-->Exception " + e.getMessage());
				}
			}	
		
		LOG.info("DeletedMessageDataController-->editDeleteMessagesData()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	
	/**
	 * Method to add the  messages
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> addDeleteMessageData(@RequestBody DeleteMessagesDataDto dto) {
		LOG.info("DeletedMessageDataController-->addDeleteMessageData()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;


		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			service.addDeleteMessagesData(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);

		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("DeletedMessageDataController-->addDeleteMessageData()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("DeletedMessageDataController-->addDeleteMessageData()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to delete the  messages
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> deleteDeleteMessageData(@RequestBody DeleteMessagesDataDto dto) {
		LOG.info("DeletedMessageDataController-->deleteDeleteMessageData()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;


		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.deleteDeleteMessagesData(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("DeletedMessageDataController-->deleteManufacuturer()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ManufacuturerController-->deleteManufacuturer()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to view  the Delete messages
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewDeleteMessageData(@RequestParam (value = "messageId") final int id) {
		LOG.info("DeletedMessageDataController-->viewDeleteMessageData()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			DeleteMessagesDataDto obj=service.loadDeleteMessagesData(id);
			if(obj == null) {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("DeletedMessageDataController-->viewDeleteMessageData()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ManufacuturerController-->viewDeleteMessageData()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	
	/**
	 * to validate the duplicate messages
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateDeleteMessagesData(@RequestParam (value = "messageText") final String name) {
		LOG.info("DeletedMessageDataController-->checkDuplicateDeleteMessagesData()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated=service.checkDuplicateDeleteMessagesData(name);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("DeletedMessageDataController-->checkDuplicateDeleteMessagesData()-->Exception " + e.getMessage());
			}
		}
		LOG.info("DeletedMessageDataController-->checkDuplicateDeleteMessagesData()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
