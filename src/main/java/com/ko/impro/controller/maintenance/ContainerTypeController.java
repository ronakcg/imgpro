package com.ko.impro.controller.maintenance;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.dto.MaintenanceResponseDto;
import com.ko.impro.service.containertype.ContainerTypeService;
import com.ko.impro.util.AppConstants;

/**
 * @author swshedge
 * 
 * ContainerTypeController to handle the 
 * CRUD operations of ContainerType
 *
 */
@RestController
@RequestMapping(path = "/ContainerType")
public class ContainerTypeController extends BaseController{
	
	/**
	 * Reference of ContainerTypeService to call the implementation method
	 */
	@Autowired
	private ContainerTypeService service;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ContainerTypeController.class);
	
	/**
	 * To edit the containerType
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> editContainerType(@RequestBody ContainerTypeDto dto) {
		LOG.info("ContainerTypeController-->editContainerType()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.editContainerType(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ContainerTypeController-->editContainerType()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ContainerTypeController-->editContainerType()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	
	/**
	 * Method to add container Type
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/add",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> addContainerType(@RequestBody ContainerTypeDto dto) {
		LOG.info("ContainerTypeController-->addContainerType()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			service.addContainerType(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.DATA_ADD_SUCC_MSG);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ContainerTypeController-->addContainerType()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ContainerTypeController-->addContainerType()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to delete ContainerType
	 * @param dto
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */
	@POST
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<MaintenanceResponseDto> deleteContainerType(@RequestBody ContainerTypeDto dto) {
		LOG.info("ContainerTypeController-->deleteContainerType()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			String msg=service.deleteContainerType(dto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(msg);	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ContainerTypeController-->deleteContainerType()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ManufacuturerController-->deleteContainerType()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	/**
	 * Method to view Container Type
	 * @param containerTypeCode
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/view",method = RequestMethod.GET)
	@Produces(type)	
	protected  ResponseEntity<MaintenanceResponseDto> viewContainerType(@RequestParam (value = "containerTypeCode") int containerTypeCode) {
		LOG.info("ContainerTypeController-->viewContainerType()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response=new MaintenanceResponseDto();
		try {
			ContainerTypeDto obj=service.loadContainerType(containerTypeCode);
			if(null==obj) {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}else {
				response.setResponseObject(obj);
				response.setCode(AppConstants.SUCCESS_CODE);
				response.setMessage("");
			}	
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ContainerTypeController-->viewContainerType()-->Exception " + e.getMessage());
			}
		}		
		LOG.info("ContainerTypeController-->viewContainerType()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
	
	
	/**
	 * Method to validate duplicate ContainerType Description
	 * @param name
	 * @return ResponseEntity<MaintenanceResponseDto>
	 */

	@RequestMapping(value = "/checkDuplicate",method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<MaintenanceResponseDto> checkDuplicateExtendedProdDesc(@RequestParam (value = "containerTypeDesc") String name) {
		LOG.info("ContainerTypeController-->checkDuplicateExtendedProdDesc()-->Begin");
		ResponseEntity<MaintenanceResponseDto> returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		MaintenanceResponseDto response = new MaintenanceResponseDto();
		try {
			boolean isDuplicated=service.checkDuplicateContainerType(name);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception e) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if(LOG.isErrorEnabled()) {
				LOG.error("ContainerTypeController-->checkDuplicateExtendedProdDesc()-->Exception " + e.getMessage());
			}
		}
		LOG.info("ContainerTypeController-->checkDuplicateExtendedProdDesc()-->End");
		returnEntity = new ResponseEntity<MaintenanceResponseDto>(response, httpStatus);
		return returnEntity; 
	}
}
