package com.ko.impro.controller.maintenance;

import java.util.List;

import javax.ws.rs.Produces;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.dto.DeleteMessagesDataDto;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.dto.PackageStyleDto;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.ShapeDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.brand.BrandService;
import com.ko.impro.service.category.CategoryService;
import com.ko.impro.service.containertype.ContainerTypeService;
import com.ko.impro.service.deletemessagedata.DeleteMessageDataService;
import com.ko.impro.service.extendedproductdesc.ExtendedProductDescService;
import com.ko.impro.service.flavour.FlavorService;
import com.ko.impro.service.manufacturer.ManufacuturerService;
import com.ko.impro.service.packages.PackageService;
import com.ko.impro.service.packagesize.PackageSizeService;
import com.ko.impro.service.packagestyle.PackageStyleService;
import com.ko.impro.service.segment.SegmentService;
import com.ko.impro.service.shape.ShapeService;
import com.ko.impro.service.subcategory.SubCategoryService;
import com.ko.impro.service.subsegment.SubSegmentService;

/**
 * @author swshedge
 * 
 * MaintenanceController to handle the 
 * load all the maintenance section
 */
@RestController
@RequestMapping(path = "/maintenance")
public class MaintenanceController extends BaseController{
	
	/**
	 * Reference of ManufacuturerService to call the implementation method
	 */
	@Autowired
	private ManufacuturerService manufService;
	
	/**
	 * Reference of BrandService to call the implementation method
	 */
	@Autowired
	private BrandService brandService;
	
	/**
	 * Reference of CategoryService to call the implementation method
	 */
	@Autowired
	private CategoryService catetgoryService;
	
	/**
	 * Reference of SubCategoryService to call the implementation method
	 */
	@Autowired
	private SubCategoryService subCatService;
	
	/**
	 * Reference of PackageService to call the implementation method
	 */
	@Autowired
	private PackageService packageService;
	
	/**
	 * Reference of PackageSizeService to call the implementation method
	 */
	@Autowired
	private PackageSizeService pkgSizeSrvc;
	
	/**
	 * Reference of PackageStyleService to call the implementation method
	 */
	@Autowired
	private PackageStyleService pkgStyleService;
	
	/**
	 * Reference of ContainerTypeService to call the implementation method
	 */
	@Autowired
	private ContainerTypeService containerTyeSrvc;
	
	/**
	 * Reference of ShapeService to call the implementation method
	 */
	@Autowired
	private ShapeService shapeService;
	
	/**
	 * Reference of FlavorService to call the implementation method
	 */
	@Autowired
	private FlavorService flavorService;
	
	/**
	 * Reference of SegmentService to call the implementation method
	 */
	@Autowired
	private SegmentService segmentService;
	
	/**
	 * Reference of SubSegmentService to call the implementation method
	 */
	@Autowired
	private SubSegmentService subSegmentService;
	
	/**
	 * Reference of ExtendedProductDescService to call the implementation method
	 */
	@Autowired
	private ExtendedProductDescService extedProdSrvc;
	
	/**
	 * Reference of DeleteMessageDataService to call the implementation method
	 */
	@Autowired
	private DeleteMessageDataService deltedMsgDataSrvc;
	
	/** The Constant LOG. */	
	private static final Logger LOG = LoggerFactory.getLogger(MaintenanceController.class);
	
	/**
	 * Method to load the manufacturers.
	 * @return ResponseEntity<List<ManufacturerDto>>
	 * @throws ImproException
	 */

	@RequestMapping(path = "/load/manufacturers",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<ManufacturerDto>> loadManufacturers() throws ImproException {
		LOG.info("MaintenanceController-->loadManufacturers()-->Begin");
		List<ManufacturerDto> manufDtoList =  manufService.loadManufacuturer();
		LOG.info("MaintenanceController-->loadManufacturers()-->End");
		return new ResponseEntity<List<ManufacturerDto>>(manufDtoList, HttpStatus.OK);
	}
	
	/**
	 * Method to load brands.
	 * @return ResponseEntity<List<BrandDto>>
	 * @throws ImproException
	 */

	@RequestMapping(path = "/load/brands",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<BrandDto>> loadBrands() throws ImproException {
		LOG.info("MaintenanceController-->loadBrands()-->Begin");
		List<BrandDto> brandDtoList= brandService.loadBrands();
		LOG.info("MaintenanceController-->loadBrands()-->End");
		return new ResponseEntity<List<BrandDto>>(brandDtoList, HttpStatus.OK);
	}
	
	/**
	 * Method to load the Packages.
	 * @return ResponseEntity<List<PackageDto>>
	 * @throws ImproException
	 */

	@RequestMapping(path = "/load/packages",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<PackageDto>> loadPackages() throws ImproException {
		LOG.info("MaintenanceController-->loadPackages()-->Begin");
		List<PackageDto> pkgDtoList =  packageService.loadPackages();
		LOG.info("MaintenanceController-->loadPackages()-->End");
		return new ResponseEntity<List<PackageDto>>(pkgDtoList, HttpStatus.OK);
	}
	
	/**
	 * Method to load the packageSizes.
	 * @return ResponseEntity<List<PackageSizeDto>>
	 * @throws ImproException
	 */

	@RequestMapping(path = "/load/packageSizes",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<PackageSizeDto>> loadPackagesizes() throws ImproException {
		LOG.info("MaintenanceController-->loadPackagesizes()-->Begin");
		List<PackageSizeDto> pkgSizeDtoList =  pkgSizeSrvc.loadPackageSize();
		LOG.info("MaintenanceController-->loadPackagesizes()-->End");
		return new ResponseEntity<List<PackageSizeDto>>(pkgSizeDtoList, HttpStatus.OK);
	}
	
	/**
	 * Method to load the categories.
	 * @return ResponseEntity<List<CategoryDto>>
	 * @throws ImproException
	 */

	@RequestMapping(path = "/load/categories",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<CategoryDto>> loadCategories() throws ImproException {
		LOG.info("MaintenanceController-->loadCategories()-->Begin");
		List<CategoryDto> categoryDtoList =  catetgoryService.loadCategory();
		LOG.info("MaintenanceController-->loadCategories()-->End");
		return new ResponseEntity<List<CategoryDto>>(categoryDtoList, HttpStatus.OK);
	}
	
	/**
	 * Method to load subcategories.
	 * @return ResponseEntity<List<SubCategoryDto>>
	 * @throws ImproException
	 */

	@RequestMapping(path = "/load/subSubCategories",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<SubCategoryDto>> loadSubCategories() throws ImproException {
		LOG.info("MaintenanceController-->loadSubCategories()-->Begin");
		List<SubCategoryDto> subCatDtoList=subCatService.loadSubCategory();
		LOG.info("MaintenanceController-->loadSubCategories()-->End");
		return new ResponseEntity<List<SubCategoryDto>>(subCatDtoList, HttpStatus.OK);
	}
	
	/**
	 * Method to load flavors.
	 * @return ResponseEntity<List<FlavorDto>>
	 * @throws ImproException
	 */

	@RequestMapping(path = "/load/flavors",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<FlavorDto>> loadFlavors() throws ImproException {
		LOG.info("MaintenanceController-->loadFlavors()-->Begin");
		List<FlavorDto> flavorDtoList =flavorService.loadFlavor();
		LOG.info("MaintenanceController-->loadFlavors()-->End");
		return new ResponseEntity<List<FlavorDto>>(flavorDtoList, HttpStatus.OK);
	}
	
	/**
	 * Method to load segments.
	 * @return ResponseEntity<List<SegmentDto>>
	 * @throws ImproException
	 */

	@RequestMapping(path = "/load/segments",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<SegmentDto>> loadSegments() throws ImproException {
		LOG.info("MaintenanceController-->loadSegments()-->Begin");
		List<SegmentDto> segmentDtoList=segmentService.loadSegment();
		LOG.info("MaintenanceController-->loadSegments()-->End");
		return new ResponseEntity<List<SegmentDto>>(segmentDtoList, HttpStatus.OK);
	}
	
	/**
	 * Method to load subsegments.
	 * @return ResponseEntity<List<SubSegmentDto>>
	 * @throws ImproException
	 */

	@RequestMapping(path = "/load/subSubSegments",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<SubSegmentDto>> loadSubSegments() throws ImproException {
		LOG.info("MaintenanceController-->loadSubSegments()-->Begin");
		List<SubSegmentDto> subSegmentDtoList=subSegmentService.loadSubSegment();
		LOG.info("MaintenanceController-->loadSubloadSubSegmentsSegments()-->End");
		return new ResponseEntity<List<SubSegmentDto>>(subSegmentDtoList, HttpStatus.OK);
	}


	/**
	 * Method to load ExtendedDesc.
	 * @return ResponseEntity<List<ExtendedProductDescDto>>
	 * @throws ImproException
	 *//*
	
	@RequestMapping(path = "/load/extendedProductDesc",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<ExtendedProductDescDto>> loadExtendedBrandDesc() throws ImproException {
		LOG.info("MaintenanceController-->loadExtendedBrandDesc()-->Begin");
		List<ExtendedProductDescDto> extedDescDtoLst=extedProdSrvc.loadExtendedProductDesc();
		LOG.info("MaintenanceController-->loadExtendedBrandDesc()-->End");
		return new ResponseEntity<List<ExtendedProductDescDto>>(extedDescDtoLst, HttpStatus.OK);
	}*/

	/**
	 * Method to load ExtendedDesc.
	 * @return ResponseEntity<List<ExtendedProductDescDto>>
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/load/extendedProductDesc",method = RequestMethod.POST)
	@Produces(type)
	protected  ResponseEntity<List<ExtendedProductDescDto>> loadExtendedBrandDesc(@RequestBody List<String> brandList) throws ImproException {
		LOG.info("MaintenanceController-->loadExtendedBrandDesc()-->Begin");
		List<ExtendedProductDescDto> extedDescDtoLst=null;
		if(CollectionUtils.isNotEmpty(brandList)) {
			extedDescDtoLst=extedProdSrvc.loadExtendedProductDesc(brandList);
		}else {
			extedDescDtoLst=extedProdSrvc.loadExtendedProductDesc();
		}
		
		LOG.info("MaintenanceController-->loadExtendedBrandDesc()-->End");
		return new ResponseEntity<List<ExtendedProductDescDto>>(extedDescDtoLst, HttpStatus.OK);
	}
	
	/**
	 * Method to load ContainerTypes.
	 * @return ResponseEntity<List<ContainerTypeDto>>
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/load/containerTypes",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<ContainerTypeDto>> loadContainerTypes() throws ImproException {
		LOG.info("MaintenanceController-->loadContainerTypes()-->Begin");
		List<ContainerTypeDto> contnerTypDtoLst=containerTyeSrvc.loadContainerTypes();
		LOG.info("MaintenanceController-->loadContainerTypes()-->End");
		return new ResponseEntity<List<ContainerTypeDto>>(contnerTypDtoLst, HttpStatus.OK);
	}
	
	/**
	 * Load PackageStyles
	 * @return ResponseEntity<List<PackageStyleDto>>
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/load/packageStyle",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<PackageStyleDto>> loadPackageStyle() throws ImproException {
		LOG.info("MaintenanceController-->loadPackageStyle()-->Begin");
		List<PackageStyleDto> packageStyleList =  pkgStyleService.loadPackageStyle();
		LOG.info("MaintenanceController-->loadPackageStyle()-->End");
		return new ResponseEntity<List<PackageStyleDto>>(packageStyleList, HttpStatus.OK);
	}	
	
	/**
	 * Load DeleteMessagesData
	 * @return ResponseEntity<List<DeleteMessagesDataDto>>
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/load/deletedMessagesData",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<DeleteMessagesDataDto>> loadDeleteMessagesDatas() throws ImproException {
		LOG.info("MaintenanceController-->loadDeleteMessagesDatas()-->Begin");
		List<DeleteMessagesDataDto> deltedMsgDataList =  deltedMsgDataSrvc.loadDeleteMessagesDatas();
		LOG.info("MaintenanceController-->loadDeleteMessagesDatas()-->End");
		return new ResponseEntity<List<DeleteMessagesDataDto>>(deltedMsgDataList, HttpStatus.OK);
	}	
	
	/**
	 * Load all Shapes
	 * @return List<ShapeDto>
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/load/shapes",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<List<ShapeDto>> loadShape() throws ImproException {
		LOG.info("MaintenanceController-->loadShape()-->Begin");
		List<ShapeDto> shapeList =  shapeService.loadShapes();
		LOG.info("MaintenanceController-->loadShape()-->End");
		return new ResponseEntity<List<ShapeDto>>(shapeList, HttpStatus.OK);
	}	
	
}
