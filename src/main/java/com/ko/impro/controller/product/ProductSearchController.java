package com.ko.impro.controller.product;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.ArchivedProductResponseDto;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductResponseDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.brand.BrandService;
import com.ko.impro.service.category.CategoryService;
import com.ko.impro.service.flavour.FlavorService;
import com.ko.impro.service.manufacturer.ManufacuturerService;
import com.ko.impro.service.packages.PackageService;
import com.ko.impro.service.packagesize.PackageSizeService;
import com.ko.impro.service.product.ArchiveProductService;
import com.ko.impro.service.product.ProductService;
import com.ko.impro.service.segment.SegmentService;
import com.ko.impro.service.subcategory.SubCategoryService;
import com.ko.impro.service.subsegment.SubSegmentService;
import com.ko.impro.util.AppConstants;

/**
 * @author nvohra
 * Controller class to call Service Level methods.
 */
@RestController
public class ProductSearchController extends BaseController {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ProductSearchController.class);
	
	@Autowired
	private ManufacuturerService manufService;
	
	@Autowired
	private BrandService brandService;
	
	@Autowired
	private CategoryService catetgoryService;
	
	@Autowired
	private SubCategoryService subCatetgoryService;
	
	@Autowired
	private PackageService packageService;
	
	@Autowired
	private PackageSizeService packageSizeService;
	
	@Autowired
	private FlavorService flavorService;
	
	@Autowired
	private SegmentService segmentService;
	
	@Autowired
	private SubSegmentService subSegmentService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ArchiveProductService archiveProductService;

	/**
	 * Method to load the manufacturers.
	 * @return ResponseEntity<List<ManufacturerDto>>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/load/manufacturers", method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<?> loadManufacturers() throws ImproException {
		LOG.info("ProductSearchController-->loadManufacturers()-->Begin");
		List<ManufacturerDto> manufDtoList =  manufService.loadManufacuturer();
		LOG.info("ProductSearchController-->loadManufacturers()-->End");
		if(CollectionUtils.isEmpty(manufDtoList)) {
			ProductResponseDto response=new ProductResponseDto();
			response.setCode(AppConstants.NOT_FOUND_CODE);
			response.setMessage(AppConstants.NOT_FOUND_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
		}else {
		return new ResponseEntity<List<ManufacturerDto>>(manufDtoList, HttpStatus.OK);
		}

	}
	
	/**
	 * Method to load brands.If the manufList is null it will load all the brnads.
	 * Other wise it will load based 
	 * on selected manufacturers
	 * @param brandList
	 * @return ResponseEntity<List<BrandDto>>
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/load/brands",method = RequestMethod.POST)
	@Produces(type)
	protected  ResponseEntity<?> loadBrands(@RequestBody List<String> manufactList) throws ImproException {
		LOG.info("ProductSearchController-->loadBrands()-->Begin");
		List<BrandDto> brandDtoList= null;
		if(CollectionUtils.isNotEmpty(manufactList)) 
			brandDtoList =  brandService.loadBrands(manufactList);
		else 
			brandDtoList =  brandService.loadBrands();
		LOG.info("ProductSearchController-->loadBrands()-->End");
		if(CollectionUtils.isEmpty(brandDtoList)) {
			ProductResponseDto response=new ProductResponseDto();
			response.setCode(AppConstants.NOT_FOUND_CODE);
			response.setMessage(AppConstants.NOT_FOUND_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
		}else {
		return new ResponseEntity<List<BrandDto>>(brandDtoList, HttpStatus.OK);
		}

	}
	
	/**
	 * Method to load the Packages.
	 * @return ResponseEntity<List<PackageDto>>
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/load/packages",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<?> loadPackages() throws ImproException {
		LOG.info("ProductSearchController-->loadPackages()-->Begin");
		List<PackageDto> pkgDtoList =  packageService.loadPackages();
		LOG.info("ProductSearchController-->loadPackages()-->End");
		if(CollectionUtils.isEmpty(pkgDtoList)) {
			ProductResponseDto response=new ProductResponseDto();
			response.setCode(AppConstants.NOT_FOUND_CODE);
			response.setMessage(AppConstants.NOT_FOUND_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
		}else {
		return new ResponseEntity<List<PackageDto>>(pkgDtoList, HttpStatus.OK);
		}
	}
	
	/**
	 * Method to load the packageSizes based on PackageCode.
	 * @return ResponseEntity<List<PackageSizeDto>>
	 * @throws ImproException
	 */
	@RequestMapping(path="/load/packageSizes", method=RequestMethod.POST)
	@Produces(type)
	protected ResponseEntity<?> loadPackagesizes(@RequestBody List<String> packageList) throws ImproException {
		LOG.info("ProductSearchController-->loadPackageSizeforPackage()-->Begin");
		List<PackageSizeDto> packageSizeList=null;
		if(CollectionUtils.isNotEmpty(packageList)) 
			packageSizeList =  packageSizeService.loadPackageSize(packageList);
		else 
			packageSizeList =  packageSizeService.loadPackageSize();
		LOG.info("ProductSearchController-->loadPackageSizeforPackage()--End");
		if(CollectionUtils.isEmpty(packageSizeList)) {
			ProductResponseDto response=new ProductResponseDto();
			response.setCode(AppConstants.NOT_FOUND_CODE);
			response.setMessage(AppConstants.NOT_FOUND_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
		}else {
		return new ResponseEntity<List<PackageSizeDto>>(packageSizeList, HttpStatus.OK);
		}
	}
	
	/**
	 * Method to load the categories.
	 * @return ResponseEntity<List<CategoryDto>>
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/load/categories",method = RequestMethod.GET)
	@Produces(type)
	protected  ResponseEntity<?> loadCategories() throws ImproException {
		LOG.info("ProductSearchController-->loadCategories()-->Begin");
		List<CategoryDto> categoryDtoList =  catetgoryService.loadCategory();
		LOG.info("ProductSearchController-->loadCategories()-->End");
		if(CollectionUtils.isEmpty(categoryDtoList)) {
			ProductResponseDto response=new ProductResponseDto();
			response.setCode(AppConstants.NOT_FOUND_CODE);
			response.setMessage(AppConstants.NOT_FOUND_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
		}else {
		return new ResponseEntity<List<CategoryDto>>(categoryDtoList, HttpStatus.OK);
		}
	}
	
	/**
	 * Method to load subcategories.If the categoryList is null it will load 
	 * all the subcategories.
	 * Other wise it will load based on selected categories
	 * @param categoryList
	 * @return
	 * @throws ImproException
	 */
	@RequestMapping(path = "/load/subSubCategories",method = RequestMethod.POST)
	@Produces(type)
	protected  ResponseEntity<?> loadSubCategories(@RequestBody List<String> categoryList) throws ImproException {
		LOG.info("ProductSearchController-->loadSubCategories()-->Begin");
		List<SubCategoryDto> subCatDtoList=null;
		if(CollectionUtils.isNotEmpty(categoryList)) 
			subCatDtoList =  subCatetgoryService.loadSubCategory(categoryList);
		else
			subCatDtoList =  subCatetgoryService.loadSubCategory();
		LOG.info("ProductSearchController-->loadSubCategories()-->End");
		if(CollectionUtils.isEmpty(subCatDtoList)) {
			ProductResponseDto response=new ProductResponseDto();
			response.setCode(AppConstants.NOT_FOUND_CODE);
			response.setMessage(AppConstants.NOT_FOUND_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
		}else {
		return new ResponseEntity<List<SubCategoryDto>>(subCatDtoList, HttpStatus.OK);
		}

	}
	
	
	/**
	 * Method to load flavors.If the subcatgoryList is null it will 
	 * load all the flavors.
	 * Other wise it will load based on selected subcategories.
	 * @param subCategoryList
	 * @return ResponseEntity<List<FlavorDto>>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/load/flavors",method = RequestMethod.POST)
	@Produces(type)
	protected  ResponseEntity<?> loadFlavors(@RequestBody List<String> subCategoryList) throws ImproException {
		LOG.info("ProductSearchController-->loadFlavors()-->Begin");
		List<FlavorDto> flavorDtoList =null;
		if(CollectionUtils.isNotEmpty(subCategoryList)) 
			flavorDtoList=  flavorService.loadFlavor(subCategoryList);
		else
			flavorDtoList =  flavorService.loadFlavor();
		LOG.info("ProductSearchController-->loadFlavors()-->End");
		
		if(CollectionUtils.isEmpty(flavorDtoList)) {
			ProductResponseDto response=new ProductResponseDto();
			response.setCode(AppConstants.NOT_FOUND_CODE);
			response.setMessage(AppConstants.NOT_FOUND_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
		}else {
		return new ResponseEntity<List<FlavorDto>>(flavorDtoList, HttpStatus.OK);
		}
	}
	
	
	/**
	 * Method to load segments.If the subcategoryList is null it will load all the segments.Other wise it will load based 
	 * on selected subcategories
	 * @param subCategoryList
	 * @return ResponseEntity<List<SegmentDto>>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/load/segments",method = RequestMethod.POST)
	@Produces(type)
	protected  ResponseEntity<?> loadSegments(@RequestBody List<String> subCategoryList) throws ImproException {
		LOG.info("ProductSearchController-->loadSegments()-->Begin");
		List<SegmentDto> segmentDtoList=null;
		if(CollectionUtils.isNotEmpty(subCategoryList)) 
			segmentDtoList =  segmentService.loadSegment(subCategoryList);
		else
			segmentDtoList =  segmentService.loadSegment();
		LOG.info("ProductSearchController-->loadSegments()-->End");
		if(CollectionUtils.isEmpty(segmentDtoList)) {
			ProductResponseDto response=new ProductResponseDto();
			response.setCode(AppConstants.NOT_FOUND_CODE);
			response.setMessage(AppConstants.NOT_FOUND_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
		}else {
		return new ResponseEntity<List<SegmentDto>>(segmentDtoList, HttpStatus.OK);
		}
	}
	
	
	/**
	 * Method to load subsegments.If the segmentList is null it will load all the segments.
	 * Other wise it will load based on selected segments
	 * @param segmentList
	 * @return
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/load/subSubSegments",method = RequestMethod.POST)
	@Produces(type)
	protected  ResponseEntity<?> loadSubSegments(@RequestBody List<String> segmentList) throws ImproException {
		LOG.info("ProductSearchController-->loadSubSegments()-->Begin");
		List<SubSegmentDto> subSegmentDtoList=null;
		if(CollectionUtils.isNotEmpty(segmentList)) 
			subSegmentDtoList =  subSegmentService.loadSubSegment(segmentList);
		else
			subSegmentDtoList =  subSegmentService.loadSubSegment();
		LOG.info("ProductSearchController-->loadSubloadSubSegmentsSegments()-->End");
		if(CollectionUtils.isEmpty(subSegmentDtoList)) {
			ProductResponseDto response=new ProductResponseDto();
			response.setCode(AppConstants.NOT_FOUND_CODE);
			response.setMessage(AppConstants.NOT_FOUND_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
		}else {
		return new ResponseEntity<List<SubSegmentDto>>(subSegmentDtoList, HttpStatus.OK);
		}
	}
	
	/**
	 * This method will search product based on the filter input 
	 * @param productSearchDto
	 * @return ResponseEntity<ProductResponseDto>
	 * @throws ImproException
	 */
	@RequestMapping(value = "/searchProducts", method = RequestMethod.POST)
	@Consumes(type)
	@Produces(type)
	protected  ResponseEntity<ProductResponseDto> searchProducts(@RequestBody ProductSearchDto productSearchDto) throws ImproException {
		LOG.info("ProductSearchController-->searchProducts()-->Begin");
		ProductResponseDto response=new ProductResponseDto();
		response.setCode(AppConstants.SUCCESS_CODE);
		response.setMessage("");
		try {
			response =  productService.searchProducts(productSearchDto);
			if(!CollectionUtils.isNotEmpty(response.getProductDtoList()) ) {				
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}
		} catch(Exception ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductSearchController-->searchProducts()-->Exception "+ex.getMessage());
			}
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.UNPROCESSABLE_ENTITY);

		}
		LOG.info("ProductSearchController-->searchProducts()-->End");

		return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);

	}
	
	
	/**
	 * This method will return archived product list based on the input and displays
	 *  total record count
	 * @param productSearchDto
	 * @return ResponseEntity<ArchivedProductResponseDto>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/searchArchivedProducts",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<ArchivedProductResponseDto> searchArchivedProducts(@RequestBody ProductSearchDto productSearchDto) throws ImproException {
		LOG.info("ProductSearchController-->searchArchivedProducts()-->Begin");
		ArchivedProductResponseDto response=new ArchivedProductResponseDto();
		response.setCode(AppConstants.SUCCESS_CODE);
		response.setMessage("");
		try {
			response =  archiveProductService.searchArchivedProducts(productSearchDto);
			if(!CollectionUtils.isNotEmpty(response.getArchiveProductDtoList()) ) {				
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}
		} catch(Exception ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductSearchController-->searchArchivedProducts()-->Exception "+ex.getMessage());
			}
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			return new ResponseEntity<ArchivedProductResponseDto>(response, HttpStatus.UNPROCESSABLE_ENTITY);

		}
		LOG.info("ProductSearchController-->searchArchivedProducts()-->End");
		return new ResponseEntity<ArchivedProductResponseDto>(response, HttpStatus.OK);

	}
	
	/**
	 * This method will return similar product
	 * @param productSearchDto
	 * @return ResponseEntity<List<ProductDto>>
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/searchSimilarProducts",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<ProductResponseDto> getSimilarProducts(@RequestBody ProductSearchDto productSearchDto) throws ImproException {
		LOG.info("ProductSearchController-->  getSimilarProducts()-->Begin");
		ProductResponseDto response=new ProductResponseDto();
		response.setCode(AppConstants.SUCCESS_CODE);
		response.setMessage("");
		try {
			List<ProductDto> productDtoList =  productService.getSimilarProductDetails(productSearchDto);
			if(!(productDtoList != null && productDtoList.isEmpty())) {
				response.setProductDtoList(productDtoList);
			} else {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}
		} catch(Exception ex) {
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductSearchController-->getSimilarProducts()-->Exception "+ex.getMessage());
			}
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.UNPROCESSABLE_ENTITY);

		}
		LOG.info("ProductSearchController-->getSimilarProducts()-->End");
		return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);

	}	
	
}

