package com.ko.impro.controller.product;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.MultiProductDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductRequestWrapperDto;
import com.ko.impro.dto.ProductResponseDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.report.ExcelGeneratorFactory;
import com.ko.impro.report.ProductExcelReportGenerator;
import com.ko.impro.service.image.ImageService;
import com.ko.impro.service.product.ArchiveProductService;
import com.ko.impro.service.product.ProductService;
import com.ko.impro.util.AppConstants;

/**
 * @author nvohra
 * Controller class to call the Service Level methods. 
 */
@RestController
public class ProductController extends BaseController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ArchiveProductService archiveProductService;
	
	@Autowired
	private ImageService imageService;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ProductSearchController.class);

	
	/**
	 * Method to archive the products to the product table 
	 * @param ProductRequestWrapperDto requestDto
	 * @return ResponseEntity<ProductResponseDto>
	 */
	
	@RequestMapping(path = "/archiveProducts",  method = RequestMethod.POST)
	@Produces(type)
	protected ResponseEntity<ProductResponseDto> archiveProducts(@RequestBody ProductRequestWrapperDto requestDto )  {
		LOG.info("ProductSearchController-->archiveProducts()-->Begin");
		ProductResponseDto response=new ProductResponseDto();
		response.setCode(AppConstants.SUCCESS_CODE);
		response.setMessage(AppConstants.PRODUCT_ARCHIVE_MSG);
		try {
			archiveProductService.archiveProducts(requestDto.getProductList(), requestDto.getDeleteReason(), "");
		} catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductSearchController-->archiveProducts()-->Exception " + ex.getMessage());
			}
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.UNPROCESSABLE_ENTITY);

		}
		LOG.info("ProductSearchController-->archiveProducts()-->END");
		return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
	}
	
	/**
	 * Method to restore the products to the Main products table 
	 * @param ArchivedProductDto dto
	 * @return ResponseEntity<ProductResponseDto>
	 */
	
	@RequestMapping(path = "/restoreProducts", method = RequestMethod.POST)
	@Produces(type)
	protected ResponseEntity<ProductResponseDto> restoreProducts(@RequestBody ArchivedProductDto dto)  {
		LOG.info("ProductSearchController-->restoreProducts()-->Begin");
		ProductResponseDto response=new ProductResponseDto();
		response.setCode(AppConstants.SUCCESS_CODE);
		response.setMessage(AppConstants.PRODUCT_RESTRORE_SUCC_MSG);
		try {
			boolean isDuplicate=productService.restoreProducts(dto);
			if(isDuplicate) {
				response.setMessage(AppConstants.PRODUCT_RESTRORE_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			}
		} catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductSearchController-->restoreProducts()-->Exception " + ex.getMessage());
			}
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.UNPROCESSABLE_ENTITY);

		}
		LOG.info("ProductSearchController-->restoreProducts()-->END");
		return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
	}
	

	
	/**
	 * Method to edit a product
	 * @param ProductDto productDto
	 * @return ResponseEntity<ProductResponseDto>
	 */
	
	@RequestMapping(path = "/editProduct", method = RequestMethod.POST)
	@Produces(type)
	protected  ResponseEntity<ProductResponseDto> editProduct(@RequestBody ProductDto productDto) {
		LOG.info("ProductSearchController-->editProduct()-->Begin");
		ProductResponseDto response=new ProductResponseDto();
		try {
			productService.editProduct(productDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setCode(AppConstants.PRODUCT_UPDATE_SUCC_MSG);
		} catch (Exception e) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setCode(AppConstants.FAILURE_MSG);
		}
		
		LOG.info("ProductSearchController-->editProduct()-->End");
		return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);

	}
	
	/**
	 * Method to clone/add a product
	 * @param dto
	 * @return ResponseEntity<ProductResponseDto>
	 */
	
	@RequestMapping(path = "/addProduct", method = RequestMethod.POST)
	@Produces(type)
	public ResponseEntity<ProductResponseDto> addProduct(@RequestBody ProductDto dto) {
		LOG.info("ProductController-->adduser()-->Begin");
		ProductResponseDto response = new ProductResponseDto();
		try {
			response.setMessage(AppConstants.PRODUCT_ADD_SUCC_MSG);
			response.setCode(AppConstants.SUCCESS_CODE);
			productService.addProduct(dto);
		} catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductController-->addproduct()-->Exception " + ex.getMessage());
			}
		    return new ResponseEntity<ProductResponseDto>(response,HttpStatus.UNPROCESSABLE_ENTITY);
		}
		LOG.info("ProductController-->addproduct()-->End");
		return new ResponseEntity<ProductResponseDto>(response,HttpStatus.OK);
	}
	
	
	/**
	 * Method to check duplicate of a product.
	 * @param String upc
	 * @return ResponseEntity<ProductResponseDto>
	 */
	
	@RequestMapping(path = "/checkDuplicateProduct", method = RequestMethod.GET)
	@Produces(type)
	public ResponseEntity<ProductResponseDto> checkDuplicateProduct(@RequestParam (value="upc") String upc) {
		LOG.info("ProductController-->checkDuplicateProduct()-->Begin");
		ProductResponseDto response = new ProductResponseDto();
		try {
			boolean isDuplicated=productService.checkDuplicateProduct(upc);
			if(isDuplicated){			
				response.setMessage(AppConstants.PRODUCT_DUPL_MSG);
				response.setCode(AppConstants.DUPLICATE_CODE);
			} else {
				response.setMessage("");
				response.setCode(AppConstants.SUCCESS_CODE);
			}
		} catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductController-->checkDuplicateProduct()-->Exception " + ex.getMessage());
			}
		    return new ResponseEntity<ProductResponseDto>(response,HttpStatus.UNPROCESSABLE_ENTITY);
		}
		LOG.info("ProductController-->checkDuplicateProduct()-->End");
		return new ResponseEntity<ProductResponseDto>(response,HttpStatus.OK);
	}
	

	/**
	 * Method to create excel for Product Export
	 * @return ProductExcelReportGenerator
	 * @throws IOException
	 */
	public ProductExcelReportGenerator getExcelGenerator() throws IOException {
		return ExcelGeneratorFactory.getInstance().getProductExcelReportGenerator(AppConstants.PRODUCT_FILENAME,
				AppConstants.PRODUCT_SHEETNAME);
	}
	
	/**
	 * Method used to export all product details when the users clicks exportButton
	 * without selecting any check boxes.
	 * @param ProductRequestWrapperDto productRequestWrapperDto
	 * @return ResponseEntity<byte[]>
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	 */
	
	@RequestMapping(path = "/exportAllProducts", method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<byte[]> exportAllProducts(@RequestBody ProductRequestWrapperDto productRequestWrapperDto)  {
		LOG.info("ProductController-->exportAllProducts()-->Begin");
		byte[] bytes = null;
		HttpHeaders headers = new HttpHeaders();
		List<ProductDto> productDtoList = null;
		try {
			productDtoList = productService.exportAllProducts(productRequestWrapperDto.getProductSearchDto()); 
			ProductExcelReportGenerator excelGenerator = getExcelGenerator(); 
			XSSFWorkbook workBook = excelGenerator.generateReport(productDtoList);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			workBook.write(bos);
			bos.close();
			bytes = bos.toByteArray();
			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.set("charset", "utf-8");
			headers.set("Content-Disposition", "attachment; filename=" + "products");
		} catch (Exception ex) {
			ResponseEntity<byte[]> returnEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.NOT_FOUND);
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductController-->exportAllProducts()-->Exception" + ex.getMessage());
			}
			return returnEntity;
		}

		ResponseEntity<byte[]> returnEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
		LOG.info("ProductController-->exportAllProducts()-->End");
		return returnEntity;

	}
	
	/**
	 * Method used to export all product details when the users clicks the Selected Check boxes/select ALL and click exportButton
	 * @param ProductRequestWrapperDto productRequestWrapperDto
	 * @return ResponseEntity<byte[]>
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	*/ 
	
	@RequestMapping(path = "/exportSelectedProducts", method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<byte[]> exportSelectedProducts(@RequestBody ProductRequestWrapperDto productRequestWrapperDto) {
		LOG.info("ProductController-->exportSelectedProducts()-->Begin");
		byte[] bytes = null;
		HttpHeaders headers = new HttpHeaders();
		try {
			ProductExcelReportGenerator excelGenerator = getExcelGenerator(); 
			XSSFWorkbook workBook = excelGenerator.generateReport(productRequestWrapperDto.getProductList());
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			workBook.write(bos);
			bos.close();
			bytes = bos.toByteArray();
			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.set("charset", "utf-8");
			headers.set("Content-Disposition", "attachment; filename=" + "products");
			
		} catch (Exception ex) {
			ResponseEntity<byte[]> returnEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.NOT_FOUND);
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductController-->exportSelectedProducts()-->Exception" + ex.getMessage());
			}
			return returnEntity;
			
		}

		ResponseEntity<byte[]> returnEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
		LOG.info("ProductController-->exportSelectedProducts()-->End");
		return returnEntity;
	}
	
	/**
	 * Method used to export all product details when the users clicks exportButton
	 * @param ProductRequestWrapperDto productRequestWrapperDto
	 * @return ResponseEntity<byte[]>
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	 */
	
	@RequestMapping(path = "/exportAllArchivedProducts", method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<byte[]> exportAllArchivedProducts(@RequestBody ProductRequestWrapperDto productRequestWrapperDto) {

		LOG.info("ProductController-->exportAllArchivedProducts()-->Begin");
		byte[] bytes = null;
		HttpHeaders headers = new HttpHeaders();
		List<ArchivedProductDto> productDtoList = null; 
		try {
			productDtoList = archiveProductService.exportArchivedAllProducts(productRequestWrapperDto.getProductSearchDto()); 
			ProductExcelReportGenerator excelGenerator = getExcelGenerator(); 
			XSSFWorkbook workBook = excelGenerator.generateArchivedReport(productDtoList);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			workBook.write(bos);
			bos.close();
			bytes = bos.toByteArray();
			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.set("charset", "utf-8");
			headers.set("Content-Disposition", "attachment; filename=" + "products");
		} catch (Exception ex){
			ResponseEntity<byte[]> returnEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.NOT_FOUND);
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductController-->exportAllArchivedProducts()-->Exception" + ex.getMessage());
			}
			return returnEntity;
		}

		ResponseEntity<byte[]> returnEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
		LOG.info("ProductController-->exportAllArchivedProducts()-->End");
		return returnEntity;

	}
	
	/**
	 * Method used to export all product details when the users clicks the Selected Check boxes/select ALL and click exportButton
	 * @param ProductRequestWrapperDto productRequestWrapperDto
	 * @return ResponseEntity<byte[]>
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	*/ 
	
	@RequestMapping(path = "/exportSelectedArchivedProducts", method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected ResponseEntity<byte[]> exportSelectedArchivedProducts(@RequestBody ProductRequestWrapperDto productRequestWrapperDto) {
		LOG.info("ProductController-->exportSelectedArchivedProducts()-->Begin");
		byte[] bytes = null;
		HttpHeaders headers = new HttpHeaders();
		try {
			ProductExcelReportGenerator excelGenerator = getExcelGenerator(); 
			XSSFWorkbook workBook = excelGenerator.generateArchivedReport(productRequestWrapperDto.getArchivedProductList());
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			workBook.write(bos);
			bos.close();
			bytes = bos.toByteArray();
			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.set("charset", "utf-8");
			headers.set("Content-Disposition", "attachment; filename=" + "products");
		} catch (Exception ex){
			ResponseEntity<byte[]> returnEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.NOT_FOUND);
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductController-->exportSelectedArchivedProducts()-->Exception" + ex.getMessage());
			}
			return returnEntity;
		}

		ResponseEntity<byte[]> returnEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
		LOG.info("ProductController-->exportSelectedArchivedProducts()-->End");
		return returnEntity;
	}
	
	/**
	 * Method used to purge all the selected archived products
	 * @param List<ArchivedProductDto> archivedProductDtoList
	 * @return ResponseEntity<byte[]>
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	*/ 
	
	@RequestMapping(path = "/purgeArchivedProducts", method = RequestMethod.POST)
	@Produces(type)
	protected ResponseEntity<ProductResponseDto> purgeArchivedProducts(@RequestBody List<ArchivedProductDto> archivedDtoList) {
		LOG.info("ProductController-->purgeArchivedProducts()-->Begin");
		ProductResponseDto response = new ProductResponseDto();
		response.setCode(AppConstants.SUCCESS_CODE);
		response.setMessage(AppConstants.ARCHIVE_PRODUCT_PURGE_MSG);
		try {
			archiveProductService.purgeArchivedProducts(archivedDtoList);
		} catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductSearchController-->purgeArchivedProducts()-->Exception " + ex.getMessage());
			}
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.UNPROCESSABLE_ENTITY);

		}
		LOG.info("ProductController-->purgeArchivedProducts()-->END");
		return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);
	}

	
	/**
	 * Method to save multiple products.
	 * @param MultiProductDto multiProdList
	 * @return ResponseEntity<ProductResponseDto>
	 */
	
	@RequestMapping(path = "/multiEditProduct", method = RequestMethod.POST)
	@Produces(type)
	protected ResponseEntity<ProductResponseDto> multiEditProduct(@RequestBody MultiProductDto multiProdList) {
		ProductResponseDto response=new ProductResponseDto();
		response.setCode(AppConstants.SUCCESS_CODE);
		response.setMessage(AppConstants.MULTI_PRODUCT_UPDATE_SUCC_MSG);
		try {
			ProductDto productdto = new ProductDto();
			productdto = multiProdList.getProductdto();
			List<ProductDto>  productdtoList =  multiProdList.getProductdtoList();
			productService.multiEditProduct(productdto,productdtoList);		
		} catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			if(LOG.isErrorEnabled()) {
				LOG.error("ProductController-->saveProduct()-->Exception" + ex.getMessage());
			}
			return new ResponseEntity<ProductResponseDto>(response, HttpStatus.UNPROCESSABLE_ENTITY);
		}
		LOG.info("ProductController-->multiEditProduct()-->End");
		return new ResponseEntity<ProductResponseDto>(response, HttpStatus.OK);

	}
	
	/**
	 * This method will return additional details of a particular product/more info in grid view
	 * @param productDto
	 * @return ResponseEntity<ProductDto>
	 * @throws ImproException
	 */
	
	@RequestMapping(path = "/getMoreInfo",method = RequestMethod.GET)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<ProductDto> getProductDetails(@RequestParam (value="productCode") String id) throws ImproException {
		LOG.info("ProductController-->getProductDetails()-->Begin");
		ProductDto productDtoResponse =  productService.getProductDetails(id);
		LOG.info("ProductController-->getProductDetails()-->End");
		return new ResponseEntity<ProductDto>(productDtoResponse, HttpStatus.OK);

	}
	
	
	/**
	 * This method will return additional details of a particular archived product
	 * @param deletedProductDto
	 * @return ResponseEntity<ArchivedProductDto>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/getMoreInfoForArchive",method = RequestMethod.GET)
	@Produces(type)
	@Consumes(type)
	protected   ResponseEntity<ArchivedProductDto> getArchivedProductDetails(@RequestParam (value="productCode") String id) throws ImproException {
		LOG.info("ProductController-->getArchivedProductDetails()-->Begin");
		ArchivedProductDto productDtoResponse =  archiveProductService.getArchivedProductDetails(id);
		LOG.info("ProductController-->getArchivedProductDetails()-->End");
		return new ResponseEntity<ArchivedProductDto>(productDtoResponse, HttpStatus.OK);

	}
	
	/**
	 * This method will return additional details of a particular product for edit with default standard images
	 * @param productCode
	 * @return ResponseEntity<ProductDto>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/load/editProductDetails",method = RequestMethod.GET)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<ProductDto> loadEditProductDetails(@RequestParam (value="productCode") String id) throws ImproException {
		LOG.info("ProductController-->loadForEditProductDetails-->Begin");
		ProductDto productDto =  productService.getProductDetails(id);
		productDto.setImageType(AppConstants.IMAGE_TYPE_STD);
		imageService.getAllBase64String(productDto);
		LOG.info("ProductController-->loadForEditProductDetails()-->End");
		return new ResponseEntity<ProductDto>(productDto, HttpStatus.OK);
	}
	
	/**
	 * This method will return additional base64 string of images
	 * @param productCode
	 * @return ResponseEntity<ProductDto>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/load/images",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<ProductDto> loadImages(@RequestBody ProductDto productDto) throws ImproException {
		LOG.info("ProductController-->loadImages-->Begin");		
		imageService.getAllBase64String(productDto);
		LOG.info("ProductController-->loadImages()-->End");
		return new ResponseEntity<ProductDto>(productDto, HttpStatus.OK);
	}

}



