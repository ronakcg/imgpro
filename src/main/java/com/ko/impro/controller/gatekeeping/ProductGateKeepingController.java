package com.ko.impro.controller.gatekeeping;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ko.impro.controller.base.BaseController;
import com.ko.impro.dto.GateKeepingResponseDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.report.ExcelGeneratorFactory;
import com.ko.impro.report.GatekeepingExcelReportGenerator;
import com.ko.impro.service.gatekeeping.ProductGateKeepingService;
import com.ko.impro.service.image.ImageGatekeepingService;
import com.ko.impro.util.AppConstants;

/**
 * Controller of ProductGateKeeping
 */
@RestController
@RequestMapping(path = "/gateKeeping")
public class ProductGateKeepingController extends BaseController {

	/**
	 * Reference of ProductGateKeepingService
	 */
	@Autowired
	private ProductGateKeepingService service;
	
	/**
	 * Reference of ImageGatekeepingService
	 */
	@Autowired
	private ImageGatekeepingService imgGatekepService;
	
	/**
	 * The LOG Constant
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ProductGateKeepingController.class);

	/**
	 * Method to load the all productGateKeeping based on status.
	 * 
	 * @return ResponseEntity<GateKeepingResponseDto>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/load", method = RequestMethod.GET)
	@Produces(type)
	protected ResponseEntity<GateKeepingResponseDto> loadGateKeeping(@RequestParam(value = "isInComplete") boolean isInComplete) throws ImproException {
		LOG.info("ProductGateKeepingController-->loadGateKeeping()-->Begin");
		
		ResponseEntity<GateKeepingResponseDto> returnEntity;
		HttpStatus httpStatus=HttpStatus.OK;
		
		GateKeepingResponseDto response = new GateKeepingResponseDto();
		List<ProductGateKeepingDto> gatekeepingList = null;
		response.setCode(AppConstants.SUCCESS_CODE);
		try {
			gatekeepingList = service.loadGateKeeping(isInComplete);
			if (!CollectionUtils.isNotEmpty(gatekeepingList)) {
				response.setCode(AppConstants.NOT_FOUND_CODE);
				response.setMessage(AppConstants.NOT_FOUND_MSG);
			}
			response.setGateKeepingDtoList(gatekeepingList);
		} catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if (LOG.isInfoEnabled()) {
				LOG.info("ProductGateKeepingController-->loadGateKeeping()-->Exception " + ex.getMessage());
			}
			
		}
		LOG.info("ProductGateKeepingController-->loadGateKeeping()-->End");
		returnEntity = new ResponseEntity<GateKeepingResponseDto>(response, httpStatus);
		return returnEntity; 
	}

	/**
	 * Method to view the productGateKeeping.
	 * 
	 * @return ResponseEntity<GateKeepingResponseDto>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/view",method = RequestMethod.GET)
	@Produces(type)
	protected ResponseEntity<GateKeepingResponseDto> loadGateKeeping(@RequestParam(value = "id") int gateKeepingId)
			throws ImproException {
		LOG.info("ProductGateKeepingController-->loadGateKeeping()-->Begin");
		
		ResponseEntity<GateKeepingResponseDto> returnEntity;
		HttpStatus httpStatus=HttpStatus.OK;
		
		GateKeepingResponseDto response = new GateKeepingResponseDto();
		response.setCode(AppConstants.SUCCESS_CODE);
		try {
				ProductGateKeepingDto gatekeepingDto = new ProductGateKeepingDto();
				gatekeepingDto = service.loadGateKeeping(gateKeepingId);
				
				gatekeepingDto.setImageType(AppConstants.IMAGE_TYPE_STD);
				
				ProductDto productDto = new ProductDto();
				productDto.setImageType(AppConstants.IMAGE_TYPE_STD_MOCK);
				productDto.setBitMapName(gatekeepingDto.getProductId());
				gatekeepingDto.setProductDto(productDto);
		
				imgGatekepService.getAllBase64String(gatekeepingDto);
							
				if (gatekeepingDto != null) {
					response.setGateKeepingDto(gatekeepingDto);
					response.setCode(AppConstants.SUCCESS_CODE);
					response.setMessage("");
				}
			}
			catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if (LOG.isInfoEnabled()) {
				LOG.info("ProductGateKeepingController-->loadGateKeeping()-->Exception " + ex.getMessage());
			}
		}
		LOG.info("ProductGateKeepingController-->loadGateKeeping()-->End");
		returnEntity = new ResponseEntity<GateKeepingResponseDto>(response, httpStatus);
		return returnEntity; 
	}

	/**
	 * Method used to export All GateKeeping Products
	 * @return ResponseEntity<byte[]>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/exportAll",method = RequestMethod.GET)
	@Produces(type)
	protected ResponseEntity<byte[]> exportAllGateKeepingProducts(
			@RequestParam(value = "isInComplete") boolean isInComplete) {
		LOG.info("GateKeepingController-->exportGateKeepingProducts()-->Begin");
		byte[] bytes = null;
		
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<byte[]> returnEntity;
		HttpStatus httpStatus=HttpStatus.OK;
		
		try {
			List<ProductGateKeepingDto> gatekeepingList = service.loadGateKeeping(isInComplete);
			GatekeepingExcelReportGenerator excelGenerator = getExcelGenerator();
			XSSFWorkbook workBook = excelGenerator.generateReport(gatekeepingList);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			workBook.write(bos);
			bos.close();
			bytes = bos.toByteArray();
			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.set("charset", "utf-8");
			headers.set("Content-Disposition", "attachment; filename=" + "products");

		} catch (Exception ex) {
			if (LOG.isErrorEnabled()) {
				httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
				LOG.error("GateKeepingController-->exportGateKeepingProducts()-->Exception" + ex.getMessage());
			}
		}

		returnEntity = new ResponseEntity<byte[]>(bytes, headers, httpStatus);
		LOG.info("GateKeepingController-->exportGateKeepingProducts()-->End");
		return returnEntity;
	}

	/**
	 * Method to create excel for Product Export
	 * 
	 * @return ProductExcelReportGenerator
	 * @throws IOException
	 */
	public GatekeepingExcelReportGenerator getExcelGenerator() throws IOException {
		return ExcelGeneratorFactory.getInstance().getGateKeepingExcelReportGenerator(AppConstants.GATEKEEPING_FILENAME,AppConstants.GATEKEEPING_SHEETNAME);
	}

	/**
	 * Method to purge Gatekeeping
	 * @param productGateKeepingListDto
	 * @return ResponseEntity<GateKeepingResponseDto>
	 */
	@RequestMapping(path = "/purge",method = RequestMethod.GET)
	@Produces(type)
	protected ResponseEntity<GateKeepingResponseDto> purgeProductGateKeeping(@RequestParam(value = "id") int gateKeepingId) {
		LOG.info("GateKeepingController-->purgeProductGateKeeping()-->Begin");
		
		ResponseEntity<GateKeepingResponseDto> returnEntity;
		HttpStatus httpStatus=HttpStatus.OK;
		
		GateKeepingResponseDto response = new GateKeepingResponseDto();
		response.setCode(AppConstants.SUCCESS_CODE);
		response.setMessage(AppConstants.GATEKEEPING_PRODUCT_PURGE_SUCC_MSG);
		try {
			service.purgeProductGateKeeping(gateKeepingId);
		} catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			
			if (LOG.isErrorEnabled()) {
				LOG.error("GateKeepingController-->purgeProductGateKeeping()-->Exception" + ex.getMessage());
			}
		}
		LOG.info("GateKeepingController-->purgeProductGateKeeping()-->End");
		returnEntity = new ResponseEntity<GateKeepingResponseDto>(response, httpStatus);
		return returnEntity; 
	}

	/**
	 * Method to trigger StoredProcedure [dbo].[ProductUpdateFromGatekeeping]
	 * 
	 * @param dto
	 * @return ResponseEntity<GateKeepingResponseDto>
	 */
	@RequestMapping(path = "/pushProducts",method = RequestMethod.POST)
	@Produces(type)
	public ResponseEntity<GateKeepingResponseDto> pushProducts() {
		LOG.info("GateKeepingController-->pushProducts()-->Begin");
		
		ResponseEntity<GateKeepingResponseDto>returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;
		
		GateKeepingResponseDto response = new GateKeepingResponseDto();
		try {
			response.setMessage(AppConstants.GATEKEEPING_PUSH_PRODUCTS_SUCC_MSG);
			response.setCode(AppConstants.SUCCESS_CODE);
			service.pushProducts();
		} catch (Exception ex) {
			response.setMessage(AppConstants.FAILURE_MSG);
			response.setCode(AppConstants.FAILURE_CODE);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if (LOG.isInfoEnabled()) {
				LOG.info("GateKeepingController-->pushProducts()-->Exception " + ex.getMessage());
			}
		}
		LOG.info("GateKeepingController-->pushProducts()-->End");
		returnEntity = new ResponseEntity<GateKeepingResponseDto>(response, httpStatus);
		return returnEntity;
	}


	/**
	 * Method to edit Gate keeping
	 * @param gatekeepingDto
	 * @return ResponseEntity<GateKeepingResponseDto>
	 */
	@RequestMapping(path = "/edit",method = RequestMethod.POST)
	@Produces(type)
	protected ResponseEntity<GateKeepingResponseDto> editGateKeeping(@RequestBody ProductGateKeepingDto gatekeepingDto) {
		
		LOG.info("ProductGateKeepingController-->editGateKeeping()-->Begin");
		ResponseEntity<GateKeepingResponseDto>returnEntity;
		HttpStatus httpStatus = HttpStatus.OK;

		GateKeepingResponseDto response = new GateKeepingResponseDto();
		try {
			service.editGateKeeping(gatekeepingDto);
			response.setCode(AppConstants.SUCCESS_CODE);
			response.setMessage(AppConstants.GATEKEEPING_PRODUCT_UPDATE_SUCC_MSG);
		} catch (Exception ex) {
			response.setCode(AppConstants.FAILURE_CODE);
			response.setMessage(AppConstants.FAILURE_MSG);
			httpStatus=HttpStatus.UNPROCESSABLE_ENTITY;
			if (LOG.isErrorEnabled()) {
				LOG.error("ProductGateKeepingController-->editGateKeeping()-->Exception" + ex.getMessage());
			}
		}
		LOG.info("ProductGateKeepingController-->editGateKeeping()-->End");
		returnEntity = new ResponseEntity<GateKeepingResponseDto>(response, httpStatus);
		return returnEntity; 

	}
	
	/**
	 * This method will return additional base64 string of images
	 * @param gatekeepingDto
	 * @return ResponseEntity<gatekeepingDto>
	 * @throws ImproException
	 */
	@RequestMapping(path = "/load/images",method = RequestMethod.POST)
	@Produces(type)
	@Consumes(type)
	protected  ResponseEntity<ProductGateKeepingDto> loadImages(@RequestBody ProductGateKeepingDto gatekeepingDto) throws ImproException {
		LOG.info("ProductGateKeepingController-->loadImages-->Begin");		
		imgGatekepService.getAllBase64String(gatekeepingDto);
		LOG.info("ProductGateKeepingController-->loadImages()-->End");
		return new ResponseEntity<ProductGateKeepingDto>(gatekeepingDto, HttpStatus.OK);
	}
}


