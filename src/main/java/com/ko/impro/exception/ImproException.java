package com.ko.impro.exception;

/**
 * @author swshedge
 * User Defined Exception
 * ImproException is an extension of Exception for specific to IMPRO
 */
public class ImproException extends Exception {

	/**
	 * Reference of  Serializable class
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Reference for String value 
	 */
	private String errorCode = "";

	/**
	 * @param errorCode
	 * @param message
	 */
	public ImproException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 */
	public ImproException(String errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	/**
	 * @param exception
	 */
	public ImproException(Throwable exception) {
		super(exception);
	}

}
