package com.ko.impro.report;

import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ko.impro.dto.AuditLogDto;
import com.ko.impro.dto.AuditLogRequestDto;

/**
 *  @author swshedge
 *  AuditLogExcelReportGenerator to create an excel file 
 *  for Audit Logs
 */
public class AuditLogExcelReportGenerator  extends BasicExcelGenerator  {

	/**
	 * Use to supply Excel sheetName
	 */
	private String sheetName = "";	
	
	/**
	 * Use to supply headers required for Excel sheet
	 */
	private static String[] header = { "Transaction Date", "Transaction User","Table Updated","Record Updated","Transaction Type"};

	/**
	 * @param fileName
	 * @param sheetName
	 */
	public AuditLogExcelReportGenerator(String fileName, String sheetName) {
		this.sheetName = sheetName;
	}

	/**
	 * Method used to generate excel file for all auditlogs 
	 * @param adtLgDtoLst
	 * @return
	 *
	 */
	public XSSFWorkbook generateReport(List<AuditLogDto> adtLgDtoLst) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(sheetName);
		String[] selctColsHead = {};
		selctColsHead = header;
		createHeaderRow(sheet, selctColsHead);
		int rowCount = 0;
		XSSFCellStyle style = workbook.createCellStyle();
		XSSFCellStyle style1 = workbook.createCellStyle();
		if(adtLgDtoLst != null ) { 
			for (AuditLogDto auditLogDto : adtLgDtoLst) {
				Row row = sheet.createRow(++rowCount);
				writeBook(auditLogDto, row, workbook, style, style1, selctColsHead);
			}
		}
		autoSizeColumns(workbook);
		sheet.createFreezePane(0, 1);
		return workbook;
	}


	/**
	 * Method used to create headers for excel sheet based on selected columns and
	 * image filter
	 * @param sheet
	 * @param selctColsHead
	 */
	private void createHeaderRow(Sheet sheet, String[] selctColsHead) {

		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		Font font = sheet.getWorkbook().createFont();
		font.setFontName("Calibri");
		font.setFontHeightInPoints((short) 11);
		cellStyle.setFont(font);
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
		cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		Row row = sheet.createRow(0);
		row.setHeight((short) 800);
		Cell cell = null;
		String dynamicColumnName = "";

		for (int i = 0; i < selctColsHead.length; i++) {
			dynamicColumnName = selctColsHead[i];
			cell = row.createCell(i);
			cell.setCellStyle(cellStyle);
			cell.setCellValue(dynamicColumnName.toUpperCase());
		}
	}

	/**
	 * Method used to write generated auditLog data to an excel sheet
	 * @param adtLogDto
	 * @param row
	 * @param workbook
	 * @param style
	 * @param style1
	 * @param selctColsHead
	 */
	private void writeBook(AuditLogDto adtLogDto, Row row, XSSFWorkbook workbook, XSSFCellStyle style,XSSFCellStyle style1, String[] selctColsHead) {
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style1.setBorderLeft(CellStyle.BORDER_THIN);
		style1.setBorderRight(CellStyle.BORDER_THIN);
		Cell cell = null;
		String dynamicColumnName = "";
		for (int i = 0; i < selctColsHead.length; i++) {
			dynamicColumnName = selctColsHead[i];
			if (dynamicColumnName.equalsIgnoreCase("Transaction Date")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(adtLogDto.getTableUpdated());
			} else if (dynamicColumnName.equalsIgnoreCase("Transaction User")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(adtLogDto.getTransactionUser());
			} else if (dynamicColumnName.equalsIgnoreCase("Table Updated")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(adtLogDto.getTableUpdated());
			} else if (dynamicColumnName.equalsIgnoreCase("Record Updated")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(adtLogDto.getRecordUpdated());
			} else if (dynamicColumnName.equalsIgnoreCase("Transaction Type")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(adtLogDto.getTransactionType());
			}

		}
	}

}
