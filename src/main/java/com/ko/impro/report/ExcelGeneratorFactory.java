package com.ko.impro.report;

import java.io.IOException;

/**
 * @author swshedge
 * ExcelGeneratorFactory contains Excel Generators 
 * for every Excel Generation class
 */
public final class ExcelGeneratorFactory {
	
	
	/**
	 * Reference of ExcelGeneratorFactory
	 */
	private static ExcelGeneratorFactory factory;

	/**
	 * @return ExcelGeneratorFactory
	 * @throws IOException
	 */
	public static ExcelGeneratorFactory getInstance() throws IOException {
		if (factory == null) {
			factory = new ExcelGeneratorFactory();
		}
		return factory;
	}

	/**
	 * Product Excel Generator
	 * @param fileName
	 * @param sheetName
	 * @return
	 */
	public ProductExcelReportGenerator getProductExcelReportGenerator(String fileName, String sheetName) {
		return new ProductExcelReportGenerator(fileName, sheetName);
	}

	/**
	 * Audit Excel Generator
	 * @param auditFilename
	 * @param auditSheetname
	 * @return
	 */
	public AuditLogExcelReportGenerator getAuditLogExcelReportGenerator(String auditFilename, String auditSheetname) {
		return new AuditLogExcelReportGenerator(auditFilename, auditSheetname);

	}
	
	/**
	 * GateKeeping Excel Generator
	 * @param productSheetname 
	 * @param gatekeepingFilename
	 * @param gatekeepingSheetname
	 * @return
	 */
	public GatekeepingExcelReportGenerator getGateKeepingExcelReportGenerator( String filename, String sheetName) {
		return new GatekeepingExcelReportGenerator( filename, sheetName);
	}

}
