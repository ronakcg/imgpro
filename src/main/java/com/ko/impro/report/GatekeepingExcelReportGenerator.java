package com.ko.impro.report;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ko.impro.dto.ProductGateKeepingDto;

/**
 * @author swshedge
 * GatekeepingExcelReportGenerator to create an excel file 
 *  for Gatekeeping
 */
public class GatekeepingExcelReportGenerator extends BasicExcelGenerator {
	/**
	 * Use to supply Excel sheetName
	 */
	private String sheetName = "";
	
	/**
	 * Use to supply headers required for Excel sheet
	 */
	private static String[] header = {"PRODUCT NAME","PRODUCT ID","UPC","PACKAGE CODE","PACKAGE NAME","BRAND CODE","BRAND NAME",
			"FLAVOR CODE","FLAVOR NAME","CATEGORY CODE","CATEGORY NAME","DATE MAINTAINED","SUBCATEGORY CODE","SUBCATEGORY NAME","SEGMENT CODE","SEGMENT NAME",
			"SUBSEGMENT CODE","SUBSEGMENT NAME","MANUFACTURE CODE","MANUFACTURE NAME","EXTENDED_DESC_ID","UPC12","JPG","LOWRES1","LOWRES2","LOWRES3","LOWRES7","LOWRES8","LOWRES9",
			"HIRES1","HIRES2","HIRES3","HIRES7","HIRES8","HIRES9","MOCK1","MOCK2","MOCK3","MOCK7","MOCK8","MOCK9","SMSB DATA","JDA DATA","INFORMATICA DATA"};

	/**
	 * @param fileName
	 * @param sheetName
	 */
	public GatekeepingExcelReportGenerator(String fileName, String sheetName) {
		this.sheetName = sheetName;
	}
	
	/**
	 * Method used to generate excel file for all GateKeeping products
	 * @param List<ProductGateKeepingDto> gateKeepingDtoList
	 * @return XSSFWorkbook
	 *
	 */
	public XSSFWorkbook generateReport(List<ProductGateKeepingDto> gatKeepDtoList) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(sheetName);
		String[] selctColHeader = {};
		selctColHeader = header;
		
		createHeaderRow(sheet,selctColHeader);
		int rowCount = 0;
		XSSFCellStyle style = workbook.createCellStyle();
		XSSFCellStyle style1 = workbook.createCellStyle();
		if(gatKeepDtoList != null) {
			for (ProductGateKeepingDto dto : gatKeepDtoList) {
				Row row = sheet.createRow(++rowCount);
				writeBook(dto, row, style, style1, header);
			}
		}
		
		autoSizeColumns(workbook);
		sheet.createFreezePane(0, 1);
		return workbook;
	}
	
	/**
	 * Method used to create headers for excel sheet
	 * @param sheet
	 * @param selctColHeader
	 */
	private void createHeaderRow(Sheet sheet, String[] selctColHeader) {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		Font font = sheet.getWorkbook().createFont();
		font.setFontName("Calibri");
		font.setFontHeightInPoints((short) 11);
		cellStyle.setFont(font);
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
		cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		Row row = sheet.createRow(0);
		row.setHeight((short) 800);
		Cell cell = null;
		String dynamicColumnName = "";
		
		for (int i = 0; i < selctColHeader.length; i++) {
			dynamicColumnName = selctColHeader[i];
			cell = row.createCell(i);
			cell.setCellStyle(cellStyle);
			cell.setCellValue(dynamicColumnName.toUpperCase());
		}
	}
	
	/**
	 * Method used to write GateKeeping product data to an excel sheet
	 * @param productDto
	 * @param row
	 * @param workbook
	 * @param style
	 * @param style1
	 * @param selctColHeader
	 */
	private void writeBook(ProductGateKeepingDto prodGtKeepDto, Row row,  XSSFCellStyle style,XSSFCellStyle style1, String[] selctColHeader) {
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style1.setBorderLeft(CellStyle.BORDER_THIN);
		style1.setBorderRight(CellStyle.BORDER_THIN);
		Cell cell = null;
		String dynamicColumnName = "";
		for (int i = 0; i < selctColHeader.length; i++) {
			dynamicColumnName = selctColHeader[i];
			if(dynamicColumnName.equalsIgnoreCase("PRODUCT ID")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getProductId());
			
			} else if(dynamicColumnName.equalsIgnoreCase("PRODUCT NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getProductName());
			}  else if(dynamicColumnName.equalsIgnoreCase("PACKAGE CODE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getPackageDto().getPackageCode());
			} else if(dynamicColumnName.equalsIgnoreCase("PACKAGE NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getPackageDto().getPackageName());
			} else if(dynamicColumnName.equalsIgnoreCase("BRAND CODE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getBrandDto().getBrandCode());
			} else if(dynamicColumnName.equalsIgnoreCase("BRAND NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getBrandDto().getBrandName());
			} else if(dynamicColumnName.equalsIgnoreCase("UPC")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getUpc());
			} else if(dynamicColumnName.equalsIgnoreCase("FLAVOR CODE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getFlavorDto().getFlavorCode());
			} else if(dynamicColumnName.equalsIgnoreCase("FLAVOR NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getFlavorDto().getFlavorDescription());
			} else if(dynamicColumnName.equalsIgnoreCase("CATEGORY CODE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getCategoryDto().getCategoryCode());
			}  else if(dynamicColumnName.equalsIgnoreCase("CATEGORY NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getCategoryDto().getCategoryDescription());
			}  else if(dynamicColumnName.equalsIgnoreCase("DATE MAINTAINED")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getDateMaintained());
			} else if(dynamicColumnName.equalsIgnoreCase("SUBCATEGORY CODE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getSubCategoryDto().getSubCategoryCode());
			} else if(dynamicColumnName.equalsIgnoreCase("SUBCATEGORY NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getSubCategoryDto().getSubCategoryDescription());
			} else if(dynamicColumnName.equalsIgnoreCase("SEGMENT CODE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getSegmentDto().getSegmentCode());
			} else if(dynamicColumnName.equalsIgnoreCase("SEGMENT NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getSegmentDto().getSegmentDescription());
			} else if(dynamicColumnName.equalsIgnoreCase("SUBSEGMENT CODE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getSubSegmentDto().getSubSegmentCode());
			} else if(dynamicColumnName.equalsIgnoreCase("SUBSEGMENT NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getSubSegmentDto().getSubSegmentDescription());
			} else if(dynamicColumnName.equalsIgnoreCase("MANUFACTURE CODE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getManufacturerDto().getManufacturerCode());
			} else if(dynamicColumnName.equalsIgnoreCase("MANUFACTURE NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getManufacturerDto().getManufacturerName());
				
			} else if(dynamicColumnName.equalsIgnoreCase("EXTENDED_DESC_ID")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getExtPrdDescDto().getExtendedDescId());
			} else if(dynamicColumnName.equalsIgnoreCase("UPC12")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getUpc12());
			}/* else if(dynamicColumnName.equalsIgnoreCase("SMSB DATA")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getSmsbData());
			}  else if(dynamicColumnName.equalsIgnoreCase("JDA DATA")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getJdaData());
			}  else if(dynamicColumnName.equalsIgnoreCase("INFORMATICA DATA")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(prodGtKeepDto.getInformaticaData());
			}*/
			
			/*if(prodGtKeepDto.getMissingImageDto() != null) {
				if (dynamicColumnName.equalsIgnoreCase("JPG")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getJpg());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES1")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getLowRes1());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES2")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getLowRes2());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES3")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getLowRes3());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES7")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getLowRes7());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES8")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getLowRes8());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES9")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getLowRes9());
				}  else if (dynamicColumnName.equalsIgnoreCase("HIRES1")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getHiRes1());
				}  else if (dynamicColumnName.equalsIgnoreCase("HIRES2")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getHiRes2());
				}   else if (dynamicColumnName.equalsIgnoreCase("HIRES3")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getHiRes3());
				}  else if (dynamicColumnName.equalsIgnoreCase("HIRES7")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getHiRes7());
				}  else if (dynamicColumnName.equalsIgnoreCase("HIRES8")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getHiRes8());
				}  else if (dynamicColumnName.equalsIgnoreCase("HIRES9")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getHiRes9());
				}  else if (dynamicColumnName.equalsIgnoreCase("MOCK1")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getMock1());
				}  else if (dynamicColumnName.equalsIgnoreCase("MOCK2")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getMock2());
				}  else if (dynamicColumnName.equalsIgnoreCase("MOCK3")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getMock3());
				}  else if (dynamicColumnName.equalsIgnoreCase("MOCK7")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getMock7());
				}  else if (dynamicColumnName.equalsIgnoreCase("MOCK8")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getMock8());
				}  else if (dynamicColumnName.equalsIgnoreCase("MOCK9")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(prodGtKeepDto.getMissingImageDto().getMock9());
				}
				
			}
*/		}
	}
}

