package com.ko.impro.report;

import java.util.*;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.ProductDto;

/**
 * @author swshedge
 *  ProductExcelReportGenerator to create an excel file 
 *  for Product
 */
public class ProductExcelReportGenerator extends BasicExcelGenerator {
	
	/**
	 * Use to supply Excel sheetName
	 */
	private String sheetName = "";
	
	/**
	 * Use to supply headers required for Excel sheet
	 */
	private static String[] activeHeader = { "PRODUCT CODE","NAME", "SHORT NAME","UPC", "UPC12","ID", "DATE MAINTAINED", "DATE ADDED",
			 "MFG", "BRAND", "EXTENDED DESC","PACKAGE", "PKG SIZE", "CATEGORY",
			"SUB-CATEGORY", "FLAVOR", "SEGMENT", "SUB-SEGMENT","CONSUMPTION", "JPG", "LOWRES1", "LOWRES2", "LOWRES3", "LOWRES7", "LOWRES8", 
			"LOWRES9", "HIRES1", "HIRES2", "HIRES3", "HIRES7", "HIRES8", "HIRES9", "MOCK1", "MOCK2", "MOCK3", "MOCK7", "MOCK8", "MOCK9" ,
			"NIELSENUPC","NIELSENDESC","ACN CATEGORY","ACN SUBCATEGORY","ACN SEGMENT","ACN SUBSEGMENT","ACN BEVERAGETYPE","ACN MANUFACTURER",
			"ACN BRAND","ACN FLAVOR","ACN LONGNAME","ACN CONSUMPTIONTYPE"};
	
	/**
	 * Use to supply archiveHeader required for Excel sheet
	 */
	private static String[] archiveHeader = { "PRODUCT CODE","NAME", "SHORT NAME","UPC", "UPC12","ID", "DATE MAINTAINED", "DATE ADDED", "DATE ARCHIVED","ARCHIVE REASON",
			 "MFG", "BRAND", "EXTENDED DESC","PACKAGE", "PKG SIZE", "CATEGORY",
			"SUB-CATEGORY", "FLAVOR", "SEGMENT", "SUB-SEGMENT", "CONSUMPTION", "JPG", "LOWRES1", "LOWRES2", "LOWRES3", "LOWRES7", "LOWRES8", 
			"LOWRES9", "HIRES1", "HIRES2", "HIRES3", "HIRES7", "HIRES8", "HIRES9", "MOCK1", "MOCK2", "MOCK3", "MOCK7", "MOCK8", "MOCK9" ,
			"NIELSENUPC","NIELSENDESC","ACN CATEGORY","ACN SUBCATEGORY","ACN SEGMENT","ACN SUBSEGMENT","ACN BEVERAGETYPE","ACN MANUFACTURER",
			"ACN BRAND","ACN FLAVOR","ACN LONGNAME","ACN CONSUMPTIONTYPE"};

	/**
	 * @param fileName
	 * @param sheetName
	 */
	public ProductExcelReportGenerator(String fileName, String sheetName) {
		this.sheetName = sheetName;
	}

	/**
	 * Method used to generate excel file for all products
	 * @param productDtoList
	 * @param productRequestWrapperDto
	 * @return
	 *
	 */
	public XSSFWorkbook generateReport(List<ProductDto> productDtoList) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(sheetName);
		createHeaderRow(sheet, activeHeader);
		int rowCount = 0;
		XSSFCellStyle style = workbook.createCellStyle();
		XSSFCellStyle style1 = workbook.createCellStyle();
		if(productDtoList != null) {
			for (ProductDto procustDto : productDtoList) {
				Row row = sheet.createRow(++rowCount);
				writeBook(procustDto, row, workbook, style, style1, activeHeader);
			}
		}
		autoSizeColumns(workbook);
		sheet.createFreezePane(0, 1);
		return workbook;
	}

	/**
	 * Method used to generate excel file for all archived products
	 * @param productDtoList
	 * @param productRequestWrapperDto
	 * @return XSSFWorkbook
	 */
	public XSSFWorkbook generateArchivedReport(List<ArchivedProductDto> productDtoList) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(sheetName);
		createHeaderRow(sheet, archiveHeader);
		int rowCount = 0;
		XSSFCellStyle style = workbook.createCellStyle();
		XSSFCellStyle style1 = workbook.createCellStyle();
		if(productDtoList != null) {
			for (ArchivedProductDto procustDto : productDtoList) {
				Row row = sheet.createRow(++rowCount);
				writeBook(procustDto, row, workbook, style, style1, archiveHeader);
			}
		}

		autoSizeColumns(workbook);
		sheet.createFreezePane(0, 1);
		return workbook;
	}

	/**
	 * Method used to create headers for excel sheet based on selected columns and
	 * image filter
	 * @param sheet
	 * @param selectedColumnsHeader
	 */
	private void createHeaderRow(Sheet sheet, String[] selctedColHead) {

		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		Font font = sheet.getWorkbook().createFont();
		font.setFontName("Calibri");
		font.setFontHeightInPoints((short) 11);
		cellStyle.setFont(font);
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
		cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		Row row = sheet.createRow(0);
		row.setHeight((short) 800);
		Cell cell = null;
		String dynamicColumnName = "";

		for (int i = 0; i < selctedColHead.length; i++) {
			dynamicColumnName = selctedColHead[i];
			cell = row.createCell(i);
			cell.setCellStyle(cellStyle);
			cell.setCellValue(dynamicColumnName.toUpperCase());
		}
	}

	/**
	 * Method used to write generated product data to an excel sheet
	 * @param productDto
	 * @param row
	 * @param workbook
	 * @param style
	 * @param style1
	 * @param selectedColumnsHeader
	 */
	private void writeBook(ProductDto productDto, Row row, XSSFWorkbook workbook, XSSFCellStyle style,XSSFCellStyle style1, String[] selctedColHead) {
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style1.setBorderLeft(CellStyle.BORDER_THIN);
		style1.setBorderRight(CellStyle.BORDER_THIN);
		Cell cell = null;
		String dynamicColumnName = "";
		for (int i = 0; i < selctedColHead.length; i++) {
			dynamicColumnName = selctedColHead[i];
			if (dynamicColumnName.equalsIgnoreCase("PRODUCT CODE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getProductCode());
			} if (dynamicColumnName.equalsIgnoreCase("NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getShortProductName());
			} else if (dynamicColumnName.equalsIgnoreCase("SHORT NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getProductName());
			} else if (dynamicColumnName.equalsIgnoreCase("UPC")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getUpc());
			} else if (dynamicColumnName.equalsIgnoreCase("UPC12")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getUpc12());
			} else if (dynamicColumnName.equalsIgnoreCase("ID")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getBitMapName());
			} else if (dynamicColumnName.equalsIgnoreCase("DATE MAINTAINED")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getDateMaintained());
			} else if (dynamicColumnName.equalsIgnoreCase("DATE ADDED")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getDateAdded());
			}else if (dynamicColumnName.equalsIgnoreCase("MFG")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getManufacturerDto().getManufacturerName());
			} else if (dynamicColumnName.equalsIgnoreCase("Brand")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getBrandDto().getBrandName());
			}else if (dynamicColumnName.equalsIgnoreCase("Extended Desc")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getExtendedProductDescDto().getLongProductDesc());
			} else if (dynamicColumnName.equalsIgnoreCase("PACKAGE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getPkgdto().getPackageName());
			} else if (dynamicColumnName.equalsIgnoreCase("PKG SIZE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getPackageSizeDto().getPackageSizeDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("Category")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getCategoryDto().getCategoryDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("Sub-Category")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getSubCategoryDto().getSubCategoryDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("Flavor")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getFlavorDto().getFlavorDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("Segment")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getSegmentDto().getSegmentDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("Sub-Segment")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getSubSegmentDto().getSubSegmentDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("CONSUMPTION")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getConsumptionType());
			} else if (dynamicColumnName.equalsIgnoreCase("NIELSENUPC")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getNielsenUpc());
			} else if (dynamicColumnName.equalsIgnoreCase("NIELSENDESC")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getNielsenDesc());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN CATEGORY")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnCategory());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN SUBCATEGORY")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnSubCategory());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN SEGMENT")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnSegment());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN SUBSEGMENT")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnSubSegment());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN BEVERAGETYPE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnBeverageType());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN MANUFACTURER")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnConsumptionType());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN BRAND")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnBrand());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN FLAVOR")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnKeyFlavor());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN LONGNAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnLongName());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN CONSUMPTIONTYPE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnConsumptionType());
			}
			
			if(productDto.getMissingImagesDto() != null) {
				if (dynamicColumnName.equalsIgnoreCase("JPG")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getJpg());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES1")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes1());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES2")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes2());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES3")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes3());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES7")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes7());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES8")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes8());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES9")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes9());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES1")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes1());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES2")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes2());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES3")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes3());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES7")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes7());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES8")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes8());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES9")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes9());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK1")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock1());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK2")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock2());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK3")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock3());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK7")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock7());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK8")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock8());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK9")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock9());
				} else if (dynamicColumnName.equalsIgnoreCase("CONSUMPTION")) {
					cell = row.createCell(i);
					cell.setCellStyle(style1);
					cell.setCellValue(productDto.getConsumptionType());
				}
				
			}

		}
	}

	/**
	 * Method used to write generated archived product data to an excel sheet
	 * @param productDto
	 * @param row
	 * @param workbook
	 * @param style
	 * @param style1
	 * @param selctedColHead
	 */
	private void writeBook(ArchivedProductDto productDto, Row row, XSSFWorkbook workbook, XSSFCellStyle style, XSSFCellStyle style1, String[] selctedColHead) {
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style1.setBorderLeft(CellStyle.BORDER_THIN);
		style1.setBorderRight(CellStyle.BORDER_THIN);
		Cell cell = null;
		String dynamicColumnName = "";

		for (int i = 0; i < selctedColHead.length; i++) {
			dynamicColumnName = selctedColHead[i];
			if (dynamicColumnName.equalsIgnoreCase("PRODUCT CODE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getProductCode());
			}if (dynamicColumnName.equalsIgnoreCase("NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getProductName());
			}else if (dynamicColumnName.equalsIgnoreCase("SHORT NAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getShortProductName());
			} else if (dynamicColumnName.equalsIgnoreCase("UPC")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getUpc());
			} else if (dynamicColumnName.equalsIgnoreCase("UPC12")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getUpc12());
			} else if (dynamicColumnName.equalsIgnoreCase("ID")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getBitMapName());
			} else if (dynamicColumnName.equalsIgnoreCase("DATE MAINTAINED")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getDateMaintained());
			} else if (dynamicColumnName.equalsIgnoreCase("DATE ADDEED")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getDateAdded());
			} else if (dynamicColumnName.equalsIgnoreCase("DATE ARCHIVED")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getArchiveDate());
			} else if (dynamicColumnName.equalsIgnoreCase("ARCHIVE REASON")) {
				cell = row.createCell(i);
				cell.setCellStyle(style);
				cell.setCellValue(productDto.getArchiveReason());
			} else if (dynamicColumnName.equalsIgnoreCase("MFG")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getManufacturerDto().getManufacturerName());
			} else if (dynamicColumnName.equalsIgnoreCase("Brand")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getBrandDto().getBrandName());
			} else if (dynamicColumnName.equalsIgnoreCase("PACKAGE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getPkgdto().getPackageName());
			} else if (dynamicColumnName.equalsIgnoreCase("PKG SIZE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getPackageSizeDto().getPackageSizeDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("Category")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getCategoryDto().getCategoryDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("Sub-Category")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getSubCategoryDto().getSubCategoryDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("Flavor")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getFlavorDto().getFlavorDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("Segment")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getSegmentDto().getSegmentDescription());
			} else if (dynamicColumnName.equalsIgnoreCase("Sub-Segment")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getSubSegmentDto().getSubSegmentDescription());
			}  else if (dynamicColumnName.equalsIgnoreCase("CONSUMPTION")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getConsumptionType());
			} else if (dynamicColumnName.equalsIgnoreCase("NIELSENUPC")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getNielsenUpc());
			} else if (dynamicColumnName.equalsIgnoreCase("NIELSENDESC")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getNielsenDesc());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN CATEGORY")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnCategory());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN SUBCATEGORY")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnSubCategory());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN SEGMENT")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnSegment());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN SUBSEGMENT")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnSubSegment());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN BEVERAGETYPE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnBeverageType());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN MANUFACTURER")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnConsumptionType());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN BRAND")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnBrand());
			} else if (dynamicColumnName.equalsIgnoreCase("Extended Desc")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getExtendedProductDescDto().getLongProductDesc());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN FLAVOR")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnKeyFlavor());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN LONGNAME")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnLongName());
			}else if (dynamicColumnName.equalsIgnoreCase("ACN CONSUMPTIONTYPE")) {
				cell = row.createCell(i);
				cell.setCellStyle(style1);
				cell.setCellValue(productDto.getAcnConsumptionType());
			}
			if(productDto.getMissingImagesDto() != null) {
				if (dynamicColumnName.equalsIgnoreCase("JPG")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getJpg());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES1")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes1());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES2")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes2());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES3")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes3());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES7")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes7());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES8")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes8());
				} else if (dynamicColumnName.equalsIgnoreCase("LOWRES9")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getLowRes9());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES1")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes1());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES2")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes2());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES3")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes3());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES7")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes7());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES8")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes8());
				} else if (dynamicColumnName.equalsIgnoreCase("HIRES9")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getHiRes9());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK1")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock1());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK2")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock2());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK3")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock3());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK7")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock7());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK8")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock8());
				} else if (dynamicColumnName.equalsIgnoreCase("MOCK9")) {
					cell = row.createCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(productDto.getMissingImagesDto().getMock9());
				} 
			}

		}
	}
}