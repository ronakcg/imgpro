package com.ko.impro.report;

import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author swshedge
 * BasicExcelGenerator to build basic functionality
 * Required while excel generation
 */
public  abstract class BasicExcelGenerator {
	
	/**
	 * This is Constant Log
	 */
	protected static final Logger LOG = LoggerFactory.getLogger(BasicExcelGenerator.class);
	/**
	 * Use to supply fileName required for Excel sheet
	 */
	protected String fileName;
	
	/**
	 * Use to supply sheetName required for Excel sheet
	 */
	protected String sheetName;
/*	protected short general = (short)BuiltinFormats.getBuiltinFormat("General");
	protected short format = (short)BuiltinFormats.getBuiltinFormat("#,##0.00");
	protected short numberFormat = (short)BuiltinFormats.getBuiltinFormat("0");
	protected NumberFormat fmt = NumberFormat.getInstance(Locale.US);
*/	

	/**
	 * Method used to autosize the columns inside excel sheet
	 * @param workbook
	 *
	 */
	public void autoSizeColumns(XSSFWorkbook workbook) {
	    int numberOfSheets = workbook.getNumberOfSheets();
	    for (int i = 0; i < numberOfSheets; i++) {
	    	Sheet sheet = workbook.getSheetAt(i);
	    	if (sheet.getPhysicalNumberOfRows() > 0) {
	            Row row = sheet.getRow(0);
	            Iterator<Cell> cellIterator = row.cellIterator();
	            while (cellIterator.hasNext()) {
	                Cell cell = cellIterator.next();
	                int columnIndex = cell.getColumnIndex();
	                sheet.autoSizeColumn(columnIndex);
	            }
	        }
	    }
	}

	/**
	 * To validate provided String is number OR not
	 * @param string
	 * @return boolean
	 */
	public static boolean isNumber(String string) {
	    if (string == null || string.isEmpty()) {
	        return false;
	    }
	    int count = 0;
	    int stringLength = string.length();
	    if (string.charAt(0) == '-' || string.charAt(0) == '.') {
	        if (stringLength > 1) {
	        	count++;
	        } else {
	            return false;
	        }
	    }
	    if (!Character.isDigit(string.charAt(count))
	            || !Character.isDigit(string.charAt(stringLength - 1))) {
	        return false;
	    }
	    count++;
	    stringLength--;
	    if (count >= stringLength) {
	        return true;
	    }
	    for (; count < stringLength; count++) {
	        if (!Character.isDigit(string.charAt(count))
	                && string.charAt(count) != '.') {
	            return false;
	        }
	    }
	    return true;
	}
}
