package com.ko.impro.model.brand;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.manufacturer.Manufacturers;


@Entity
@Table(name = "Brands")
public class Brands implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private int brandCode;
	private Manufacturers manufacturers;
	private String brandName;
	private String shortBrandName;
	private Short viewingOrder;
	
	@Id
	@Column(name = "BrandCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.BrandCode_Seq",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
	public int getBrandCode() {
		return this.brandCode;
	}

	public void setBrandCode(int brandCode) {
		this.brandCode = brandCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BrandMfgCode", nullable = false)
	public Manufacturers getManufacturers() {
		return this.manufacturers;
	}

	public void setManufacturers(Manufacturers manufacturers) {
		this.manufacturers = manufacturers;
	}

	@Column(name = "BrandName", nullable = false)
	public String getBrandName() {
		return this.brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	@Column(name = "ShortBrandName", nullable = false)
	public String getShortBrandName() {
		return this.shortBrandName;
	}

	public void setShortBrandName(String shortBrandName) {
		this.shortBrandName = shortBrandName;
	}

	@Column(name = "ViewingOrder")
	public Short getViewingOrder() {
		return this.viewingOrder;
	}

	public void setViewingOrder(Short viewingOrder) {
		this.viewingOrder = viewingOrder;
	}

	
}
