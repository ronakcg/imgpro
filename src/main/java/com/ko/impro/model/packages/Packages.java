package com.ko.impro.model.packages;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.containertype.ContainerType;
import com.ko.impro.model.packagesize.PackageSize;
import com.ko.impro.model.packagestyle.PackageStyle;
import com.ko.impro.model.shape.Shapes;

/**
 * Packages 
 */
@Entity
@Table(name = "Packages")
public class Packages implements BaseEntity {
	
	private static final long serialVersionUID = 1L;
	private int packageCode;
	private String packageName;
	private ContainerType containerType;
	private PackageStyle packageStyle;
	private PackageSize packageSize;
	private Shapes shapes;
	private Short unitPerTray;
	private BigDecimal liquidVolumePerPkg;
	private BigDecimal unitWidth;
	private BigDecimal unitHeight;
	private BigDecimal unitDepth;
	private BigDecimal trayWidth;
	private BigDecimal trayHeight;
	private BigDecimal trayDepth;
	private BigDecimal trayWide;
	private BigDecimal trayHigh;
	private BigDecimal trayDeep;
	
	@Id
	@Column(name = "PackageCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.PackageCode_Seq" ,allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
	public int getPackageCode() {
		return this.packageCode;
	}

	public void setPackageCode(int packageCode) {
		this.packageCode = packageCode;
	}

	
	@Column(name = "PackageName", nullable = false)
	public String getPackageName() {
		return this.packageName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ContainerType", nullable = false)
	public ContainerType getContainerType() {
		return this.containerType;
	}

	public void setContainerType(ContainerType containerType) {
		this.containerType = containerType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PackageSizeCode", nullable = false)
	public PackageSize getPackageSize() {
		return this.packageSize;
	}

	public void setPackageSize(PackageSize packageSize) {
		this.packageSize = packageSize;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ShapeId", nullable = false)
	public Shapes getShapes() {
		return this.shapes;
	}

	public void setShapes(Shapes shapes) {
		this.shapes = shapes;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PackageStyle" , nullable = false)
	public PackageStyle getPackageStyle() {
		return this.packageStyle;
	}

	public void setPackageStyle(PackageStyle packageStyle) {
		this.packageStyle = packageStyle;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	@Column(name = "UnitPerTray")
	public Short getUnitPerTray() {
		return unitPerTray;
	}

	public void setUnitPerTray(Short unitPerTray) {
		this.unitPerTray = unitPerTray;
	}

	@Column(name = "LiquidVolumePerPkg", precision = 5)
	public BigDecimal getLiquidVolumePerPkg() {
		return this.liquidVolumePerPkg;
	}

	public void setLiquidVolumePerPkg(BigDecimal liquidVolumePerPkg) {
		this.liquidVolumePerPkg = liquidVolumePerPkg;
	}

	@Column(name = "UnitWidth", precision = 5, scale = 3)
	public BigDecimal getUnitWidth() {
		return this.unitWidth;
	}

	public void setUnitWidth(BigDecimal unitWidth) {
		this.unitWidth = unitWidth;
	}

	@Column(name = "UnitHeight", precision = 5, scale = 3)
	public BigDecimal getUnitHeight() {
		return this.unitHeight;
	}

	public void setUnitHeight(BigDecimal unitHeight) {
		this.unitHeight = unitHeight;
	}

	@Column(name = "UnitDepth", precision = 5, scale = 3)
	public BigDecimal getUnitDepth() {
		return this.unitDepth;
	}

	public void setUnitDepth(BigDecimal unitDepth) {
		this.unitDepth = unitDepth;
	}
	
	@Column(name = "TrayWidth", precision = 5)
	public BigDecimal getTrayWidth() {
		return trayWidth;
	}

	public void setTrayWidth(BigDecimal trayWidth) {
		this.trayWidth = trayWidth;
	}

	@Column(name = "TrayHeight", precision = 5, scale = 3)
	public BigDecimal getTrayHeight() {
		return trayHeight;
	}

	public void setTrayHeight(BigDecimal trayHeight) {
		this.trayHeight = trayHeight;
	}

	@Column(name = "TrayDepth", precision = 5, scale = 3)
	public BigDecimal getTrayDepth() {
		return trayDepth;
	}

	public void setTrayDepth(BigDecimal trayDepth) {
		this.trayDepth = trayDepth;
	}

	@Column(name = "TrayWide", precision = 5, scale = 3)
	public BigDecimal getTrayWide() {
		return trayWide;
	}

	public void setTrayWide(BigDecimal trayWide) {
		this.trayWide = trayWide;
	}

	@Column(name = "TrayHigh", precision = 5, scale = 3)
	public BigDecimal getTrayHigh() {
		return trayHigh;
	}

	public void setTrayHigh(BigDecimal trayHigh) {
		this.trayHigh = trayHigh;
	}

	@Column(name = "TrayDeep", precision = 5, scale = 3)
	public BigDecimal getTrayDeep() {
		return trayDeep;
	}

	public void setTrayDeep(BigDecimal trayDeep) {
		this.trayDeep = trayDeep;
	}

}
