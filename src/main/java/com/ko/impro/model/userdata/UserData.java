package com.ko.impro.model.userdata;

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.userrole.UserRoles;

/**
 * ImgProUserData
 */
@Entity
@Table(name = "ImgPro_User_Data")
public class UserData implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private UserDataId id;
	private UserRoles imgProUserRoles;
	private String userStatus;
	private String userLastName;
	private String userFirstName;
	private String userPhoneNumber;
	private Date userDownLoadDateTime;
	private Date userLastLogInDate;
	private Date userLastLogOutDate;
	private String userEmailAddress;

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "userId", column = @Column(name = "User_Id", nullable = false)),
			@AttributeOverride(name = "roleId", column = @Column(name = "Role_Id", nullable = false)) })
	public UserDataId getId() {
		return this.id;
	}

	public void setId(UserDataId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Role_Id", nullable = false, insertable = false, updatable = false)
	public UserRoles getImgProUserRoles() {
		return this.imgProUserRoles;
	}

	public void setImgProUserRoles(UserRoles imgProUserRoles) {
		this.imgProUserRoles = imgProUserRoles;
	}

	@Column(name = "User_Status", length = 20)
	public String getUserStatus() {
		return this.userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	@Column(name = "User_Last_Name", length = 20)
	public String getUserLastName() {
		return this.userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	@Column(name = "User_First_Name", length = 20)
	public String getUserFirstName() {
		return this.userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	@Column(name = "User_Phone_Number", length = 20)
	public String getUserPhoneNumber() {
		return this.userPhoneNumber;
	}

	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "User_DownLoad_DateTime", length = 23)
	public Date getUserDownLoadDateTime() {
		return this.userDownLoadDateTime;
	}

	public void setUserDownLoadDateTime(Date userDownLoadDateTime) {
		this.userDownLoadDateTime = userDownLoadDateTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "User_Last_LogIn_date", length = 23)
	public Date getUserLastLogInDate() {
		return this.userLastLogInDate;
	}

	public void setUserLastLogInDate(Date userLastLogInDate) {
		this.userLastLogInDate = userLastLogInDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "User_Last_LogOut_date", length = 23)
	public Date getUserLastLogOutDate() {
		return this.userLastLogOutDate;
	}

	public void setUserLastLogOutDate(Date userLastLogOutDate) {
		this.userLastLogOutDate = userLastLogOutDate;
	}

	@Column(name = "User_Email_Address", length = 75)
	public String getUserEmailAddress() {
		return this.userEmailAddress;
	}

	public void setUserEmailAddress(String userEmailAddress) {
		this.userEmailAddress = userEmailAddress;
	}

}
