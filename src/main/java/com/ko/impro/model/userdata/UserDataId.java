package com.ko.impro.model.userdata;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.ko.impro.model.base.BaseEntity;

/**
 * ImgProUserDataId 
 */
@Embeddable
public class UserDataId implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private String userId;
	private int roleId;
	
	@Column(name = "User_Id", nullable = false)
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "Role_Id", nullable = false)
	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof UserDataId))
			return false;
		UserDataId castOther = (UserDataId) other;

		return ((this.getUserId() == castOther.getUserId()) || (this.getUserId() != null
				&& castOther.getUserId() != null && this.getUserId().equals(castOther.getUserId())))
				&& (this.getRoleId() == castOther.getRoleId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getUserId() == null ? 0 : this.getUserId().hashCode());
		result = 37 * result + this.getRoleId();
		return result;
	}

}
