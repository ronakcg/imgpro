package com.ko.impro.model.auditlog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Audit_Trail_Log 
 */
@Entity
@Table(name = "Audit_Log")
public class AuditLogs {
	private int auditId;
	private String  transactionDate;
	private String  transactionUser;
	private String  tableUpdated;
	private String  recordUpdated;
	private String  transactionType;

	@Id
	@Column(name = "SNO", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.SNO_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence" ) 
	/**
	 * @return the auditId
	 */
	public int getAuditId() {
		return auditId;
	}
	/**
	 * @param auditId the auditId to set
	 */
	public void setAuditId(int auditId) {
		this.auditId = auditId;
	}
	
	@Column(name = "TransactionDate")
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
		
	@Column(name = "TransactionUser")
	public String getTransactionUser() {
		return transactionUser;
	}
	public void setTransactionUser(String transactionUser) {
		this.transactionUser = transactionUser;
	}
	
	@Column(name = "TableUpdated")
	public String getTableUpdated() {
		return tableUpdated;
	}
	public void setTableUpdated(String tableUpdated) {
		this.tableUpdated = tableUpdated;
	}
	
	@Column(name = "RecordUpdated")
	public String getRecordUpdated() {
		return recordUpdated;
	}
	public void setRecordUpdated(String recordUpdated) {
		this.recordUpdated = recordUpdated;
	}
	
	@Column(name = "TransactionType")
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	
	
}
