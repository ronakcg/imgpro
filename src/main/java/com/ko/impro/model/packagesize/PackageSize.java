package com.ko.impro.model.packagesize;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.packages.Packages;

/**
 */
@Entity
@Table(name = "PackageSize")
public class PackageSize implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private short packageSizeCode;
	private String packageSizeDescription;
	private Float packageSizeOz;
	//private byte[] upsizeTs;
	private Set<Packages> packages = new HashSet<Packages>(0);


	@Id
	@Column(name = "PackageSizeCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.PackagesSizeCode_Seq" ,allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
	public short getPackageSizeCode() {
		return this.packageSizeCode;
	}

	public void setPackageSizeCode(short packageSizeCode) {
		this.packageSizeCode = packageSizeCode;
	}

	@Column(name = "PackageSizeDescription")
	public String getPackageSizeDescription() {
		return this.packageSizeDescription;
	}

	public void setPackageSizeDescription(String packageSizeDescription) {
		this.packageSizeDescription = packageSizeDescription;
	}

	@Column(name = "packageSizeOZ", precision = 24, scale = 0)
	public Float getPackageSizeOz() {
		return this.packageSizeOz;
	}

	public void setPackageSizeOz(Float packageSizeOz) {
		this.packageSizeOz = packageSizeOz;
	}

	/*@Column(name = "upsize_ts")
	public byte[] getUpsizeTs() {
		return this.upsizeTs;
	}

	public void setUpsizeTs(byte[] upsizeTs) {
		this.upsizeTs = upsizeTs;
	}*/

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "packageSize")
	public Set<Packages> getPackages() {
		return this.packages;
	}

	public void setPackages(Set<Packages> packages) {
		this.packages = packages;
	}
	
}
