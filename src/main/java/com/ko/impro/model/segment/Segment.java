package com.ko.impro.model.segment;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.subcategory.SubCategory;
import com.ko.impro.model.subsegment.SubSegment;

/**
 * Segment 
 */
@Entity
@Table(name = "Segment")
public class Segment implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private short segmentCode;
	private String segmentDescription;
	private Set<SubCategory> subCategories = new HashSet<SubCategory>(0);
	private Set<SubSegment> subSegments = new HashSet<SubSegment>(0);

	@Id
	@Column(name = "SegmentCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.SegmentCode_Seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence" ) 
	public short getSegmentCode() {
		return this.segmentCode;
	}

	public void setSegmentCode(short segmentCode) {
		this.segmentCode = segmentCode;
	}

	@Column(name = "SegmentDescription", nullable = false)
	public String getSegmentDescription() {
		return this.segmentDescription;
	}

	public void setSegmentDescription(String segmentDescription) {
		this.segmentDescription = segmentDescription;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "segments")
	public Set<SubCategory> getSubCategories() {
		return this.subCategories;
	}

	public void setSubCategories(Set<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ImgPro_Segment_Subsegment",  joinColumns = {
			@JoinColumn(name = "SegmentCode", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "SubSegmentCode", nullable = false, updatable = false) })
	public Set<SubSegment> getSubSegments() {
		return this.subSegments;
	}

	public void setSubSegments(Set<SubSegment> subSegments) {
		this.subSegments = subSegments;
	}

}
