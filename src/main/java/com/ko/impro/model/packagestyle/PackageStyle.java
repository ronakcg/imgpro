package com.ko.impro.model.packagestyle;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.packages.Packages;

/**
 * PackageStyle 
 */
@Entity
@Table(name = "PackageStyle")
public class PackageStyle implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private int styleCode;
	private String styleDesc;
	private Set<Packages> packages;
	
	@Id
	@Column(name = "styleCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.StyleCode_Seq",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
	public int getStyleCode() {
		return styleCode;
	}
	
	public void setStyleCode(int styleCode) {
		this.styleCode = styleCode;
	}
	
	@Column(name = "styleDesc")
	public String getStyleDesc() {
		return styleDesc;
	}
	public void setStyleDesc(String styleDesc) {
		this.styleDesc = styleDesc;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "packageStyle")
	public Set<Packages> getPackages() {
		return this.packages;
	}

	public void setPackages(Set<Packages> packages) {
		this.packages = packages;
	}
	
	
}
