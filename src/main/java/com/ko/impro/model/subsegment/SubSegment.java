package com.ko.impro.model.subsegment;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.segment.Segment;

/**
 * SubSegment 
 */
@Entity
@Table(name = "SubSegment")
public class SubSegment implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private short subSegmentCode;
	private String subSegmentDescription;
	private Set<Segment> segments = new HashSet<Segment>(0);

	@Id
	@Column(name = "SubSegmentCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.SubSegmentCode_Seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence" ) 
	public short getSubSegmentCode() {
		return this.subSegmentCode;
	}

	public void setSubSegmentCode(short subSegmentCode) {
		this.subSegmentCode = subSegmentCode;
	}

	@Column(name = "SubSegmentDescription", nullable = false)
	public String getSubSegmentDescription() {
		return this.subSegmentDescription;
	}

	public void setSubSegmentDescription(String subSegmentDescription) {
		this.subSegmentDescription = subSegmentDescription;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "subSegments")
	public Set<Segment> getSegments() {
		return this.segments;
	}

	public void setSegments(Set<Segment> segments) {
		this.segments = segments;
	}
}
