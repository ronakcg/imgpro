package com.ko.impro.model.deletemessagedata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;

/**
 * DeletemessagesDataDao 
 */
@Entity
@Table(name = "imgpro_Deletemessages_data")
public class DeleteMessagesData implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private Integer deleteMessageId;
	private String deleteMessageText;
	
	@Id
	@Column(name = "Delete_Message_ID", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.DeleteMessageId_Seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence" ) 
	public Integer getDeleteMessageId() {
		return this.deleteMessageId;
	}

	public void setDeleteMessageId(Integer deleteMessageId) {
		this.deleteMessageId = deleteMessageId;
	}

	@Column(name = "Delete_Message_Text")
	public String getDeleteMessageText() {
		return this.deleteMessageText;
	}

	public void setDeleteMessageText(String deleteMessageText) {
		this.deleteMessageText = deleteMessageText;
	}
	
}
