package com.ko.impro.model.gatekeeping;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;

/**
 * GateKeeping Model
 */
@Entity
@Table(name = "Gatekeeping")
public class GateKeeping implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private int productCode;
	private int productId;
	private boolean deleteFlag;
	private String deleteDate;
	private String productName;
	private String shortProductName;
	private int packageCode;
	private String packageName;
	private int packageSizeCode;
	private String packageSizeName;
	private int brandCode;
	private String brandName;
	private String upc;
	private int flavorCode;
	private String flavorName;
	private int categoryCode;
	private String categoryName;
	private String dateMaintained;
	private int subCategoryCode;
	private String subCategoryName;
	private int segmentCode;
	private String segmentName;
	private int subSegmentCode;
	private String subSegmentName;
	private int manufacturerCode;
	private String manufacturerName;
	private String extendedDescId;
	private String upc12;
	private BigDecimal height;
	private BigDecimal width;
	private BigDecimal depth;
	private char jpg;
	private char lowRes1;
	private char lowRes2;
	private char lowRes3;
	private char lowRes7;
	private char lowRes8;
	private char lowRes9;
	private char hiRes1;
	private char hiRes2;
	private char hiRes3;
	private char hiRes7;
	private char hiRes8;
	private char hiRes9;
	private char mock1;
	private char mock2;
	private char mock3;
	private char mock7;
	private char mock8;
	private char mock9;
	private char sMSBData;
	private boolean jDAData;
	private boolean informaticaData;
	private boolean completeFlag;

	/**
	 * @return the productCode
	 */
	@Id
	@Column(name = "ProductCode", unique = true, nullable = false)
	public int getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(int productCode) {
		this.productCode = productCode;
	}
	
	
	@Column(name = "ProductId")
	/**
	 * @param get  the productCode 
	 */
	public int getProductId() {
		return productId;
	}

	/**
	 *  productCode the productCode to set
	 */
	public void setProductId(int productId) {
		this.productId = productId;
	}

	/**
	 * @return the deleteFlag
	 */
	@Column(name = "DeleteFlag")
	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	/**
	 * @param deleteFlag the deleteFlag to set
	 */
	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	/**
	 * @return the deleteDate
	 */
	@Column(name = "DeleteDate")
	public String getDeleteDate() {
		return deleteDate;
	}

	/**
	 * @param deleteDate the deleteDate to set
	 */

	public void setDeleteDate(String deleteDate) {
		this.deleteDate = deleteDate;
	}

	/**
	 * @return the productName
	 */
	@Column(name = "ProductName")
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the shortProductName
	 */
	@Column(name = "ShortProductName")
	public String getShortProductName() {
		return shortProductName;
	}

	/**
	 * @param shortProductName the shortProductName to set
	 */
	public void setShortProductName(String shortProductName) {
		this.shortProductName = shortProductName;
	}

	/**
	 * @return the packageCode
	 */
	@Column(name = "PackageCode")
	public int getPackageCode() {
		return packageCode;
	}

	/**
	 * @param packageCode the packageCode to set
	 */
	public void setPackageCode(int packageCode) {
		this.packageCode = packageCode;
	}

	/**
	 * @return the packageName
	 */
	@Column(name = "PackageName")
	public String getPackageName() {
		return packageName;
	}

	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @return the packageSizeCode
	 */
	@Column(name = "PackageSizeCode")
	public int getPackageSizeCode() {
		return packageSizeCode;
	}

	/**
	 * @param packageSizeCode the packageSizeCode to set
	 */
	public void setPackageSizeCode(int packageSizeCode) {
		this.packageSizeCode = packageSizeCode;
	}

	/**
	 * @return the packageSizeName
	 */
	@Column(name = "PackageSizeName")
	public String getPackageSizeName() {
		return packageSizeName;
	}

	/**
	 * @param packageSizeName the packageSizeName to set
	 */
	public void setPackageSizeName(String packageSizeName) {
		this.packageSizeName = packageSizeName;
	}

	/**
	 * @return the brandCode
	 */
	@Column(name = "BrandCode")
	public int getBrandCode() {
		return brandCode;
	}

	/**
	 * @param brandCode the brandCode to set
	 */
	public void setBrandCode(int brandCode) {
		this.brandCode = brandCode;
	}

	/**
	 * @return the brandName
	 */
	@Column(name = "BrandName")
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the upc
	 */
	@Column(name = "UPC")
	public String getUpc() {
		return upc;
	}

	/**
	 * @param upc the upc to set
	 */
	public void setUpc(String upc) {
		this.upc = upc;
	}

	/**
	 * @return the flavorCode
	 */
	@Column(name = "FlavorCode")
	public int getFlavorCode() {
		return flavorCode;
	}

	/**
	 * @param flavorCode the flavorCode to set
	 */
	public void setFlavorCode(int flavorCode) {
		this.flavorCode = flavorCode;
	}

	/**
	 * @return the flavorName
	 */
	@Column(name = "FlavorName")
	public String getFlavorName() {
		return flavorName;
	}

	/**
	 * @param flavorName the flavorName to set
	 */
	public void setFlavorName(String flavorName) {
		this.flavorName = flavorName;
	}

	/**
	 * @return the categoryCode
	 */
	@Column(name = "CategoryCode")
	public int getCategoryCode() {
		return categoryCode;
	}

	/**
	 * @param categoryCode the categoryCode to set
	 */
	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}

	/**
	 * @return the categoryName
	 */
	@Column(name = "CategoryName")
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the dateMaintained
	 */
	@Column(name = "DateMaintained")
	public String getDateMaintained() {
		return dateMaintained;
	}

	/**
	 * @param dateMaintained the dateMaintained to set
	 */
	public void setDateMaintained(String dateMaintained) {
		this.dateMaintained = dateMaintained;
	}

	/**
	 * @return the subCategoryCode
	 */
	@Column(name = "SubCategoryCode")
	public int getSubCategoryCode() {
		return subCategoryCode;
	}

	/**
	 * @param subCategoryCode the subCategoryCode to set
	 */
	public void setSubCategoryCode(int subCategoryCode) {
		this.subCategoryCode = subCategoryCode;
	}

	/**
	 * @return the subCategoryName
	 */
	@Column(name = "SubCategoryName")
	public String getSubCategoryName() {
		return subCategoryName;
	}

	/**
	 * @param subCategoryName the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	/**
	 * @return the segmentCode
	 */
	@Column(name = "SegmentCode")
	public int getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode the segmentCode to set
	 */
	public void setSegmentCode(int segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the segmentName
	 */
	@Column(name = "SegmentName")
	public String getSegmentName() {
		return segmentName;
	}

	/**
	 * @param segmentName the segmentName to set
	 */
	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	/**
	 * @return the subSegmentCode
	 */
	@Column(name = "SubSegmentCode")
	public int getSubSegmentCode() {
		return subSegmentCode;
	}

	/**
	 * @param subSegmentCode the subSegmentCode to set
	 */
	public void setSubSegmentCode(int subSegmentCode) {
		this.subSegmentCode = subSegmentCode;
	}

	/**
	 * @return the subSegmentName
	 */
	@Column(name = "SubSegmentName")
	public String getSubSegmentName() {
		return subSegmentName;
	}

	/**
	 * @param subSegmentName the subSegmentName to set
	 */
	public void setSubSegmentName(String subSegmentName) {
		this.subSegmentName = subSegmentName;
	}

	/**
	 * @return the manufacturerCode
	 */
	@Column(name = "ManufacturerCode")
	public int getManufacturerCode() {
		return manufacturerCode;
	}

	/**
	 * @param manufacturerCode the manufacturerCode to set
	 */
	public void setManufacturerCode(int manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}

	/**
	 * @return the manufacturerName
	 */
	@Column(name = "ManufacturerName")
	public String getManufacturerName() {
		return manufacturerName;
	}

	/**
	 * @param manufacturerName the manufacturerName to set
	 */
	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	/**
	 * @return the extendedDescId
	 */
	@Column(name = "Extended_Desc_Id")
	public String getExtendedDescId() {
		return extendedDescId;
	}

	/**
	 * @param extendedDescId the extendedDescId to set
	 */
	public void setExtendedDescId(String extendedDescId) {
		this.extendedDescId = extendedDescId;
	}

	/**
	 * @return the upc12
	 */
	@Column(name = "UPC12")
	public String getUpc12() {
		return upc12;
	}

	/**
	 * @param upc12 the upc12 to set
	 */
	public void setUpc12(String upc12) {
		this.upc12 = upc12;
	}

	/**
	 * @return the height
	 */
	@Column(name = "Height")
	public BigDecimal getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(BigDecimal height) {
		this.height = height;
	}

	/**
	 * @return the width
	 */
	@Column(name = "Width")
	public BigDecimal getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(BigDecimal width) {
		this.width = width;
	}

	/**
	 * @return the depth
	 */
	@Column(name = "Depth")
	public BigDecimal getDepth() {
		return depth;
	}

	/**
	 * @param depth the depth to set
	 */
	public void setDepth(BigDecimal depth) {
		this.depth = depth;
	}

	/**
	 * @return the jpg
	 */
	@Column(name = "JPG")
	public char getJpg() {
		return jpg;
	}

	/**
	 * @param jpg the jpg to set
	 */
	public void setJpg(char jpg) {
		this.jpg = jpg;
	}

	/**
	 * @return the lowRes1
	 */
	@Column(name = "LowRes1")
	public char getLowRes1() {
		return lowRes1;
	}

	/**
	 * @param lowRes1 the lowRes1 to set
	 */
	public void setLowRes1(char lowRes1) {
		this.lowRes1 = lowRes1;
	}

	/**
	 * @return the lowRes2
	 */
	@Column(name = "LowRes2")
	public char getLowRes2() {
		return lowRes2;
	}

	/**
	 * @param lowRes2 the lowRes2 to set
	 */
	public void setLowRes2(char lowRes2) {
		this.lowRes2 = lowRes2;
	}

	/**
	 * @return the lowRes3
	 */
	@Column(name = "LowRes3")
	public char getLowRes3() {
		return lowRes3;
	}

	/**
	 * @param lowRes3 the lowRes3 to set
	 */
	public void setLowRes3(char lowRes3) {
		this.lowRes3 = lowRes3;
	}

	/**
	 * @return the lowRes7
	 */
	@Column(name = "LowRes7")
	public char getLowRes7() {
		return lowRes7;
	}

	/**
	 * @param lowRes7 the lowRes7 to set
	 */
	public void setLowRes7(char lowRes7) {
		this.lowRes7 = lowRes7;
	}

	/**
	 * @return the lowRes8
	 */
	@Column(name = "LowRes8")
	public char getLowRes8() {
		return lowRes8;
	}

	/**
	 * @param lowRes8 the lowRes8 to set
	 */
	public void setLowRes8(char lowRes8) {
		this.lowRes8 = lowRes8;
	}

	/**
	 * @return the lowRes9
	 */
	@Column(name = "LowRes9")
	public char getLowRes9() {
		return lowRes9;
	}

	/**
	 * @param lowRes9 the lowRes9 to set
	 */
	public void setLowRes9(char lowRes9) {
		this.lowRes9 = lowRes9;
	}

	/**
	 * @return the hiRes1
	 */
	@Column(name = "HiRes1")
	public char getHiRes1() {
		return hiRes1;
	}

	/**
	 * @param hiRes1 the hiRes1 to set
	 */
	public void setHiRes1(char hiRes1) {
		this.hiRes1 = hiRes1;
	}

	/**
	 * @return the hiRes2
	 */
	@Column(name = "HiRes2")
	public char getHiRes2() {
		return hiRes2;
	}

	/**
	 * @param hiRes2 the hiRes2 to set
	 */
	public void setHiRes2(char hiRes2) {
		this.hiRes2 = hiRes2;
	}

	/**
	 * @return the hiRes3
	 */
	@Column(name = "HiRes3")
	public char getHiRes3() {
		return hiRes3;
	}

	/**
	 * @param hiRes3 the hiRes3 to set
	 */
	public void setHiRes3(char hiRes3) {
		this.hiRes3 = hiRes3;
	}

	/**
	 * @return the hiRes7
	 */
	@Column(name = "HiRes7")
	public char getHiRes7() {
		return hiRes7;
	}

	/**
	 * @param hiRes7 the hiRes7 to set
	 */
	public void setHiRes7(char hiRes7) {
		this.hiRes7 = hiRes7;
	}

	/**
	 * @return the hiRes8
	 */
	@Column(name = "HiRes8")
	public char getHiRes8() {
		return hiRes8;
	}

	/**
	 * @param hiRes8 the hiRes8 to set
	 */
	public void setHiRes8(char hiRes8) {
		this.hiRes8 = hiRes8;
	}

	/**
	 * @return the hiRes9
	 */
	@Column(name = "HiRes9")
	public char getHiRes9() {
		return hiRes9;
	}

	/**
	 * @param hiRes9 the hiRes9 to set
	 */
	public void setHiRes9(char hiRes9) {
		this.hiRes9 = hiRes9;
	}

	/**
	 * @return the mock1
	 */
	@Column(name = "Mock1")
	public char getMock1() {
		return mock1;
	}

	/**
	 * @param mock1 the mock1 to set
	 */
	public void setMock1(char mock1) {
		this.mock1 = mock1;
	}

	/**
	 * @return the mock2
	 */
	@Column(name = "Mock2")
	public char getMock2() {
		return mock2;
	}

	/**
	 * @param mock2 the mock2 to set
	 */
	public void setMock2(char mock2) {
		this.mock2 = mock2;
	}

	/**
	 * @return the mock3
	 */
	@Column(name = "Mock3")
	public char getMock3() {
		return mock3;
	}

	/**
	 * @param mock3 the mock3 to set
	 */
	public void setMock3(char mock3) {
		this.mock3 = mock3;
	}

	/**
	 * @return the mock7
	 */
	@Column(name = "Mock7")
	public char getMock7() {
		return mock7;
	}

	/**
	 * @param mock7 the mock7 to set
	 */
	public void setMock7(char mock7) {
		this.mock7 = mock7;
	}

	/**
	 * @return the mock8
	 */
	@Column(name = "Mock8")
	public char getMock8() {
		return mock8;
	}

	/**
	 * @param mock8 the mock8 to set
	 */
	public void setMock8(char mock8) {
		this.mock8 = mock8;
	}

	/**
	 * @return the mock9
	 */
	@Column(name = "Mock9")
	public char getMock9() {
		return mock9;
	}

	/**
	 * @param mock9 the mock9 to set
	 */
	public void setMock9(char mock9) {
		this.mock9 = mock9;
	}

	/**
	 * @return the sMSBData
	 */
	@Column(name = "SMSBData")
	public char getsMSBData() {
		return sMSBData;
	}

	/**
	 * @param sMSBData the sMSBData to set
	 */
	public void setsMSBData(char sMSBData) {
		this.sMSBData = sMSBData;
	}

	/**
	 * @return the jDAData
	 */
	@Column(name = "JDAData")
	public boolean getjDAData() {
		return jDAData;
	}

	/**
	 * @param jDAData the jDAData to set
	 */
	public void setjDAData(boolean jDAData) {
		this.jDAData = jDAData;
	}

	/**
	 * @return the informaticaData
	 */
	@Column(name = "InformaticaData")
	public boolean getInformaticaData() {
		return informaticaData;
	}

	/**
	 * @param informaticaData the informaticaData to set
	 */
	public void setInformaticaData(boolean informaticaData) {
		this.informaticaData = informaticaData;
	}

	/**
	 * @return the completeFlag
	 */
	@Column(name = "CompleteFlag")
	public boolean getCompleteFlag() {
		return completeFlag;
	}

	/**
	 * @param completeFlag the completeFlag to set
	 */
	public void setCompleteFlag(boolean completeFlag) {
		this.completeFlag = completeFlag;
	}

}
