package com.ko.impro.model.userrole;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.userdata.UserData;

/**
 * ImgProUserRoles 
 */
@Entity
@Table(name = "ImgPro_User_Roles")
public class UserRoles implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private int roleId;
	private String roleDesc;
	private Set<UserData> userDatas = new HashSet<UserData>(0);

	@Id
	@Column(name = "Role_Id", unique = true, nullable = false)
	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	@Column(name = "Role_Desc", length = 60)
	public String getRoleDesc() {
		return this.roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "imgProUserRoles")
	public Set<UserData> getUserDatas() {
		return this.userDatas;
	}

	public void setUserDatas(Set<UserData> userDatas) {
		this.userDatas = userDatas;
	}

}
