package com.ko.impro.model.subcategory;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.category.Category;
import com.ko.impro.model.flavour.Flavors;
import com.ko.impro.model.segment.Segment;

/**
 * SubCategory 
 */
@Entity
@Table(name = "SubCategory")
public class SubCategory implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private short subCategoryCode;
	private String subCategoryDescription; 
	private Set<Segment> segments ;
	private Set<Category> categories ;
	private Set<Flavors> flavors ;

	@Id
	@Column(name = "SubCategoryCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.SubcategoryCode_Seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence" ) 
	public short getSubCategoryCode() {
		return this.subCategoryCode;
	}

	public void setSubCategoryCode(short subCategoryCode) {
		this.subCategoryCode = subCategoryCode;
	}

	@Column(name = "SubCategoryDescription", nullable = false)
	public String getSubCategoryDescription() {
		return this.subCategoryDescription;
	}

	public void setSubCategoryDescription(String subCategoryDescription) {
		this.subCategoryDescription = subCategoryDescription;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ImgPro_Segment_SubCategory",  joinColumns = {
			@JoinColumn(name = "SubCategoryCode", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "SegmentCode", nullable = false, updatable = false) })
	public Set<Segment> getSegments() {
		return this.segments;
	}

	public void setSegments(Set<Segment> segments) {
		this.segments = segments;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "subCategories")
	public Set<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "subCategories")
	public Set<Flavors> getFlavors() {
		return this.flavors;
	}

	public void setFlavors(Set<Flavors> flavors) {
		this.flavors = flavors;
	}
}
