package com.ko.impro.model.flavour;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.subcategory.SubCategory;

/**
 * Flavors 
 */
@Entity
@Table(name = "Flavors")
public class Flavors implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private short flavorCode;
	private String flavorDescription;
	private Set<SubCategory> subCategories;
	
	@Id
	@Column(name = "FlavorCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.FlavorCode_Seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence" ) 
	public short getFlavorCode() {
		return this.flavorCode;
	}

	public void setFlavorCode(short flavorCode) {
		this.flavorCode = flavorCode;
	}

	@Column(name = "FlavorDescription")
	public String getFlavorDescription() {
		return this.flavorDescription;
	}

	public void setFlavorDescription(String flavorDescription) {
		this.flavorDescription = flavorDescription;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ImgPro_Flavor_SubCategory",  joinColumns = {
			@JoinColumn(name = "FlavorCode", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "SubCategoryCode", nullable = false, updatable = false) })
	public Set<SubCategory> getSubCategories() {
		return this.subCategories;
	}

	public void setSubCategories(Set<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}
	
}
