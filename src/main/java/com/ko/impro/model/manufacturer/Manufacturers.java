package com.ko.impro.model.manufacturer;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.brand.Brands;


@Entity
@Table(name = "Manufacturers")
public class Manufacturers implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private int manufacturerCode;
	private String manufacturerName;
	/*private Integer manufacturerColor;*/
	private Short payDelay;
	private Short manufacturerViewingOrder;
	private Integer distributorCode;
	private Short supplierFlag;
	private Set<Brands> brands = new HashSet<Brands>(0);

	
	@Id
	@Column(name = "ManufacturerCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.ManufacturerCode_Seq" ,allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
	public int getManufacturerCode() {
		return this.manufacturerCode;
	}

	public void setManufacturerCode(int manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}

	@Column(name = "ManufacturerName", nullable = false)
	public String getManufacturerName() {
		return this.manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	/*@Column(name = "ManufacturerColor")
	public Integer getManufacturerColor() {
		return this.manufacturerColor;
	}

	public void setManufacturerColor(Integer manufacturerColor) {
		this.manufacturerColor = manufacturerColor;
	}*/

	@Column(name = "PayDelay")
	public Short getPayDelay() {
		return this.payDelay;
	}

	public void setPayDelay(Short payDelay) {
		this.payDelay = payDelay;
	}

	@Column(name = "ManufacturerViewingOrder")
	public Short getManufacturerViewingOrder() {
		return this.manufacturerViewingOrder;
	}

	public void setManufacturerViewingOrder(Short manufacturerViewingOrder) {
		this.manufacturerViewingOrder = manufacturerViewingOrder;
	}

	@Column(name = "DistributorCode")
	public Integer getDistributorCode() {
		return this.distributorCode;
	}

	public void setDistributorCode(Integer distributorCode) {
		this.distributorCode = distributorCode;
	}

	@Column(name = "Supplier_Flag")
	public Short getSupplierFlag() {
		return this.supplierFlag;
	}

	public void setSupplierFlag(Short supplierFlag) {
		this.supplierFlag = supplierFlag;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "manufacturers")
	public Set<Brands> getBrands() {
		return this.brands;
	}

	public void setBrands(Set<Brands> brands) {
		this.brands = brands;
	}
		
	
}
