package com.ko.impro.model.base;

import java.io.Serializable;


/**
 * @author hlekshmy
 * Parent Entity Class
 */
public interface BaseEntity extends Serializable {
	
}
