package com.ko.impro.model.category;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.subcategory.SubCategory;

/**
 * Category 
 */
@Entity
@Table(name = "Category")
public class Category implements BaseEntity {
	private static final long serialVersionUID = 1L;
	private short categoryCode;
	private String categoryDescription;
	private Set<SubCategory> subCategories;
	
	@Id
	@Column(name = "CategoryCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.CategoryCode_Seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence" ) 
	public short getCategoryCode() {
		return this.categoryCode;
	}

	public void setCategoryCode(short categoryCode) {
		this.categoryCode = categoryCode;
	}

	@Column(name = "CategoryDescription", nullable = false)
	public String getCategoryDescription() {
		return this.categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ImgPro_Category_SubCategory",  joinColumns = {
	@JoinColumn(name = "CategoryCode", nullable = false, updatable = false) }, inverseJoinColumns = {
	@JoinColumn(name = "SubCategoryCode", nullable = false, updatable = false) })
	public Set<SubCategory> getSubCategories() {
		return this.subCategories;
	}

	public void setSubCategories(Set<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}
}
