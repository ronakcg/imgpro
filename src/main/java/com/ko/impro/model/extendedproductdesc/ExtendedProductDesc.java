package com.ko.impro.model.extendedproductdesc;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.brand.Brands;

/**
 * ImgProExtendedProductDesc 
 */
@Entity
@Table(name = "ImgPro_Extended_Product_Desc")
public class ExtendedProductDesc implements BaseEntity {
	private static final long serialVersionUID = 1L;
	private int extendedDescId;
	private String longProductDesc;
	private String shortProductDesc;
	private Set<Brands> brands ;
	
	@Id
	@Column(name = "Extended_Desc_id", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.Extended_Desc_id_Seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence" ) 
	public int getExtendedDescId() {
		return this.extendedDescId;
	}

	public void setExtendedDescId(int extendedDescId) {
		this.extendedDescId = extendedDescId;
	}

	@Column(name = "Long_Product_Desc", nullable = false, length = 60)
	public String getLongProductDesc() {
		return this.longProductDesc;
	}

	public void setLongProductDesc(String longProductDesc) {
		this.longProductDesc = longProductDesc;
	}

	@Column(name = "Short_Product_Desc", nullable = false, length = 60)
	public String getShortProductDesc() {
		return this.shortProductDesc;
	}

	public void setShortProductDesc(String shortProductDesc) {
		this.shortProductDesc = shortProductDesc;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ImgPro_Brand_Extended_Desc",  joinColumns = {
	@JoinColumn(name = "Extended_Desc_id", nullable = false, updatable = false) }, inverseJoinColumns = {
	@JoinColumn(name = "BrandCode", nullable = false, updatable = false) })
	public Set<Brands> getBrands() {
		return this.brands;
	}

	public void setBrands(Set<Brands> brands) {
		this.brands = brands;
	}
	
}
