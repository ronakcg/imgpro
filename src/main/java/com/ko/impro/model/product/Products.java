package com.ko.impro.model.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;

/**
 * @author hlekshmy
 * Entity class for Products
 */
@Entity
@Table(name = "products")
public class Products implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private byte archiveFlag;

	private String productName;
	private String shortProductName;
	private int packageCode;
	private int brandCode;
	private String upc;

	private int flavorCode;
	private int categoryCode;
	private String dateMaintained;
	private int subCategoryCode;
	private int segmentCode;
	private int subSegmentCode;

	private int manufacturerCode;

	private String nielsenUPC;
	private String nielsenDesc;

	private int extendedDescId;

	private String consumptionType;
	private String upc12;
	private String dateAdded;

	private int productCode;

	private String archiveDate;
	private String bitmapName;
	private int archiveReason;
	
	private  boolean jpg;
	private boolean lowRes1;
	private boolean lowRes2;
	private boolean lowRes3;
	private boolean lowRes7;
	private boolean lowRes8;
	private boolean lowRes9;
	
	private boolean hiRes1;
	private boolean hiRes2;
	private boolean hiRes3;
	private boolean hiRes7;
	private boolean hiRes8;
	private boolean hiRes9;
	
	private boolean mock1;
	private boolean mock2;
	private boolean mock3;
	private boolean mock7;
	private boolean mock8;
	private boolean mock9;
	
	private String acnCategory;
	private String acnSubCategory;
	private String acnSegment;
	private String acnSubSegment;
	private String acnBeverageType;
	private String acnKeyManufacturer;
	private String acnBrand;
	private String acnKeyFlavor;
	private String acnLongName;
	private String acnConsumptionType;

	@Id
	@Column(name = "ProductCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.ProductCode_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence" ) 
	public int getProductCode() {
		return productCode;
	}

	public void setProductCode(int productCode) {
		this.productCode = productCode;
	}

	@Column(name="ProductId")
	public String getBitmapName() {
		return bitmapName;
	}

	public void setBitmapName(String bitmapName) {
		this.bitmapName = bitmapName;
	}
	
	@Column(name="JPG")
	public boolean isJpg() {
		return jpg;
	}

	public void setJpg(boolean jpg) {
		this.jpg = jpg;
	}

	@Column(name="LowRes1")
	public boolean isLowRes1() {
		return lowRes1;
	}

	public void setLowRes1(boolean lowRes1) {
		this.lowRes1 = lowRes1;
	}

	@Column(name="LowRes2")
	public boolean isLowRes2() {
		return lowRes2;
	}

	public void setLowRes2(boolean lowRes2) {
		this.lowRes2 = lowRes2;
	}

	@Column(name="LowRes3")
	public boolean isLowRes3() {
		return lowRes3;
	}

	public void setLowRes3(boolean lowRes3) {
		this.lowRes3 = lowRes3;
	}

	@Column(name="LowRes7")
	public boolean isLowRes7() {
		return lowRes7;
	}

	public void setLowRes7(boolean lowRes7) {
		this.lowRes7 = lowRes7;
	}

	@Column(name="LowRes8")
	public boolean isLowRes8() {
		return lowRes8;
	}

	public void setLowRes8(boolean lowRes8) {
		this.lowRes8 = lowRes8;
	}

	@Column(name="LowRes9")
	public boolean isLowRes9() {
		return lowRes9;
	}

	public void setLowRes9(boolean lowRes9) {
		this.lowRes9 = lowRes9;
	}

	@Column(name="HiRes1")
	public boolean isHiRes1() {
		return hiRes1;
	}

	public void setHiRes1(boolean hiRes1) {
		this.hiRes1 = hiRes1;
	}

	@Column(name="HiRes2")
	public boolean isHiRes2() {
		return hiRes2;
	}

	public void setHiRes2(boolean hiRes2) {
		this.hiRes2 = hiRes2;
	}

	@Column(name="HiRes3")
	public boolean isHiRes3() {
		return hiRes3;
	}

	public void setHiRes3(boolean hiRes3) {
		this.hiRes3 = hiRes3;
	}

	@Column(name="HiRes7")
	public boolean isHiRes7() {
		return hiRes7;
	}

	public void setHiRes7(boolean hiRes7) {
		this.hiRes7 = hiRes7;
	}

	@Column(name="HiRes8")
	public boolean isHiRes8() {
		return hiRes8;
	}

	public void setHiRes8(boolean hiRes8) {
		this.hiRes8 = hiRes8;
	}

	@Column(name="HiRes9")
	public boolean isHiRes9() {
		return hiRes9;
	}

	public void setHiRes9(boolean hiRes9) {
		this.hiRes9 = hiRes9;
	}

	@Column(name="Mock1")
	public boolean isMock1() {
		return mock1;
	}

	public void setMock1(boolean mock1) {
		this.mock1 = mock1;
	}

	@Column(name="Mock2")
	public boolean isMock2() {
		return mock2;
	}

	public void setMock2(boolean mock2) {
		this.mock2 = mock2;
	}

	@Column(name="Mock3")
	public boolean isMock3() {
		return mock3;
	}

	public void setMock3(boolean mock3) {
		this.mock3 = mock3;
	}

	@Column(name="Mock7")
	public boolean isMock7() {
		return mock7;
	}

	public void setMock7(boolean mock7) {
		this.mock7 = mock7;
	}

	@Column(name="Mock8")
	public boolean isMock8() {
		return mock8;
	}

	public void setMock8(boolean mock8) {
		this.mock8 = mock8;
	}

	@Column(name="Mock9")
	public boolean isMock9() {
		return mock9;
	}

	public void setMock9(boolean mock9) {
		this.mock9 = mock9;
	}

	@Column(name = "ArchiveFlag")
	public byte getArchiveFlag() {
		return archiveFlag;
	}


	@Column(name = "ProductName")
	public String getProductName() {
		return productName;
	}

	@Column(name = "ShortProductName")
	public String getShortProductName() {
		return shortProductName;
	}

	@Column(name = "PackageCode")
	public int getPackageCode() {
		return packageCode;
	}

	@Column(name = "BrandCode")
	public int getBrandCode() {
		return brandCode;
	}

	@Column(name = "UPC")
	public String getUpc() {
		return upc;
	}

	
	@Column(name = "FlavorCode")
	public int getFlavorCode() {
		return flavorCode;
	}

	@Column(name = "CategoryCode")
	public int getCategoryCode() {
		return categoryCode;
	}

	@Column(name = "DateMaintained")
	public String getDateMaintained() {
		return dateMaintained;
	}


	@Column(name = "ManufacturerCode")
	public int getManufacturerCode() {
		return manufacturerCode;
	}


	@Column(name = "NielsenUPC")
	public String getNielsenUPC() {
		return nielsenUPC;
	}

	@Column(name = "NielsenDesc")
	public String getNielsenDesc() {
		return nielsenDesc;
	}



	

	@Column(name = "UPC12")
	public String getUpc12() {
		return upc12;
	}

	public void setArchiveFlag(byte archiveFlag) {
		this.archiveFlag = archiveFlag;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setShortProductName(String shortProductName) {
		this.shortProductName = shortProductName;
	}

	public void setPackageCode(int packageCode) {
		this.packageCode = packageCode;
	}

	public void setBrandCode(int brandCode) {
		this.brandCode = brandCode;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public void setFlavorCode(int flavorCode) {
		this.flavorCode = flavorCode;
	}

	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}

	public void setDateMaintained(String dateMaintained) {
		this.dateMaintained = dateMaintained;
	}


	public void setManufacturerCode(int manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}

	public void setNielsenUPC(String nielsenUPC) {
		this.nielsenUPC = nielsenUPC;
	}

	public void setNielsenDesc(String nielsenDesc) {
		this.nielsenDesc = nielsenDesc;
	}


	public void setUpc12(String upc12) {
		this.upc12 = upc12;
	}

	@Column(name = "SubCategoryCode")
	public int getSubCategoryCode() {
		return subCategoryCode;
	}

	public void setSubCategoryCode(int subCategoryCode) {
		this.subCategoryCode = subCategoryCode;
	}

	@Column(name = "SegmentCode")
	public int getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(int segmentCode) {
		this.segmentCode = segmentCode;
	}

	@Column(name = "SubSegmentCode")
	public int getSubSegmentCode() {
		return subSegmentCode;
	}

	public void setSubSegmentCode(int subSegmentCode) {
		this.subSegmentCode = subSegmentCode;
	}
	
	
	@Column(name = "DateAdded")
	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}
	
	@Column(name="ArchiveDate")
	public String getArchiveDate() {
		return archiveDate;
	}

	public void setArchiveDate(String archiveDate) {
		this.archiveDate = archiveDate;
	}
	
	@Column(name="ArchiveReason")
	public int getArchiveReason() {
		return archiveReason;
	}

	public void setArchiveReason(int archiveReason) {
		this.archiveReason = archiveReason;
	}

	@Column(name="ACNCategory")
	public String getAcnCategory() {
		return acnCategory;
	}

	public void setAcnCategory(String acnCategory) {
		this.acnCategory = acnCategory;
	}

	@Column(name="ACNSubCategory")
	public String getAcnSubCategory() {
		return acnSubCategory;
	}

	public void setAcnSubCategory(String acnSubCategory) {
		this.acnSubCategory = acnSubCategory;
	}

	@Column(name="ACNSegment")
	public String getAcnSegment() {
		return acnSegment;
	}

	public void setAcnSegment(String acnSegment) {
		this.acnSegment = acnSegment;
	}

	@Column(name="ACNSubSegment")
	public String getAcnSubSegment() {
		return acnSubSegment;
	}

	public void setAcnSubSegment(String acnSubSegment) {
		this.acnSubSegment = acnSubSegment;
	}

	@Column(name="ACNBeverageType")
	public String getAcnBeverageType() {
		return acnBeverageType;
	}

	public void setAcnBeverageType(String acnBeverageType) {
		this.acnBeverageType = acnBeverageType;
	}

	@Column(name="ACNKeyManufacturer")
	public String getAcnKeyManufacturer() {
		return acnKeyManufacturer;
	}

	public void setAcnKeyManufacturer(String acnKeyManufacturer) {
		this.acnKeyManufacturer = acnKeyManufacturer;
	}

	@Column(name="ACNBrand")
	public String getAcnBrand() {
		return acnBrand;
	}

	public void setAcnBrand(String acnBrand) {
		this.acnBrand = acnBrand;
	}

	@Column(name="ACNKeyFlavor")
	public String getAcnKeyFlavor() {
		return acnKeyFlavor;
	}

	public void setAcnKeyFlavor(String acnKeyFlavor) {
		this.acnKeyFlavor = acnKeyFlavor;
	}

	@Column(name="ACNLongName")
	public String getAcnLongName() {
		return acnLongName;
	}

	public void setAcnLongName(String acnLongName) {
		this.acnLongName = acnLongName;
	}
	
	@Column(name="ACNConsumptionType")
	public String getAcnConsumptionType() {
		return acnConsumptionType;
	}

	public void setAcnConsumptionType(String acnConsumptionType) {
		this.acnConsumptionType = acnConsumptionType;
	}

	@Column(name = "Extended_Desc_id")
	public int getExtendedDescId() {
		return extendedDescId;
	}

	public void setExtendedDescId(int extendedDescId) {
		this.extendedDescId = extendedDescId;
	}

	@Column(name = "Consumption_Type")
	public String getConsumptionType() {
		return consumptionType;
	}

	public void setConsumptionType(String consumptionType) {
		this.consumptionType = consumptionType;
	}




}



