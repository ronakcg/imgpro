package com.ko.impro.model.containertype;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.packages.Packages;

/**
 * ContainerType 
 */
@Entity
@Table(name = "ContainerType")
public class ContainerType implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private short containerTypeCode;
	private String containerTypeDescription;
	private Set<Packages> packages;

	@Id
	@Column(name = "ContainerTypeCode", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.ContainerTypeCode_Seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence" )
	public short getContainerTypeCode() {
		return this.containerTypeCode;
	}

	public void setContainerTypeCode(short containerTypeCode) {
		this.containerTypeCode = containerTypeCode;
	}

	@Column(name = "ContainerTypeDescription", nullable = false)
	public String getContainerTypeDescription() {
		return this.containerTypeDescription;
	}

	public void setContainerTypeDescription(String containerTypeDescription) {
		this.containerTypeDescription = containerTypeDescription;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "containerType")
	public Set<Packages> getPackages() {
		return this.packages;
	}

	public void setPackages(Set<Packages> packages) {
		this.packages = packages;
	}

}
