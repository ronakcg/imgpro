package com.ko.impro.model.shape;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ko.impro.model.base.BaseEntity;
import com.ko.impro.model.packages.Packages;

/**
 * Shapes 
 */
@Entity
@Table(name = "Shapes")
public class Shapes implements BaseEntity {

	private static final long serialVersionUID = 1L;
	private int shapeId;
	private String shapeCode;
	private Set<Packages> packages;

	@Id
	@Column(name = "ShapeId", unique = true, nullable = false)
	@SequenceGenerator(name="sequence", sequenceName="dbo.SHAPEID_Seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence")
	public int getShapeId() {
		return this.shapeId;
	}

	public void setShapeId(int shapeId) {
		this.shapeId = shapeId;
	}

	@Column(name = "ShapeCode")
	public String getShapeCode() {
		return shapeCode;
	}

	public void setShapeCode(String shapeCode) {
		this.shapeCode = shapeCode;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shapes")
	public Set<Packages> getPackages() {
		return this.packages;
	}

	public void setPackages(Set<Packages> packages) {
		this.packages = packages;
	}
	
	
	
}
