package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class ExtendedProductDescDto  extends MaintenanceDto {

	private String extendedDescId;
	private String longProductDesc;
	private String shortProductDesc;
	private BrandDto brandDto;
	/**
	 * @return the extendedDescId
	 */
	public String getExtendedDescId() {
		return extendedDescId;
	}
	/**
	 * @param extendedDescId the extendedDescId to set
	 */
	public void setExtendedDescId(String extendedDescId) {
		this.extendedDescId = extendedDescId;
	}
	/**
	 * @return the longProductDesc
	 */
	public String getLongProductDesc() {
		return longProductDesc;
	}
	/**
	 * @param longProductDesc the longProductDesc to set
	 */
	public void setLongProductDesc(String longProductDesc) {
		this.longProductDesc = longProductDesc;
	}
	/**
	 * @return the shortProductDesc
	 */
	public String getShortProductDesc() {
		return shortProductDesc;
	}
	/**
	 * @param shortProductDesc the shortProductDesc to set
	 */
	public void setShortProductDesc(String shortProductDesc) {
		this.shortProductDesc = shortProductDesc;
	}
	/**
	 * @return the brandDto
	 */
	public BrandDto getBrandDto() {
		return brandDto;
	}
	/**
	 * @param brandDto the brandDto to set
	 */
	public void setBrandDto(BrandDto brandDto) {
		this.brandDto = brandDto;
	}
	
}
