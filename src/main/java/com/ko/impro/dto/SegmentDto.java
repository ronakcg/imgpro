package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class SegmentDto extends MaintenanceDto {
	
	private String segmentCode;
	private String segmentDescription;
	private boolean isYesButton;
	private SubCategoryDto subCategoryDto;
	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}
	/**
	 * @param segmentCode the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
	/**
	 * @return the segmentDescription
	 */
	public String getSegmentDescription() {
		return segmentDescription;
	}
	/**
	 * @param segmentDescription the segmentDescription to set
	 */
	public void setSegmentDescription(String segmentDescription) {
		this.segmentDescription = segmentDescription;
	}
	/**
	 * @return the isYesButton
	 */
	public boolean isYesButton() {
		return isYesButton;
	}
	/**
	 * @param isYesButton the isYesButton to set
	 */
	public void setYesButton(boolean isYesButton) {
		this.isYesButton = isYesButton;
	}
	/**
	 * @return the subCategoryDto
	 */
	public SubCategoryDto getSubCategoryDto() {
		return subCategoryDto;
	}
	/**
	 * @param subCategoryDto the subCategoryDto to set
	 */
	public void setSubCategoryDto(SubCategoryDto subCategoryDto) {
		this.subCategoryDto = subCategoryDto;
	}
}
