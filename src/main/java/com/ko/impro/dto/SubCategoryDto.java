package com.ko.impro.dto;

import java.util.List;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layersDTO class have setters and getters for Data transfer between various layers
 */
public class SubCategoryDto  extends MaintenanceDto {

	private String subCategoryCode;
	private String subCategoryDescription;

	private List<CategoryDto> categoryDtolist;
	private List<CategoryDto> addCategoryDtolist;
	private List<CategoryDto> removeCategoryDtoList;
	/**
	 * @return the subCategoryCode
	 */
	public String getSubCategoryCode() {
		return subCategoryCode;
	}
	/**
	 * @param subCategoryCode the subCategoryCode to set
	 */
	public void setSubCategoryCode(String subCategoryCode) {
		this.subCategoryCode = subCategoryCode;
	}
	/**
	 * @return the subCategoryDescription
	 */
	public String getSubCategoryDescription() {
		return subCategoryDescription;
	}
	/**
	 * @param subCategoryDescription the subCategoryDescription to set
	 */
	public void setSubCategoryDescription(String subCategoryDescription) {
		this.subCategoryDescription = subCategoryDescription;
	}
	/**
	 * @return the categoryDtolist
	 */
	public List<CategoryDto> getCategoryDtolist() {
		return categoryDtolist;
	}
	/**
	 * @param categoryDtolist the categoryDtolist to set
	 */
	public void setCategoryDtolist(List<CategoryDto> categoryDtolist) {
		this.categoryDtolist = categoryDtolist;
	}
	/**
	 * @return the addCategoryDtolist
	 */
	public List<CategoryDto> getAddCategoryDtolist() {
		return addCategoryDtolist;
	}
	/**
	 * @param addCategoryDtolist the addCategoryDtolist to set
	 */
	public void setAddCategoryDtolist(List<CategoryDto> addCategoryDtolist) {
		this.addCategoryDtolist = addCategoryDtolist;
	}
	/**
	 * @return the removeCategoryDtoList
	 */
	public List<CategoryDto> getRemoveCategoryDtoList() {
		return removeCategoryDtoList;
	}
	/**
	 * @param removeCategoryDtoList the removeCategoryDtoList to set
	 */
	public void setRemoveCategoryDtoList(List<CategoryDto> removeCategoryDtoList) {
		this.removeCategoryDtoList = removeCategoryDtoList;
	}

}
