package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class ManufacturerDto  extends MaintenanceDto {

	private String manufacturerCode;
	private String manufacturerName;
	private String manufacturerColor;
	private String payDelay;
	private String manufacturerViewingOrder;
	private String distributorCode;
	private String supplierFlag;
	/**
	 * @return the manufacturerCode
	 */
	public String getManufacturerCode() {
		return manufacturerCode;
	}
	/**
	 * @param manufacturerCode the manufacturerCode to set
	 */
	public void setManufacturerCode(String manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}
	/**
	 * @return the manufacturerName
	 */
	public String getManufacturerName() {
		return manufacturerName;
	}
	/**
	 * @param manufacturerName the manufacturerName to set
	 */
	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}
	/**
	 * @return the manufacturerColor
	 */
	public String getManufacturerColor() {
		return manufacturerColor;
	}
	/**
	 * @param manufacturerColor the manufacturerColor to set
	 */
	public void setManufacturerColor(String manufacturerColor) {
		this.manufacturerColor = manufacturerColor;
	}
	/**
	 * @return the payDelay
	 */
	public String getPayDelay() {
		return payDelay;
	}
	/**
	 * @param payDelay the payDelay to set
	 */
	public void setPayDelay(String payDelay) {
		this.payDelay = payDelay;
	}
	/**
	 * @return the manufacturerViewingOrder
	 */
	public String getManufacturerViewingOrder() {
		return manufacturerViewingOrder;
	}
	/**
	 * @param manufacturerViewingOrder the manufacturerViewingOrder to set
	 */
	public void setManufacturerViewingOrder(String manufacturerViewingOrder) {
		this.manufacturerViewingOrder = manufacturerViewingOrder;
	}
	/**
	 * @return the distributorCode
	 */
	public String getDistributorCode() {
		return distributorCode;
	}
	/**
	 * @param distributorCode the distributorCode to set
	 */
	public void setDistributorCode(String distributorCode) {
		this.distributorCode = distributorCode;
	}
	/**
	 * @return the supplierFlag
	 */
	public String getSupplierFlag() {
		return supplierFlag;
	}
	/**
	 * @param supplierFlag the supplierFlag to set
	 */
	public void setSupplierFlag(String supplierFlag) {
		this.supplierFlag = supplierFlag;
	}
}
