package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class MissingImagesDto {
	private boolean isSelectAll;
	
	private boolean isjpg;
	private boolean islowRes1;
	private boolean islowRes2;
	private boolean islowRes3;
	private boolean islowRes7;
	private boolean islowRes8;
	private boolean islowRes9;
	
	private boolean ishiRes1;
	private boolean ishiRes2;
	private boolean ishiRes3;
	private boolean ishiRes7;
	private boolean ishiRes8;
	private boolean ishiRes9;
	
	private boolean ismock1;
	private boolean ismock2;
	private boolean ismock3;
	private boolean ismock7;
	private boolean ismock8;
	private boolean ismock9;
	
	private String jpg;
	private String lowRes1;
	private String lowRes2;
	private String lowRes3;
	private String lowRes7;
	private String lowRes8;
	private String lowRes9;
	
	private String hiRes1;
	private String hiRes2;
	private String hiRes3;
	private String hiRes7;
	private String hiRes8;
	private String hiRes9;
	
	private String mock1;
	private String mock2;
	private String mock3;
	private String mock7;
	private String mock8;
	private String mock9;
	
	
	private String jpgUrl;
	private String lowRes1Url;
	private String lowRes2Url;
	private String lowRes3Url;
	private String lowRes7Url;
	private String lowRes8Url;
	private String lowRes9Url;
	
	private String hiRes1Url;
	private String hiRes2Url;
	private String hiRes3Url;
	private String hiRes7Url;
	private String hiRes8Url;
	private String hiRes9Url;
	
	private String mock1Url;
	private String mock2Url;
	private String mock3Url;
	private String mock7Url;
	private String mock8Url;
	private String mock9Url;
	
	/**
	 * @return the isSelectAll
	 */
	public boolean isSelectAll() {
		return isSelectAll;
	}
	/**
	 * @param isSelectAll the isSelectAll to set
	 */
	public void setSelectAll(boolean isSelectAll) {
		this.isSelectAll = isSelectAll;
	}
	/**
	 * @return the isjpg
	 */
	public boolean isIsjpg() {
		return isjpg;
	}
	/**
	 * @param isjpg the isjpg to set
	 */
	public void setIsjpg(boolean isjpg) {
		this.isjpg = isjpg;
	}
	/**
	 * @return the islowRes1
	 */
	public boolean isIslowRes1() {
		return islowRes1;
	}
	/**
	 * @param islowRes1 the islowRes1 to set
	 */
	public void setIslowRes1(boolean islowRes1) {
		this.islowRes1 = islowRes1;
	}
	/**
	 * @return the islowRes2
	 */
	public boolean isIslowRes2() {
		return islowRes2;
	}
	/**
	 * @param islowRes2 the islowRes2 to set
	 */
	public void setIslowRes2(boolean islowRes2) {
		this.islowRes2 = islowRes2;
	}
	/**
	 * @return the islowRes3
	 */
	public boolean isIslowRes3() {
		return islowRes3;
	}
	/**
	 * @param islowRes3 the islowRes3 to set
	 */
	public void setIslowRes3(boolean islowRes3) {
		this.islowRes3 = islowRes3;
	}
	/**
	 * @return the islowRes7
	 */
	public boolean isIslowRes7() {
		return islowRes7;
	}
	/**
	 * @param islowRes7 the islowRes7 to set
	 */
	public void setIslowRes7(boolean islowRes7) {
		this.islowRes7 = islowRes7;
	}
	/**
	 * @return the islowRes8
	 */
	public boolean isIslowRes8() {
		return islowRes8;
	}
	/**
	 * @param islowRes8 the islowRes8 to set
	 */
	public void setIslowRes8(boolean islowRes8) {
		this.islowRes8 = islowRes8;
	}
	/**
	 * @return the islowRes9
	 */
	public boolean isIslowRes9() {
		return islowRes9;
	}
	/**
	 * @param islowRes9 the islowRes9 to set
	 */
	public void setIslowRes9(boolean islowRes9) {
		this.islowRes9 = islowRes9;
	}
	/**
	 * @return the ishiRes1
	 */
	public boolean isIshiRes1() {
		return ishiRes1;
	}
	/**
	 * @param ishiRes1 the ishiRes1 to set
	 */
	public void setIshiRes1(boolean ishiRes1) {
		this.ishiRes1 = ishiRes1;
	}
	/**
	 * @return the ishiRes2
	 */
	public boolean isIshiRes2() {
		return ishiRes2;
	}
	/**
	 * @param ishiRes2 the ishiRes2 to set
	 */
	public void setIshiRes2(boolean ishiRes2) {
		this.ishiRes2 = ishiRes2;
	}
	/**
	 * @return the ishiRes3
	 */
	public boolean isIshiRes3() {
		return ishiRes3;
	}
	/**
	 * @param ishiRes3 the ishiRes3 to set
	 */
	public void setIshiRes3(boolean ishiRes3) {
		this.ishiRes3 = ishiRes3;
	}
	/**
	 * @return the ishiRes7
	 */
	public boolean isIshiRes7() {
		return ishiRes7;
	}
	/**
	 * @param ishiRes7 the ishiRes7 to set
	 */
	public void setIshiRes7(boolean ishiRes7) {
		this.ishiRes7 = ishiRes7;
	}
	/**
	 * @return the ishiRes8
	 */
	public boolean isIshiRes8() {
		return ishiRes8;
	}
	/**
	 * @param ishiRes8 the ishiRes8 to set
	 */
	public void setIshiRes8(boolean ishiRes8) {
		this.ishiRes8 = ishiRes8;
	}
	/**
	 * @return the ishiRes9
	 */
	public boolean isIshiRes9() {
		return ishiRes9;
	}
	/**
	 * @param ishiRes9 the ishiRes9 to set
	 */
	public void setIshiRes9(boolean ishiRes9) {
		this.ishiRes9 = ishiRes9;
	}
	/**
	 * @return the ismock1
	 */
	public boolean isIsmock1() {
		return ismock1;
	}
	/**
	 * @param ismock1 the ismock1 to set
	 */
	public void setIsmock1(boolean ismock1) {
		this.ismock1 = ismock1;
	}
	/**
	 * @return the ismock2
	 */
	public boolean isIsmock2() {
		return ismock2;
	}
	/**
	 * @param ismock2 the ismock2 to set
	 */
	public void setIsmock2(boolean ismock2) {
		this.ismock2 = ismock2;
	}
	/**
	 * @return the ismock3
	 */
	public boolean isIsmock3() {
		return ismock3;
	}
	/**
	 * @param ismock3 the ismock3 to set
	 */
	public void setIsmock3(boolean ismock3) {
		this.ismock3 = ismock3;
	}
	/**
	 * @return the ismock7
	 */
	public boolean isIsmock7() {
		return ismock7;
	}
	/**
	 * @param ismock7 the ismock7 to set
	 */
	public void setIsmock7(boolean ismock7) {
		this.ismock7 = ismock7;
	}
	/**
	 * @return the ismock8
	 */
	public boolean isIsmock8() {
		return ismock8;
	}
	/**
	 * @param ismock8 the ismock8 to set
	 */
	public void setIsmock8(boolean ismock8) {
		this.ismock8 = ismock8;
	}
	/**
	 * @return the ismock9
	 */
	public boolean isIsmock9() {
		return ismock9;
	}
	/**
	 * @param ismock9 the ismock9 to set
	 */
	public void setIsmock9(boolean ismock9) {
		this.ismock9 = ismock9;
	}
	/**
	 * @return the jpg
	 */
	public String getJpgUrl() {
		return jpgUrl;
	}
	/**
	 * @param jpg the jpg to set
	 */
	public void setJpgUrl(String jpgUrl) {
		this.jpgUrl = jpgUrl;
	}
	/**
	 * @return the lowRes1
	 */
	public String getLowRes1() {
		return lowRes1;
	}
	/**
	 * @param lowRes1 the lowRes1 to set
	 */
	public void setLowRes1(String lowRes1) {
		this.lowRes1 = lowRes1;
	}
	/**
	 * @return the lowRes2
	 */
	public String getLowRes2() {
		return lowRes2;
	}
	/**
	 * @param lowRes2 the lowRes2 to set
	 */
	public void setLowRes2(String lowRes2) {
		this.lowRes2 = lowRes2;
	}
	/**
	 * @return the lowRes3
	 */
	public String getLowRes3() {
		return lowRes3;
	}
	/**
	 * @param lowRes3 the lowRes3 to set
	 */
	public void setLowRes3(String lowRes3) {
		this.lowRes3 = lowRes3;
	}
	/**
	 * @return the lowRes7
	 */
	public String getLowRes7() {
		return lowRes7;
	}
	/**
	 * @param lowRes7 the lowRes7 to set
	 */
	public void setLowRes7(String lowRes7) {
		this.lowRes7 = lowRes7;
	}
	/**
	 * @return the lowRes8
	 */
	public String getLowRes8() {
		return lowRes8;
	}
	/**
	 * @param lowRes8 the lowRes8 to set
	 */
	public void setLowRes8(String lowRes8) {
		this.lowRes8 = lowRes8;
	}
	/**
	 * @return the lowRes9
	 */
	public String getLowRes9() {
		return lowRes9;
	}
	/**
	 * @param lowRes9 the lowRes9 to set
	 */
	public void setLowRes9(String lowRes9) {
		this.lowRes9 = lowRes9;
	}
	/**
	 * @return the hiRes1
	 */
	public String getHiRes1() {
		return hiRes1;
	}
	/**
	 * @param hiRes1 the hiRes1 to set
	 */
	public void setHiRes1(String hiRes1) {
		this.hiRes1 = hiRes1;
	}
	/**
	 * @return the hiRes2
	 */
	public String getHiRes2() {
		return hiRes2;
	}
	/**
	 * @param hiRes2 the hiRes2 to set
	 */
	public void setHiRes2(String hiRes2) {
		this.hiRes2 = hiRes2;
	}
	/**
	 * @return the hiRes3
	 */
	public String getHiRes3() {
		return hiRes3;
	}
	/**
	 * @param hiRes3 the hiRes3 to set
	 */
	public void setHiRes3(String hiRes3) {
		this.hiRes3 = hiRes3;
	}
	/**
	 * @return the hiRes7
	 */
	public String getHiRes7() {
		return hiRes7;
	}
	/**
	 * @param hiRes7 the hiRes7 to set
	 */
	public void setHiRes7(String hiRes7) {
		this.hiRes7 = hiRes7;
	}
	/**
	 * @return the hiRes8
	 */
	public String getHiRes8() {
		return hiRes8;
	}
	/**
	 * @param hiRes8 the hiRes8 to set
	 */
	public void setHiRes8(String hiRes8) {
		this.hiRes8 = hiRes8;
	}
	/**
	 * @return the hiRes9
	 */
	public String getHiRes9() {
		return hiRes9;
	}
	/**
	 * @param hiRes9 the hiRes9 to set
	 */
	public void setHiRes9(String hiRes9) {
		this.hiRes9 = hiRes9;
	}
	/**
	 * @return the mock1
	 */
	public String getMock1() {
		return mock1;
	}
	/**
	 * @param mock1 the mock1 to set
	 */
	public void setMock1(String mock1) {
		this.mock1 = mock1;
	}
	/**
	 * @return the mock2
	 */
	public String getMock2() {
		return mock2;
	}
	/**
	 * @param mock2 the mock2 to set
	 */
	public void setMock2(String mock2) {
		this.mock2 = mock2;
	}
	/**
	 * @return the mock3
	 */
	public String getMock3() {
		return mock3;
	}
	/**
	 * @param mock3 the mock3 to set
	 */
	public void setMock3(String mock3) {
		this.mock3 = mock3;
	}
	/**
	 * @return the mock7
	 */
	public String getMock7() {
		return mock7;
	}
	/**
	 * @param mock7 the mock7 to set
	 */
	public void setMock7(String mock7) {
		this.mock7 = mock7;
	}
	/**
	 * @return the mock8
	 */
	public String getMock8() {
		return mock8;
	}
	/**
	 * @param mock8 the mock8 to set
	 */
	public void setMock8(String mock8) {
		this.mock8 = mock8;
	}
	/**
	 * @return the mock9
	 */
	public String getMock9() {
		return mock9;
	}
	/**
	 * @param mock9 the mock9 to set
	 */
	public void setMock9(String mock9) {
		this.mock9 = mock9;
	}
	/**
	 * @return the lowRes1Url
	 */
	public String getLowRes1Url() {
		return lowRes1Url;
	}
	/**
	 * @param lowRes1Url the lowRes1Url to set
	 */
	public void setLowRes1Url(String lowRes1Url) {
		this.lowRes1Url = lowRes1Url;
	}
	/**
	 * @return the lowRes2Url
	 */
	public String getLowRes2Url() {
		return lowRes2Url;
	}
	/**
	 * @param lowRes2Url the lowRes2Url to set
	 */
	public void setLowRes2Url(String lowRes2Url) {
		this.lowRes2Url = lowRes2Url;
	}
	/**
	 * @return the lowRes3Url
	 */
	public String getLowRes3Url() {
		return lowRes3Url;
	}
	/**
	 * @param lowRes3Url the lowRes3Url to set
	 */
	public void setLowRes3Url(String lowRes3Url) {
		this.lowRes3Url = lowRes3Url;
	}
	/**
	 * @return the lowRes7Url
	 */
	public String getLowRes7Url() {
		return lowRes7Url;
	}
	/**
	 * @param lowRes7Url the lowRes7Url to set
	 */
	public void setLowRes7Url(String lowRes7Url) {
		this.lowRes7Url = lowRes7Url;
	}
	/**
	 * @return the lowRes8Url
	 */
	public String getLowRes8Url() {
		return lowRes8Url;
	}
	/**
	 * @param lowRes8Url the lowRes8Url to set
	 */
	public void setLowRes8Url(String lowRes8Url) {
		this.lowRes8Url = lowRes8Url;
	}
	/**
	 * @return the lowRes9Url
	 */
	public String getLowRes9Url() {
		return lowRes9Url;
	}
	/**
	 * @param lowRes9Url the lowRes9Url to set
	 */
	public void setLowRes9Url(String lowRes9Url) {
		this.lowRes9Url = lowRes9Url;
	}
	/**
	 * @return the hiRes1Url
	 */
	public String getHiRes1Url() {
		return hiRes1Url;
	}
	/**
	 * @param hiRes1Url the hiRes1Url to set
	 */
	public void setHiRes1Url(String hiRes1Url) {
		this.hiRes1Url = hiRes1Url;
	}
	/**
	 * @return the hiRes2Url
	 */
	public String getHiRes2Url() {
		return hiRes2Url;
	}
	/**
	 * @param hiRes2Url the hiRes2Url to set
	 */
	public void setHiRes2Url(String hiRes2Url) {
		this.hiRes2Url = hiRes2Url;
	}
	/**
	 * @return the hiRes3Url
	 */
	public String getHiRes3Url() {
		return hiRes3Url;
	}
	/**
	 * @param hiRes3Url the hiRes3Url to set
	 */
	public void setHiRes3Url(String hiRes3Url) {
		this.hiRes3Url = hiRes3Url;
	}
	/**
	 * @return the hiRes7Url
	 */
	public String getHiRes7Url() {
		return hiRes7Url;
	}
	/**
	 * @param hiRes7Url the hiRes7Url to set
	 */
	public void setHiRes7Url(String hiRes7Url) {
		this.hiRes7Url = hiRes7Url;
	}
	/**
	 * @return the hiRes8Url
	 */
	public String getHiRes8Url() {
		return hiRes8Url;
	}
	/**
	 * @param hiRes8Url the hiRes8Url to set
	 */
	public void setHiRes8Url(String hiRes8Url) {
		this.hiRes8Url = hiRes8Url;
	}
	/**
	 * @return the hiRes9Url
	 */
	public String getHiRes9Url() {
		return hiRes9Url;
	}
	/**
	 * @param hiRes9Url the hiRes9Url to set
	 */
	public void setHiRes9Url(String hiRes9Url) {
		this.hiRes9Url = hiRes9Url;
	}
	/**
	 * @return the mock1Url
	 */
	public String getMock1Url() {
		return mock1Url;
	}
	/**
	 * @param mock1Url the mock1Url to set
	 */
	public void setMock1Url(String mock1Url) {
		this.mock1Url = mock1Url;
	}
	/**
	 * @return the mock2Url
	 */
	public String getMock2Url() {
		return mock2Url;
	}
	/**
	 * @param mock2Url the mock2Url to set
	 */
	public void setMock2Url(String mock2Url) {
		this.mock2Url = mock2Url;
	}
	/**
	 * @return the mock3Url
	 */
	public String getMock3Url() {
		return mock3Url;
	}
	/**
	 * @param mock3Url the mock3Url to set
	 */
	public void setMock3Url(String mock3Url) {
		this.mock3Url = mock3Url;
	}
	/**
	 * @return the mock7Url
	 */
	public String getMock7Url() {
		return mock7Url;
	}
	/**
	 * @param mock7Url the mock7Url to set
	 */
	public void setMock7Url(String mock7Url) {
		this.mock7Url = mock7Url;
	}
	/**
	 * @return the mock8Url
	 */
	public String getMock8Url() {
		return mock8Url;
	}
	/**
	 * @param mock8Url the mock8Url to set
	 */
	public void setMock8Url(String mock8Url) {
		this.mock8Url = mock8Url;
	}
	/**
	 * @return the mock9Url
	 */
	public String getMock9Url() {
		return mock9Url;
	}
	/**
	 * @param mock9Url the mock9Url to set
	 */
	public void setMock9Url(String mock9Url) {
		this.mock9Url = mock9Url;
	}
	/**
	 * @return the jpg
	 */
	public String getJpg() {
		return jpg;
	}
	/**
	 * @param jpg the jpg to set
	 */
	public void setJpg(String jpg) {
		this.jpg = jpg;
	}
	
}
