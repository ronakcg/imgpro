package com.ko.impro.dto;

import java.util.List;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class AuditLogRequestDto {

	private String code;
	private String message;
	private AuditLogDto auditLogdto;
	private List<AuditLogDto> auditLogDtoList;
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the auditLogdto
	 */
	public AuditLogDto getAuditLogdto() {
		return auditLogdto;
	}
	/**
	 * @param auditLogdto the auditLogdto to set
	 */
	public void setAuditLogdto(AuditLogDto auditLogdto) {
		this.auditLogdto = auditLogdto;
	}
	/**
	 * @return the auditLogDtoList
	 */
	public List<AuditLogDto> getAuditLogDtoList() {
		return auditLogDtoList;
	}
	/**
	 * @param auditLogDtoList the auditLogDtoList to set
	 */
	public void setAuditLogDtoList(List<AuditLogDto> auditLogDtoList) {
		this.auditLogDtoList = auditLogDtoList;
	}
}
