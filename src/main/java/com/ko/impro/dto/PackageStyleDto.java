package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */ 
public class PackageStyleDto extends MaintenanceDto {
	private String styleCode;
	
	private String styleDesc;

	/**
	 * @return the styleCode
	 */
	public String getStyleCode() {
		return styleCode;
	}

	/**
	 * @param styleCode the styleCode to set
	 */
	public void setStyleCode(String styleCode) {
		this.styleCode = styleCode;
	}

	/**
	 * @return the styleDesc
	 */
	public String getStyleDesc() {
		return styleDesc;
	}

	/**
	 * @param styleDesc the styleDesc to set
	 */
	public void setStyleDesc(String styleDesc) {
		this.styleDesc = styleDesc;
	}
	

}
