package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class PackageSizeDto  extends MaintenanceDto {

	private String packageSizeCode;
	private String packageSizeDescription;
	private String packageSizeOz;
	/**
	 * @return the packageSizeCode
	 */
	public String getPackageSizeCode() {
		return packageSizeCode;
	}
	/**
	 * @param packageSizeCode the packageSizeCode to set
	 */
	public void setPackageSizeCode(String packageSizeCode) {
		this.packageSizeCode = packageSizeCode;
	}
	/**
	 * @return the packageSizeDescription
	 */
	public String getPackageSizeDescription() {
		return packageSizeDescription;
	}
	/**
	 * @param packageSizeDescription the packageSizeDescription to set
	 */
	public void setPackageSizeDescription(String packageSizeDescription) {
		this.packageSizeDescription = packageSizeDescription;
	}
	/**
	 * @return the packageSizeOz
	 */
	public String getPackageSizeOz() {
		return packageSizeOz;
	}
	/**
	 * @param packageSizeOz the packageSizeOz to set
	 */
	public void setPackageSizeOz(String packageSizeOz) {
		this.packageSizeOz = packageSizeOz;
	}
	
	
}
