package com.ko.impro.dto;


/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class ProductDto {
	
	
	private String productCode;
	private BrandDto brandDto;
	private ManufacturerDto manufacturerDto;
	private ExtendedProductDescDto extendedProductDescDto;
	private PackageDto pkgdto;
	private FlavorDto flavorDto;
	private CategoryDto categoryDto;
	private SubCategoryDto subCategoryDto;
	private SegmentDto segmentDto;
	private SubSegmentDto subSegmentDto;
	private PackageSizeDto packageSizeDto;
	private boolean archiveFlag;
	private String productName;
	private String shortProductName;
	private String upc;
	private String dateMaintained;
	private String dateAdded;
	private String archiveDate;
	
	private String nielsenUpc;
	private String nielsenDesc;
	private String consumptionType;
	private String upc12;
	private MissingImagesDto  missingImagesDto;

	private int archiveReasonCode;
	private String archiveReason;
	private String acnCategory;
	private String acnSubCategory;
	private String acnSegment;
	private String acnSubSegment;
	private String acnBeverageType;
	private String acnKeyManufacturer;
	private String acnBrand;
	private String acnKeyFlavor;
	private String acnLongName;
	private String acnConsumptionType;
	private String bitMapName;
	private String oldBitMapName;

	private EditImageDto editImageDto;
	
	private String imageType;
	
	/**
	 * @return the bitMapName
	 */
	public String getBitMapName() {
		return bitMapName;
	}
	/**
	 * @param bitMapName the bitMapName to set
	 */
	public void setBitMapName(String bitMapName) {
		this.bitMapName = bitMapName;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the brandDto
	 */
	public BrandDto getBrandDto() {
		return brandDto;
	}
	/**
	 * @param brandDto the brandDto to set
	 */
	public void setBrandDto(BrandDto brandDto) {
		this.brandDto = brandDto;
	}
	/**
	 * @return the manufacturerDto
	 */
	public ManufacturerDto getManufacturerDto() {
		return manufacturerDto;
	}
	/**
	 * @param manufacturerDto the manufacturerDto to set
	 */
	public void setManufacturerDto(ManufacturerDto manufacturerDto) {
		this.manufacturerDto = manufacturerDto;
	}
	/**
	 * @return the extendedProductDescDto
	 */
	public ExtendedProductDescDto getExtendedProductDescDto() {
		return extendedProductDescDto;
	}
	/**
	 * @param extendedProductDescDto the extendedProductDescDto to set
	 */
	public void setExtendedProductDescDto(ExtendedProductDescDto extendedProductDescDto) {
		this.extendedProductDescDto = extendedProductDescDto;
	}
	/**
	 * @return the pkgdto
	 */
	public PackageDto getPkgdto() {
		return pkgdto;
	}
	/**
	 * @param pkgdto the pkgdto to set
	 */
	public void setPkgdto(PackageDto pkgdto) {
		this.pkgdto = pkgdto;
	}
	/**
	 * @return the flavorDto
	 */
	public FlavorDto getFlavorDto() {
		return flavorDto;
	}
	/**
	 * @param flavorDto the flavorDto to set
	 */
	public void setFlavorDto(FlavorDto flavorDto) {
		this.flavorDto = flavorDto;
	}
	/**
	 * @return the categoryDto
	 */
	public CategoryDto getCategoryDto() {
		return categoryDto;
	}
	/**
	 * @param categoryDto the categoryDto to set
	 */
	public void setCategoryDto(CategoryDto categoryDto) {
		this.categoryDto = categoryDto;
	}
	/**
	 * @return the subCategoryDto
	 */
	public SubCategoryDto getSubCategoryDto() {
		return subCategoryDto;
	}
	/**
	 * @param subCategoryDto the subCategoryDto to set
	 */
	public void setSubCategoryDto(SubCategoryDto subCategoryDto) {
		this.subCategoryDto = subCategoryDto;
	}
	/**
	 * @return the segmentDto
	 */
	public SegmentDto getSegmentDto() {
		return segmentDto;
	}
	/**
	 * @param segmentDto the segmentDto to set
	 */
	public void setSegmentDto(SegmentDto segmentDto) {
		this.segmentDto = segmentDto;
	}
	/**
	 * @return the subSegmentDto
	 */
	public SubSegmentDto getSubSegmentDto() {
		return subSegmentDto;
	}
	/**
	 * @param subSegmentDto the subSegmentDto to set
	 */
	public void setSubSegmentDto(SubSegmentDto subSegmentDto) {
		this.subSegmentDto = subSegmentDto;
	}
	/**
	 * @return the packageSizeDto
	 */
	public PackageSizeDto getPackageSizeDto() {
		return packageSizeDto;
	}
	/**
	 * @param packageSizeDto the packageSizeDto to set
	 */
	public void setPackageSizeDto(PackageSizeDto packageSizeDto) {
		this.packageSizeDto = packageSizeDto;
	}
	/**
	 * @return the archiveFlag
	 */
	public boolean isArchiveFlag() {
		return archiveFlag;
	}
	/**
	 * @param archiveFlag the archiveFlag to set
	 */
	public void setArchiveFlag(boolean archiveFlag) {
		this.archiveFlag = archiveFlag;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the shortProductName
	 */
	public String getShortProductName() {
		return shortProductName;
	}
	/**
	 * @param shortProductName the shortProductName to set
	 */
	public void setShortProductName(String shortProductName) {
		this.shortProductName = shortProductName;
	}
	/**
	 * @return the upc
	 */
	public String getUpc() {
		return upc;
	}
	/**
	 * @param upc the upc to set
	 */
	public void setUpc(String upc) {
		this.upc = upc;
	}
	/**
	 * @return the dateMaintained
	 */
	public String getDateMaintained() {
		return dateMaintained;
	}
	/**
	 * @param dateMaintained the dateMaintained to set
	 */
	public void setDateMaintained(String dateMaintained) {
		this.dateMaintained = dateMaintained;
	}
	/**
	 * @return the archiveDate
	 */
	public String getArchiveDate() {
		return archiveDate;
	}
	/**
	 * @param archiveDate the archiveDate to set
	 */
	public void setArchiveDate(String archiveDate) {
		this.archiveDate = archiveDate;
	}
	/**
	 * @return the nielsenUpc
	 */
	public String getNielsenUpc() {
		return nielsenUpc;
	}
	/**
	 * @param nielsenUpc the nielsenUpc to set
	 */
	public void setNielsenUpc(String nielsenUpc) {
		this.nielsenUpc = nielsenUpc;
	}
	/**
	 * @return the nielsenDesc
	 */
	public String getNielsenDesc() {
		return nielsenDesc;
	}
	/**
	 * @param nielsenDesc the nielsenDesc to set
	 */
	public void setNielsenDesc(String nielsenDesc) {
		this.nielsenDesc = nielsenDesc;
	}
	/**
	 * @return the consumptionType
	 */
	public String getConsumptionType() {
		return consumptionType;
	}
	/**
	 * @param consumptionType the consumptionType to set
	 */
	public void setConsumptionType(String consumptionType) {
		this.consumptionType = consumptionType;
	}
	/**
	 * @return the upc12
	 */
	public String getUpc12() {
		return upc12;
	}
	/**
	 * @param upc12 the upc12 to set
	 */
	public void setUpc12(String upc12) {
		this.upc12 = upc12;
	}
	/**
	 * @return the missingImagesDto
	 */
	public MissingImagesDto getMissingImagesDto() {
		return missingImagesDto;
	}
	/**
	 * @param missingImagesDto the missingImagesDto to set
	 */
	public void setMissingImagesDto(MissingImagesDto missingImagesDto) {
		this.missingImagesDto = missingImagesDto;
	}
	/**
	 * @return the archiveReasonCode
	 */
	public int getArchiveReasonCode() {
		return archiveReasonCode;
	}
	/**
	 * @param archiveReasonCode the archiveReasonCode to set
	 */
	public void setArchiveReasonCode(int archiveReasonCode) {
		this.archiveReasonCode = archiveReasonCode;
	}
	/**
	 * @return the acnCategory
	 */
	public String getAcnCategory() {
		return acnCategory;
	}
	/**
	 * @param acnCategory the acnCategory to set
	 */
	public void setAcnCategory(String acnCategory) {
		this.acnCategory = acnCategory;
	}
	/**
	 * @return the acnSubCategory
	 */
	public String getAcnSubCategory() {
		return acnSubCategory;
	}
	/**
	 * @param acnSubCategory the acnSubCategory to set
	 */
	public void setAcnSubCategory(String acnSubCategory) {
		this.acnSubCategory = acnSubCategory;
	}
	/**
	 * @return the acnSegment
	 */
	public String getAcnSegment() {
		return acnSegment;
	}
	/**
	 * @param acnSegment the acnSegment to set
	 */
	public void setAcnSegment(String acnSegment) {
		this.acnSegment = acnSegment;
	}
	/**
	 * @return the acnSubSegment
	 */
	public String getAcnSubSegment() {
		return acnSubSegment;
	}
	/**
	 * @param acnSubSegment the acnSubSegment to set
	 */
	public void setAcnSubSegment(String acnSubSegment) {
		this.acnSubSegment = acnSubSegment;
	}
	/**
	 * @return the acnBeverageType
	 */
	public String getAcnBeverageType() {
		return acnBeverageType;
	}
	/**
	 * @param acnBeverageType the acnBeverageType to set
	 */
	public void setAcnBeverageType(String acnBeverageType) {
		this.acnBeverageType = acnBeverageType;
	}
	/**
	 * @return the acnKeyManufacturer
	 */
	public String getAcnKeyManufacturer() {
		return acnKeyManufacturer;
	}
	/**
	 * @param acnKeyManufacturer the acnKeyManufacturer to set
	 */
	public void setAcnKeyManufacturer(String acnKeyManufacturer) {
		this.acnKeyManufacturer = acnKeyManufacturer;
	}
	/**
	 * @return the acnBrand
	 */
	public String getAcnBrand() {
		return acnBrand;
	}
	/**
	 * @param acnBrand the acnBrand to set
	 */
	public void setAcnBrand(String acnBrand) {
		this.acnBrand = acnBrand;
	}
	/**
	 * @return the acnKeyFlavor
	 */
	public String getAcnKeyFlavor() {
		return acnKeyFlavor;
	}
	/**
	 * @param acnKeyFlavor the acnKeyFlavor to set
	 */
	public void setAcnKeyFlavor(String acnKeyFlavor) {
		this.acnKeyFlavor = acnKeyFlavor;
	}
	/**
	 * @return the acnLongName
	 */
	public String getAcnLongName() {
		return acnLongName;
	}
	/**
	 * @param acnLongName the acnLongName to set
	 */
	public void setAcnLongName(String acnLongName) {
		this.acnLongName = acnLongName;
	}
	/**
	 * @return the acnConsumptionType
	 */
	public String getAcnConsumptionType() {
		return acnConsumptionType;
	}
	/**
	 * @param acnConsumptionType the acnConsumptionType to set
	 */
	public void setAcnConsumptionType(String acnConsumptionType) {
		this.acnConsumptionType = acnConsumptionType;
	}

	/**
	 * @return the editImageDto
	 */
	public EditImageDto getEditImageDto() {
		return editImageDto;
	}
	/**
	 * @param editImageDto the editImageDto to set
	 */
	public void setEditImageDto(EditImageDto editImageDto) {
		this.editImageDto = editImageDto;
	}
	/**
	 * @return the oldBitMapName
	 */
	public String getOldBitMapName() {
		return oldBitMapName;
	}
	/**
	 * @param oldBitMapName the oldBitMapName to set
	 */
	public void setOldBitMapName(String oldBitMapName) {
		this.oldBitMapName = oldBitMapName;
	}
	/**
	 * 
	 * @return the imageType
	 */
	public String getImageType() {
		return imageType;
	}
	/**
	 * @param imageType
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	/**
	 * @return the dateAdded
	 */
	public String getDateAdded() {
		return dateAdded;
	}
	/**
	 * @param dateAdded the dateAdded to set
	 */
	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}
	/**
	 * @return the archiveReason
	 */
	public String getArchiveReason() {
		return archiveReason;
	}
	/**
	 * @param archiveReason the archiveReason to set
	 */
	public void setArchiveReason(String archiveReason) {
		this.archiveReason = archiveReason;
	}
	
	
	
}
