package com.ko.impro.dto;

import java.util.List;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class ProductRequestWrapperDto {
	private List<ProductDto> productList;
	private String deleteReason;
	private ProductSearchDto productSearchDto ;
	private List<ArchivedProductDto> archivedProductList;
	/**
	 * @return the productList
	 */
	public List<ProductDto> getProductList() {
		return productList;
	}
	/**
	 * @param productList the productList to set
	 */
	public void setProductList(List<ProductDto> productList) {
		this.productList = productList;
	}
	/**
	 * @return the deleteReason
	 */
	public String getDeleteReason() {
		return deleteReason;
	}
	/**
	 * @param deleteReason the deleteReason to set
	 */
	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}
	/**
	 * @return the productSearchDto
	 */
	public ProductSearchDto getProductSearchDto() {
		return productSearchDto;
	}
	/**
	 * @param productSearchDto the productSearchDto to set
	 */
	public void setProductSearchDto(ProductSearchDto productSearchDto) {
		this.productSearchDto = productSearchDto;
	}
	/**
	 * @return the archivedProductList
	 */
	public List<ArchivedProductDto> getArchivedProductList() {
		return archivedProductList;
	}
	/**
	 * @param archivedProductList the archivedProductList to set
	 */
	public void setArchivedProductList(List<ArchivedProductDto> archivedProductList) {
		this.archivedProductList = archivedProductList;
	}
}