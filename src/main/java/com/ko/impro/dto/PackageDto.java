package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class PackageDto  extends MaintenanceDto {

	private String packageCode;

	private String packageName;
	private String validOrientations;
	private String unitPerTray;
	private String liquidVolumePerPkg;
	private String casesPerPallet;
	private String unitWidth;
	private String unitHeight;
	private String unitDepth;
	
	private String trayWidth;
	private String trayHeight;
	private String trayDepth;
	private String trayWide;
	private String trayHigh;
	private String trayDeep;
	
	private String maxStackOver;

	private String metorder;
	private String stockFace;

	private ContainerTypeDto containerTypeDto;
	private PackageSizeDto packageSizeDto;
	private ShapeDto shapesDto;
	private PackageStyleDto styleDto;
	/**
	 * @return the packageCode
	 */
	public String getPackageCode() {
		return packageCode;
	}
	/**
	 * @param packageCode the packageCode to set
	 */
	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}
	/**
	 * @return the packageName
	 */
	public String getPackageName() {
		return packageName;
	}
	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	/**
	 * @return the validOrientations
	 */
	public String getValidOrientations() {
		return validOrientations;
	}
	/**
	 * @param validOrientations the validOrientations to set
	 */
	public void setValidOrientations(String validOrientations) {
		this.validOrientations = validOrientations;
	}
	/**
	 * @return the unitPerTray
	 */
	public String getUnitPerTray() {
		return unitPerTray;
	}
	/**
	 * @param unitPerTray the unitPerTray to set
	 */
	public void setUnitPerTray(String unitPerTray) {
		this.unitPerTray = unitPerTray;
	}
	/**
	 * @return the liquidVolumePerPkg
	 */
	public String getLiquidVolumePerPkg() {
		return liquidVolumePerPkg;
	}
	/**
	 * @param liquidVolumePerPkg the liquidVolumePerPkg to set
	 */
	public void setLiquidVolumePerPkg(String liquidVolumePerPkg) {
		this.liquidVolumePerPkg = liquidVolumePerPkg;
	}
	/**
	 * @return the casesPerPallet
	 */
	public String getCasesPerPallet() {
		return casesPerPallet;
	}
	/**
	 * @param casesPerPallet the casesPerPallet to set
	 */
	public void setCasesPerPallet(String casesPerPallet) {
		this.casesPerPallet = casesPerPallet;
	}
	/**
	 * @return the unitWidth
	 */
	public String getUnitWidth() {
		return unitWidth;
	}
	/**
	 * @param unitWidth the unitWidth to set
	 */
	public void setUnitWidth(String unitWidth) {
		this.unitWidth = unitWidth;
	}
	/**
	 * @return the unitHeight
	 */
	public String getUnitHeight() {
		return unitHeight;
	}
	/**
	 * @param unitHeight the unitHeight to set
	 */
	public void setUnitHeight(String unitHeight) {
		this.unitHeight = unitHeight;
	}
	/**
	 * @return the unitDepth
	 */
	public String getUnitDepth() {
		return unitDepth;
	}
	/**
	 * @param unitDepth the unitDepth to set
	 */
	public void setUnitDepth(String unitDepth) {
		this.unitDepth = unitDepth;
	}
	/**
	 * @return the trayWidth
	 */
	public String getTrayWidth() {
		return trayWidth;
	}
	/**
	 * @param trayWidth the trayWidth to set
	 */
	public void setTrayWidth(String trayWidth) {
		this.trayWidth = trayWidth;
	}
	/**
	 * @return the trayHeight
	 */
	public String getTrayHeight() {
		return trayHeight;
	}
	/**
	 * @param trayHeight the trayHeight to set
	 */
	public void setTrayHeight(String trayHeight) {
		this.trayHeight = trayHeight;
	}
	/**
	 * @return the trayDepth
	 */
	public String getTrayDepth() {
		return trayDepth;
	}
	/**
	 * @param trayDepth the trayDepth to set
	 */
	public void setTrayDepth(String trayDepth) {
		this.trayDepth = trayDepth;
	}
	/**
	 * @return the trayWide
	 */
	public String getTrayWide() {
		return trayWide;
	}
	/**
	 * @param trayWide the trayWide to set
	 */
	public void setTrayWide(String trayWide) {
		this.trayWide = trayWide;
	}
	/**
	 * @return the trayHigh
	 */
	public String getTrayHigh() {
		return trayHigh;
	}
	/**
	 * @param trayHigh the trayHigh to set
	 */
	public void setTrayHigh(String trayHigh) {
		this.trayHigh = trayHigh;
	}
	/**
	 * @return the trayDeep
	 */
	public String getTrayDeep() {
		return trayDeep;
	}
	/**
	 * @param trayDeep the trayDeep to set
	 */
	public void setTrayDeep(String trayDeep) {
		this.trayDeep = trayDeep;
	}
	/**
	 * @return the maxStackOver
	 */
	public String getMaxStackOver() {
		return maxStackOver;
	}
	/**
	 * @param maxStackOver the maxStackOver to set
	 */
	public void setMaxStackOver(String maxStackOver) {
		this.maxStackOver = maxStackOver;
	}
	/**
	 * @return the metorder
	 */
	public String getMetorder() {
		return metorder;
	}
	/**
	 * @param metorder the metorder to set
	 */
	public void setMetorder(String metorder) {
		this.metorder = metorder;
	}
	/**
	 * @return the stockFace
	 */
	public String getStockFace() {
		return stockFace;
	}
	/**
	 * @param stockFace the stockFace to set
	 */
	public void setStockFace(String stockFace) {
		this.stockFace = stockFace;
	}
	/**
	 * @return the containerTypeDto
	 */
	public ContainerTypeDto getContainerTypeDto() {
		return containerTypeDto;
	}
	/**
	 * @param containerTypeDto the containerTypeDto to set
	 */
	public void setContainerTypeDto(ContainerTypeDto containerTypeDto) {
		this.containerTypeDto = containerTypeDto;
	}
	/**
	 * @return the packageSizeDto
	 */
	public PackageSizeDto getPackageSizeDto() {
		return packageSizeDto;
	}
	/**
	 * @param packageSizeDto the packageSizeDto to set
	 */
	public void setPackageSizeDto(PackageSizeDto packageSizeDto) {
		this.packageSizeDto = packageSizeDto;
	}
	/**
	 * @return the shapesDto
	 */
	public ShapeDto getShapesDto() {
		return shapesDto;
	}
	/**
	 * @param shapesDto the shapesDto to set
	 */
	public void setShapesDto(ShapeDto shapesDto) {
		this.shapesDto = shapesDto;
	}
	/**
	 * @return the styleDto
	 */
	public PackageStyleDto getStyleDto() {
		return styleDto;
	}
	/**
	 * @param styleDto the styleDto to set
	 */
	public void setStyleDto(PackageStyleDto styleDto) {
		this.styleDto = styleDto;
	}
}