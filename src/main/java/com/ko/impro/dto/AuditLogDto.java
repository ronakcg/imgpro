package com.ko.impro.dto;


/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class AuditLogDto {
	
	private String  transactionDate;
	private String  transactionUser;
	private String  tableUpdated;
	private String  recordUpdated;
	private String  transactionType;
	private String  DescAction;
	/**
	 * @return the transactionDate
	 */
	public String getTransactionDate() {
		return transactionDate;
	}
	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	/**
	 * @return the transactionUser
	 */
	public String getTransactionUser() {
		return transactionUser;
	}
	/**
	 * @param transactionUser the transactionUser to set
	 */
	public void setTransactionUser(String transactionUser) {
		this.transactionUser = transactionUser;
	}
	/**
	 * @return the tableUpdated
	 */
	public String getTableUpdated() {
		return tableUpdated;
	}
	/**
	 * @param tableUpdated the tableUpdated to set
	 */
	public void setTableUpdated(String tableUpdated) {
		this.tableUpdated = tableUpdated;
	}
	/**
	 * @return the recordUpdated
	 */
	public String getRecordUpdated() {
		return recordUpdated;
	}
	/**
	 * @param recordUpdated the recordUpdated to set
	 */
	public void setRecordUpdated(String recordUpdated) {
		this.recordUpdated = recordUpdated;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * @return the descAction
	 */
	public String getDescAction() {
		return DescAction;
	}
	/**
	 * @param descAction the descAction to set
	 */
	public void setDescAction(String descAction) {
		DescAction = descAction;
	}
	
	
}
