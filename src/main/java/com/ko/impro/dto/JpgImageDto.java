package com.ko.impro.dto;

public class JpgImageDto {

	private String file1;
	
	private boolean deleteFLag1;
	private boolean addFLag1;
	private boolean editFLag1;


	public String getFile1() {
		return file1;
	}

	public void setFile1(String file1) {
		this.file1 = file1;
	}

	public boolean isDeleteFLag1() {
		return deleteFLag1;
	}

	public void setDeleteFLag1(boolean deleteFLag1) {
		this.deleteFLag1 = deleteFLag1;
	}

	public boolean isAddFLag1() {
		return addFLag1;
	}

	public void setAddFLag1(boolean addFLag1) {
		this.addFLag1 = addFLag1;
	}

	public boolean isEditFLag1() {
		return editFLag1;
	}

	public void setEditFLag1(boolean editFLag1) {
		this.editFLag1 = editFLag1;
	}

	
	
}
