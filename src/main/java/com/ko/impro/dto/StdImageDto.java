package com.ko.impro.dto;

public class StdImageDto {
	
	private boolean deleteFLag1;
	private boolean deleteFLag2;
	private boolean deleteFLag3;
	private boolean deleteFLag7;
	private boolean deleteFLag8;
	private boolean deleteFLag9;

	private boolean addFLag1;
	private boolean addFLag2;
	private boolean addFLag3;
	private boolean addFLag7;
	private boolean addFLag8;
	private boolean addFLag9;

	private boolean editFLag1;
	private boolean editFLag2;
	private boolean editFLag3;
	private boolean editFLag7;
	private boolean editFLag8;
	private boolean editFLag9;
	
	private String file1;
	private String file2;
	private String file3;
	private String file7;
	private String file8;
	private String file9;
	
	public String getFile1() {
		return file1;
	}
	public void setFile1(String file1) {
		this.file1 = file1;
	}
	public String getFile2() {
		return file2;
	}
	public void setFile2(String file2) {
		this.file2 = file2;
	}
	public String getFile3() {
		return file3;
	}
	public void setFile3(String file3) {
		this.file3 = file3;
	}
	public String getFile7() {
		return file7;
	}
	public void setFile7(String file7) {
		this.file7 = file7;
	}
	public String getFile8() {
		return file8;
	}
	public void setFile8(String file8) {
		this.file8 = file8;
	}
	public String getFile9() {
		return file9;
	}
	public void setFile9(String file9) {
		this.file9 = file9;
	}
	public boolean isDeleteFLag1() {
		return deleteFLag1;
	}
	public void setDeleteFLag1(boolean deleteFLag1) {
		this.deleteFLag1 = deleteFLag1;
	}
	public boolean isDeleteFLag2() {
		return deleteFLag2;
	}
	public void setDeleteFLag2(boolean deleteFLag2) {
		this.deleteFLag2 = deleteFLag2;
	}
	public boolean isDeleteFLag3() {
		return deleteFLag3;
	}
	public void setDeleteFLag3(boolean deleteFLag3) {
		this.deleteFLag3 = deleteFLag3;
	}
	public boolean isDeleteFLag7() {
		return deleteFLag7;
	}
	public void setDeleteFLag7(boolean deleteFLag7) {
		this.deleteFLag7 = deleteFLag7;
	}
	public boolean isDeleteFLag8() {
		return deleteFLag8;
	}
	public void setDeleteFLag8(boolean deleteFLag8) {
		this.deleteFLag8 = deleteFLag8;
	}
	public boolean isDeleteFLag9() {
		return deleteFLag9;
	}
	public void setDeleteFLag9(boolean deleteFLag9) {
		this.deleteFLag9 = deleteFLag9;
	}
	public boolean isAddFLag1() {
		return addFLag1;
	}
	public void setAddFLag1(boolean addFLag1) {
		this.addFLag1 = addFLag1;
	}
	public boolean isAddFLag2() {
		return addFLag2;
	}
	public void setAddFLag2(boolean addFLag2) {
		this.addFLag2 = addFLag2;
	}
	public boolean isAddFLag3() {
		return addFLag3;
	}
	public void setAddFLag3(boolean addFLag3) {
		this.addFLag3 = addFLag3;
	}
	public boolean isAddFLag7() {
		return addFLag7;
	}
	public void setAddFLag7(boolean addFLag7) {
		this.addFLag7 = addFLag7;
	}
	public boolean isAddFLag8() {
		return addFLag8;
	}
	public void setAddFLag8(boolean addFLag8) {
		this.addFLag8 = addFLag8;
	}
	public boolean isAddFLag9() {
		return addFLag9;
	}
	public void setAddFLag9(boolean addFLag9) {
		this.addFLag9 = addFLag9;
	}
	public boolean isEditFLag1() {
		return editFLag1;
	}
	public void setEditFLag1(boolean editFLag1) {
		this.editFLag1 = editFLag1;
	}
	public boolean isEditFLag2() {
		return editFLag2;
	}
	public void setEditFLag2(boolean editFLag2) {
		this.editFLag2 = editFLag2;
	}
	public boolean isEditFLag3() {
		return editFLag3;
	}
	public void setEditFLag3(boolean editFLag3) {
		this.editFLag3 = editFLag3;
	}
	public boolean isEditFLag7() {
		return editFLag7;
	}
	public void setEditFLag7(boolean editFLag7) {
		this.editFLag7 = editFLag7;
	}
	public boolean isEditFLag8() {
		return editFLag8;
	}
	public void setEditFLag8(boolean editFLag8) {
		this.editFLag8 = editFLag8;
	}
	public boolean isEditFLag9() {
		return editFLag9;
	}
	public void setEditFLag9(boolean editFLag9) {
		this.editFLag9 = editFLag9;
	}




}
