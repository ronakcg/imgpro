package com.ko.impro.dto;

public class EditImageDto {
	
	private StdImageDto stdImageDto;
	private HiResImageDto hiResImageDto;
	private MockImageDto mockImageDto;
	private JpgImageDto jpgImageDto;

	public StdImageDto getStdImageDto() {
		return stdImageDto;
	}
	public void setStdImageDto(StdImageDto stdImageDto) {
		this.stdImageDto = stdImageDto;
	}
	public HiResImageDto getHiResImageDto() {
		return hiResImageDto;
	}
	public void setHiResImageDto(HiResImageDto hiResImageDto) {
		this.hiResImageDto = hiResImageDto;
	}
	public MockImageDto getMockImageDto() {
		return mockImageDto;
	}
	public void setMockImageDto(MockImageDto mockImageDto) {
		this.mockImageDto = mockImageDto;
	}
	public JpgImageDto getJpgImageDto() {
		return jpgImageDto;
	}
	public void setJpgImageDto(JpgImageDto jpgImageDto) {
		this.jpgImageDto = jpgImageDto;
	}
	
	
}
