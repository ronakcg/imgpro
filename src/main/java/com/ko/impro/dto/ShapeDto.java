package com.ko.impro.dto;


/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class ShapeDto extends MaintenanceDto {
	
	private String shapeId;
	private String shapeCode;
	
	/**
	 * @return the shapeId
	 */
	public String getShapeId() {
		return shapeId;
	}
	/**
	 * @param shapeId the shapeId to set
	 */
	public void setShapeId(String shapeId) {
		this.shapeId = shapeId;
	}
	/**
	 * @return the shapeCode
	 */
	public String getShapeCode() {
		return shapeCode;
	}
	/**
	 * @param shapeCode the shapeCode to set
	 */
	public void setShapeCode(String shapeCode) {
		this.shapeCode = shapeCode;
	}
	/**
	 * @return the inActiveFlag
	 */
	
}
