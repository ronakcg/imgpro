package com.ko.impro.dto;

import java.util.List;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class ProductSearchDto {
	private List<String> brandList;
	private List<String> manufactList;
	private List<String> categoryList;
	private List<String> subCategoryList;
	private List<String> segmentList;
	private List<String> subSegmentList;
	private List<String> packageSizeList;
	private List<String> packageList;
	private List<String> flavorList;
	private String upc;
	private String id;
	private String fromDate;
	private String toDate;
	private MissingImagesDto missingImagesDto;
	private PaginationDto pageDto;
	private boolean activeSearch;

	private String archiveFlag;
	private boolean isMissingImages;
	
	private String columnName;
	private String columnOrder;
	
	private String brandCode;
	private String packageCode;
	private String productName;
	private String productCode;
	private boolean gridView;
	/**
	 * @return the brandList
	 */
	public List<String> getBrandList() {
		return brandList;
	}
	/**
	 * @param brandList the brandList to set
	 */
	public void setBrandList(List<String> brandList) {
		this.brandList = brandList;
	}
	/**
	 * @return the manufactList
	 */
	public List<String> getManufactList() {
		return manufactList;
	}
	/**
	 * @param manufactList the manufactList to set
	 */
	public void setManufactList(List<String> manufactList) {
		this.manufactList = manufactList;
	}
	/**
	 * @return the categoryList
	 */
	public List<String> getCategoryList() {
		return categoryList;
	}
	/**
	 * @param categoryList the categoryList to set
	 */
	public void setCategoryList(List<String> categoryList) {
		this.categoryList = categoryList;
	}
	/**
	 * @return the subCategoryList
	 */
	public List<String> getSubCategoryList() {
		return subCategoryList;
	}
	/**
	 * @param subCategoryList the subCategoryList to set
	 */
	public void setSubCategoryList(List<String> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	/**
	 * @return the segmentList
	 */
	public List<String> getSegmentList() {
		return segmentList;
	}
	/**
	 * @param segmentList the segmentList to set
	 */
	public void setSegmentList(List<String> segmentList) {
		this.segmentList = segmentList;
	}
	/**
	 * @return the subSegmentList
	 */
	public List<String> getSubSegmentList() {
		return subSegmentList;
	}
	/**
	 * @param subSegmentList the subSegmentList to set
	 */
	public void setSubSegmentList(List<String> subSegmentList) {
		this.subSegmentList = subSegmentList;
	}
	/**
	 * @return the packageSizeList
	 */
	public List<String> getPackageSizeList() {
		return packageSizeList;
	}
	/**
	 * @param packageSizeList the packageSizeList to set
	 */
	public void setPackageSizeList(List<String> packageSizeList) {
		this.packageSizeList = packageSizeList;
	}
	/**
	 * @return the packageList
	 */
	public List<String> getPackageList() {
		return packageList;
	}
	/**
	 * @param packageList the packageList to set
	 */
	public void setPackageList(List<String> packageList) {
		this.packageList = packageList;
	}
	/**
	 * @return the flavorList
	 */
	public List<String> getFlavorList() {
		return flavorList;
	}
	/**
	 * @param flavorList the flavorList to set
	 */
	public void setFlavorList(List<String> flavorList) {
		this.flavorList = flavorList;
	}
	/**
	 * @return the upc
	 */
	public String getUpc() {
		return upc;
	}
	/**
	 * @param upc the upc to set
	 */
	public void setUpc(String upc) {
		this.upc = upc;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the missingImagesDto
	 */
	public MissingImagesDto getMissingImagesDto() {
		return missingImagesDto;
	}
	/**
	 * @param missingImagesDto the missingImagesDto to set
	 */
	public void setMissingImagesDto(MissingImagesDto missingImagesDto) {
		this.missingImagesDto = missingImagesDto;
	}
	/**
	 * @return the pageDto
	 */
	public PaginationDto getPageDto() {
		return pageDto;
	}
	/**
	 * @param pageDto the pageDto to set
	 */
	public void setPageDto(PaginationDto pageDto) {
		this.pageDto = pageDto;
	}
	/**
	 * @return the activeSearch
	 */
	public boolean isActiveSearch() {
		return activeSearch;
	}
	/**
	 * @param activeSearch the activeSearch to set
	 */
	public void setActiveSearch(boolean activeSearch) {
		this.activeSearch = activeSearch;
	}
	/**
	 * @return the archiveFlag
	 */
	public String getArchiveFlag() {
		return archiveFlag;
	}
	/**
	 * @param archiveFlag the archiveFlag to set
	 */
	public void setArchiveFlag(String archiveFlag) {
		this.archiveFlag = archiveFlag;
	}
	/**
	 * @return the isMissingImages
	 */
	public boolean isMissingImages() {
		return isMissingImages;
	}
	/**
	 * @param isMissingImages the isMissingImages to set
	 */
	public void setMissingImages(boolean isMissingImages) {
		this.isMissingImages = isMissingImages;
	}
	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		return columnName;
	}
	/**
	 * @param columnName the columnName to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	/**
	 * @return the columnOrder
	 */
	public String getColumnOrder() {
		return columnOrder;
	}
	/**
	 * @param columnOrder the columnOrder to set
	 */
	public void setColumnOrder(String columnOrder) {
		this.columnOrder = columnOrder;
	}
	/**
	 * @return the brandCode
	 */
	public String getBrandCode() {
		return brandCode;
	}
	/**
	 * @param brandCode the brandCode to set
	 */
	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}
	/**
	 * @return the packageCode
	 */
	public String getPackageCode() {
		return packageCode;
	}
	/**
	 * @param packageCode the packageCode to set
	 */
	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the gridView
	 */
	public boolean isGridView() {
		return gridView;
	}
	/**
	 * @param gridView the gridView to set
	 */
	public void setGridView(boolean gridView) {
		this.gridView = gridView;
	}

	
}
