package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class BrandDto extends MaintenanceDto {

	private String brandCode;
	private String brandName;
	private String shortBrandName;
	private String viewingOrder;
	private boolean isYesButton;
	
	private ManufacturerDto manufacturerDto;

	/**
	 * @return the brandCode
	 */
	public String getBrandCode() {
		return brandCode;
	}

	/**
	 * @param brandCode the brandCode to set
	 */
	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the shortBrandName
	 */
	public String getShortBrandName() {
		return shortBrandName;
	}

	/**
	 * @param shortBrandName the shortBrandName to set
	 */
	public void setShortBrandName(String shortBrandName) {
		this.shortBrandName = shortBrandName;
	}

	/**
	 * @return the viewingOrder
	 */
	public String getViewingOrder() {
		return viewingOrder;
	}

	/**
	 * @param viewingOrder the viewingOrder to set
	 */
	public void setViewingOrder(String viewingOrder) {
		this.viewingOrder = viewingOrder;
	}

	/**
	 * @return the isYesButton
	 */
	public boolean isYesButton() {
		return isYesButton;
	}

	/**
	 * @param isYesButton the isYesButton to set
	 */
	public void setYesButton(boolean isYesButton) {
		this.isYesButton = isYesButton;
	}

	/**
	 * @return the manufacturerDto
	 */
	public ManufacturerDto getManufacturerDto() {
		return manufacturerDto;
	}

	/**
	 * @param manufacturerDto the manufacturerDto to set
	 */
	public void setManufacturerDto(ManufacturerDto manufacturerDto) {
		this.manufacturerDto = manufacturerDto;
	}
}
