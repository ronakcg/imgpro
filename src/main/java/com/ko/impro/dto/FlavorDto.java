package com.ko.impro.dto;

import java.util.List;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class FlavorDto  extends MaintenanceDto {
	private String flavorCode;
	private String flavorDescription;
	private List<SubCategoryDto> subCategoryList;
	private List<SubCategoryDto> addSubCatList;
	private List<SubCategoryDto> removeSubCatList;
	/**
	 * @return the flavorCode
	 */
	public String getFlavorCode() {
		return flavorCode;
	}
	/**
	 * @param flavorCode the flavorCode to set
	 */
	public void setFlavorCode(String flavorCode) {
		this.flavorCode = flavorCode;
	}
	/**
	 * @return the flavorDescription
	 */
	public String getFlavorDescription() {
		return flavorDescription;
	}
	/**
	 * @param flavorDescription the flavorDescription to set
	 */
	public void setFlavorDescription(String flavorDescription) {
		this.flavorDescription = flavorDescription;
	}
	/**
	 * @return the subCategoryList
	 */
	public List<SubCategoryDto> getSubCategoryList() {
		return subCategoryList;
	}
	/**
	 * @param subCategoryList the subCategoryList to set
	 */
	public void setSubCategoryList(List<SubCategoryDto> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	/**
	 * @return the addSubCatList
	 */
	public List<SubCategoryDto> getAddSubCatList() {
		return addSubCatList;
	}
	/**
	 * @param addSubCatList the addSubCatList to set
	 */
	public void setAddSubCatList(List<SubCategoryDto> addSubCatList) {
		this.addSubCatList = addSubCatList;
	}
	/**
	 * @return the removeSubCatList
	 */
	public List<SubCategoryDto> getRemoveSubCatList() {
		return removeSubCatList;
	}
	/**
	 * @param removeSubCatList the removeSubCatList to set
	 */
	public void setRemoveSubCatList(List<SubCategoryDto> removeSubCatList) {
		this.removeSubCatList = removeSubCatList;
	}
	
	
}
