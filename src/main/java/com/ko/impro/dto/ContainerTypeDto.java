package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class ContainerTypeDto extends MaintenanceDto {

	private String containerTypeCode;
	private String containerTypeDescription;
	/**
	 * @return the containerTypeCode
	 */
	public String getContainerTypeCode() {
		return containerTypeCode;
	}
	/**
	 * @param containerTypeCode the containerTypeCode to set
	 */
	public void setContainerTypeCode(String containerTypeCode) {
		this.containerTypeCode = containerTypeCode;
	}
	/**
	 * @return the containerTypeDescription
	 */
	public String getContainerTypeDescription() {
		return containerTypeDescription;
	}
	/**
	 * @param containerTypeDescription the containerTypeDescription to set
	 */
	public void setContainerTypeDescription(String containerTypeDescription) {
		this.containerTypeDescription = containerTypeDescription;
	}
	
	
}
