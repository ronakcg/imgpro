package com.ko.impro.dto;

import java.util.List;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class ProductResponseDto {
	private String code;
	private String message;
	private PaginationDto pageDto;
	private List<ProductDto> productDtoList;
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the pageDto
	 */
	public PaginationDto getPageDto() {
		return pageDto;
	}
	/**
	 * @param pageDto the pageDto to set
	 */
	public void setPageDto(PaginationDto pageDto) {
		this.pageDto = pageDto;
	}
	/**
	 * @return the productDtoList
	 */
	public List<ProductDto> getProductDtoList() {
		return productDtoList;
	}
	/**
	 * @param productDtoList the productDtoList to set
	 */
	public void setProductDtoList(List<ProductDto> productDtoList) {
		this.productDtoList = productDtoList;
	}	
}
