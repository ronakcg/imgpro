package com.ko.impro.dto;

import java.util.List;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class SubSegmentDto extends MaintenanceDto {
	
	private String subSegmentCode;
	private String subSegmentDescription;
	
	private List<SegmentDto> segmentDtolist;
	private List<SegmentDto> removeSegmentDtoList;
	private List<SegmentDto> addSegmentDtoList;
	/**
	 * @return the subSegmentCode
	 */
	public String getSubSegmentCode() {
		return subSegmentCode;
	}
	/**
	 * @param subSegmentCode the subSegmentCode to set
	 */
	public void setSubSegmentCode(String subSegmentCode) {
		this.subSegmentCode = subSegmentCode;
	}
	/**
	 * @return the subSegmentDescription
	 */
	public String getSubSegmentDescription() {
		return subSegmentDescription;
	}
	/**
	 * @param subSegmentDescription the subSegmentDescription to set
	 */
	public void setSubSegmentDescription(String subSegmentDescription) {
		this.subSegmentDescription = subSegmentDescription;
	}
	/**
	 * @return the segmentDtolist
	 */
	public List<SegmentDto> getSegmentDtolist() {
		return segmentDtolist;
	}
	/**
	 * @param segmentDtolist the segmentDtolist to set
	 */
	public void setSegmentDtolist(List<SegmentDto> segmentDtolist) {
		this.segmentDtolist = segmentDtolist;
	}
	/**
	 * @return the removeSegmentDtoList
	 */
	public List<SegmentDto> getRemoveSegmentDtoList() {
		return removeSegmentDtoList;
	}
	/**
	 * @param removeSegmentDtoList the removeSegmentDtoList to set
	 */
	public void setRemoveSegmentDtoList(List<SegmentDto> removeSegmentDtoList) {
		this.removeSegmentDtoList = removeSegmentDtoList;
	}
	/**
	 * @return the addSegmentDtoList
	 */
	public List<SegmentDto> getAddSegmentDtoList() {
		return addSegmentDtoList;
	}
	/**
	 * @param addSegmentDtoList the addSegmentDtoList to set
	 */
	public void setAddSegmentDtoList(List<SegmentDto> addSegmentDtoList) {
		this.addSegmentDtoList = addSegmentDtoList;
	}
		
	
}
