package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class DeleteMessagesDataDto  extends MaintenanceDto {

	private String deleteMessageId;
	private String deleteMessageText;
	/**
	 * @return the deleteMessageId
	 */
	public String getDeleteMessageId() {
		return deleteMessageId;
	}
	/**
	 * @param deleteMessageId the deleteMessageId to set
	 */
	public void setDeleteMessageId(String deleteMessageId) {
		this.deleteMessageId = deleteMessageId;
	}
	/**
	 * @return the deleteMessageText
	 */
	public String getDeleteMessageText() {
		return deleteMessageText;
	}
	/**
	 * @param deleteMessageText the deleteMessageText to set
	 */
	public void setDeleteMessageText(String deleteMessageText) {
		this.deleteMessageText = deleteMessageText;
	}

	
}
