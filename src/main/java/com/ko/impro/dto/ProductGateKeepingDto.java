package com.ko.impro.dto;
/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class ProductGateKeepingDto {

	private boolean deleteFlag;
	private String deleteDate;
	private boolean completeFlag;
	private boolean approvedFlag;
	private String productCode;
	private String productId;

	private String productName;
	private String upc;
	private String upc12;
	private String shortProductName;
	private String dateMaintained;
	private String height;
	private String width;
	private String depth;
	private PackageDto packageDto;
	private PackageSizeDto packageSizeDto;
	private ContainerTypeDto containerTypeDto;
	private BrandDto brandDto;
	private ManufacturerDto manufacturerDto;
	private ExtendedProductDescDto extPrdDescDto;
	private PackageDto pkgdto;
	private FlavorDto flavorDto;
	private CategoryDto categoryDto;
	private SubCategoryDto subCategoryDto;
	private SegmentDto segmentDto;
	private SubSegmentDto subSegmentDto;
	private String bitMapName;
	private MissingImagesDto missingImagesDto;
	private ProductDto productDto;
	private EditImageDto editImageDto;
	private String imageType;


	
	/**
	 * @return the deleteFlag
	 */
	public boolean isDeleteFlag() {
		return deleteFlag;
	}
	/**
	 * @param deleteFlag the deleteFlag to set
	 */
	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	/**
	 * @return the deleteDate
	 */
	public String getDeleteDate() {
		return deleteDate;
	}
	/**
	 * @param deleteDate the deleteDate to set
	 */
	public void setDeleteDate(String deleteDate) {
		this.deleteDate = deleteDate;
	}
	/**
	 * @return the completeFlag
	 */
	public boolean isCompleteFlag() {
		return completeFlag;
	}
	/**
	 * @param completeFlag the completeFlag to set
	 */
	public void setCompleteFlag(boolean completeFlag) {
		this.completeFlag = completeFlag;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the upc
	 */
	public String getUpc() {
		return upc;
	}
	/**
	 * @param upc the upc to set
	 */
	public void setUpc(String upc) {
		this.upc = upc;
	}
	/**
	 * @return the upc12
	 */
	public String getUpc12() {
		return upc12;
	}
	/**
	 * @param upc12 the upc12 to set
	 */
	public void setUpc12(String upc12) {
		this.upc12 = upc12;
	}
	/**
	 * @return the shortProductName
	 */
	public String getShortProductName() {
		return shortProductName;
	}
	/**
	 * @param shortProductName the shortProductName to set
	 */
	public void setShortProductName(String shortProductName) {
		this.shortProductName = shortProductName;
	}
	/**
	 * @return the dateMaintained
	 */
	public String getDateMaintained() {
		return dateMaintained;
	}
	/**
	 * @param dateMaintained the dateMaintained to set
	 */
	public void setDateMaintained(String dateMaintained) {
		this.dateMaintained = dateMaintained;
	}
	/**
	 * @return the height
	 */
	public String getHeight() {
		return height;
	}
	/**
	 * @param height the height to set
	 */
	public void setHeight(String height) {
		this.height = height;
	}
	/**
	 * @return the width
	 */
	public String getWidth() {
		return width;
	}
	/**
	 * @param width the width to set
	 */
	public void setWidth(String width) {
		this.width = width;
	}
	/**
	 * @return the depth
	 */
	public String getDepth() {
		return depth;
	}
	/**
	 * @param depth the depth to set
	 */
	public void setDepth(String depth) {
		this.depth = depth;
	}
	/**
	 * @return the packageDto
	 */
	public PackageDto getPackageDto() {
		return packageDto;
	}
	/**
	 * @param packageDto the packageDto to set
	 */
	public void setPackageDto(PackageDto packageDto) {
		this.packageDto = packageDto;
	}
	/**
	 * @return the packageSizeDto
	 */
	public PackageSizeDto getPackageSizeDto() {
		return packageSizeDto;
	}
	/**
	 * @param packageSizeDto the packageSizeDto to set
	 */
	public void setPackageSizeDto(PackageSizeDto packageSizeDto) {
		this.packageSizeDto = packageSizeDto;
	}
	/**
	 * @return the containerTypeDto
	 */
	public ContainerTypeDto getContainerTypeDto() {
		return containerTypeDto;
	}
	/**
	 * @param containerTypeDto the containerTypeDto to set
	 */
	public void setContainerTypeDto(ContainerTypeDto containerTypeDto) {
		this.containerTypeDto = containerTypeDto;
	}
	/**
	 * @return the brandDto
	 */
	public BrandDto getBrandDto() {
		return brandDto;
	}
	/**
	 * @param brandDto the brandDto to set
	 */
	public void setBrandDto(BrandDto brandDto) {
		this.brandDto = brandDto;
	}
	/**
	 * @return the manufacturerDto
	 */
	public ManufacturerDto getManufacturerDto() {
		return manufacturerDto;
	}
	/**
	 * @param manufacturerDto the manufacturerDto to set
	 */
	public void setManufacturerDto(ManufacturerDto manufacturerDto) {
		this.manufacturerDto = manufacturerDto;
	}
	/**
	 * @return the extPrdDescDto
	 */
	public ExtendedProductDescDto getExtPrdDescDto() {
		return extPrdDescDto;
	}
	/**
	 * @param extPrdDescDto the extPrdDescDto to set
	 */
	public void setExtPrdDescDto(ExtendedProductDescDto extPrdDescDto) {
		this.extPrdDescDto = extPrdDescDto;
	}
	/**
	 * @return the pkgdto
	 */
	public PackageDto getPkgdto() {
		return pkgdto;
	}
	/**
	 * @param pkgdto the pkgdto to set
	 */
	public void setPkgdto(PackageDto pkgdto) {
		this.pkgdto = pkgdto;
	}
	/**
	 * @return the flavorDto
	 */
	public FlavorDto getFlavorDto() {
		return flavorDto;
	}
	/**
	 * @param flavorDto the flavorDto to set
	 */
	public void setFlavorDto(FlavorDto flavorDto) {
		this.flavorDto = flavorDto;
	}
	/**
	 * @return the categoryDto
	 */
	public CategoryDto getCategoryDto() {
		return categoryDto;
	}
	/**
	 * @param categoryDto the categoryDto to set
	 */
	public void setCategoryDto(CategoryDto categoryDto) {
		this.categoryDto = categoryDto;
	}
	/**
	 * @return the subCategoryDto
	 */
	public SubCategoryDto getSubCategoryDto() {
		return subCategoryDto;
	}
	/**
	 * @param subCategoryDto the subCategoryDto to set
	 */
	public void setSubCategoryDto(SubCategoryDto subCategoryDto) {
		this.subCategoryDto = subCategoryDto;
	}
	/**
	 * @return the segmentDto
	 */
	public SegmentDto getSegmentDto() {
		return segmentDto;
	}
	/**
	 * @param segmentDto the segmentDto to set
	 */
	public void setSegmentDto(SegmentDto segmentDto) {
		this.segmentDto = segmentDto;
	}
	/**
	 * @return the subSegmentDto
	 */
	public SubSegmentDto getSubSegmentDto() {
		return subSegmentDto;
	}
	/**
	 * @param subSegmentDto the subSegmentDto to set
	 */
	public void setSubSegmentDto(SubSegmentDto subSegmentDto) {
		this.subSegmentDto = subSegmentDto;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	/**
	 * @return the bitMapName
	 */
	public String getBitMapName() {
		return bitMapName;
	}
	/**
	 * @param bitMapName the bitMapName to set
	 */
	public void setBitMapName(String bitMapName) {
		this.bitMapName = bitMapName;
	}
	/**
	 * @return the missingImagesDto
	 */
	public MissingImagesDto getMissingImagesDto() {
		return missingImagesDto;
	}
	/**
	 * @param missingImagesDto the missingImagesDto to set
	 */
	public void setMissingImagesDto(MissingImagesDto missingImagesDto) {
		this.missingImagesDto = missingImagesDto;
	}
	/**
	 * @return the productDto
	 */
	public ProductDto getProductDto() {
		return productDto;
	}
	/**
	 * @param productDto the productDto to set
	 */
	public void setProductDto(ProductDto productDto) {
		this.productDto = productDto;
	}
	/**
	 * @return the editImageDto
	 */
	public EditImageDto getEditImageDto() {
		return editImageDto;
	}
	/**
	 * @param editImageDto the editImageDto to set
	 */
	public void setEditImageDto(EditImageDto editImageDto) {
		this.editImageDto = editImageDto;
	}
	/**
	 * @return the approvedFlag
	 */
	public boolean isApprovedFlag() {
		return approvedFlag;
	}
	/**
	 * @param approvedFlag the approvedFlag to set
	 */
	public void setApprovedFlag(boolean approvedFlag) {
		this.approvedFlag = approvedFlag;
	}
	/**
	 * @return the imageType
	 */
	public String getImageType() {
		return imageType;
	}
	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	
	
	
	
}
