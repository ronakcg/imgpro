package com.ko.impro.dto;

import java.util.List;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class ArchivedProductResponseDto {
	private String code;
	private String message;
	private List<ArchivedProductDto> archiveProductDtoList;
	private PaginationDto pageDto;
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the archiveProductDtoList
	 */
	public List<ArchivedProductDto> getArchiveProductDtoList() {
		return archiveProductDtoList;
	}
	/**
	 * @param archiveProductDtoList the archiveProductDtoList to set
	 */
	public void setArchiveProductDtoList(List<ArchivedProductDto> archiveProductDtoList) {
		this.archiveProductDtoList = archiveProductDtoList;
	}
	/**
	 * @return the pageDto
	 */
	public PaginationDto getPageDto() {
		return pageDto;
	}
	/**
	 * @param pageDto the pageDto to set
	 */
	public void setPageDto(PaginationDto pageDto) {
		this.pageDto = pageDto;
	}
	
} 