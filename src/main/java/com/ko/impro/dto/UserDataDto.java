package com.ko.impro.dto;

import java.util.Date;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class UserDataDto {

	private String userId;
	private String roleId;
	private String userStatus;
	private String userLastName;
	private String userFirstName;
	private String userPhoneNumber;
	private Date userDownLoadDateTime;
	private Date userLastLogInDate;
	private Date userLastLogOutDate;
	private String userEmailAddress;
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the roleId
	 */
	public String getRoleId() {
		return roleId;
	}
	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	/**
	 * @return the userStatus
	 */
	public String getUserStatus() {
		return userStatus;
	}
	/**
	 * @param userStatus the userStatus to set
	 */
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	/**
	 * @return the userLastName
	 */
	public String getUserLastName() {
		return userLastName;
	}
	/**
	 * @param userLastName the userLastName to set
	 */
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	/**
	 * @return the userFirstName
	 */
	public String getUserFirstName() {
		return userFirstName;
	}
	/**
	 * @param userFirstName the userFirstName to set
	 */
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	/**
	 * @return the userPhoneNumber
	 */
	public String getUserPhoneNumber() {
		return userPhoneNumber;
	}
	/**
	 * @param userPhoneNumber the userPhoneNumber to set
	 */
	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}
	/**
	 * @return the userDownLoadDateTime
	 */
	public Date getUserDownLoadDateTime() {
		return userDownLoadDateTime;
	}
	/**
	 * @param userDownLoadDateTime the userDownLoadDateTime to set
	 */
	public void setUserDownLoadDateTime(Date userDownLoadDateTime) {
		this.userDownLoadDateTime = userDownLoadDateTime;
	}
	/**
	 * @return the userLastLogInDate
	 */
	public Date getUserLastLogInDate() {
		return userLastLogInDate;
	}
	/**
	 * @param userLastLogInDate the userLastLogInDate to set
	 */
	public void setUserLastLogInDate(Date userLastLogInDate) {
		this.userLastLogInDate = userLastLogInDate;
	}
	/**
	 * @return the userLastLogOutDate
	 */
	public Date getUserLastLogOutDate() {
		return userLastLogOutDate;
	}
	/**
	 * @param userLastLogOutDate the userLastLogOutDate to set
	 */
	public void setUserLastLogOutDate(Date userLastLogOutDate) {
		this.userLastLogOutDate = userLastLogOutDate;
	}
	/**
	 * @return the userEmailAddress
	 */
	public String getUserEmailAddress() {
		return userEmailAddress;
	}
	/**
	 * @param userEmailAddress the userEmailAddress to set
	 */
	public void setUserEmailAddress(String userEmailAddress) {
		this.userEmailAddress = userEmailAddress;
	}
		
	
}
