package com.ko.impro.dto;

import java.util.List;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class MultiProductDto {
	private ProductDto productdto;
	private List<ProductDto>  productdtoList;
	/**
	 * @return the productdto
	 */
	public ProductDto getProductdto() {
		return productdto;
	}
	/**
	 * @param productdto the productdto to set
	 */
	public void setProductdto(ProductDto productdto) {
		this.productdto = productdto;
	}
	/**
	 * @return the productdtoList
	 */
	public List<ProductDto> getProductdtoList() {
		return productdtoList;
	}
	/**
	 * @param productdtoList the productdtoList to set
	 */
	public void setProductdtoList(List<ProductDto> productdtoList) {
		this.productdtoList = productdtoList;
	}
	
	
}
