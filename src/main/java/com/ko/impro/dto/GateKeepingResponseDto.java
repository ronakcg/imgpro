package com.ko.impro.dto;

import java.util.List;

/**
 * @author swshedge 
 * DTO class have setters and getters for Data transfer between various layers
 */
public class GateKeepingResponseDto {
	
	private String code;
	private String message;
	private List<ProductGateKeepingDto> gateKeepingDtoList;
	private ProductGateKeepingDto gateKeepingDto;
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the gateKeepingDtoList
	 */
	public List<ProductGateKeepingDto> getGateKeepingDtoList() {
		return gateKeepingDtoList;
	}
	/**
	 * @param gateKeepingDtoList the gateKeepingDtoList to set
	 */
	public void setGateKeepingDtoList(List<ProductGateKeepingDto> gateKeepingDtoList) {
		this.gateKeepingDtoList = gateKeepingDtoList;
	}
	/**
	 * @return the gateKeepingDto
	 */
	public ProductGateKeepingDto getGateKeepingDto() {
		return gateKeepingDto;
	}
	/**
	 * @param gateKeepingDto the gateKeepingDto to set
	 */
	public void setGateKeepingDto(ProductGateKeepingDto gateKeepingDto) {
		this.gateKeepingDto = gateKeepingDto;
	}
	
	
}
