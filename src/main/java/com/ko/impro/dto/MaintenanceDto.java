package com.ko.impro.dto;

/**
 * @author swshedge
 * DTO class have setters and getters for Data transfer between various layers
 */
public class MaintenanceDto {
	private int isInactiveFlagChecked;
	private String oldValue;
	private String oldCode;
	private int counter;
	
	/**
	 * @return the isInactiveFlagChecked
	 */
	public int getIsInactiveFlagChecked() {
		return isInactiveFlagChecked;
	}
	/**
	 * @param isInactiveFlagChecked the isInactiveFlagChecked to set
	 */
	public void setIsInactiveFlagChecked(int isInactiveFlagChecked) {
		this.isInactiveFlagChecked = isInactiveFlagChecked;
	}
	/**
	 * @return the oldValue
	 */
	public String getOldValue() {
		return oldValue;
	}
	/**
	 * @param oldValue the oldValue to set
	 */
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}
	/**
	 * @return the oldCode
	 */
	public String getOldCode() {
		return oldCode;
	}
	/**
	 * @param oldCode the oldCode to set
	 */
	public void setOldCode(String oldCode) {
		this.oldCode = oldCode;
	}
	/**
	 * @return the counter
	 */
	public int getCounter() {
		return counter;
	}
	/**
	 * @param counter the counter to set
	 */
	public void setCounter(int counter) {
		this.counter = counter;
	}
	
	
}
