package com.ko.impro.config;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;

/**
 * @author swshedge HibernateUtil set up a shared Hibernate SessionFactory in a
 *         Spring application context
 */

public class HibernateUtil {

	/**
	 * Retrieve current session using entity manager
	 * 
	 * @param manager
	 * @return Session
	 */
	public static Session getCurrentSession(EntityManager manager) {
		return getSessionFactory(manager).getCurrentSession();
	}

	/**
	 * Close session
	 * 
	 * @param session
	 */
	public static void closeSession(Session session) {
		if (session != null && session.isOpen()) {
			session.close();
		}
	}

	/**
	 * @param em
	 * @return SessionFactory
	 */
	public static SessionFactory getSessionFactory(EntityManager em) {
		return ((Session) em.getDelegate()).getSessionFactory();
	}
}
