package com.ko.impro.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author swshedge
 * DataBaseConfig represents database connection properties 
 * and Hibernate Configuration 
 */
@EnableWebMvc
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFacory", transactionManagerRef = "transactionManager", basePackages = {
		"com.ko.impro.dao" })
@ComponentScan(basePackages = "com.ko.impro")
@PropertySource("classpath:${life.cycle}-jdbc.properties")
@Import(SwaggerConfig.class)
public class DataBaseConfig implements WebMvcConfigurer{
	
	/**
	 * reference of Environment
	 */
	@Autowired
	private Environment env;
	
	/**
	 * Method to configure a shared JPA EntityManagerFactory 
	 * in a Spring application context
	 * @return LocalContainerEntityManagerFactoryBean 
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFacory() {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(dataSource());
		factory.setPackagesToScan(new String[] { "com.ko.impro.model" });
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setJpaProperties(additionalProperties());
		return factory;
	}

	
	/**
	 * Method to setup credential required for JDBC connection
	 * @return DataSource
	 */
	@Bean
	public DataSource dataSource() {
		String driverName = env.getProperty("jdbc.driver.classname");
		String url = env.getProperty("jdbc.url");
		String userId = env.getProperty("jdbc.username");
		String password = env.getProperty("jdbc.password");

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(driverName);
		dataSource.setUrl(url);
		dataSource.setUsername(userId);
		dataSource.setPassword(password);
		return dataSource;
	}

	/**
	 * @return PersistenceExceptionTranslationPostProcessor
	 */
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	
	/**
	 * @return HibernateExceptionTranslator
	 */
	@Bean
	public HibernateExceptionTranslator hibernateExceptionTranslator() {
		return new HibernateExceptionTranslator();
	}

	/**
	 * Method to configure hibernate properties to Properties files
	 * @return Properties
	 */
	private Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServer2012Dialect");
		properties.setProperty("hibernate.show_sql", "true");

		 
		properties.setProperty("org.hibernate.fetchSize", "1000");
		properties.setProperty("hibernate.jdbc.batch_size", "50");
		
		return properties;
	}

	/**
	 * @return PlatformTransactionManager
	 */
	@Bean
	public PlatformTransactionManager transactionManager() {
		
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFacory().getObject());
		return txManager;
	}
	
	
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.config.annotation
	 * .WebMvcConfigurer#addResourceHandlers
	 * (org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry)
	 */
	@Override 
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
	
	
	/**
	 * Method to set up a shared Hibernate SessionFactory 
	 * in a Spring application context
	 * @return LocalSessionFactoryBean
	 */
	@Bean
    public LocalSessionFactoryBean sessionFactoryBean(){	   
        final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { "com.ko.impro.model" });
        sessionFactory.setHibernateProperties(this.additionalProperties());
        return sessionFactory;
    }
}
