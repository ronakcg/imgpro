package com.ko.impro.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;


/**
 * @author hlekshmy
 * Class used to initialize the DispatcherServlet,EntityClass 
 * and DB configurations
 */
public class ImproAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer
		implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext container) throws ServletException {

		AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
		// Configuration for Spring Data JPA
		appContext.register(DataBaseConfig.class);
		appContext.register(CommonsMultipartResolver.class);
		// Configuration for JSON Web Token for REST service security.
		//appContext.register(JWTSecurityConfig.class);

		//Spring Dispatcher servlet configuration
		ServletRegistration.Dynamic dispatcher = container.addServlet("SpringDispatcher",
				new DispatcherServlet(appContext));
		container.addListener(new ContextLoaderListener(appContext));
		container.setAttribute("allowCasualMultipartParsing", true);
		
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/services/*");
		// Configuration for Swagger API
		dispatcher.addMapping("/configuration/ui");
		dispatcher.addMapping("/configuration/security");
		dispatcher.addMapping("/swagger-resources");
		dispatcher.addMapping("/v2/api-docs");
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return ArrayUtils.EMPTY_CLASS_ARRAY;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return ArrayUtils.EMPTY_CLASS_ARRAY;
	}

	@Override
	protected String[] getServletMappings() {
		return ArrayUtils.EMPTY_STRING_ARRAY;
	}

	//@Override
	/*protected Filter[] getServletFilters() {
		return new Filter[] { new AuthenticationTokenFilter() };
	}*/

}
