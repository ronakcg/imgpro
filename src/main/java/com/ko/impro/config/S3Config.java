package com.ko.impro.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
 
/**
 * @author hlekshmy
 * Configurastion class for S3 bucket
 *
 */
@Configuration
@PropertySource("classpath:${life.cycle}-image.properties")
public class S3Config {
	
	/**
	 * Access ID
	 */
	@Value("${aws.access_key_id}")
	private String awsId;
 
	/**
	 * Access key
	 */
	@Value("${aws.secret_access_key}")
	private String awsKey;
	
	/**
	 * Region name
	 */
	@Value("${s3.region.product}")
	private String region;
 
	/**
	 * Method for authenticating to the S3 region
	 * @return AmazonS3 object
	 */
	@Bean
	public AmazonS3 s3client() {
		
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsId, awsKey);
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
								.withRegion(Regions.fromName(region))
		                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
		                        .build();
		
		return s3Client;
	}
}