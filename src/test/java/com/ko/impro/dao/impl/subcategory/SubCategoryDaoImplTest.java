package com.ko.impro.dao.impl.subcategory;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.subcategory.SubCategoryDao;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.exception.ImproException;
/**
 * Test Cases of SubCategoryDaoImpl methods
 */
public class SubCategoryDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SubCategoryDaoImplTest.class);
	
	/**
	 * Reference of SubCategoryDao
	 */
	@Autowired(required = true)
	SubCategoryDao subCategoryDao;
	
	/**
	 * Test case to Load SubCategory
	 */
	@Test
	public void testLoadSubCategory() {
		try {
			List<SubCategoryDto> cat=subCategoryDao.loadSubCategory();
			
			assertTrue(!cat.isEmpty(),"Records Present");
			for(SubCategoryDto dto:cat) {
				LOG.info(dto.getSubCategoryDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to Load SubCategoryList
	 */
	@Test
	public void testLoadSubCategoryListOfString() {
		try {
			List<String> categoryList=new ArrayList<String>();
			categoryList.add("1");
			categoryList.add("2");

			List<SubCategoryDto> subCat=subCategoryDao.loadSubCategory(categoryList);
			
			assertTrue(!subCat.isEmpty(),"Records Present");
			for(SubCategoryDto dto:subCat) {
				LOG.info(dto.getSubCategoryDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to Add SubCategory
	 */
	@Test
	public void testAddSubCategory() throws ImproException {		//(Working)
		SubCategoryDto subcategoryDto = new SubCategoryDto();
		
		List<CategoryDto> categoryDtolist = new ArrayList<CategoryDto>();
		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setCategoryCode("1");
		categoryDtolist.add(categoryDto);
		
		subcategoryDto.setSubCategoryDescription("NewTestCategory");
		subcategoryDto.setCategoryDtolist(categoryDtolist);
		try {
			int newSubCatCode = subCategoryDao.addSubCategory(subcategoryDto);
			if (LOG.isInfoEnabled()) {
			LOG.info("generatedSubCategoryCode "+newSubCatCode);
			}
			if(newSubCatCode!=0) {
				subCategoryDao.addCategoyMapping(subcategoryDto, newSubCatCode);
			}			
		}catch (ImproException e) {
			fail(loadingMsg);
		} 
		
	}

	/**
	 * Test case to Delete SubCategory
	 */
	@Test
	public void testDeleteSubCategory() {		//(Working)
		try {
			SubCategoryDto subcategoryDto = new SubCategoryDto();
			subcategoryDto.setSubCategoryCode("1");
			String msg = subCategoryDao.deleteSubCategory(subcategoryDto);
			LOG.info(msg);
		} catch (ImproException e) {
			fail(loadingMsg);
		} 
	}
	
	/**
	 * Test case to Edit SubCategory
	 */
	@Test
	public void testEditSubCategory1() throws ImproException {		
		SubCategoryDto subcategoryDto = new SubCategoryDto();
		subcategoryDto.setSubCategoryCode("6");
		subcategoryDto.setSubCategoryDescription("EditTestSubCategory");		
		try {	
			String msg = subCategoryDao.editSubCategory(subcategoryDto);
			LOG.info(msg);		
		} catch (ImproException e) {
			fail(loadingMsg);
		} 

	}
		
	/**
	 * Test case to Edit SubCategory
	 */
	@Test
	public void testEditSubCategory2() throws ImproException {		// Add new Categories to particular SubCategory		(Working)
		SubCategoryDto subcategoryDto = new SubCategoryDto();
		subcategoryDto.setSubCategoryCode("6");
		subcategoryDto.setSubCategoryDescription("EditTestSubCategory");

		List<CategoryDto> categoryDtolist = new ArrayList<CategoryDto>();
		CategoryDto categoryDto1 = new CategoryDto();		
		categoryDto1.setCategoryCode("2");
		
		categoryDtolist.add(categoryDto1);
	
		subcategoryDto.setAddCategoryDtolist(categoryDtolist);
		try {
			String msg = subCategoryDao.editSubCategory(subcategoryDto);
			LOG.info(msg);			
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}
	
	/**
	 * Test case to Edit SubCategory
	 */
	@Test
	public void testEditSubCategory3() throws ImproException {		// Remove existing Categories from particular SubCategory	(Working)
		SubCategoryDto subcategoryDto = new SubCategoryDto();
		subcategoryDto.setSubCategoryCode("6");
		subcategoryDto.setSubCategoryDescription("EditTestSubCategory");

		List<CategoryDto> categoryDtolist = new ArrayList<CategoryDto>();
		CategoryDto categoryDto1 = new CategoryDto();		
		categoryDto1.setCategoryCode("2");
		CategoryDto categoryDto2 = new CategoryDto();
		categoryDto2.setCategoryCode("1");
		categoryDtolist.add(categoryDto1);
		categoryDtolist.add(categoryDto2);
		
		subcategoryDto.setRemoveCategoryDtoList(categoryDtolist);
		try {
			String msg = subCategoryDao.editSubCategory(subcategoryDto);
			LOG.info(msg);			
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	
	/**
	 * Test case to Load SubCategory By Id
	 */
	@Test
	public void testLoadSubCategoryById() {			//Working
		try {
			SubCategoryDto subcat = subCategoryDao.loadSubCategory(1);
			if (LOG.isInfoEnabled()) {
			LOG.info("SubCategory Code "+subcat.getSubCategoryCode());
			LOG.info("SubCategory Description "+subcat.getSubCategoryDescription());
			LOG.info("Cateogries Associated SubCateogry");
			}
			for(CategoryDto dto : subcat.getCategoryDtolist()) {
				LOG.info(dto.getCategoryCode());
				LOG.info(dto.getCategoryDescription());
			}
			
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	/**
	 * Test case to Check Duplicate
	 */
	@Test
	public void testCheckDuplicate() {		//If Duplicate value Present 	(Working)
		try {
			String name="SSD";
			boolean flag = subCategoryDao.checkDuplicateSubCategory(name);
			if(flag) {
			LOG.info("Duplicate Present ");
			}
			else {
				LOG.info("Duplicate Not Present ");
			}	
			assertTrue(flag,"Flag: True");
		} catch(ImproException e) {
			fail("CheckDuplicate failed");
		}
	}
	
	/**
	 * Test case to check Presence In Product For Category
	 */
	@Test
	public void testcheckPresenceInProductForCategory() throws NumberFormatException, ImproException	//(Working)
	{
		SubCategoryDto subcategoryDto = new SubCategoryDto();
		subcategoryDto.setSubCategoryCode("1");
		boolean flag = subCategoryDao.checkPresenceInProductForCategory(Integer.parseInt(subcategoryDto.getSubCategoryCode()));
		if(flag)
		{ 
			LOG.info("Combiantion of entered SubcatagoryCode and CategoryCode present in Products table");
		} else {
			LOG.info("Combiantion of entered SubcatagoryCode and CategoryCode aren't present in Products table");
		}
		assertTrue(flag,"Flag: True");
	}
	
	/**
	 * Test case to check Presence In Product For Flavor
	 */
	@Test
	public void testcheckPresenceInProductForFlavor() throws NumberFormatException, ImproException		//(Working)
	{
		SubCategoryDto subcategoryDto = new SubCategoryDto();
		subcategoryDto.setSubCategoryCode("1");
		boolean flag = subCategoryDao.checkPresenceInProductForFlavor(Integer.parseInt(subcategoryDto.getSubCategoryCode()));
		if(flag)
		{ 
			LOG.info("Combiantion of entered SubcatagoryCode and FlavorCode present in Products table");
		} else {
			LOG.info("Combiantion of entered SubcatagoryCode and FlavorCode aren't present in Products table");
		}
		assertTrue(flag,"Flag: True");
	}
	
	/**
	 * Test case to check Presence In Product For Segment
	 */
	@Test
	public void testcheckPresenceInProductForSegment() throws NumberFormatException, ImproException {		//(Working)
		SubCategoryDto subcategoryDto = new SubCategoryDto();
		subcategoryDto.setSubCategoryCode("1");
		boolean flag = subCategoryDao.checkPresenceInProductForSegment(Integer.parseInt(subcategoryDto.getSubCategoryCode()));
		if(flag)
		{	
			LOG.info("Combiantion of entered SubcatagoryCode and SegementCode present in Products table");
		} else {
			LOG.info("Combiantion of entered SubcatagoryCode and SegementCode aren't present in Products table");
		}
		assertTrue(flag,"Flag: True");
	}
	
	/**
	 * Test case to load categories
	 */
	@Test
	public void testcheckCombinationInProduct() throws NumberFormatException, ImproException {		//(Working)
		SubCategoryDto subcategoryDto = new SubCategoryDto();
		List<String> removeList = new ArrayList<String>();
		
		removeList.add("1");
		removeList.add("2");
		
		subcategoryDto.setSubCategoryCode("3");
		
		boolean flag = subCategoryDao.checkPresenceInProduct(removeList,Integer.parseInt(subcategoryDto.getSubCategoryCode()));
		if(flag)
		{  
			LOG.info("Combination Present in Products");
		} else {
			LOG.info("Combination isn't Present in Products");
		}
		assertTrue(flag,"Flag: True");
	}
}
