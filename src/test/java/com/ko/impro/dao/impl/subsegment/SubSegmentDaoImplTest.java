package com.ko.impro.dao.impl.subsegment;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.subsegment.SubSegmentDao;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of SubSegmentDao methods
 */
public class SubSegmentDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SubSegmentDaoImplTest.class);

	/**
	 * Reference of SubSegmentDao
	 */
	@Autowired(required = true)
	private SubSegmentDao subSegmentDao;

	/**
	 * Test case to load SubSegment
	 */
	@Test
	public void testLoadSubSegment() {
		try {
			List<SubSegmentDto> segmentList = subSegmentDao.loadSubSegment();

			assertTrue(!segmentList.isEmpty(), "Records Present");
			for (SubSegmentDto dto : segmentList) {
				LOG.info(dto.getSubSegmentDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	/**
	 * Test case to load SubSegment ListOfString
	 */
	@Test
	public void testLoadSubSegmentsListOfString() {
		try {
			List<String> segmentList = new ArrayList<String>();
			segmentList.add("1");
			segmentList.add("2");
			segmentList.add("5");
			List<SubSegmentDto> subSegmentList = subSegmentDao.loadSubSegment(segmentList);

			assertTrue(!segmentList.isEmpty(), "Records Present");
			for (SubSegmentDto dto : subSegmentList) {
				LOG.info(dto.getSubSegmentDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	/**
	 * Test case to Add SubSegment
	 */
	@Test
	public void testAddSubSegment() throws ImproException {
		try {
			SubSegmentDto subSegmentDto = new SubSegmentDto();
			List<SegmentDto> segmentDtoList = new ArrayList<SegmentDto>();
			SegmentDto segmentDto = new SegmentDto();
			segmentDto.setSegmentCode("4");
			segmentDto.setSegmentDescription("Segment");
			segmentDtoList.add(segmentDto);

			subSegmentDto.setSubSegmentCode("2");
			subSegmentDto.setSubSegmentDescription("New-SubSegment");
			subSegmentDto.setSegmentDtolist(segmentDtoList);
			subSegmentDao.addSubSegment(subSegmentDto);
			LOG.info("added...!");
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to Edit SubSegment
	 */
	@Test
	public void testEditSubSegment() throws ImproException {

		try {
			SubSegmentDto subSegmentDto = new SubSegmentDto();
			SegmentDto segmentDto = new SegmentDto();
			// SegmentDto removeSegmentDto = new SegmentDto();

			List<SegmentDto> segmentDtolist = new ArrayList<SegmentDto>();
			segmentDto.setSegmentCode("5");
			segmentDtolist.add(segmentDto);

			/*
			 * List<SegmentDto> removeSegmentDtolist = new ArrayList<SegmentDto>();
			 * removeSegmentDto.setSegmentCode("4");
			 * removeSegmentDtolist.add(removeSegmentDto);
			 */

			subSegmentDto.setSubSegmentCode("25");
			subSegmentDto.setSubSegmentDescription("updatedNew");
			subSegmentDto.setSegmentDtolist(segmentDtolist);
			// subSegmentDto.setRemoveSegmentDtoList(removeSegmentDtolist);

			subSegmentDao.editSubSegment(subSegmentDto);
			LOG.info("updated...!");
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to Delete SubSegment
	 */
	@Test
	public void testDeleteSubSegment() throws ImproException {

		try {
			SubSegmentDto subSegmentDto = new SubSegmentDto();
			subSegmentDto.setSubSegmentCode("26");
			String msg = subSegmentDao.deleteSubSegment(subSegmentDto);
			LOG.info("deleted...!" + msg);
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	/**
	 * Test case to load SubSegment By ID
	 */
	@Test
	public void testLoadSubSegmentById() {
		try {
			SubSegmentDto ssd = subSegmentDao.loadSubSegment(15);
			if (LOG.isInfoEnabled()) {
				LOG.info("SubSegmentCode " + ssd.getSubSegmentCode());
				LOG.info("SubSegment Description " + ssd.getSubSegmentDescription());
				LOG.info("Segment Associated with SubSegment");
			}
			for (SegmentDto dto : ssd.getSegmentDtolist()) {
				LOG.info(dto.getSegmentCode());
				LOG.info(dto.getSegmentDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to CheckDuplicate
	 */
	@Test
	public void testCheckDuplicate() {
		try {
			String subSegmentDesc = "Spring";
			boolean flag = subSegmentDao.checkDuplicateSubSegment(subSegmentDesc);
			if (flag) {
				LOG.info("Duplicate Present ");
			} else {
				LOG.info("Duplicate Not Present ");
			}
			assertTrue(flag, "Flag Status");

		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("checkDuplicate failed");
		}
	}

	/*@Test
	public void testCheckPresenceInProduct() {
		try {
			int subSegmentCode = 15;
			boolean flag = SubSegmentDao.checkPresenceInProduct(subSegmentCode);
			if (flag)
				LOG.info("SubSegment present in Product table :" + flag);
			else
				LOG.info("SubSegment present in Product table :" + flag);
			assertTrue(flag);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("Failed to check");
		}
	}*/

}
