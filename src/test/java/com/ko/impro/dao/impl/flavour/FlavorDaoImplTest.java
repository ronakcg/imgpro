package com.ko.impro.dao.impl.flavour;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.flavour.FlavorDao;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of FlavorDaoImpl methods
 */
public class FlavorDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(FlavorDaoImplTest.class);	
	
	/**
	 * Reference of FlavorDao
	 */
	@Autowired(required = true)
	private FlavorDao flavorDao;
	
	/**
	 * Test case to load flavors
	 */
	@Test
	public void testLoadFlavour() {		//(Working)
		try {
			List<FlavorDto> flavorList=flavorDao.loadFlavor();		
			assertTrue(!flavorList.isEmpty(),"Check for empty list");
			for(FlavorDto dto:flavorList) {
				LOG.info(dto.getFlavorDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
		
	}
		
	
	/**
	 * Test case to load flavors using sub category list
	 */
	@Test
	public void testLoadFlavor() {		//(Working)
		try {
			List<String> subCatIdList=new ArrayList<String>();
			subCatIdList.add("1");
			subCatIdList.add("2");
			subCatIdList.add("5");
			List<FlavorDto> flavorList=flavorDao.loadFlavor(subCatIdList);			
			assertTrue(!flavorList.isEmpty(),"Check for empty list");
			for(FlavorDto dto:flavorList) {
				LOG.info(dto.getFlavorDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	
	/**
	 * Test case to add flavor
	 */
	@Test
	public void testAddFlavor() {			//(Working)
		try {
			FlavorDto flavorDto = new FlavorDto();
			
			SubCategoryDto subDto = new SubCategoryDto();
			subDto.setSubCategoryCode("2");
			
			List<SubCategoryDto> subCategoryList = new ArrayList<SubCategoryDto>();
			subCategoryList.add(subDto);
			
			flavorDto.setFlavorDescription("Test Flavor1");
			flavorDto.setSubCategoryList(subCategoryList);
			
			int flavorCode =flavorDao.addFlavor(flavorDto);
			flavorDto.setFlavorCode(String.valueOf(flavorCode));
			flavorDao.addFlavorSubCategory(flavorDto);
			
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	
	/**
	 * Test case to edit flavor
	 */
	@Test
	public void testEditFlavor1() {		//Make Inactiveflag  to 'Y'(parent & child tables) if not present in products
		try {
			FlavorDto dto = new FlavorDto();
			dto.setFlavorCode("70");
			dto.setFlavorDescription("TestFlavor");
			
			List<SubCategoryDto> subCatDtoList = new ArrayList<SubCategoryDto>();
			List<SubCategoryDto> removeDtoList = new ArrayList<SubCategoryDto>();
			
			dto.setAddSubCatList(subCatDtoList);
			dto.setRemoveSubCatList(removeDtoList);
			
			String msg = flavorDao.editFlavor(dto);
			LOG.info(msg);
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	
	/**
	 * Test case to edit flavor
	 */
	@Test
	public void testEditFlavor2() {		//Make Inactive flag'N' for parent & impro_category_Subcategory table 	
		try {
			FlavorDto dto = new FlavorDto();
			dto.setFlavorCode("70");
			dto.setFlavorDescription("TestFlavor");
			
			List<SubCategoryDto> addSubCatDtoList = new ArrayList<SubCategoryDto>();
			
			List<SubCategoryDto> rmvSubCatDtoList = new ArrayList<SubCategoryDto>();
			
			dto.setAddSubCatList(addSubCatDtoList);
			dto.setRemoveSubCatList(rmvSubCatDtoList);
			
			String msg = flavorDao.editFlavor(dto);
			LOG.info(msg);
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	
	/**
	 * Test case to edit flavor
	 */
	@Test
	public void testEditFlavor3() {		// Add new SubCategories to particular Flavor (Working) 	
		try {
			FlavorDto dto = new FlavorDto();
			dto.setFlavorCode("70");
			dto.setFlavorDescription("TestFlavor");
			
			List<SubCategoryDto> addSubCatDtoList = new ArrayList<SubCategoryDto>();
			SubCategoryDto addSubCatDto = new SubCategoryDto();
			addSubCatDto.setSubCategoryCode("1");
			addSubCatDtoList.add(addSubCatDto);
			
			List<SubCategoryDto> removeDtoList = new ArrayList<SubCategoryDto>();
			
			dto.setAddSubCatList(addSubCatDtoList);
			dto.setRemoveSubCatList(removeDtoList);
			
			String msg = flavorDao.editFlavor(dto);
			LOG.info(msg);
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	
	/**
	 * Test case to  edit flavor
	 */
	@Test
	public void testEditFlavor4() {		// Remove existing SubCategories to particular Flavor		(Working) 	
		try {
			FlavorDto dto = new FlavorDto();
			dto.setFlavorCode("70");
			dto.setFlavorDescription("TestFlavor");
			
			List<SubCategoryDto> addSubCatDtoList = new ArrayList<SubCategoryDto>();
		
			List<SubCategoryDto> rmvSubCatDtoList = new ArrayList<SubCategoryDto>();
			SubCategoryDto removeSubCatDto = new SubCategoryDto();
			removeSubCatDto.setSubCategoryCode("1");
			rmvSubCatDtoList.add(removeSubCatDto);
			
			dto.setAddSubCatList(addSubCatDtoList);
			dto.setRemoveSubCatList(rmvSubCatDtoList);
			
			String msg = flavorDao.editFlavor(dto);
			LOG.info(msg);
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	
	/**
	 * Test case to  edit flavor
	 */
	@Test
	public void testEditFlavor5() {		//If no activity happened on Inactive Or DeleteFlag		(Working) 	
		try {
			FlavorDto dto = new FlavorDto();
			dto.setFlavorCode("70");
			dto.setFlavorDescription("TestEditFlavor5");
			
			List<SubCategoryDto> addDtoList = new ArrayList<SubCategoryDto>();
			List<SubCategoryDto> removeDtoList = new ArrayList<SubCategoryDto>();

			dto.setAddSubCatList(addDtoList);
			dto.setRemoveSubCatList(removeDtoList);
			
			String msg = flavorDao.editFlavor(dto);
			LOG.info(msg);
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	
	/**
	 * Test case to delete flavor
	 */
	@Test
	public void testDeleteFlavor1() {	//If Flavor present in Products table, Delete Flag remains unchanged
		try {
			FlavorDto dto = new FlavorDto();
			dto.setFlavorCode("70");
			String msg;
			msg = flavorDao.deleteFlavor(dto);
			LOG.info(msg);	
		} catch (ImproException e) {
			fail(loadingMsg);
		}	
	}
	
	
	/**
	 * Test case to check for duplicate flavors
	 */
	@Test 
	public void testcheckDuplicateFlavor1() {	//If Duplicate Value Present	(Working)
		try {
			String flavorDesc = "Chocolate";
			boolean flag = flavorDao.checkDuplicateFlavor(flavorDesc);
			if(flag)
			{
				LOG.info("Duplicate Value Present");
			} else {
				LOG.info("No Duplicate Found");
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	
	/**
	 * Test case to check for duplicate flavors
	 */
	@Test 
	public void testcheckDuplicateFlavor2() {	//If no Duplicate Value found		(Working)
		try {
			String flavorDesc = "TestFlavor1";
			boolean flag = flavorDao.checkDuplicateFlavor(flavorDesc);
			if(flag)
			{
				LOG.info("Duplicate Value Present");
			} else {
				LOG.info("No Duplicate Found");
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	
	/**
	 * Test case to load flavor width using flavorCode
	 */
	@Test
	public void testLoadFlavorWithId() {		//(Working)
		try {
			FlavorDto dto = flavorDao.loadFlavor(11);
			List<SubCategoryDto> dtoList = dto.getSubCategoryList();
			LOG.info(dto.getFlavorCode());
			LOG.info(dto.getFlavorDescription());

			for(SubCategoryDto subDto:dtoList) {
				LOG.info(subDto.getSubCategoryCode());
				LOG.info(subDto.getSubCategoryDescription());
			}
		}catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	
	/**
	 * Test case to check SubcatagoryCode and FlavorCode presence In productTable
	 */
	@Test
	public void testcheckPresenceInProductForFlavor() {		//(Working)
		try {
			FlavorDto flavorDto = new FlavorDto();
			flavorDto.setFlavorCode("11");
			boolean flag = flavorDao.checkPresenceInProductForFlavor(Integer.parseInt(flavorDto.getFlavorCode()));
			if(flag)
			{
				LOG.info("Combiantion of entered SubcatagoryCode and FlavorCode present in Products table");
			} else {
				LOG.info("Combiantion of entered SubcatagoryCode and FlavorCode aren't present in Products table");
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	
	/**
	 * Test case to check presence product
	 */
	@Test
	public void testcheckPresenceInProduct() {		//(Working)
		try {
			FlavorDto flavorDto = new FlavorDto();
			List<String> removeList = new ArrayList<String>();
			removeList.add("1");
			removeList.add("2");
			flavorDto.setFlavorCode("11");
			boolean flag = flavorDao.checkPresenceInProduct(removeList, Integer.parseInt(flavorDto.getFlavorCode()));
			if(flag)
			{
				LOG.info("Combination Present in Products");
			} else {
				LOG.info("Combination isn't Present in Products");
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
}


