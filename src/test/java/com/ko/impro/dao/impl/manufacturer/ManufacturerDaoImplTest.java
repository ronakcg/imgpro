package com.ko.impro.dao.impl.manufacturer;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.manufacturer.ManufacuturerDao;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of ManufacturerDaoImpl methods
 */
public class ManufacturerDaoImplTest  extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ManufacturerDaoImplTest.class);
	
	/**
	 * Reference of ManufacuturerDao
	 */
	@Autowired(required = true)
	private ManufacuturerDao manufacturerDao;
	
	/**
	 * Test case to load manufacturer
	 */
	@Test
	public void testLoadManufacturer() {
		try {
			List<ManufacturerDto> cat=manufacturerDao.loadManufacuturer();
			assertTrue(!cat.isEmpty(),"Records Present");
			for(ManufacturerDto dto:cat) {
				LOG.info(dto.getManufacturerName());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	/**
	 * Test case to load manufacturer by ID
	 */
	@Test
	public void testLoadManufacturerById() {
		try {
			ManufacturerDto cat=manufacturerDao.loadManufacuturer(1564);
			assertTrue(!cat.getManufacturerCode().equals(""),"");
			LOG.info(cat.getManufacturerName());
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to add manufacturer
	 */
	@Test
	public void testAddManufacturer() {
		try {
			ManufacturerDto manufacturerDto=new ManufacturerDto();			
			manufacturerDto.setManufacturerName("Checking 14");
			//manufacturerDto.setManufacturerColor("11113");
			manufacturerDao.addManufacuturer(manufacturerDto);
			//assertTrue();
			LOG.info(" Add Done - ");
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Add failed");
		}		
	}

	/**
	 * Test case to delete manufacturer 
	 */
	@Test
	public void testDeleteManufacturer() {
		try {
			ManufacturerDto manufacturerDto=new ManufacturerDto();
			manufacturerDto.setManufacturerCode("1564");
			
			manufacturerDao.deleteManufacuturer(manufacturerDto);
			//assertTrue(flag);
			LOG.info("Delete done - ");
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("delete failed");
		}
	}

	/**
	 * Test case to edit manufacturer 
	 */
	@Test
	public void testEditManufacturer() {
		try {
			ManufacturerDto manufacturerDto=new ManufacturerDto();
			manufacturerDto.setManufacturerCode("1564");
			manufacturerDto.setManufacturerName("Checking 5");
			//manufacturerDto.setManufacturerColor("11111");
			manufacturerDao.editManufacuturer(manufacturerDto);
			//assertTrue(flag);
			LOG.info("Edit done - ");
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Edit failed");
		}
	}
	
	/**
	 * Test case to check duplicate manufacturer 
	 */
	@Test
	public void testCheckDuplicate() {
		try {
			
			boolean flag=manufacturerDao.checkDuplicateManufacuturer("Checking 5");
			if(flag) {
				LOG.info("Duplicate Present ");
			}else {
				LOG.info("Duplicate Not Present ");
			}
			assertTrue(flag,"Flag Status");
			
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Add failed");
		}
	}
	
	/**
	 * Test case to check presence in product
	 */
	@Test
	public void testCheckPresenceInProduct() {
		try {
			
			boolean flag=manufacturerDao.checkPresenceInProduct(1);
			if(flag) {
				LOG.info("Manufacuturer Present ");

			}
			else {
				LOG.info("Manufacuturer Not Present ");
			}
			assertTrue(flag,"Flag Status");
			
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Add failed");
		}
	}
	
	
	

}
