package com.ko.impro.dao.impl.deletemessagedata;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.deletemessagedata.DeleteMessageDataDao;
import com.ko.impro.dao.impl.containertype.ContainerTypeDaoImplTest;
import com.ko.impro.dto.DeleteMessagesDataDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of DeleteMessagesDataDaoImpl methods
 */
public class DeleteMessagesDataDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ContainerTypeDaoImplTest.class);

	/**
	 * Reference of CategoryDao
	 */
	@Autowired(required = true)
	private DeleteMessageDataDao deleteMsgDataDao;
	
	/**
	 * Test case to add delete message
	 */
	@Test
	public void testAddDeleteMessagesData() {
		DeleteMessagesDataDto msgDto= new DeleteMessagesDataDto();
		msgDto.setDeleteMessageText("Duplicate Product1");
		try {
			deleteMsgDataDao.addDeleteMessagesData(msgDto);
			LOG.info("Product inserted");
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to check duplicate products
	 */
	@Test
	public void testCheckDuplicateDeleteMessagesData() {
		try {
			assertTrue("Duplicate Product",!deleteMsgDataDao.checkDuplicateDeleteMessagesData("Duplicate Product"));
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to delete msg data using ID
	 */
	@Test
	public void testLoadDeleteMessagesDataById() {
		try {
			deleteMsgDataDao.loadDeleteMessagesData(1);
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to edit delete_msg 
	 */
	@Test
	public void testEditDeleteMessagesData() {
		DeleteMessagesDataDto deleteMsgDataDto=new DeleteMessagesDataDto();
		deleteMsgDataDto.setDeleteMessageId("9");
		deleteMsgDataDto.setDeleteMessageText("Product Descirption defined");
		try {
			deleteMsgDataDao.editDeleteMessagesData(deleteMsgDataDto);
			LOG.info("Record Updated Successfully");
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to delete delete_msg 
	 */
	@Test
	public void testDeleteDeleteMessagesData() {
		DeleteMessagesDataDto msgDto=new DeleteMessagesDataDto();
		msgDto.setDeleteMessageId("9");
		//msgDto.setDeleteMessageText("Product Descirption defi");
		try {
			String msg=deleteMsgDataDao.deleteDeleteMessagesData(msgDto);
			LOG.info(msg);
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to load delete_msg data
	 */
	@Test
	public void testLoadDeleteMessagesData() {
		try {
			List<DeleteMessagesDataDto> msgDto=deleteMsgDataDao.loadDeleteMessagesData();
			for(DeleteMessagesDataDto dto:msgDto) {
				LOG.info(dto.getDeleteMessageText());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
		
	}
	
}
