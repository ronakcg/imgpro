package com.ko.impro.dao.impl.shape;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.impl.segment.SegmentDaoImplTest;
import com.ko.impro.dao.shape.ShapeDao;
import com.ko.impro.dto.ShapeDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of ShapeDaoImpl methods
 */
public class ShapeDaoImplTest extends  BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SegmentDaoImplTest.class);
	
	
	/**
	 * Reference of ShapeDao
	 */
	@Autowired(required = true)
	private ShapeDao shapeDao;
	
	/**
	 * Test case to Load Shape
	 */
	@Test
	public void testLoadShape() {
		try {
			List<ShapeDto> shapeList=shapeDao.loadShapes();
			assertTrue(!shapeList.isEmpty(),"Records Present");
			for(ShapeDto dto:shapeList) {
				LOG.info(dto.getShapeId());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
		
	}
	
	/**
	 * Test case to Load ShapeID By Id
	 */
	@Test
	public void testLoadShapeIDById() {
		try {
			ShapeDto cat=shapeDao.loadshapeID(133);
			assertTrue(!cat.getShapeCode().equals(""),"Check for ShapeCode");
			LOG.info(cat.getShapeCode());
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to Check Duplicate
	 */
	@Test
	public void testCheckDuplicate() {
		try {
			boolean flag=shapeDao.checkDuplicateShape("Test2");
			if(flag) {
				LOG.info("Duplicate Present ");
			}
			else {
				LOG.info("Duplicate Not Present ");
			}
			assertTrue(flag,"Flag status");
  		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Add failed");
		}
	}
	
	/*@Test
	public void testCheckPresenceInProduct() {
		try {
			
			boolean flag=shapeDao.checkPresenceInPackages(1);
			if(flag)
				LOG.info("Shape Present ");
			else
				LOG.info("Shape Not Present ");
			assertTrue(flag);
			
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Add failed");
		}
	}*/
	
	/**
	 * Test case to Add Shape
	 */
	@Test
	public void testAddShape() {
		try {
			ShapeDto shapeDto = new ShapeDto();
			shapeDto.setShapeCode("Test1");
			shapeDao.addShapes(shapeDto);
			LOG.info("Record has been inserted successfully");
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to edit Shapes
	 */
	@Test
	public void testeditShapes() {
		ShapeDto shapeDto= new ShapeDto();
		shapeDto.setShapeId("133");
		shapeDto.setShapeCode("Test2");
		try {
			String msg = shapeDao.editShapes(shapeDto);
			LOG.info(msg);
		} catch (ImproException e) {
			
			fail(e.getMessage());
		}
		
	}
	
	/**
	 * Test case to load categories
	 */
	@Test
	public void testDeleteShapes() {
		ShapeDto shapeDto= new ShapeDto();
		shapeDto.setShapeId("133");
		try {
			shapeDao.deleteShapes(shapeDto);
			LOG.info("Deletion Done");
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	
}
