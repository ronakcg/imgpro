package com.ko.impro.dao.impl.product;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.product.ArchiveProductDao;
import com.ko.impro.dao.product.ProductDao;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.PaginationDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.exception.ImproException;
/**
 * Test Cases of ArchiveProductDaoImpl methods
 */
public class ArchiveProductDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ArchiveProductDaoImplTest.class);

	/**
	 * Reference of ArchiveProductDao
	 */
	@Autowired(required = true)
	private ArchiveProductDao archproductDao;

	/**
	 * Reference of ProductDao
	 */
	@Autowired(required = true)
	ProductDao productDao;

	/**
	 * Test case to load ArchiveProducts
	 */
	@Test
	public void testArchiveProducts() {
		try {
			ProductDto dto = new ProductDto();
			dto.setBitMapName("0000029241");
			dto.setProductCode("15725703");
			ProductDto dto1 = new ProductDto();
			dto1.setBitMapName("0000029241YZ");
			dto1.setProductCode("15725704");
			List<ProductDto> prodList = new ArrayList<ProductDto>();
			prodList.add(dto);
			prodList.add(dto1);
			archproductDao.archiveProducts(prodList, "test", "");

		} catch (ImproException e) {
			fail("Loading failed");
		}

	}

		// Test cases for searching archived products
	/**
	 * Test case to search  ArchiveProducts
	 */
	@Test
	public void testSearchArchivedProducts() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(productList.size() > 0);
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to search ArchiveProducts by BrandList
	 */
	@Test
	public void testSearchArchivedProductsByBrandList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> brandList = new ArrayList<String>();
			brandList.add("20");
			brandList.add("6");
			productSearchDto.setBrandList(brandList);
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"IsEmpty Of list");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to search  ArchivedProducts By ManufacturerList
	 */
	@Test
	public void testSearchArchivedProductsByManufacturerList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			List<String> manufactList = new ArrayList<String>();
			manufactList.add("1");
			manufactList.add("11");
			productSearchDto.setManufactList(manufactList);
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(productList.size() > 0);
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Archived Products By PackageList
	 */
	@Test
	public void testSearchArchivedProductsByPackageList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> packageList = new ArrayList<String>();
			packageList.add("82");
			packageList.add("112");
			productSearchDto.setPackageList(packageList);
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);

			assertTrue(!productList.isEmpty(),"IsEmpty of productList");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Archived Products By CategoryList
	 */
	@Test
	public void testSearchArchivedProductsByCategoryList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> packageList = new ArrayList<String>();
			packageList.add("8062");
			packageList.add("8213");
			productSearchDto.setPackageList(packageList);
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);

			assertTrue(!productList.isEmpty(),"Records present");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Archived Products By SubCategoryList
	 */
	@Test
	public void testSearchArchivedProductsBySubCategoryList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> subCategoryList = new ArrayList<String>();
			subCategoryList.add("1");
			subCategoryList.add("4");
			productSearchDto.setSubCategoryList(subCategoryList);
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Archived Products By FlavorList
	 */
	@Test
	public void testSearchArchivedProductsByFlavorList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> flavorList = new ArrayList<String>();
			flavorList.add("64");
			flavorList.add("37");
			productSearchDto.setFlavorList(flavorList);
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");

			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Archived Products By SegmentList
	 */
	@Test
	public void testSearchArchivedProductsBySegmentList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> segmentList = new ArrayList<String>();
			segmentList.add("4");
			segmentList.add("17");
			productSearchDto.setSegmentList(segmentList);
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search ArchivedProducts By SubSegmentList
	 */
	@Test
	public void testSearchArchivedProductsBySubSegmentList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> subSegmentList = new ArrayList<String>();
			subSegmentList.add("26");
			productSearchDto.setSubSegmentList(subSegmentList);
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to load categories
	 */
	@Test
	public void testSearchArchivedProductsByNameList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setProductName("Hawaiian Punch Fruit Juicy Red 64oz bottle");
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search ArchivedProducts By UPC
	 */
	@Test
	public void testSearchArchivedProductsByUPC() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setUpc("1820000468");
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search ArchivedProducts By Id
	 */
	@Test
	public void testSearchArchivedProductsById() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setId("4900001801S");
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search ArchivedProducts By Date
	 */
	@Test
	public void testSearchArchivedProductsByDate() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setFromDate("2016-07-24");
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search ArchivedProducts By DateRange
	 */
	@Test
	public void testSearchArchivedProductsByDateRange() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setFromDate("2016-07-21");
			productSearchDto.setToDate("2016-07-24");
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search ArchivedProducts By All
	 */
	@Test
	public void testSearchArchivedProductsByAll() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> brandList = new ArrayList<String>();
			brandList.add("83");
			brandList.add("2");
			productSearchDto.setBrandList(brandList);

			List<String> manufactList = new ArrayList<String>();
			manufactList.add("348");
			manufactList.add("1");
			productSearchDto.setManufactList(manufactList);

			List<String> packageList = new ArrayList<String>();
			packageList.add("8563");
			packageList.add("82");
			productSearchDto.setPackageList(packageList);

			List<String> categoryList = new ArrayList<String>();
			categoryList.add("1");
			categoryList.add("2");
			productSearchDto.setCategoryList(categoryList);

			List<String> subCategoryList = new ArrayList<String>();
			subCategoryList.add("3");
			subCategoryList.add("1");
			productSearchDto.setSubCategoryList(subCategoryList);

			List<String> segmentList = new ArrayList<String>();
			segmentList.add("7");
			segmentList.add("1");
			productSearchDto.setSegmentList(segmentList);

			List<String> subSegmentList = new ArrayList<String>();
			subSegmentList.add("6");
			subSegmentList.add("7");
			productSearchDto.setSubSegmentList(subSegmentList);

			productSearchDto.setFromDate("2005-05-20");
			productSearchDto.setToDate("2006-05-31");
			productSearchDto.setProductName("Bud Light 1/24oz can");

			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ArchivedProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	// Test cases for archived product details
	/**
	 * Test case to Get ArchivedProduct Details
	 */
	@Test
	public void testGetArchivedProductDetails() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			List<ArchivedProductDto> productList = productDao.searchArchivedProducts(productSearchDto,false);
			ArchivedProductDto archivedDto = productList.get(2);
			ArchivedProductDto prDto = archproductDao.getArchivedProductDetails(archivedDto.getProductCode());
			LOG.info(prDto.toString());
			LOG.info(prDto.getPkgdto().getUnitDepth());
			assertTrue(!productList.isEmpty(),"Records Present");
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}
	
	/**
	 * Test case to load categories
	 */
	@Test
	public void testPurgeArchivedProducts() {
		try {
			List<ArchivedProductDto> archivedList = new ArrayList<ArchivedProductDto>();
			ArchivedProductDto archivedDto = new ArchivedProductDto();
			archivedDto.setProductCode("15725767");
			//archivedProductDto.setBitmapName("4900000044S");
			archivedList.add(archivedDto);
			archproductDao.purgeArchivedProducts(archivedList);

		}catch(ImproException e) {
			fail("Loading Failed");
		}
	}
	

}


	


