package com.ko.impro.dao.impl.auditlog;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.auditlog.AuditLogDao;
import com.ko.impro.dto.AuditLogDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.util.AppConstants;


/**
 * Test Cases of AuditLog methods
 */
public class AuditLogDaoImplTest  extends BaseTest{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AuditLogDaoImplTest.class);

	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private AuditLogDao auditDao;


	/**
	 * Test case to load auditlog entries 
	 */
	@Test
	public void testLoadAuditLog() {
		try {
			final List<AuditLogDto> auditLogList=auditDao.loadAuditLog();
			
			if(auditLogList.isEmpty()) {
				LOG.info("No Records");
			}else {
				LOG.info("Records Present");
			}
			for(final AuditLogDto dto:auditLogList) {
				LOG.info(dto.getTransactionDate());
				LOG.info(dto.getTransactionUser());
				LOG.info(dto.getTableUpdated());
				LOG.info(dto.getRecordUpdated());
				LOG.info(dto.getTransactionType());				
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
		
	}
	

	
	/**
	 * Test Case to verify add Log
	 */
	@Test
	public void testAddLog() {
		try {
			auditDao.addLog(AppConstants.USER_NAME,AppConstants.TABLE_PRODUCTS,"Product 123",AppConstants.TRANSACTION_TYPE_ADD);
			LOG.info("Done");
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Add failed");
		}
	}
}
