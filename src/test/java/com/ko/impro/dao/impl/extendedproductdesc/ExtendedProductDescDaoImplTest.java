package com.ko.impro.dao.impl.extendedproductdesc;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.extendedproductdesc.ExtendedProductDescDao;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of ExtendedProductDescDaoImpl methods
 */
public class ExtendedProductDescDaoImplTest extends BaseTest{
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ExtendedProductDescDaoImpl.class);

	/**
	 * Reference of ExtendedProductDescDao
	 */
	@Autowired(required = true)
	private ExtendedProductDescDao extProdDescDao;
	
	/**
	 * Test case to add ExtendedProduct
	 */
	@Test
	public void testAddExtendedProductDesc() {
		ExtendedProductDescDto extDto= new ExtendedProductDescDto();
		extDto.setLongProductDesc("Testing172");
		extDto.setShortProductDesc("test172");
		BrandDto brandDto= new BrandDto();
		brandDto.setBrandName("Crave Milk");
		brandDto.setBrandCode("3703");

		extDto.setBrandDto(brandDto);
		//extendedProductDescDto.setExtendedDescId(String.valueOf(id));
		try {
			extProdDescDao.addExtendedProductDesc(extDto);
			LOG.info("Record Inserted Successfully");
		} catch (ImproException e) {
			
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to check duplicate value of ExtendedProductDesc
	 */
	@Test
	public void testCheckDuplicateExtendedProductDesc() {
		try {
			assertTrue("Duplicate Check", !extProdDescDao.checkDuplicateExtendedProductDesc("Lemon dt", false));
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to load ExtendedProductDesc using ID
	 */
	@Test
	public void testLoadExtendedProductDesc() {
		try {
			ExtendedProductDescDto extDto= extProdDescDao.loadExtendedProductDesc(8194);
			assertTrue("Desc",!"".equals(extDto.getLongProductDesc()));
			LOG.info("Content Loaded");
			if(LOG.isInfoEnabled()) {
			LOG.info(extDto.getExtendedDescId()+ "  " + " "+ extDto.getBrandDto().getBrandName() + " " 
					+ extDto.getLongProductDesc() +" "+extDto.getShortProductDesc() + " ");
			}
		} catch (ImproException e) {
			fail(e.getMessage());
			LOG.info("Content Loaded failed");
		}
	}

	/**
	 * Test case to edit ExtendedProductDesc
	 */
	@Test
	public void testEditExtendedProductDesc() {
		ExtendedProductDescDto extDto= new ExtendedProductDescDto();
		extDto.setExtendedDescId("8169");
		extDto.setLongProductDesc("Testing173");
		extDto.setShortProductDesc("test173");
		try {
			LOG.info(extProdDescDao.editExtendedProductDesc(extDto));
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to delete ExtendedProductDesc
	 */
	@Test
	public void testDeleteExtendedProductDesc() {
		ExtendedProductDescDto extDto= new ExtendedProductDescDto();
		extDto.setExtendedDescId("8169");
		try {
			LOG.info(extProdDescDao.deleteExtendedProductDesc(extDto));
		} catch (ImproException e) {
			fail(e.getMessage());
		}	
	}
	
	/**
	 * Test case to load ExtendedProduct
	 */
	@Test
	public void testloadExtendedProductDesc() {
		try {
			List<ExtendedProductDescDto> extendedProducts=extProdDescDao.loadExtendedProductDesc();
			for(ExtendedProductDescDto ex:extendedProducts) {
				LOG.info(ex.getLongProductDesc());
			}
			assertTrue("IsEmpty",!extendedProducts.isEmpty());
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	/**
	 * Test case to load ExtendedProduct using brandCode
	 */
	@Test
	public void testloadExtendedProductDescByBrand() {
		try {
			List<String> list=new ArrayList<String>();
			list.add("3703");
			
			List<ExtendedProductDescDto> extendedProducts=extProdDescDao.loadExtendedProductDesc(list);
			assertTrue("Check for null",extendedProducts != null );
			
			
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	
		
	}
	
	
	


