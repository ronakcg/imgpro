package com.ko.impro.dao.impl.packagestyle;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.impl.packagesize.PackageSizeDaoImplTest;
import com.ko.impro.dao.packagestyle.PackageStyleDao;
import com.ko.impro.dto.PackageStyleDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of PackageStyleDaoImpl methods
 */
public class PackageStyleDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PackageSizeDaoImplTest.class);
	
	/**
	 * Reference of CategoryDao
	 */
	@Autowired(required = true)
	private PackageStyleDao pkgStyleDao;
	
	/**
	 * Test case to load PackageStyle by ID
	 */
	@Test
	public void testLoadPackageStyleById() {
		try {
			final PackageStyleDto cat=pkgStyleDao.loadPackageStyle(1);
			assertTrue(!cat.getStyleCode().equals(""),"Success");
			LOG.info(cat.getStyleDesc());
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to check duplicates of PackageStyle Name
	 */
	@Test
	public void testCheckDuplicate() {
		try {
			final boolean flag=pkgStyleDao.checkDuplicatePackageStyle("Test");
			if(flag)
				LOG.info("Duplicate Present ");
			else
				LOG.info("Duplicate Not Present ");
			
  		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("checkDuplicate failed");
		}
	}
	
	/**
	 * Test case to load PackageStyle
	 */
	@Test
	public void testLoadPackageStyle() {
		try {
			final List<PackageStyleDto> pkgStyle=pkgStyleDao.loadPackageStyle();
			assertTrue(!pkgStyle.isEmpty(),"Load Package sytle is success");
			for(final PackageStyleDto dto:pkgStyle) {
				LOG.info(dto.getStyleDesc());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	/**
	 * Test case to add PackageStyle
	 */
	@Test
	public void testAddPackageStyle() {
		try {
			final PackageStyleDto packageStyleDto = new PackageStyleDto();
			packageStyleDto.setStyleDesc("TestNew1");
			pkgStyleDao.addPackageStyle(packageStyleDto);
			LOG.info("Record has been inserted successfully");
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to edit PackageStyle
	 */
	@Test
	public void testeditPackageStyle() {
		final PackageStyleDto packageStyleDto = new PackageStyleDto();
		packageStyleDto.setStyleCode("-43");
		packageStyleDto.setStyleDesc("TestCoke");
		packageStyleDto.setIsInactiveFlagChecked(1);
		try {
			final String msg = pkgStyleDao.editPackageStyle(packageStyleDto);
			LOG.info(msg);
		} catch (ImproException e) {
			
			fail(e.getMessage());
		}
		
	}
	
	/**
	 * Test case to delete PackageStyle
	 */
	@Test
	public void testdeletePackageStyle() {
		final PackageStyleDto packageStyleDto = new PackageStyleDto();
		packageStyleDto.setStyleCode("1");
		try {
			final String msg = pkgStyleDao.deletePackageStyle(packageStyleDto);
			LOG.info("Deletion Done");
			LOG.info(msg);
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
}
