package com.ko.impro.dao.impl.segment;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.segment.SegmentDao;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of SegmentDao methods
 */
public class SegmentDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SegmentDaoImplTest.class);

	/**
	 * Reference of SegmentDao
	 */
	@Autowired(required = true)
	private SegmentDao segmentDao;

	/**
	 * Test case to Load Segment
	 */
	@Test
	public void testLoadSegment() {
		try {
			List<SegmentDto> segmentList = segmentDao.loadSegment();
			assertTrue(!segmentList.isEmpty(), "Records Present");
			for (SegmentDto dto : segmentList) {
				LOG.info(dto.getSegmentDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	/**
	 * Test case to Load Segments ListOfString
	 */
	@Test
	public void testLoadSegmentsListOfString() {
		try {
			List<String> subCatIdList = new ArrayList<String>();
			subCatIdList.add("1");
			subCatIdList.add("2");
			subCatIdList.add("5");
			List<SegmentDto> segmentList = segmentDao.loadSegment(subCatIdList);
			assertTrue(!segmentList.isEmpty(), "Records Present");
			for (SegmentDto dto : segmentList) {
				LOG.info(dto.getSegmentDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	/**
	 * Test case to Add Segment
	 */
	@Test
	public void testAddSegment() throws ImproException {
		try {
			SegmentDto segmentDto = new SegmentDto();
			SubCategoryDto subCatDto = new SubCategoryDto();
			subCatDto.setSubCategoryCode("2");
			subCatDto.setSubCategoryDescription("Old SubCategory");

			segmentDto.setSegmentCode("4");
			segmentDto.setSegmentDescription("New Segment");

			segmentDto.setSubCategoryDto(subCatDto);
			segmentDao.addSegment(segmentDto);
			LOG.info("Added...!");
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to Delete Segment
	 */
	@Test
	public void testDeleteSegment() throws ImproException {
		try {
			SegmentDto segmentDto = new SegmentDto();
			segmentDto.setSegmentCode("19");
			segmentDao.deleteSegment(segmentDto);
			LOG.info("deleted...!");
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	/**
	 * Test case to Edit Segment
	 */
	@Test
	public void testEditSegment() throws ImproException {

		try {
			SegmentDto segmentDto = new SegmentDto();
			SubCategoryDto subCatDto = new SubCategoryDto();
			subCatDto.setSubCategoryCode("5");

			segmentDto.setSegmentCode("16");
			segmentDto.setSegmentDescription("updated");
			segmentDto.setSubCategoryDto(subCatDto);

			segmentDao.editSegment(segmentDto);
			LOG.info("updated...!");
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to Load Segment By Id
	 */
	@Test
	public void testLoadSegmentById() {
		try {
			SegmentDto sd = segmentDao.loadSegment(15);
			if (LOG.isInfoEnabled()) {
				LOG.info("SegmentCode " + sd.getSegmentCode());
				LOG.info("Segment Description " + sd.getSegmentDescription());
				LOG.info("SubCategory Associated with Segment");
			}
			SubCategoryDto dto = sd.getSubCategoryDto();
			LOG.info(dto.getSubCategoryCode());
			LOG.info(dto.getSubCategoryDescription());
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to CheckDuplicate
	 */
	@Test
	public void testCheckDuplicate() {
		try {
			String segmentDesc = "Dairy";
			boolean flag = segmentDao.checkDuplicateSegment(segmentDesc);
			if (flag) {
				LOG.info("Duplicate Present ");
			} else {
				LOG.info("Duplicate Not Present ");
			}
			assertTrue(flag, "Flag status");

		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("checkDuplicate failed");
		}
	}

	/**
	 * Test case to Update Product
	 */
	@Test
	public void testUpdateProduct() {
		try {	
			final SegmentDto dto = new SegmentDto();
			final SubCategoryDto subCat = new SubCategoryDto();
			dto.setSegmentCode("15");
			dto.setOldValue("2");
			subCat.setSubCategoryCode("4");
			dto.setSubCategoryDto(subCat);
			segmentDao.updateProduct(dto);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("Update Product Failed");
		}
	}

	/*@Test
	public void testCheckPresenceInProduct_SubCategory() {
		try {
			int segmentCode = 4;
			boolean flag = SegmentDao.checkPresenceInProduct_SubCategory(segmentCode);
			if (flag)
				LOG.info("Segment-SubCategory present in Product table :" + flag);
			else
				LOG.info("Segment-SubCategory present in Product table :" + flag);
			assertTrue(flag);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("Failed to check");
		}
	}

	@Test
	public void testCheckPresenceInProduct_SubSegment() {
		try {
			int segmentCode = 4;
			boolean flag = SegmentDao.checkPresenceInProduct_SubSegment(segmentCode);
			if (flag)
				LOG.info("Segment-SubSegment present in Product table :" + flag);
			else
				LOG.info("Segment-SubSegment present in Product table :" + flag);
			assertTrue(flag);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("Failed to check");
		}
	}*/
}
