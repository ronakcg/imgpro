package com.ko.impro.dao.impl.packages;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.packages.PackageDao;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.dto.PackageStyleDto;
import com.ko.impro.dto.ShapeDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of PackageDaoImpl methods
 */
public class PackageDaoImplTest  extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PackageDaoImplTest.class);
	
	/**
	 * Reference of PackageDao
	 */
	@Autowired(required = true)
	private PackageDao pkgDao;
	
	/**
	 * Test case to load PackageSize
	 */
	@Test
	public void testLoadPackageSize() {
		try {
			List<PackageDto> cat=pkgDao.loadPackages();
			assertTrue(!cat.isEmpty(),"IsEmpty true");
			for(PackageDto dto:cat) {
				LOG.info(dto.getPackageName());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	/**
	 * Test case to add PackageSize
	 */
	@Test
	public void testAddPackage() {
		try {
			PackageDto dto = new PackageDto();
			
			PackageSizeDto packageSizeDto = new PackageSizeDto();
			packageSizeDto.setPackageSizeCode("1");
			ContainerTypeDto containerTypeDto = new ContainerTypeDto();
			containerTypeDto.setContainerTypeCode("356");
			PackageStyleDto packageStyleDto =  new PackageStyleDto();
			packageStyleDto.setStyleCode("1");
			ShapeDto shapeDto = new ShapeDto();
			shapeDto.setShapeId("1");
			
			dto.setPackageName("TestPackage100");
			dto.setUnitPerTray("1");
			dto.setLiquidVolumePerPkg("1.28");
			
			dto.setTrayHeight("1");
			dto.setTrayDepth("1");
			dto.setTrayWidth("1");
			dto.setTrayDeep("1");
			dto.setTrayWide("1");
			dto.setTrayHigh("1");
			
			dto.setUnitHeight("1.1");
			dto.setUnitDepth("1.1");
			dto.setUnitWidth("1.1");
			
			dto.setPackageSizeDto(packageSizeDto);
			dto.setContainerTypeDto(containerTypeDto);
			dto.setStyleDto(packageStyleDto);
			dto.setShapesDto(shapeDto);
			
			pkgDao.addPackage(dto);
			LOG.info("Done");
		} catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Add failed");
		}
	}

	/**
	 * Test case to delete PackageSize
	 */
	@Test
	public void testDeletePackage() {
		try {
			PackageDto dto = new PackageDto();
			dto.setPackageCode("11559");
			pkgDao.deletePackage(dto);
			LOG.info("Delete done - ");
		} catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Delete failed");
		}
	}

	/**
	 * Test case to edit PackageSize
	 */
	@Test
	public void testEditPackage2() {
		try {
			PackageDto dto = new PackageDto();
			
			PackageSizeDto packageSizeDto = new PackageSizeDto();
			packageSizeDto.setPackageSizeCode("2");
			ContainerTypeDto containerTypeDto = new ContainerTypeDto();
			containerTypeDto.setContainerTypeCode("357");
			PackageStyleDto packageStyleDto =  new PackageStyleDto();
			packageStyleDto.setStyleCode("3");
			ShapeDto shapeDto = new ShapeDto();
			shapeDto.setShapeId("2");
			
			dto.setPackageCode("11559");
			dto.setPackageName("TestEditPackage");
			dto.setUnitPerTray("1");
			dto.setLiquidVolumePerPkg("1.28");
			
			dto.setTrayHeight("1");
			dto.setTrayDepth("1");
			dto.setTrayWidth("1");
			dto.setTrayDeep("1");
			dto.setTrayWide("1");
			dto.setTrayHigh("1");
			
			dto.setUnitHeight("2.1");
			dto.setUnitDepth("1.1");
			dto.setUnitWidth("2.1");
			
			dto.setPackageSizeDto(packageSizeDto);
			dto.setContainerTypeDto(containerTypeDto);
			dto.setStyleDto(packageStyleDto);
			dto.setShapesDto(shapeDto);
			
			pkgDao.editPackage(dto);
			LOG.info("Edit done - ");
			
		} catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Edit failed");
		}
	}
	
	/**
	 * Test case to load PackageSize using packageCode
	 */
	@Test
	public void testLoadPackageById() {	
		PackageDto dto;
		try {
			dto = pkgDao.loadPackage(11559);
			
			LOG.info(dto.getPackageCode());
			LOG.info(dto.getPackageName());
			LOG.info(dto.getPackageSizeDto().getPackageSizeDescription());
			LOG.info(dto.getContainerTypeDto().getContainerTypeDescription());
			LOG.info(dto.getShapesDto().getShapeCode());
			LOG.info(dto.getStyleDto().getStyleDesc());
			
		} catch (ImproException e) {
			fail(loadingMsg);
		}	
	}
	
	/**
	 * Test case to check duplicates for packageName
	 */
	@Test
	public void testCheckDuplicate() {
		try {
			String name="12/12o (7.956)";
			boolean flag = pkgDao.checkDuplicatePackageName(name);
			if(flag) {
				LOG.info("Duplicate Present ");
	
			}
			else {
				LOG.info("Duplicate Not Present ");	
			}
			assertTrue(flag,"Flag status");
			
		} catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Add failed");
		}
	}
}
