package com.ko.impro.dao.impl.gatekeeping;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.gatekeeping.ProductGateKeepingDao;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.dto.MissingImagesDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;

import junit.framework.Assert;

/**
 * @author hlekshmy
 * DaoImpl test class for ProductGateKeeping DAO
 */
public class ProductGateKeepingDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ProductGateKeepingDaoImplTest.class);

	/** The Reference  ProductGateKeepingDao. */
	@Autowired(required = true)
	private ProductGateKeepingDao gateKeepingDao;

	/**
	 * Test case to  Load ProductGateKeeping Complete
	 */
	@Test
	public void testLoadProductGateKeepingComplete() {
		try {
			List<ProductGateKeepingDto> cat = gateKeepingDao.loadGateKeeping(true);
			if(cat!= null ) {			
				for (ProductGateKeepingDto dto : cat) {
					LOG.info(dto.getProductName());
					LOG.info(dto.getProductCode());
					LOG.info(dto.getUpc());
	
					LOG.info(dto.getPackageDto().getUnitHeight());
					LOG.info(dto.getPackageDto().getUnitWidth());
					LOG.info(dto.getPackageDto().getUnitDepth());
				}
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to  Load ProductGateKeeping InComplete
	 */
	@Test
	public void testLoadProductGateKeepingInComplete() {
		try {
			List<ProductGateKeepingDto> cat = gateKeepingDao.loadGateKeeping(false);
			if(cat!= null ) {
				for (ProductGateKeepingDto dto : cat) {
					LOG.info(dto.getProductName());
					LOG.info(dto.getProductCode());
					LOG.info(dto.getUpc());
	
					LOG.info(dto.getPackageDto().getUnitHeight());
					LOG.info(dto.getPackageDto().getUnitWidth());
					LOG.info(dto.getPackageDto().getUnitDepth());
				}
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to  Load ProductGateKeeping by ID
	 */
	@Test
	public void testLoadProductGateKeepingID() {
		try {
			ProductGateKeepingDto dto = gateKeepingDao.loadGateKeeping(21000);
			assertTrue("Loading the iD failed",dto != null);
			LOG.info(dto.getProductName());
			LOG.info(dto.getProductCode());
			LOG.info(dto.getUpc());

			LOG.info(dto.getPackageDto().getUnitHeight());
			LOG.info(dto.getPackageDto().getUnitWidth());
			LOG.info(dto.getPackageDto().getUnitDepth());

		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}


	/**
	 * Test case to  Push Gatekeeping Products
	 */
	@Test
	public void testPushGatekeepingProducts() {
		try {
			gateKeepingDao.pushProducts();
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("Stored Procedure falied");

		}
	}

	/**
	 * Test case to  Push Gatekeeping Products
	 */
	@Test
	public void testPurgeGatekeepingProducts() {
		try {
			gateKeepingDao.purgeProductGateKeeping(1000053415);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("Stored Procedure falied");

		}
	}

	
	
	/**
	 * Test case to  Edit GateKeeping
	 */
	@Test
	public void testEditGateKeeping() throws ImproException {
		try {
			ProductGateKeepingDto gatekeepingDto = new ProductGateKeepingDto();
			gatekeepingDto.setProductCode("14479000");
			gatekeepingDto.setProductId("14479000");

			gatekeepingDto.setProductName("Coke update long");
			gatekeepingDto.setShortProductName("Coke update short");
			gatekeepingDto.setUpc("0000029222");
			gatekeepingDto.setUpc12("9999999999");

			PackageDto packageDto = new PackageDto();
			packageDto.setPackageCode("8080");
			packageDto.setPackageName("Package");
			gatekeepingDto.setPackageDto(packageDto);

			PackageSizeDto packageSizeDto = new PackageSizeDto();
			packageSizeDto.setPackageSizeCode("1");
			packageSizeDto.setPackageSizeDescription("1 Package");
			gatekeepingDto.setPackageSizeDto(packageSizeDto);

			ContainerTypeDto containerType = new ContainerTypeDto();
			containerType.setContainerTypeCode("143");
			containerType.setContainerTypeDescription("Container");
			gatekeepingDto.setContainerTypeDto(containerType);

			BrandDto brandDto = new BrandDto();
			brandDto.setBrandCode("4");
			brandDto.setBrandName("Coke Classic");
			gatekeepingDto.setBrandDto(brandDto);

			FlavorDto flavorDto = new FlavorDto();
			flavorDto.setFlavorCode("14");
			flavorDto.setFlavorDescription("Flavor");
			gatekeepingDto.setFlavorDto(flavorDto);

			CategoryDto categoryDto = new CategoryDto();
			categoryDto.setCategoryCode("1");
			categoryDto.setCategoryDescription("Category");
			gatekeepingDto.setCategoryDto(categoryDto);

			SubCategoryDto subCategoryDto = new SubCategoryDto();
			subCategoryDto.setSubCategoryCode("1");
			subCategoryDto.setSubCategoryDescription("SubCategory");
			gatekeepingDto.setSubCategoryDto(subCategoryDto);

			SegmentDto segmentDto = new SegmentDto();
			segmentDto.setSegmentCode("1");
			segmentDto.setSegmentDescription("Segment");
			gatekeepingDto.setSegmentDto(segmentDto);

			SubSegmentDto subSegmentDto = new SubSegmentDto();
			subSegmentDto.setSubSegmentCode("1");
			subSegmentDto.setSubSegmentDescription("SubSegment");
			gatekeepingDto.setSubSegmentDto(subSegmentDto);

			ManufacturerDto manufacturerDto = new ManufacturerDto();
			manufacturerDto.setManufacturerCode("1");
			manufacturerDto.setManufacturerName("Manufacturer");
			gatekeepingDto.setManufacturerDto(manufacturerDto);

			ExtendedProductDescDto extPrdDescDto = new ExtendedProductDescDto();
			extPrdDescDto.setExtendedDescId("9");
			gatekeepingDto.setExtPrdDescDto(extPrdDescDto);

			gatekeepingDto.setCompleteFlag(false);
			MissingImagesDto missingImagesDto=new MissingImagesDto();
			gatekeepingDto.setMissingImagesDto(missingImagesDto);
			gateKeepingDao.editGateKeeping(gatekeepingDto);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("edit failed...!");
		}
	}
}
