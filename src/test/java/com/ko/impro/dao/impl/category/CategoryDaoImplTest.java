package com.ko.impro.dao.impl.category;


import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.category.CategoryDao;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of CategoryDao methods
 */
public class CategoryDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(CategoryDaoImplTest.class);
	
	/**
	 * Reference of CategoryDao
	 */
	@Autowired(required = true)
	private CategoryDao categoryDao;
	
	/**
	 * Test case to load categories
	 */
	@Test
	public void testLoadCategory() {		//(Working)
		try {
			final List<CategoryDto> cat=categoryDao.loadCategory();
			assertTrue(!cat.isEmpty(),"Categories present");
			for(CategoryDto dto:cat) {
				LOG.info(dto.getCategoryDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	/**
	 * Test case to load load categories using ID
	 */
	@Test
	public void testLoadCategoryById() {		//(Working)
		try {
			CategoryDto cat = categoryDao.loadCategory(1);
			LOG.info(cat.getCategoryCode());
			LOG.info(cat.getCategoryDescription());
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to ADD categories
	 */
	@Test
	public void testAddCategory() {		//(Working)
		try {
			final CategoryDto categoryDto = new CategoryDto();
			categoryDto.setCategoryDescription("NewTestCategory");
			categoryDao.addCategory(categoryDto);
			LOG.info("Data Added");
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}


	/**
	 * Test case to ADD categories
	 */
	@Test
	public void testeditCategory() {		//(Working)
		try {
			final CategoryDto categoryDto = new CategoryDto();
			categoryDto.setCategoryCode("4");
			categoryDto.setCategoryDescription("Grocery");
			categoryDao.editCategory(categoryDto);
			LOG.info("Data Added");
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	/**
	 * Test case to delete categories
	 */
	@Test
	public void testDeleteCategory(){		//(Working)
		try {
			CategoryDto categoryDto = new CategoryDto();
			categoryDto.setCategoryCode("1");
			LOG.info(categoryDao.deleteCategory(categoryDto));
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	
	/**
	 * Test case to check duplicate entry of category name
	 */
	@Test
	public void testCheckDuplicate() {		//(Working)
		try {
			final boolean flag = categoryDao.checkDuplicateCategory("Soft Beverages");
			if(flag) {
				LOG.info("Duplicate Present ");
			}
			else {
				LOG.info("Duplicate Not Present ");
			}
			assertTrue(flag,"Flag status");
			
		} catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("checkDuplicate failed");
		}
	}
	
	/**
	 * Test case to check duplicate entry in products
	 */
	@Test 
	public void testcheckPresenceInProduct(){		//(Working)
		boolean flag;
		try {
			flag = categoryDao.checkPresenceInProduct(1);
			if(flag)
			{
				LOG.info("Combiantion of entered SubcatagoryCode and CategoryCode present in Products table");
			} else {
				LOG.info("Combiantion of entered SubcatagoryCode and CategoryCode aren't present in Products table");
			}
		} catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("checkDuplicate failed");
		}

		
	}
}
