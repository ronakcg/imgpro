package com.ko.impro.dao.impl.brand;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.brand.BrandDao;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.exception.ImproException;

/**
 * Test class to verify methods of Brand
 *
 */
public class BrandDaoImplTest extends BaseTest{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(BrandDaoImplTest.class);

	
	/**
	 * Reference of AuditLogDao
	 */
	@Autowired(required = true)
	private BrandDao brandDao;
	
	/**
	 * Test case to load brand list
	 */
	@Test
	public void testLoadBrands() {
		try {
			final List<BrandDto> brands=brandDao.loadBrands();
			assertTrue(!brands.isEmpty(),"Brands present");
			for(final BrandDto dto:brands) {
				LOG.info(dto.getBrandName());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}


	/**
	 * Test Case to load brand list based on manufacturer
	 */
	@Test
	public void testLoadBrandsListOfString() {
		try {
			final List<String> idList=new ArrayList<String>();
			//manufacturerIdList.add("100");
			idList.add("3");
			idList.add("33");

			final List<BrandDto> brands=brandDao.loadBrands(idList);
			assertTrue(!brands.isEmpty(),"Brand IsEmpty");
			for(final BrandDto dto:brands) {
				LOG.info(dto.getBrandName());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	/**
	 * Test case to get Brand details using BrandCode
	 */
	@Test
	public void getBrandDetails()
	{
		final BrandDto brandDto=new BrandDto();
		brandDto.setBrandCode("2807");
		try {
			final BrandDto brandDto2=brandDao.getBrandDetails(brandDto);
			LOG.info(brandDto2.toString());
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}


	/**
	 * Test case to add Brand into Brand Table.
	 */
	@Test
	public void testAddBrand() {
		try {
			final BrandDto brandDto=new BrandDto();
			final ManufacturerDto dto=new ManufacturerDto();
			dto.setManufacturerCode("947");
			brandDto.setManufacturerDto(dto);
			brandDto.setBrandName("CokeTest1");
			brandDto.setShortBrandName("Test2");			
			brandDao.addBrand(brandDto);
			LOG.info("Done");
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Add failed");
		}
	}

	/**
	 * Test case to delete Brand into Brand Table.
	 */
	@Test
	public void testDeleteBrand() {
		try {
			final BrandDto dto=new BrandDto();			
			dto.setBrandCode("4235");					
			brandDao.deleteBrand(dto);
			//assertTrue();
			LOG.info("Delete done - ");
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Delete failed");
		}
	}

	/**
	 *  Test case to edit Brand into Brand Table.
	 */
	@Test
	public void testEditBrand() {
		try {
			final BrandDto dto=new BrandDto();
			final ManufacturerDto manufacturerDto=new ManufacturerDto();
			manufacturerDto.setManufacturerCode("947");
			dto.setManufacturerDto(manufacturerDto);
			dto.setBrandCode("4235");
			dto.setBrandName("Pepsi Classic");
			dto.setShortBrandName("Pepsi Classic");		
			//dto.setIsInactiveFlagChecked(1);
			//dto.setInactiveFlag('Y');
			
			LOG.info(brandDao.editBrand(dto));
			//assertTrue();
			LOG.info("Edit done - ");
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Edit failed");
		}
	}
		
	/**
	 * Test case to load Brand using ID.
	 */
	@Test
	public void testLoadBrandById() {
		try {
			final BrandDto cat=brandDao.loadBrand(2270);
			assertTrue(!cat.getBrandCode().equals(""),"msg");
			LOG.info(cat.getBrandName());
			
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	
	/**
	 * Test case to check duplicate brand name value.
	 */
	@Test
	public void testCheckDuplicate() {
		try {
			final boolean flag=brandDao.checkDuplicateBrand("Fanta Berry",false);
			if(brandDao.checkDuplicateBrand("Fanta Berry",false))
				{
				LOG.info("Duplicate Present ");
				}
			else {
				LOG.info("Duplicate Not Present ");
			}
			
			
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Add failed");
		}
	}
	
	
	/**
	 * Test case to update product using ID.
	 */
	@Test
	public void testUpdateProduct() {
		try {
			final BrandDto dto=new BrandDto();
			final ManufacturerDto manf=new ManufacturerDto();
			dto.setBrandCode("2");
			dto.setOldValue("1");
			manf.setManufacturerCode("3");
			dto.setManufacturerDto(manf);
			brandDao.updateProduct(dto);
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Update Product Failed");
		}
	}	
		
	/**
	 * Test case to check presence of brandCode.
	 */
	@Test
	public void testCheckPresenceInProduct() {
		try {
			final boolean flag=brandDao.checkPresenceInProduct(2);
			assertTrue(flag,"Flag Status");
		}catch(ImproException e) {
			LOG.error(e.getMessage());
			fail("Failed to check");
		}
	}

}
