package com.ko.impro.dao.impl.packagesize;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.packagesize.PackageSizeDao;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.exception.ImproException;


/**
 * Test Cases of PackageSizeDaoImpl methods
 */
public class PackageSizeDaoImplTest  extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PackageSizeDaoImplTest.class);
	private String msg=null;
	
	/**
	 * Reference of PackageSizeDao
	 */
	@Autowired(required = true)
	private PackageSizeDao pkgSizeDao;
	
	/**
	 * Test case to load PackageSizes
	 */
	@Test
	public void testLoadPackageSize() {
		try {
			final List<PackageSizeDto> cat=pkgSizeDao.loadPackageSize();
			assertTrue(cat.size() > 0);
			for(final PackageSizeDto dto:cat) {
				LOG.info(dto.getPackageSizeDescription()+ " " + dto.getPackageSizeOz());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to load PackageSizes
	 */
	@Test
	public void testCheckDuplicatePackageSize() {
		try {
			boolean cat=pkgSizeDao.checkDuplicatePackageSize("7oz");
			assertTrue(cat);
			
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to add PackageSizes
	 */
	@Test
	public void testAddPackageSize() {
		PackageSizeDto packageSizeDto= new PackageSizeDto();
		packageSizeDto.setPackageSizeDescription("10");
		packageSizeDto.setPackageSizeOz("100");
		try {
			pkgSizeDao.addPackageSize(packageSizeDto);
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test case to delete PackageSize
	 */
	@Test
	public void testDeletePackageSize() {
		PackageSizeDto packageSizeDto= new PackageSizeDto();
		packageSizeDto.setPackageSizeCode("12987");
		try {
			msg=pkgSizeDao.deletePackageSize(packageSizeDto);
			LOG.info(msg);
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test case to edit PackageSize
	 */
	@Test
	public void testEditPackageSize() {
		PackageSizeDto packageSizeDto= new PackageSizeDto();
		packageSizeDto.setPackageSizeCode("12987");
		packageSizeDto.setPackageSizeDescription("20");
		packageSizeDto.setPackageSizeOz("200");
		/*packageSizeDto.setIsInactiveFlagChecked(1);
		packageSizeDto.setInactiveFlag('Y');*/
		try {
			msg=pkgSizeDao.editPackageSize(packageSizeDto);
			LOG.info(msg);
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test case to load PackageSize using Id
	 */
	@Test
	public void testLoadPackageSizeById() {
		try {
			final PackageSizeDto packageSizeDto=pkgSizeDao.loadPackageSizeById(12986);
			assertTrue("Check For Null",!"".equals(packageSizeDto.getPackageSizeDescription()));
			if(LOG.isInfoEnabled()) {
				LOG.info(packageSizeDto.getPackageSizeCode()+ "  " + packageSizeDto.getPackageSizeDescription() + " " + packageSizeDto.getPackageSizeOz());
			}
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 *  Test case to load PackageSize based on the PackageList
	 */
	@Test
	public void testPackageSizeByPackageList() {
		final List<String> packageList= new ArrayList<>();
		packageList.add("9499");
		packageList.add("9500");
		try {
			for(final PackageSizeDto dto:pkgSizeDao.loadPackageSize(packageList)) {
				LOG.info(dto.getPackageSizeDescription()+ " "+ dto.getPackageSizeOz());
			}
		} catch (ImproException e) {
			fail(e.getMessage());
		}
		
	}
}
