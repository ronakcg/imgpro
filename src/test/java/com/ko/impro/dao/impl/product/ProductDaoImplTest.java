package com.ko.impro.dao.impl.product;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.product.ProductDao;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.dto.MissingImagesDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.dto.PaginationDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;

/**
 * Test Cases of ProductDaoImpl methods
 */
public class ProductDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ProductDaoImplTest.class);

	/**
	 * Reference of ProductDao
	 */
	@Autowired(required = true)
	ProductDao productDao;

	/**
	 * Test case to Restore Products
	 */
	@Test
	public void testRestoreProducts() {
		try {
			
			ArchivedProductDto dto = new ArchivedProductDto();
			dto.setBitMapName("0000029241YZ");
			dto.setProductCode("15725704");
			//dto.setProductName("Coke Cherry 2 ltr bottle");
			dto.setShortProductName("Coke Classic 24 pk/12oz");
			
			PackageSizeDto packageSizeDto= new PackageSizeDto();
			packageSizeDto.setPackageSizeDescription("500L");
			dto.setPackageSizeDto(packageSizeDto);
			
			PackageDto packageDto=new PackageDto();
			packageDto.setPackageCode("82");
			
			ContainerTypeDto containerTypeDto= new ContainerTypeDto();
			containerTypeDto.setContainerTypeDescription("Shrink");
			packageDto.setContainerTypeDto(containerTypeDto);
			dto.setPkgdto(packageDto);
			
			BrandDto brandDto=new BrandDto();
			brandDto.setBrandCode("2");
			dto.setBrandDto(brandDto);
			
			dto.setUpc("4900001278");
			
			FlavorDto flavorDto= new FlavorDto();
			flavorDto.setFlavorCode("14");
			dto.setFlavorDto(flavorDto);
			
			CategoryDto categoryDto= new CategoryDto();
			categoryDto.setCategoryCode("1");
			dto.setCategoryDto(categoryDto);
			
			//dto.setBitmapName("00000546565421");
			//dto.setDateMaintained("2006-05-17 00:00:00.000");
			
			SubCategoryDto subCategoryDto= new SubCategoryDto();
			subCategoryDto.setSubCategoryCode("1");
			dto.setSubCategoryDto(subCategoryDto);
			
			SegmentDto segmentDto= new SegmentDto();
			segmentDto.setSegmentCode("1");
			dto.setSegmentDto(segmentDto);
			
			SubSegmentDto subSegmentDto= new SubSegmentDto();
			subSegmentDto.setSubSegmentCode("6");
			dto.setSubSegmentDto(subSegmentDto);
			
			
			ManufacturerDto manufacturerDto= new ManufacturerDto();
			manufacturerDto.setManufacturerCode("1");
			dto.setManufacturerDto(manufacturerDto);
			
			dto.setNielsenUpc("4900001278");
			dto.setNielsenDesc("COKE CLS R CL CN DOUBLE FP 24P12 OZ");
			
			ExtendedProductDescDto extendedProductDescDto= new ExtendedProductDescDto();
			extendedProductDescDto.setExtendedDescId("2");
			dto.setExtendedProductDescDto(extendedProductDescDto);
			
			dto.setUpc12(null);
			
			productDao.restoreProduct(dto);
		} catch (ImproException e) {
			fail(e.getMessage());
		}

	}

	// Test cases for Searching Product Details
	/**
	 * Test case to Search Products
	 */
	@Test
	public void testSearchProducts() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setActiveSearch(true);
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);

			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.getProductCode()+ " " + dto.getProductName() );
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By BrandList
	 */
	@Test
	public void testSearchProductsByBrandList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			List<String> brandList = new ArrayList<String>();
			brandList.add("3080");
			brandList.add("3883");
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setBrandList(brandList);
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}//
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By ManufacturerList
	 */
	@Test
	public void testSearchProductsByManufacturerList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			List<String> manufactList = new ArrayList<String>();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			manufactList.add("1");
			manufactList.add("310");
			productSearchDto.setManufactList(manufactList);
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By PackageList
	 */
	@Test
	public void testSearchProductsByPackageList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> packageList = new ArrayList<String>();
			packageList.add("82");
			packageList.add("112");
			productSearchDto.setPackageList(packageList);
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);

			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By CategoryList
	 */
	@Test
	public void testSearchProductsByCategoryList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> categoryList = new ArrayList<String>();
			categoryList.add("1");
			categoryList.add("2");
			productSearchDto.setCategoryList(categoryList);
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);

			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By SubCategoryList
	 */
	@Test
	public void testSearchProductsBySubCategoryList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> subCategoryList = new ArrayList<String>();
			subCategoryList.add("1");
			subCategoryList.add("4");
			productSearchDto.setSubCategoryList(subCategoryList);
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By FlavorList
	 */
	@Test
	public void testSearchProductsByFlavorList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> flavorList = new ArrayList<String>();
			flavorList.add("64");
			flavorList.add("37");
			productSearchDto.setFlavorList(flavorList);
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");

			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By SegmentList
	 */
	@Test
	public void testSearchProductsBySegmentList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> segmentList = new ArrayList<String>();
			segmentList.add("4");
			segmentList.add("17");
			productSearchDto.setSegmentList(segmentList);
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			// assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By SubSegmentList
	 */
	@Test
	public void testSearchProductsBySubSegmentList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> subSegmentList = new ArrayList<String>();
			subSegmentList.add("26");
			subSegmentList.add("7");
			productSearchDto.setSubSegmentList(subSegmentList);
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			// assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By NameList
	 */
	@Test
	public void testSearchProductsByNameList() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setProductName("Hawaiian Punch Fruit Juicy Red 64oz bottle");
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			// assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By UPC
	 */
	@Test
	public void testSearchProductsByUPC() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setUpc("5530300052");
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By Id
	 */
	@Test
	public void testSearchProductsById() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setId("7618317032S");
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Sarch Products By Date
	 */
	@Test
	public void testSarchProductsByDate() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setFromDate("2016-07-24");
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By Date Range
	 */
	@Test
	public void testSearchProductsByDateRange() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setFromDate("2016-07-21");
			productSearchDto.setToDate("2016-07-24");
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to Search Products By All
	 */
	@Test
	public void testSearchProductsByAll() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			List<String> brandList = new ArrayList<String>();
			brandList.add("71");
			brandList.add("2679");
			productSearchDto.setBrandList(brandList);

			List<String> manufactList = new ArrayList<String>();
			manufactList.add("10");
			manufactList.add("453");
			productSearchDto.setManufactList(manufactList);

			List<String> packageList = new ArrayList<String>();
			packageList.add("11960");
			packageList.add("8086");
			productSearchDto.setPackageList(packageList);

			List<String> categoryList = new ArrayList<String>();
			categoryList.add("1");
			categoryList.add("2");
			productSearchDto.setCategoryList(categoryList);

			List<String> subCategoryList = new ArrayList<String>();
			subCategoryList.add("2");
			subCategoryList.add("4");
			productSearchDto.setSubCategoryList(subCategoryList);

			List<String> segmentList = new ArrayList<String>();
			segmentList.add("4");
			segmentList.add("17");
			productSearchDto.setSegmentList(segmentList);

			List<String> subSegmentList = new ArrayList<String>();
			subSegmentList.add("26");
			subSegmentList.add("7");
			productSearchDto.setSubSegmentList(subSegmentList);

			productSearchDto.setFromDate("2015-06-09");
			productSearchDto.setToDate("2016-07-24");
			
			productSearchDto.setProductName("Hawaiian Punch Fruit Juicy Red 64oz bottle");

			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	// Test cases for Additional Product Details retrieval method
	/**
	 * Test case to Get ProductDetails
	 */
	@Test
	public void testGetProductDetails() {
		try {
			ProductDto prDto = productDao.getProductDetails("15725704");
			LOG.info(prDto.getPkgdto().getUnitDepth());

		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	// Test cases for similar product details
	/**
	 * Test case to search Similar Products By Brand
	 */
	@Test
	public void searchSimilarProductsByBrand() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			productSearchDto.setBrandCode("3080");
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,false);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to search Similar Products By productName
	 */
	@Test
	public void searchSimilarProductsByproductName() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			productSearchDto.setProductName("Snapple Tea Lemon 16oz bottle");
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,false);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to search Similar Products By package
	 */
	@Test
	public void searchSimilarProductsBypackage() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			productSearchDto.setPackageCode("82");
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,false);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}

	/**
	 * Test case to search Similar Products By Upc
	 */
	@Test
	public void searchSimilarProductsByUpc() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			productSearchDto.setUpc("5530300052");
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,false);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}
	
	/**
	 * Test case to Edit Product
	 */
	@Test
	public void testEditProduct1() {
		try {
			ProductDto productDto = new ProductDto();
			productDto.setProductCode("15725700");
			BrandDto brandDto=new BrandDto();
			brandDto.setBrandCode("2807");
			productDto.setBrandDto(brandDto);
			PackageDto packageDto=new PackageDto();
			packageDto.setPackageCode("8062");
			productDto.setPkgdto(packageDto);
			FlavorDto flavorDto=new FlavorDto();
			flavorDto.setFlavorCode("65");
			productDto.setFlavorDto(flavorDto);
			CategoryDto categoryDto=new CategoryDto();
			categoryDto.setCategoryCode("1");
			productDto.setCategoryDto(categoryDto);
			SubCategoryDto subCategoryDto=new SubCategoryDto();
			subCategoryDto.setSubCategoryCode("4");
			productDto.setSubCategoryDto(subCategoryDto);
			SegmentDto segmentDto=new SegmentDto();
			segmentDto.setSegmentCode("20");
			productDto.setSegmentDto(segmentDto);
			SubSegmentDto subSegmentDto=new SubSegmentDto();
			subSegmentDto.setSubSegmentCode("25");
			productDto.setSubSegmentDto(subSegmentDto);
			
			ManufacturerDto manufacturerDto=new ManufacturerDto();
			manufacturerDto.setManufacturerCode("860");
			productDto.setManufacturerDto(manufacturerDto);;
			productDto.setProductName("Hello");
			productDto.setUpc("123456");
			productDto.setShortProductName("Hi");
			
			productDto.setUpc12("");
			ExtendedProductDescDto extendedProductDescDto=new ExtendedProductDescDto();
			productDto.setExtendedProductDescDto(extendedProductDescDto);
			
			MissingImagesDto dto=new MissingImagesDto();
			productDto.setMissingImagesDto(dto);
			productDao.editProduct(productDto);
		} catch (ImproException e) {
			LOG.error(e.getMessage());

			fail("Edit failed");

		}
	}

	/**
	 * Test case to Search Product For MissingImage
	 */
	@Test
	public void testSearchProductForMissingImage() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			List<String> brandList = new ArrayList<String>();
			brandList.add("3080");
			brandList.add("3083");
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setBrandList(brandList);
			productSearchDto.setMissingImages(true);
			MissingImagesDto mid=new MissingImagesDto();
			mid.setIsjpg(true);
			productSearchDto.setMissingImagesDto(mid);
			List<ProductDto> productList = productDao.searchProducts(productSearchDto,true);
			assertTrue(!productList.isEmpty(),"Records Present");
			for (ProductDto dto : productList) {
				LOG.info(dto.toString());
			}
		} catch (ImproException e) {
			fail("Loading failed");
		}
	}
	
	/**
	 * Test case to Edit Product
	 */
	@Test
	public void testEditProduct() {
		try {
			ProductDto product=new ProductDto();
			product.setProductCode("15725703");
			product.setBitMapName("0000029241");
			product.setArchiveFlag(false);
			
			
			PackageDto packageDto = new PackageDto();
			packageDto.setPackageCode("8006");
			ContainerTypeDto containerTypeDto= new ContainerTypeDto();
			containerTypeDto.setContainerTypeDescription("Coke7");
			packageDto.setContainerTypeDto(containerTypeDto);
			product.setPkgdto(packageDto);
			
			PackageSizeDto pkg= new PackageSizeDto();
			pkg.setPackageSizeDescription("Coke8");
			product.setPackageSizeDto(pkg);
			
			BrandDto brandDto = new BrandDto();
			brandDto.setBrandCode("2");
			brandDto.setBrandName("Coke9");
			product.setBrandDto(brandDto);
			
			product.setUpc("1234567895");
			
			FlavorDto flavorDto = new FlavorDto();
			flavorDto.setFlavorCode("14");
			product.setFlavorDto(flavorDto);
			
			CategoryDto categoryDto = new CategoryDto();
			categoryDto.setCategoryCode("1");
			product.setCategoryDto(categoryDto);
			
			SubCategoryDto subCategoryDto = new SubCategoryDto();
			subCategoryDto.setSubCategoryCode("1");
			product.setSubCategoryDto(subCategoryDto);
			
			SegmentDto segmentDto = new SegmentDto();
			segmentDto.setSegmentCode("1");
			product.setSegmentDto(segmentDto);
			
			SubSegmentDto subSegmentDto = new SubSegmentDto();
			subSegmentDto.setSubSegmentCode("6");
			product.setSubSegmentDto(subSegmentDto);
			
			ManufacturerDto manufacturerDto = new ManufacturerDto();
			manufacturerDto.setManufacturerCode("1");
			product.setManufacturerDto(manufacturerDto);
			
			//product.setBitmapFlag(false);
			ExtendedProductDescDto extendedProductDescDto = new ExtendedProductDescDto();
			extendedProductDescDto.setExtendedDescId("7");
			product.setExtendedProductDescDto(extendedProductDescDto);
			product.setArchiveReasonCode(0);
			product.setUpc12("123456789");
			
			MissingImagesDto dto=new MissingImagesDto();
			product.setMissingImagesDto(dto);
			productDao.editProduct(product);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("Edit failed");

		}
	}

	/**
	 * Test case to Multiple Edit ProductNew
	 */
	@Test
	public void testMultipleEditProductNew() {
		try {
			List<ProductDto> prodList =  new ArrayList<ProductDto>();
			ProductDto product=new ProductDto();
			product.setProductCode("15725698");
			
			product.setUpc("0000029222");
			product.setArchiveFlag(true);
			
			PackageDto packageDto=new PackageDto();
			packageDto.setPackageCode("8006");
			packageDto.setPackageName("xyz");
			product.setPkgdto(packageDto);
			
			PackageSizeDto pkg= new PackageSizeDto();
			pkg.setPackageSizeDescription("Test2");
			product.setPackageSizeDto(pkg);
			
			ContainerTypeDto containerType=new ContainerTypeDto();
			containerType.setContainerTypeCode("356");
			containerType.setContainerTypeDescription("box");
			packageDto.setContainerTypeDto(containerType);
					
			
			
			BrandDto brandDto=new BrandDto();
			brandDto.setBrandCode("2");
			brandDto.setBrandName("Coke Classic Test bzzz");
			brandDto.setShortBrandName("Coke Classicc shrt bzzz");
			product.setBrandDto(brandDto);
			
			FlavorDto flavorDto=new FlavorDto();
			flavorDto.setFlavorCode("14");
			product.setFlavorDto(flavorDto);
			
			CategoryDto categoryDto=new CategoryDto();
			categoryDto.setCategoryCode("1");
			product.setCategoryDto(categoryDto);
			
			SubCategoryDto subCategoryDto=new SubCategoryDto();
			subCategoryDto.setSubCategoryCode("1");
			product.setSubCategoryDto(subCategoryDto);
			
			SegmentDto segmentDto=new SegmentDto();
			segmentDto.setSegmentCode("15");
			product.setSegmentDto(segmentDto);
			
			SubSegmentDto subSegmentDto=new SubSegmentDto();
			subSegmentDto.setSubSegmentCode("6");
			product.setSubSegmentDto(subSegmentDto);
			
			ManufacturerDto manufacturerDto=new ManufacturerDto();
			manufacturerDto.setManufacturerCode("1");
			product.setManufacturerDto(manufacturerDto);
			
			
			ExtendedProductDescDto extendedDto=new ExtendedProductDescDto();
			extendedDto.setExtendedDescId("6");
			product.setUpc12("9999999999");
			extendedDto.setLongProductDesc("abcd");
			extendedDto.setShortProductDesc("abc");
			product.setExtendedProductDescDto(extendedDto);
			
			
			prodList.add(product);
			
			ProductDto product2=new ProductDto();
			product2.setProductCode("15725699");
			
			product2.setUpc("3485800199");
			product2.setArchiveFlag(true);
			
			PackageDto packageDto2=new PackageDto();
			packageDto2.setPackageCode("10610");
			packageDto2.setPackageName("xyzz");
			product2.setPkgdto(packageDto2);
			
			PackageSizeDto pkg1= new PackageSizeDto();
			pkg1.setPackageSizeDescription("Test2");
			product2.setPackageSizeDto(pkg);
			
			ContainerTypeDto containerType2=new ContainerTypeDto();
			containerType.setContainerTypeCode("356");
			containerType.setContainerTypeDescription("box");
			packageDto2.setContainerTypeDto(containerType2);
					
			
			
			BrandDto brandDto2=new BrandDto();
			brandDto2.setBrandCode("363");
			brandDto2.setBrandName("Coke Classic Test bzcccc");
			brandDto2.setShortBrandName("Coke Classicc shrt bzcccc");
			product2.setBrandDto(brandDto2);
			
			FlavorDto flavorDto2=new FlavorDto();
			flavorDto2.setFlavorCode("39");
			product2.setFlavorDto(flavorDto2);
			
			CategoryDto categoryDto2=new CategoryDto();
			categoryDto2.setCategoryCode("1");
			product2.setCategoryDto(categoryDto2);
			
			SubCategoryDto subCategoryDto2=new SubCategoryDto();
			subCategoryDto2.setSubCategoryCode("2");
			product2.setSubCategoryDto(subCategoryDto2);
			
			SegmentDto segmentDto2=new SegmentDto();
			segmentDto2.setSegmentCode("15");
			product2.setSegmentDto(segmentDto2);
			
			SubSegmentDto subSegmentDto2=new SubSegmentDto();
			subSegmentDto2.setSubSegmentCode("6");
			product2.setSubSegmentDto(subSegmentDto2);
		    
			ManufacturerDto manufacturerDto2=new ManufacturerDto();
			manufacturerDto2.setManufacturerCode("1273");
			
			product2.setManufacturerDto(manufacturerDto2);
		
			
			ExtendedProductDescDto extendedDto2=new ExtendedProductDescDto();
			extendedDto2.setExtendedDescId("6");
			product2.setUpc12("9999999999");
			extendedDto2.setLongProductDesc("abcd");
			extendedDto2.setShortProductDesc("abc");
			product2.setExtendedProductDescDto(extendedDto2);
			
			
			prodList.add(product2);
					
			ProductDto product3=new ProductDto();
			product3.setProductCode("15725697");
			
			product3.setUpc("");
			product3.setArchiveFlag(false);
			
			PackageDto packageDto3=new PackageDto();
			packageDto3.setPackageCode("8006");
			packageDto3.setPackageName("xyz12");
			product3.setPkgdto(packageDto3);
			
			PackageSizeDto pkg3= new PackageSizeDto();
			pkg3.setPackageSizeDescription("Test2");
			product3.setPackageSizeDto(pkg);
			
			
			ContainerTypeDto containerType3=new ContainerTypeDto();
			containerType.setContainerTypeCode("356");
			containerType.setContainerTypeDescription("box");
			packageDto3.setContainerTypeDto(containerType3);
			
			BrandDto brandDto3=new BrandDto();
			brandDto3.setBrandCode("2");
			brandDto3.setBrandName("Test12");
			brandDto3.setShortBrandName("Test12");
			product3.setBrandDto(brandDto3);
			
			FlavorDto flavorDto3=new FlavorDto();
			flavorDto3.setFlavorCode("14");
			product3.setFlavorDto(flavorDto3);
			CategoryDto categoryDto3=new CategoryDto();
			categoryDto3.setCategoryCode("1");
			product3.setCategoryDto(categoryDto3);
			SubCategoryDto subCategoryDto3=new SubCategoryDto();
			subCategoryDto3.setSubCategoryCode("1");
			product3.setSubCategoryDto(subCategoryDto3);
			SegmentDto segmentDto3=new SegmentDto();
			segmentDto3.setSegmentCode("15");
			product3.setSegmentDto(segmentDto3);
			SubSegmentDto subSegmentDto3=new SubSegmentDto();
			subSegmentDto3.setSubSegmentCode("6");
			product3.setSubSegmentDto(subSegmentDto3);
			ManufacturerDto manufacturerDto3=new ManufacturerDto();
			manufacturerDto3.setManufacturerCode("1");
			product3.setManufacturerDto(manufacturerDto3);
			
			ExtendedProductDescDto extendedDto3=new ExtendedProductDescDto();
			extendedDto3.setExtendedDescId("6");
			product3.setUpc12("9999999999");
			extendedDto3.setLongProductDesc("abcd12");
			extendedDto3.setShortProductDesc("abc12");
			product3.setExtendedProductDescDto(extendedDto3);
			
			
			//prodList.add(product3);
			
			productDao.multiEditProduct(product3,prodList);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail(e.getMessage());
			fail("Edit failed");

		}
	}

	/**
	 * Test case to check Duplicate Product
	 */
	@Test
	public void testcheckDuplicateProduct() {
		try {
			boolean result=productDao.checkDuplicateProduct("4900006962");		
			assertTrue("Result: ", result);
		}catch(ImproException e) {
			fail("Check Dumplicate Product method failed "+e.getMessage());
		}
	
	}


}



