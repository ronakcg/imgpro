package com.ko.impro.dao.impl.containertype;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dao.containerType.ContainerTypeDao;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.exception.ImproException;

import io.jsonwebtoken.lang.Collections;

/**
 * @author hlekshmy
 * Test class for ContainerTypE API
 *
 */
public class ContainerTypeDaoImplTest extends BaseTest {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ContainerTypeDaoImplTest.class);

	/**
	 *  Dao Reference
	 */
	@Autowired(required = true)
	private ContainerTypeDao containerTypeDao;

	/**
	 * Add ContainerType
	 */
	@Test
	public void testAddContainerType() {
		try {
			Random rand = new Random();

			final ContainerTypeDto containerTypeDto = new ContainerTypeDto();
			containerTypeDto.setContainerTypeDescription("Can2"+rand.nextInt());
			containerTypeDao.addContainerType(containerTypeDto);
			LOG.info("Record has been inserted successfully");
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}

	/**
	 *  Duplicate Check
	 */
	@Test
	public void testCheckDuplicateContainerType() {
		try {
			assertTrue("Duplicate Check",containerTypeDao.checkDuplicateContainerType("Can1"));
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Load ContainerType ID
	 */
	@Test
	public void testLoadContainerTypeById() {
		try {
			final ContainerTypeDto containerTypeDto= containerTypeDao.loadContainerType(374);
			if(LOG.isInfoEnabled()) {
				LOG.info(containerTypeDto.getContainerTypeCode()+ "  " + containerTypeDto.getContainerTypeDescription());
			}
		} catch (ImproException e) {
			fail(e.getMessage());
			LOG.info("Content Loaded failed");
		}
	}
	
	/**
	 * Edit ContainerType
	 */
	@Test
	public void testEditContainerType() {
		final ContainerTypeDto containerTypeDto= new ContainerTypeDto();
		containerTypeDto.setContainerTypeCode("384");
		containerTypeDto.setContainerTypeDescription("Can2");
		try {
			containerTypeDao.editContainerType(containerTypeDto);
			LOG.info("Edit Done");
		} catch (ImproException e) {
			
			fail(e.getMessage());
		}
		
	}
	
	/**
	 * Delete ContainerType
	 */
	@Test
	public void testDeleteContainerType() {
		final ContainerTypeDto containerTypeDto= new ContainerTypeDto();
		containerTypeDto.setContainerTypeCode("384");
		try {
			containerTypeDao.deleteContainerType(containerTypeDto);
			LOG.info("Deletion Done");
		} catch (ImproException e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Load all the containerTypes
	 */
	@Test
	public void testLoadContainerTypes() {
		try {
			final List<ContainerTypeDto> containers=containerTypeDao.loadContainerTypes();
			assertTrue("Loading Container",containers != null && !Collections.isEmpty(containers));
			for(final ContainerTypeDto dto:containers) {
				LOG.info(dto.getContainerTypeDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}
	

}
