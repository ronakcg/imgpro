package com.ko.impro.testsuite;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;


@RunWith(JUnitPlatform.class)
@SelectPackages(
		{
			"com.ko.impro.dao.impl.category",
			"com.ko.impro.dao.impl.subcategory",
			"com.ko.impro.dao.impl.flavour",
			"com.ko.impro.dao.impl.packages",
			"com.ko.impro.dao.impl.packagestyle",
			"com.ko.impro.dao.impl.auditlog",
			"com.ko.impro.dao.impl.brand",
			"com.ko.impro.dao.impl.manufacturer",
			"com.ko.impro.dao.impl.containertype",
			"com.ko.impro.dao.impl.deletemessagedata"
			
		}
	)

public class MaintenanceTestSuite {

}
