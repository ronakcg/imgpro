package com.ko.impro.service.impl.product;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.dto.EditImageDto;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.HiResImageDto;
import com.ko.impro.dto.JpgImageDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.dto.MissingImagesDto;
import com.ko.impro.dto.MockImageDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.dto.PaginationDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductRequestWrapperDto;
import com.ko.impro.dto.ProductResponseDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.StdImageDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.report.ExcelGeneratorFactory;
import com.ko.impro.report.ProductExcelReportGenerator;
import com.ko.impro.service.product.ProductService;
import com.ko.impro.util.AppConstants;

/**
 * Test Cases of ProductServiceImpl methods
 */
public class ProductServiceImplTest extends BaseTest {

	private static final Logger LOG = LoggerFactory.getLogger(ProductServiceImplTest.class);
	/**
	 * Reference of ProductService
	 */
	@Autowired(required = true)
	private ProductService productService;

	/**
	 * Test case to Search Products
	 */
	@Test
	public void testSearchProducts() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(100);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setColumnName("ProductName");
			productSearchDto.setColumnOrder("ASC");
			productSearchDto.setActiveSearch(true);
			// productSearchDto.setProductCode("15725521");
			ProductResponseDto resp = productService.searchProducts(productSearchDto);
			for (ProductDto dto : resp.getProductDtoList()) {
				LOG.info(dto.getProductName());
			}
			assertTrue(resp.getPageDto().getTotalNoOfRecords() > 0);
			if (LOG.isInfoEnabled()) {
				LOG.info("Total Count - " + resp.getPageDto().getTotalNoOfRecords());
			}
			assertTrue(resp.getProductDtoList() != null && resp.getProductDtoList().size() > 0);
			for (ProductDto dto : resp.getProductDtoList()) {
				LOG.info(dto.getProductName());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}
	
	@Test
	public void testSearchSimilarProducts() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			List<String> brandList=new ArrayList<String>();
			brandList.add("2558");
			productSearchDto.setBrandList(brandList);
			List<ProductDto> resp = productService.getSimilarProductDetails(productSearchDto);
			for (ProductDto dto : resp) {
				LOG.info(dto.getProductName());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	/**
	 * Test case to get ExcelGenerator
	 */
	public ProductExcelReportGenerator getExcelGenerator() throws IOException {
		return ExcelGeneratorFactory.getInstance().getProductExcelReportGenerator(AppConstants.PRODUCT_FILENAME,
				AppConstants.PRODUCT_SHEETNAME);
	}

	/**
	 * Method used to export all product details when the users click exportButton
	 * 
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testExportAllProducts() throws ImproException, IOException, InvalidFormatException, ParseException {
		ProductRequestWrapperDto productRequestWrapperDto = new ProductRequestWrapperDto();
		ProductSearchDto productSearchDto = new ProductSearchDto();
		List<ProductDto> productDtoList = productService.exportAllProducts(productSearchDto);
		productRequestWrapperDto.setProductSearchDto(productSearchDto);
		ProductExcelReportGenerator excelGenerator = getExcelGenerator();
		XSSFWorkbook workBook = excelGenerator.generateReport(productDtoList);
		try (FileOutputStream outputStream = new FileOutputStream("report.xlsx")) {
			workBook.write(outputStream);
		}
	}

	/**
	 * Method used to export all product details when the users clicks the Selected
	 * Check box and click exportButton
	 * 
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testSelectedProductExport() throws ImproException, IOException {
		ProductRequestWrapperDto productRequestWrapperDto = new ProductRequestWrapperDto();
		ProductSearchDto productSearchDto = new ProductSearchDto();
		List<String> subSegmentList = new ArrayList<String>();
		subSegmentList.add("18");
		productSearchDto.setSubSegmentList(subSegmentList);
		List<ProductDto> productDtoList = productService.exportAllProducts(productSearchDto);
		productRequestWrapperDto.setProductSearchDto(productSearchDto);
		ProductExcelReportGenerator excelGenerator = getExcelGenerator();
		XSSFWorkbook workBook = excelGenerator.generateReport(productDtoList);
		try (FileOutputStream outputStream = new FileOutputStream("report.xlsx")) {
			workBook.write(outputStream);
		}
	}

	/**
	 * Method used to export all product details when the users Selects columns from
	 * Column selector Module, selects missing image checkbox and click exportButton
	 * 
	 * @throws ImproException
	 * @throws FileNotFoundException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testMissingImgExportProducts() throws ImproException, FileNotFoundException, IOException {
		ProductRequestWrapperDto productRequestWrapperDto = new ProductRequestWrapperDto();
		ProductSearchDto productSearchDto = new ProductSearchDto();
		List<ProductDto> productDtoList = productService.exportAllProducts(productSearchDto);

		productSearchDto.setMissingImages(true);
		MissingImagesDto mID = new MissingImagesDto();
		mID.setIshiRes1(true);
		mID.setIshiRes2(true);
		mID.setIshiRes3(true);
		/*
		 * mID.setStdFrntView(true); mID.setStdRgtView(true); mID.setStdTopView(true);
		 */

		/*
		 * selectedImgHeader.add("STDFRONTVIEW"); selectedImgHeader.add("STDRIGHTVIEW");
		 * selectedImgHeader.add("STDTOPVIEW");
		 */

		// productSearchDto.setSelectedImgHeader(selectedImgHeader);
		productRequestWrapperDto.setProductSearchDto(productSearchDto);
		ProductExcelReportGenerator excelGenerator = getExcelGenerator();
		XSSFWorkbook workBook = excelGenerator.generateReport(productDtoList);

		try (FileOutputStream outputStream = new FileOutputStream("report.xlsx")) {
			workBook.write(outputStream);
		}
	}

	/**
	 * Test case to Add Product For DuplicateID
	 */
	@Test
	public void testAddProductForDuplicateID() {
		ProductDto product = new ProductDto();
		product.setProductCode("20110");
		// product.setBitmapName("4900000634S");
		product.setUpc("4900000634");
		product.setArchiveFlag(false);
		PackageDto packageDto = new PackageDto();
		packageDto.setPackageCode("8006");
		product.setPkgdto(packageDto);
		BrandDto brandDto = new BrandDto();
		brandDto.setBrandCode("2");
		product.setBrandDto(brandDto);
		FlavorDto flavorDto = new FlavorDto();
		flavorDto.setFlavorCode("14");
		product.setFlavorDto(flavorDto);
		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setCategoryCode("1");
		SubCategoryDto subCategoryDto = new SubCategoryDto();
		subCategoryDto.setSubCategoryCode("1");
		product.setSubCategoryDto(subCategoryDto);
		SegmentDto segmentDto = new SegmentDto();
		segmentDto.setSegmentCode("1");
		product.setSegmentDto(segmentDto);
		SubSegmentDto subSegmentDto = new SubSegmentDto();
		subSegmentDto.setSubSegmentCode("6");
		product.setSubSegmentDto(subSegmentDto);
		ManufacturerDto manufacturerDto = new ManufacturerDto();
		manufacturerDto.setManufacturerCode("1");
		product.setManufacturerDto(manufacturerDto);
		// product.setBitmapFlag(false);
		ExtendedProductDescDto extendedProductDescDto = new ExtendedProductDescDto();
		extendedProductDescDto.setExtendedDescId("7");
		product.setUpc12("9999999999");
		product.setExtendedProductDescDto(extendedProductDescDto);
		try {
			boolean var = productService.checkDuplicateProduct("4900000634S");
			assertTrue(var);
		} catch (ImproException e) {
			fail(" Add Product failed");
		}
	}

	/**
	 * Test case to Add Product
	 *//*
	@Test
	public void testAddProduct() {
		ProductDto product = new ProductDto();
		// product.setProductCode("20072");
		// product.setBitmapName("0000029241S");
		product.setArchiveFlag(false);

		PackageDto packageDto = new PackageDto();
		packageDto.setPackageCode("8006");
		ContainerTypeDto containerTypeDto = new ContainerTypeDto();
		containerTypeDto.setContainerTypeDescription("Test1");
		packageDto.setContainerTypeDto(containerTypeDto);
		product.setPkgdto(packageDto);

		PackageSizeDto pkg = new PackageSizeDto();
		pkg.setPackageSizeDescription("Test2");
		product.setPackageSizeDto(pkg);

		BrandDto brandDto = new BrandDto();
		brandDto.setBrandCode("2");
		brandDto.setBrandName("Test3");
		product.setBrandDto(brandDto);

		product.setUpc("0000029241");

		FlavorDto flavorDto = new FlavorDto();
		flavorDto.setFlavorCode("14");
		product.setFlavorDto(flavorDto);

		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setCategoryCode("1");
		product.setCategoryDto(categoryDto);

		SubCategoryDto subCategoryDto = new SubCategoryDto();
		subCategoryDto.setSubCategoryCode("1");
		product.setSubCategoryDto(subCategoryDto);

		SegmentDto segmentDto = new SegmentDto();
		segmentDto.setSegmentCode("1");
		product.setSegmentDto(segmentDto);

		SubSegmentDto subSegmentDto = new SubSegmentDto();
		subSegmentDto.setSubSegmentCode("6");
		product.setSubSegmentDto(subSegmentDto);

		ManufacturerDto manufacturerDto = new ManufacturerDto();
		manufacturerDto.setManufacturerCode("1");
		product.setManufacturerDto(manufacturerDto);

		// product.setBitmapFlag(false);
		ExtendedProductDescDto extendedProductDescDto = new ExtendedProductDescDto();
		extendedProductDescDto.setExtendedDescId("7");
		product.setExtendedProductDescDto(extendedProductDescDto);
		product.setArchiveReasonCode(0);
		product.setUpc12("9999999999");

		// product.setAutoUpdateFlag(AppConstants.AUTO_UPDATE_FLAG_YES);
		try {
			productService.addProduct(product);

		} catch (Exception e) {

			fail(" Add Product failed " + e.getMessage());
		}

	}
*/
	/**
	 * Test case to Add Product
	 * 
	 * @throws IOException
	 */
	@Test
	public void testAddProduct() throws IOException {
		ProductDto product = new ProductDto();
		// product.setProductCode("20072");
		product.setBitMapName("9999912345S");
		product.setArchiveFlag(false);

		PackageDto packageDto = new PackageDto();
		packageDto.setPackageCode("8006");
		ContainerTypeDto containerTypeDto = new ContainerTypeDto();
		containerTypeDto.setContainerTypeDescription("Test1");
		packageDto.setContainerTypeDto(containerTypeDto);
		product.setPkgdto(packageDto);

		PackageSizeDto pkg = new PackageSizeDto();
		pkg.setPackageSizeDescription("Test2");
		product.setPackageSizeDto(pkg);

		BrandDto brandDto = new BrandDto();
		brandDto.setBrandCode("2");
		brandDto.setBrandName("Test3");
		product.setBrandDto(brandDto);

		product.setUpc("0000029241");

		FlavorDto flavorDto = new FlavorDto();
		flavorDto.setFlavorCode("14");
		product.setFlavorDto(flavorDto);

		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setCategoryCode("1");
		product.setCategoryDto(categoryDto);

		SubCategoryDto subCategoryDto = new SubCategoryDto();
		subCategoryDto.setSubCategoryCode("1");
		product.setSubCategoryDto(subCategoryDto);

		SegmentDto segmentDto = new SegmentDto();
		segmentDto.setSegmentCode("1");
		product.setSegmentDto(segmentDto);

		SubSegmentDto subSegmentDto = new SubSegmentDto();
		subSegmentDto.setSubSegmentCode("6");
		product.setSubSegmentDto(subSegmentDto);

		ManufacturerDto manufacturerDto = new ManufacturerDto();
		manufacturerDto.setManufacturerCode("1");
		product.setManufacturerDto(manufacturerDto);

		// product.setBitmapFlag(false);
		ExtendedProductDescDto extendedProductDescDto = new ExtendedProductDescDto();
		extendedProductDescDto.setExtendedDescId("7");
		product.setExtendedProductDescDto(extendedProductDescDto);
		product.setArchiveReasonCode(0);
		product.setUpc12("9999999999");

		MissingImagesDto missingImagesDto = new MissingImagesDto();
		missingImagesDto.setIslowRes1(false);
		missingImagesDto.setIslowRes2(false);
		missingImagesDto.setIslowRes3(false);
		missingImagesDto.setIslowRes7(false);
		missingImagesDto.setIslowRes8(false);
		missingImagesDto.setIslowRes9(false);

		missingImagesDto.setIshiRes1(false);
		missingImagesDto.setIshiRes2(false);
		missingImagesDto.setIshiRes3(false);
		missingImagesDto.setIshiRes7(false);
		missingImagesDto.setIshiRes8(false);
		missingImagesDto.setIshiRes9(false);

		missingImagesDto.setIsmock1(false);
		missingImagesDto.setIsmock2(false);
		missingImagesDto.setIsmock3(false);
		missingImagesDto.setIsmock7(false);
		missingImagesDto.setIsmock8(false);
		missingImagesDto.setIsmock9(false);

		missingImagesDto.setIsjpg(false);

		product.setMissingImagesDto(missingImagesDto);
		/** Image data setting */
		String filePath = "C:/Users/vmuchand/Desktop/impro base64/imgurl.txt";
		String data = readAllBytesJava7(filePath);

		EditImageDto editImageDto = new EditImageDto();
		StdImageDto stdImageDto = new StdImageDto();
		stdImageDto.setFile1(data);
		stdImageDto.setFile2(data);
		stdImageDto.setFile3(data);
		stdImageDto.setFile7(data);
		stdImageDto.setFile8(data);
		stdImageDto.setFile9(data);

		HiResImageDto hiResImageDto = new HiResImageDto();
		hiResImageDto.setFile1(data);
		hiResImageDto.setFile2(data);
		hiResImageDto.setFile3(data);
		hiResImageDto.setFile7(data);
		hiResImageDto.setFile8(data);
		hiResImageDto.setFile9(data);

		MockImageDto mockImageDto = new MockImageDto();
		mockImageDto.setFile1(data);
		mockImageDto.setFile2(data);
		mockImageDto.setFile3(data);
		mockImageDto.setFile7(data);
		mockImageDto.setFile8(data);
		mockImageDto.setFile9(data);

		JpgImageDto jpgImageDto = new JpgImageDto();
		jpgImageDto.setFile1(data);

		editImageDto.setStdImageDto(stdImageDto);
		editImageDto.setHiResImageDto(hiResImageDto);
		editImageDto.setMockImageDto(mockImageDto);
		editImageDto.setJpgImageDto(jpgImageDto);
		product.setEditImageDto(editImageDto);
		/*******************************************************/
		try {
			productService.addProduct(product);

		} catch (Exception e) {

			fail(" Add Product failed " + e.getMessage());
		}

	}

	

	/**
	 * @param filePath
	 * @return
	 */
	private static String readAllBytesJava7(String filePath) {
		String content = "";
		try {
			content = new String(Files.readAllBytes(Paths.get(filePath)));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return content;
	}

}
