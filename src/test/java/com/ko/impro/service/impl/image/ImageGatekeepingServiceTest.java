package com.ko.impro.service.impl.image;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dto.EditImageDto;
import com.ko.impro.dto.HiResImageDto;
import com.ko.impro.dto.MissingImagesDto;
import com.ko.impro.dto.MockImageDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.dto.StdImageDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.image.ImageGatekeepingService;
import com.ko.impro.util.AppConstants;

public class ImageGatekeepingServiceTest extends BaseTest {

	/**
	 * Logger object
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ImageGatekeepingServiceTest.class);
	
	/**
	 * Reference of ImageGatekeepingService
	 */
	@Autowired(required = true)
	private ImageGatekeepingService service;
		
	/**
	 * @param filePath
	 * @return
	 */
	private static String readAllBytesJava7(String filePath) {
		String content = "";
		try {
			content = new String(Files.readAllBytes(Paths.get(filePath)));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return content;
	}
	
	/**
	 * Test case to display new and existing image
	 */
	@Test
	public void getNewImagesById() throws ImproException {
		ProductGateKeepingDto prodGateDto = new ProductGateKeepingDto();
		MissingImagesDto missingImagesDto = new MissingImagesDto();
		ProductDto prodDto = new ProductDto();
		
		missingImagesDto.setHiRes1("N");
		missingImagesDto.setHiRes1Url("");
		missingImagesDto.setHiRes2("N");
		missingImagesDto.setHiRes2Url("");
		missingImagesDto.setHiRes3("N");
		missingImagesDto.setHiRes3Url("");
		missingImagesDto.setHiRes7("N");
		missingImagesDto.setHiRes7Url("");
		missingImagesDto.setHiRes8("N");
		missingImagesDto.setHiRes8Url("");
		missingImagesDto.setHiRes9("N");
		missingImagesDto.setHiRes9Url("");

		missingImagesDto.setLowRes1("N");
		missingImagesDto.setLowRes1Url("");
		missingImagesDto.setLowRes2("N");
		missingImagesDto.setLowRes2Url("");
		missingImagesDto.setLowRes3("N");
		missingImagesDto.setLowRes3Url("");
		missingImagesDto.setLowRes7("N");
		missingImagesDto.setLowRes7Url("");
		missingImagesDto.setLowRes8("N");
		missingImagesDto.setLowRes8Url("");
		missingImagesDto.setLowRes9("N");
		missingImagesDto.setLowRes9Url("");
		

		missingImagesDto.setMock1("N");
		missingImagesDto.setMock1Url("");
		missingImagesDto.setMock2("N");
		missingImagesDto.setMock2Url("");
		missingImagesDto.setMock3("N");
		missingImagesDto.setMock3Url("");
		missingImagesDto.setMock7("N");
		missingImagesDto.setMock7Url("");
		missingImagesDto.setMock8("N");
		missingImagesDto.setMock8Url("");
		missingImagesDto.setMock9("N");
		missingImagesDto.setMock9Url("");
		
		prodGateDto.setMissingImagesDto(missingImagesDto);
		
		prodDto.setMissingImagesDto(missingImagesDto);
		prodGateDto.setProductDto(prodDto);
		
		prodGateDto.setProductCode("15719027");
		prodGateDto.setBitMapName("0000029200s");
		
		LOG.info("New Image Status"+prodGateDto.getMissingImagesDto().isIshiRes1());
		LOG.info("Existing Image Status"+prodGateDto.getProductDto().getMissingImagesDto().isIshiRes1());
		
		
		service.getImagesById(prodGateDto);
		LOG.info("done.."+ prodGateDto);
	}
	

	/**
	 * Test case to Replace New OR Existing Image With Edited Images and keep a copy of old one in archive image
	 */
	@Test
	public void testEditImagesReplace() throws ImproException {
		LOG.info("Test image Edit start");
		ProductGateKeepingDto prodGateDto = new ProductGateKeepingDto();
		prodGateDto.setBitMapName("0000029100s");
		ProductDto product = new ProductDto();
		product.setBitMapName("0000029100s");
		product.setArchiveFlag(false);
		
		//For Existing Images Replace 
		MissingImagesDto missingImagesDtoProd = new MissingImagesDto();
		missingImagesDtoProd.setIslowRes1(false);
		missingImagesDtoProd.setIslowRes2(false);
		missingImagesDtoProd.setIslowRes3(false);
		missingImagesDtoProd.setIslowRes7(false);
		missingImagesDtoProd.setIslowRes8(false);
		missingImagesDtoProd.setIslowRes9(false);

		missingImagesDtoProd.setIshiRes1(false);
		missingImagesDtoProd.setIshiRes2(false);
		missingImagesDtoProd.setIshiRes3(false);
		missingImagesDtoProd.setIshiRes7(false);
		missingImagesDtoProd.setIshiRes8(false);
		missingImagesDtoProd.setIshiRes9(false);

		missingImagesDtoProd.setIsmock1(false);
		missingImagesDtoProd.setIsmock2(false);
		missingImagesDtoProd.setIsmock3(false);
		missingImagesDtoProd.setIsmock7(false);
		missingImagesDtoProd.setIsmock8(false);
		missingImagesDtoProd.setIsmock9(false);
		
		missingImagesDtoProd.setLowRes1Url("https://s3.amazonaws.com/improproductbucket1/active/Std/00000/29200s.1.png");
		missingImagesDtoProd.setLowRes2Url("https://s3.amazonaws.com/improproductbucket1/active/Std/00000/29200s.2.png");
		missingImagesDtoProd.setLowRes3Url("https://s3.amazonaws.com/improproductbucket1/active/Std/00000/29200s.3.png");
		missingImagesDtoProd.setLowRes7Url("https://s3.amazonaws.com/improproductbucket1/active/Std/00000/29200s.7.png");
		missingImagesDtoProd.setLowRes8Url("https://s3.amazonaws.com/improproductbucket1/active/Std/00000/29200s.8.png");
		missingImagesDtoProd.setLowRes9Url("https://s3.amazonaws.com/improproductbucket1/active/Std/00000/29200s.9.png");
		
		missingImagesDtoProd.setHiRes1Url("https://s3.amazonaws.com/improproductbucket1/active/HiRes/00000/29200s.1.png");
		missingImagesDtoProd.setHiRes2Url("https://s3.amazonaws.com/improproductbucket1/active/HiRes/00000/29200s.2.png");
		missingImagesDtoProd.setHiRes3Url("https://s3.amazonaws.com/improproductbucket1/active/HiRes/00000/29200s.3.png");
		missingImagesDtoProd.setHiRes7Url("https://s3.amazonaws.com/improproductbucket1/active/HiRes/00000/29200s.7.png");
		missingImagesDtoProd.setHiRes8Url("https://s3.amazonaws.com/improproductbucket1/active/HiRes/00000/29200s.8.png");
		missingImagesDtoProd.setHiRes9Url("https://s3.amazonaws.com/improproductbucket1/active/HiRes/00000/29200s.9.png");
		
		missingImagesDtoProd.setMock1Url("https://s3.amazonaws.com/improproductbucket1/active/Mock/00000/29200s.1.png");
		missingImagesDtoProd.setMock2Url("https://s3.amazonaws.com/improproductbucket1/active/Mock/00000/29200s.2.png");
		missingImagesDtoProd.setMock3Url("https://s3.amazonaws.com/improproductbucket1/active/Mock/00000/29200s.3.png");
		missingImagesDtoProd.setMock7Url("https://s3.amazonaws.com/improproductbucket1/active/Mock/00000/29200s.7.png");
		missingImagesDtoProd.setMock8Url("https://s3.amazonaws.com/improproductbucket1/active/Mock/00000/29200s.8.png");
		missingImagesDtoProd.setMock9Url("https://s3.amazonaws.com/improproductbucket1/active/Mock/00000/29200s.9.png");
		
		/** Image data setting */
		String filePath = "D:/Users/swshedge/Documents/My Received Files/imgurl.txt";
		
		String data = readAllBytesJava7(filePath);
		
		EditImageDto editImageDtoProd = new EditImageDto();
		StdImageDto stdImageDto = new StdImageDto();
		stdImageDto.setFile1(data);
		stdImageDto.setFile2(data);
		stdImageDto.setFile3(data);
		stdImageDto.setFile7(data);
		stdImageDto.setFile8(data);
		stdImageDto.setFile9(data);

		stdImageDto.setEditFLag1(true);
		stdImageDto.setEditFLag2(true);
		stdImageDto.setEditFLag3(true);
		stdImageDto.setEditFLag7(true);
		stdImageDto.setEditFLag8(true);
		stdImageDto.setEditFLag9(true);

		HiResImageDto hiResImageDto = new HiResImageDto();
		hiResImageDto.setFile1(data);
		hiResImageDto.setFile2(data);
		hiResImageDto.setFile3(data);
		hiResImageDto.setFile7(data);
		hiResImageDto.setFile8(data);
		hiResImageDto.setFile9(data);
		
		hiResImageDto.setEditFLag1(true);
		hiResImageDto.setEditFLag2(true);
		hiResImageDto.setEditFLag3(true);
		hiResImageDto.setEditFLag7(true);
		hiResImageDto.setEditFLag8(true);
		hiResImageDto.setEditFLag9(true);

		MockImageDto mockImageDto = new MockImageDto();
		mockImageDto.setFile1(data);
		mockImageDto.setFile2(data);
		mockImageDto.setFile3(data);
		mockImageDto.setFile7(data);
		mockImageDto.setFile8(data);
		mockImageDto.setFile9(data);
		
		mockImageDto.setEditFLag1(true);
		mockImageDto.setEditFLag2(true);
		mockImageDto.setEditFLag3(true);
		mockImageDto.setEditFLag7(true);
		mockImageDto.setEditFLag8(true);
		mockImageDto.setEditFLag9(true);
		

		editImageDtoProd.setStdImageDto(stdImageDto);
		editImageDtoProd.setHiResImageDto(hiResImageDto);
		editImageDtoProd.setMockImageDto(mockImageDto);
		
		product.setMissingImagesDto(missingImagesDtoProd);
		product.setEditImageDto(editImageDtoProd);
		
		//For New Images Replace
		
		MissingImagesDto missingImagesDtoGate = new MissingImagesDto();
		missingImagesDtoGate.setIslowRes1(false);
		missingImagesDtoGate.setIslowRes2(false);
		missingImagesDtoGate.setIslowRes3(false);
		missingImagesDtoGate.setIslowRes7(false);
		missingImagesDtoGate.setIslowRes8(false);
		missingImagesDtoGate.setIslowRes9(false);

		missingImagesDtoGate.setIshiRes1(false);
		missingImagesDtoGate.setIshiRes2(false);
		missingImagesDtoGate.setIshiRes3(false);
		missingImagesDtoGate.setIshiRes7(false);
		missingImagesDtoGate.setIshiRes8(false);
		missingImagesDtoGate.setIshiRes9(false);

		missingImagesDtoGate.setIsmock1(false);
		missingImagesDtoGate.setIsmock2(false);
		missingImagesDtoGate.setIsmock3(false);
		missingImagesDtoGate.setIsmock7(false);
		missingImagesDtoGate.setIsmock8(false);
		missingImagesDtoGate.setIsmock9(false);
		
		missingImagesDtoGate.setLowRes1Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.1.png");
		missingImagesDtoGate.setLowRes2Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.2.png");
		missingImagesDtoGate.setLowRes3Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.3.png");
		missingImagesDtoGate.setLowRes7Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.7.png");
		missingImagesDtoGate.setLowRes8Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.8.png");
		missingImagesDtoGate.setLowRes9Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.9.png");
		
		missingImagesDtoGate.setHiRes1Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.1.png");
		missingImagesDtoGate.setHiRes2Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.2.png");
		missingImagesDtoGate.setHiRes3Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.3.png");
		missingImagesDtoGate.setHiRes7Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.7.png");
		missingImagesDtoGate.setHiRes8Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.8.png");
		missingImagesDtoGate.setHiRes9Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.9.png");
		
		missingImagesDtoGate.setMock1Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.1.png");
		missingImagesDtoGate.setMock2Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.2.png");
		missingImagesDtoGate.setMock3Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.3.png");
		missingImagesDtoGate.setMock7Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.7.png");
		missingImagesDtoGate.setMock8Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.8.png");
		missingImagesDtoGate.setMock9Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.9.png");
			
		String filePathGate = "D:/Users/swshedge/Documents/My Received Files/imgurl.txt";
		
		String dataGate = readAllBytesJava7(filePathGate);
		
		EditImageDto editImageDtoGate = new EditImageDto();
		StdImageDto stdImageDtoGate = new StdImageDto();
		stdImageDtoGate.setFile1(dataGate);
		stdImageDtoGate.setFile2(dataGate);
		stdImageDtoGate.setFile3(dataGate);
		stdImageDtoGate.setFile7(dataGate);
		stdImageDtoGate.setFile8(dataGate);
		stdImageDtoGate.setFile9(dataGate);
		
		stdImageDtoGate.setEditFLag1(true);
		stdImageDtoGate.setEditFLag2(true);
		stdImageDtoGate.setEditFLag3(true);
		stdImageDtoGate.setEditFLag7(true);
		stdImageDtoGate.setEditFLag8(true);
		stdImageDtoGate.setEditFLag9(true);
		
		
		HiResImageDto hiResImageDtoGate = new HiResImageDto();
		hiResImageDtoGate.setFile1(dataGate);
		hiResImageDtoGate.setFile2(dataGate);
		hiResImageDtoGate.setFile3(dataGate);
		hiResImageDtoGate.setFile7(dataGate);
		hiResImageDtoGate.setFile8(dataGate);
		hiResImageDtoGate.setFile9(dataGate);
		
		hiResImageDtoGate.setEditFLag1(true);
		hiResImageDtoGate.setEditFLag2(true);
		hiResImageDtoGate.setEditFLag3(true);
		hiResImageDtoGate.setEditFLag7(true);
		hiResImageDtoGate.setEditFLag8(true);
		hiResImageDtoGate.setEditFLag9(true);
		
		MockImageDto mockImageDtoGate = new MockImageDto();
		mockImageDtoGate.setFile1(dataGate);
		mockImageDtoGate.setFile2(dataGate);
		mockImageDtoGate.setFile3(dataGate);
		mockImageDtoGate.setFile7(dataGate);
		mockImageDtoGate.setFile8(dataGate);
		mockImageDtoGate.setFile9(dataGate);
		
		mockImageDtoGate.setEditFLag1(true);
		mockImageDtoGate.setEditFLag2(true);
		mockImageDtoGate.setEditFLag3(true);
		mockImageDtoGate.setEditFLag7(true);
		mockImageDtoGate.setEditFLag8(true);
		mockImageDtoGate.setEditFLag9(true);

		editImageDtoGate.setStdImageDto(stdImageDtoGate);
		editImageDtoGate.setHiResImageDto(hiResImageDtoGate);
		editImageDtoGate.setMockImageDto(mockImageDtoGate);
		
		prodGateDto.setMissingImagesDto(missingImagesDtoGate);
		prodGateDto.setEditImageDto(editImageDtoGate);
		prodGateDto.setProductDto(product);
		
		service.editImage(prodGateDto);
		
		LOG.info("Test image Edit End");
		
	}
	
	/**
	 * Test case to add New Images while edit on
	 */
	@Test
	public void testEditImagesAdd() throws ImproException {
		LOG.info("Test image Edit start");
		ProductGateKeepingDto prodGateDto = new ProductGateKeepingDto();
		prodGateDto.setBitMapName("0000029200s");

		ProductDto product = new ProductDto();
		product.setBitMapName("0000029200s");
		product.setArchiveFlag(false);
		
		//For Add images under New Images section 
		
		MissingImagesDto missingImagesDtoGate = new MissingImagesDto();

		String filePathGate = "D:/Users/swshedge/Documents/My Received Files/imgurl.txt";
		
		String dataGate = readAllBytesJava7(filePathGate);
		
		EditImageDto editImageDtoGate = new EditImageDto();
		StdImageDto stdImageDtoGate = new StdImageDto();
		stdImageDtoGate.setFile1(dataGate);
		stdImageDtoGate.setFile2(dataGate);
		stdImageDtoGate.setFile3(dataGate);
		stdImageDtoGate.setFile7(dataGate);
		stdImageDtoGate.setFile8(dataGate);
		stdImageDtoGate.setFile9(dataGate);
		
		stdImageDtoGate.setAddFLag1(true);
		stdImageDtoGate.setAddFLag2(true);
		stdImageDtoGate.setAddFLag3(true);
		stdImageDtoGate.setAddFLag7(true);
		stdImageDtoGate.setAddFLag8(true);
		stdImageDtoGate.setAddFLag9(true);
		
		
		HiResImageDto hiResImageDtoGate = new HiResImageDto();
		hiResImageDtoGate.setFile1(dataGate);
		hiResImageDtoGate.setFile2(dataGate);
		hiResImageDtoGate.setFile3(dataGate);
		hiResImageDtoGate.setFile7(dataGate);
		hiResImageDtoGate.setFile8(dataGate);
		hiResImageDtoGate.setFile9(dataGate);
		
		hiResImageDtoGate.setAddFLag1(true);
		hiResImageDtoGate.setAddFLag2(true);
		hiResImageDtoGate.setAddFLag3(true);
		hiResImageDtoGate.setAddFLag7(true);
		hiResImageDtoGate.setAddFLag8(true);
		hiResImageDtoGate.setAddFLag9(true);
		
		MockImageDto mockImageDtoGate = new MockImageDto();
		mockImageDtoGate.setFile1(dataGate);
		mockImageDtoGate.setFile2(dataGate);
		mockImageDtoGate.setFile3(dataGate);
		mockImageDtoGate.setFile7(dataGate);
		mockImageDtoGate.setFile8(dataGate);
		mockImageDtoGate.setFile9(dataGate);
		
		mockImageDtoGate.setAddFLag1(true);
		mockImageDtoGate.setAddFLag2(true);
		mockImageDtoGate.setAddFLag3(true);
		mockImageDtoGate.setAddFLag7(true);
		mockImageDtoGate.setAddFLag8(true);
		mockImageDtoGate.setAddFLag9(true);
		
		editImageDtoGate.setStdImageDto(stdImageDtoGate);
		editImageDtoGate.setHiResImageDto(hiResImageDtoGate);
		editImageDtoGate.setMockImageDto(mockImageDtoGate);
		
		prodGateDto.setMissingImagesDto(missingImagesDtoGate);
		prodGateDto.setEditImageDto(editImageDtoGate);
		prodGateDto.setProductDto(product);
		
		service.editImage(prodGateDto);
		
		LOG.info("Test image Edit End");
		
	}
	
	/**
	 * Test case to Delete New Images while edit on
	 */
	@Test
	public void testEditImagesDelete() throws ImproException {
		LOG.info("Test image Edit start");
		ProductGateKeepingDto prodGateDto = new ProductGateKeepingDto();
		prodGateDto.setBitMapName("0000029200s");
		ProductDto product = new ProductDto();
		product.setBitMapName("0000029200s");
		product.setArchiveFlag(false);
		
		//For Delete images under New Images section 
		
		MissingImagesDto missingImagesDtoGate = new MissingImagesDto();
		
		missingImagesDtoGate.setLowRes1Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.1.png");
		missingImagesDtoGate.setLowRes2Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.2.png");
		missingImagesDtoGate.setLowRes3Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.3.png");
		missingImagesDtoGate.setLowRes7Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.7.png");
		missingImagesDtoGate.setLowRes8Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.8.png");
		missingImagesDtoGate.setLowRes9Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Std/00000/29200s.9.png");
		
		missingImagesDtoGate.setHiRes1Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.1.png");
		missingImagesDtoGate.setHiRes2Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.2.png");
		missingImagesDtoGate.setHiRes3Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.3.png");
		missingImagesDtoGate.setHiRes7Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.7.png");
		missingImagesDtoGate.setHiRes8Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.8.png");
		missingImagesDtoGate.setHiRes9Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/HiRes/00000/29200s.9.png");
		
		missingImagesDtoGate.setMock1Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.1.png");
		missingImagesDtoGate.setMock2Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.2.png");
		missingImagesDtoGate.setMock3Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.3.png");
		missingImagesDtoGate.setMock7Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.7.png");
		missingImagesDtoGate.setMock8Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.8.png");
		missingImagesDtoGate.setMock9Url("https://s3.amazonaws.com/improproductbucket1/gatekeeping/Mock/00000/29200s.9.png");

		String filePathGate = "D:/Users/swshedge/Documents/My Received Files/imgurl.txt";
		
		String dataGate = readAllBytesJava7(filePathGate);
		
		EditImageDto editImageDtoGate = new EditImageDto();
		StdImageDto stdImageDtoGate = new StdImageDto();
		stdImageDtoGate.setFile1(dataGate);
		stdImageDtoGate.setFile2(dataGate);
		stdImageDtoGate.setFile3(dataGate);
		stdImageDtoGate.setFile7(dataGate);
		stdImageDtoGate.setFile8(dataGate);
		stdImageDtoGate.setFile9(dataGate);
		
		stdImageDtoGate.setDeleteFLag1(true);
		stdImageDtoGate.setDeleteFLag2(true);
		stdImageDtoGate.setDeleteFLag3(true);
		stdImageDtoGate.setDeleteFLag7(true);
		stdImageDtoGate.setDeleteFLag8(true);
		stdImageDtoGate.setDeleteFLag9(true);
		
		
		HiResImageDto hiResImageDtoGate = new HiResImageDto();
		hiResImageDtoGate.setFile1(dataGate);
		hiResImageDtoGate.setFile2(dataGate);
		hiResImageDtoGate.setFile3(dataGate);
		hiResImageDtoGate.setFile7(dataGate);
		hiResImageDtoGate.setFile8(dataGate);
		hiResImageDtoGate.setFile9(dataGate);
		
		hiResImageDtoGate.setDeleteFLag1(true);
		hiResImageDtoGate.setDeleteFLag2(true);
		hiResImageDtoGate.setDeleteFLag3(true);
		hiResImageDtoGate.setDeleteFLag7(true);
		hiResImageDtoGate.setDeleteFLag8(true);
		hiResImageDtoGate.setDeleteFLag9(true);
		
		MockImageDto mockImageDtoGate = new MockImageDto();
		mockImageDtoGate.setFile1(dataGate);
		mockImageDtoGate.setFile2(dataGate);
		mockImageDtoGate.setFile3(dataGate);
		mockImageDtoGate.setFile7(dataGate);
		mockImageDtoGate.setFile8(dataGate);
		mockImageDtoGate.setFile9(dataGate);
		
		mockImageDtoGate.setDeleteFLag1(true);
		mockImageDtoGate.setDeleteFLag2(true);
		mockImageDtoGate.setDeleteFLag3(true);
		mockImageDtoGate.setDeleteFLag7(true);
		mockImageDtoGate.setDeleteFLag8(true);
		mockImageDtoGate.setDeleteFLag9(true);
		
		editImageDtoGate.setStdImageDto(stdImageDtoGate);
		editImageDtoGate.setHiResImageDto(hiResImageDtoGate);
		editImageDtoGate.setMockImageDto(mockImageDtoGate);
		
		prodGateDto.setMissingImagesDto(missingImagesDtoGate);
		prodGateDto.setEditImageDto(editImageDtoGate);
		prodGateDto.setProductDto(product);
		
		service.editImage(prodGateDto);
		
		LOG.info("Test image Edit End");
		
	}
	

	/**
	 * Test case to testPushImagesManual
	 * 
	 * @throws IOException
	 */
	@Test
	public void testPushImagesManual() throws IOException {
		LOG.info("testPushImagesManual>> begin");
		ProductGateKeepingDto dto = new ProductGateKeepingDto();
		dto.setBitMapName("1111112345S");
		try {
			service.pushImagesManual(dto);

		} catch (Exception e) {

			fail("Its failed.." + e.getMessage());
		}
		LOG.info("testPushImagesManual>> end");
	}

		
	@Test
	public void getAllBase64String() throws ImproException  {
		ProductGateKeepingDto dto = new ProductGateKeepingDto();
		dto.setBitMapName("0000029200s");
		dto.setImageType(AppConstants.IMAGE_TYPE_STD);
		
		ProductDto productDto = new ProductDto();
		productDto.setBitMapName("0000029200s");
		productDto.setImageType(AppConstants.IMAGE_TYPE_STD);
		dto.setProductDto(productDto);
		
		service.getAllBase64String(dto);
		
		LOG.info("done..."+dto); 
	}
	
}
