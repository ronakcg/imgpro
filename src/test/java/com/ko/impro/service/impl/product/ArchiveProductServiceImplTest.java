package com.ko.impro.service.impl.product;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.ko.impro.base.BaseTest;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.ArchivedProductResponseDto;
import com.ko.impro.dto.PaginationDto;
import com.ko.impro.dto.ProductRequestWrapperDto;
import com.ko.impro.dto.ProductSearchDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.report.ExcelGeneratorFactory;
import com.ko.impro.report.ProductExcelReportGenerator;
import com.ko.impro.service.product.ArchiveProductService;
import com.ko.impro.util.AppConstants;

public class ArchiveProductServiceImplTest extends BaseTest {

	private static final Logger LOG = LoggerFactory.getLogger(ArchiveProductServiceImplTest.class);

	@Autowired(required = true)
	ArchiveProductService productService;

	@Test
	void testSearchArchivedProducts() {
		try {
			ProductSearchDto productSearchDto = new ProductSearchDto();
			PaginationDto pageDto = new PaginationDto();
			pageDto.setRowNumber(0);
			pageDto.setRowSize(10);
			productSearchDto.setPageDto(pageDto);
			productSearchDto.setColumnName("ProductName");
			productSearchDto.setColumnOrder("ASC");
			ArchivedProductResponseDto resp = productService.searchArchivedProducts(productSearchDto);
			for (ArchivedProductDto dto : resp.getArchiveProductDtoList()) {
				LOG.info(dto.getProductName());
			}
			assertTrue(resp.getPageDto().getTotalNoOfRecords() > 0);
			if (LOG.isInfoEnabled()) {
			LOG.info("Total Count - " + resp.getPageDto().getTotalNoOfRecords());
			}
			/*for (ArchivedProductDto dto : resp.getArchiveProductDtoList()) {
				LOG.info(dto.getProductName());
			}*/
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	public ProductExcelReportGenerator getExcelGenerator() throws IOException {
		return ExcelGeneratorFactory.getInstance().getProductExcelReportGenerator(AppConstants.PRODUCT_FILENAME,
				AppConstants.PRODUCT_SHEETNAME);
	}

	/**
	 * Method used to export all archived product details when click exportButton
	 * 
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testExportArchivedAllProducts() throws ImproException, IOException {

		ProductRequestWrapperDto productRequestWrapperDto = new ProductRequestWrapperDto();
		ProductSearchDto productSearchDto = new ProductSearchDto();
		List<ArchivedProductDto> archivedProductDtoList = productService.exportArchivedAllProducts(productSearchDto);
		productRequestWrapperDto.setProductSearchDto(productSearchDto);
		ProductExcelReportGenerator excelGenerator = getExcelGenerator();
		XSSFWorkbook workBook = excelGenerator.generateArchivedReport(archivedProductDtoList );
		try (FileOutputStream outputStream = new FileOutputStream("report.xlsx")) {
			workBook.write(outputStream);
		}
	}

	/**
	 * Method used to export all archived product details when the users clicks the
	 * Selected Check box and click exportButton
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testSelectedArchivedProductExport() throws ImproException, IOException {

		ProductRequestWrapperDto productRequestWrapperDto = new ProductRequestWrapperDto();
		ProductSearchDto productSearchDto = new ProductSearchDto();
		List<String> subSegmentList = new ArrayList<String>();
		subSegmentList.add("18");
		productSearchDto.setSubSegmentList(subSegmentList);
		List<ArchivedProductDto> productDtoList = productService.exportArchivedAllProducts(productSearchDto);
		productRequestWrapperDto.setProductSearchDto(productSearchDto);
		ProductExcelReportGenerator excelGenerator = getExcelGenerator();
		XSSFWorkbook workBook = excelGenerator.generateArchivedReport(productDtoList);
		try (FileOutputStream outputStream = new FileOutputStream("report.xlsx")) {
			workBook.write(outputStream);
		}
	}

	/**
	 * Method used to export all archived product details when the users Selects
	 * columns from Column selector Module, selects missing image check box and click
	 * exportButton
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	 */

	@Test
	public void testMissingImgExportProducts() throws ImproException, IOException, InvalidFormatException, ParseException {
		ProductRequestWrapperDto productRequestWrapperDto = new ProductRequestWrapperDto();
		ProductSearchDto productSearchDto = new ProductSearchDto();
		List<ArchivedProductDto> productDtoList = productService.exportArchivedAllProducts(productSearchDto);
		productRequestWrapperDto.setProductSearchDto(productSearchDto);
		ProductExcelReportGenerator excelGenerator = getExcelGenerator();
		XSSFWorkbook workBook = excelGenerator.generateArchivedReport(productDtoList);
		try (FileOutputStream outputStream = new FileOutputStream("report.xlsx")) {
			workBook.write(outputStream);
		}
	}

}