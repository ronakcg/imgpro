package com.ko.impro.service.impl.image;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import static org.junit.jupiter.api.Assertions.fail;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.ko.impro.base.BaseTest;
import com.ko.impro.dto.ArchivedProductDto;
import com.ko.impro.dto.BrandDto;
import com.ko.impro.dto.CategoryDto;
import com.ko.impro.dto.ContainerTypeDto;
import com.ko.impro.dto.EditImageDto;
import com.ko.impro.dto.ExtendedProductDescDto;
import com.ko.impro.dto.FlavorDto;
import com.ko.impro.dto.HiResImageDto;
import com.ko.impro.dto.JpgImageDto;
import com.ko.impro.dto.ManufacturerDto;
import com.ko.impro.dto.MissingImagesDto;
import com.ko.impro.dto.MockImageDto;
import com.ko.impro.dto.PackageDto;
import com.ko.impro.dto.PackageSizeDto;
import com.ko.impro.dto.ProductDto;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.StdImageDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.dto.SubSegmentDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.image.ImageService;
import com.ko.impro.util.AppConstants;
import com.ko.impro.util.ImageHelper;

/**
 * @author lreddyru 
 * Test Cases of ImageServiceImplTest methods
 */
public class ImageServiceImplTest extends BaseTest {
	
	/**
	 * s3client Reference
	 */
	@Autowired
	protected AmazonS3 s3client;

	/**
	 * Logger object
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ImageServiceImplTest.class);
	/**
	 * Reference of ProductService
	 */
	@Autowired(required = true)
	private ImageService imageService;

	/**
	 * bucketName value for the given key in property file
	 */
	@Value("${s3.bucket.product}")
	private String bucketName;

	/**
	 * url value for the given key in propetryFile
	 */
	@Value("${s3.bucket.staticUrl}")
	private String url;

	/**
	 * activeStdFolNm value for the given key in propetryFile
	 */
	@Value("${s3.active.std.folderName}")
	private String activeStdFolNm;

	/**
	 * Test case to Add Image
	 */
	@Test
	public void testAddImage() throws ImproException {
		File file = new File("D:\\impro\\images\\koimgs.png");
		imageService.addImage(activeStdFolNm + "koimgs.png", file);

	}
	
	
	/**
	 * Test case to Archive Image
	 */
	@Test
	public void testArchiceImage() throws ImproException {
		ProductDto dto=new ProductDto();
		dto.setBitMapName("0000012345");
		List<ProductDto> list=new ArrayList<ProductDto>();
		list.add(dto);
		imageService.archiveImages(list);

	}
	
	/**
	 * Test case to Archive Image
	 */
	@Test
	public void testArchiveImages() throws ImproException {
		LOG.info("Archive Test Begin");
		ProductDto prodDto = new ProductDto();
		prodDto.setProductCode("15719027");
		prodDto.setBitMapName("0000029200s");
		List<ProductDto> prodList = new ArrayList<ProductDto>();
		prodList.add(prodDto);
		imageService.archiveImages(prodList);
	}
	
	/**
	 * Test case to Restore image
	 */
	@Test
	public void testRestoreImages() throws ImproException {
		LOG.info("Restore Test Begin");
		ArchivedProductDto archProdDto = new ArchivedProductDto();
		archProdDto.setBitMapName("0000029200s");
		imageService.restoreImages(archProdDto);
	}
	
	/**
	 * Test case to Purge Image
	 */
	@Test
	public void testPurgeImages() throws ImproException {
		LOG.info("Purge Test Begin");
		ArchivedProductDto archProdDto = new ArchivedProductDto();
		archProdDto.setBitMapName("0000029200s");
		List<ArchivedProductDto> archProdList = new ArrayList<ArchivedProductDto>();
		archProdList.add(archProdDto);
		imageService.purgeImages(archProdList);
	}
	
	/**
	 * Test case to Get the Grid View Images
	 */
	@Test
	public void testGetGridViewImages() throws ImproException {
		List<ProductDto> prodList = new ArrayList<ProductDto>();
		ProductDto dto = new ProductDto();
		MissingImagesDto missingImagesDto = new MissingImagesDto();
		dto.setBitMapName("0000029200s");		
		dto.setMissingImagesDto(missingImagesDto);
		prodList.add(dto);
		imageService.getGridViewImages(prodList);
		LOG.info("done..");
	}
	
/**
 *  Test case to Get all the Images 
 * @throws ImproException
 */
	@Test
	public void testGetProductImagesById()throws ImproException  {
		ProductDto dto = new ProductDto();
		MissingImagesDto missingImagesDto = new MissingImagesDto();
		dto.setMissingImagesDto(missingImagesDto);
		dto.setProductCode("15719027");
		dto.setBitMapName("0000029200s");
		imageService.getProductImagesById(dto);
		LOG.info("done.."+dto);
	}	
	

	/**
	 * Test case to Add Product and Images
	 * 
	 * @throws IOException
	 */
	@Test
	public void testAddImageProduct() throws IOException {
		LOG.info("Test image add start");
		ProductDto product = new ProductDto();
		// product.setProductCode("20072");
		product.setBitMapName("1111112345S");
		product.setArchiveFlag(false);

		PackageDto packageDto = new PackageDto();
		packageDto.setPackageCode("8006");
		ContainerTypeDto containerTypeDto = new ContainerTypeDto();
		containerTypeDto.setContainerTypeDescription("Test1");
		packageDto.setContainerTypeDto(containerTypeDto);
		product.setPkgdto(packageDto);

		PackageSizeDto pkg = new PackageSizeDto();
		pkg.setPackageSizeDescription("Test2");
		product.setPackageSizeDto(pkg);

		BrandDto brandDto = new BrandDto();
		brandDto.setBrandCode("2");
		brandDto.setBrandName("Test3");
		product.setBrandDto(brandDto);

		product.setUpc("0000029241");

		FlavorDto flavorDto = new FlavorDto();
		flavorDto.setFlavorCode("14");
		product.setFlavorDto(flavorDto);

		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setCategoryCode("1");
		product.setCategoryDto(categoryDto);

		SubCategoryDto subCategoryDto = new SubCategoryDto();
		subCategoryDto.setSubCategoryCode("1");
		product.setSubCategoryDto(subCategoryDto);

		SegmentDto segmentDto = new SegmentDto();
		segmentDto.setSegmentCode("1");
		product.setSegmentDto(segmentDto);

		SubSegmentDto subSegmentDto = new SubSegmentDto();
		subSegmentDto.setSubSegmentCode("6");
		product.setSubSegmentDto(subSegmentDto);

		ManufacturerDto manufacturerDto = new ManufacturerDto();
		manufacturerDto.setManufacturerCode("1");
		product.setManufacturerDto(manufacturerDto);

		// product.setBitmapFlag(false);
		ExtendedProductDescDto extendedProductDescDto = new ExtendedProductDescDto();
		extendedProductDescDto.setExtendedDescId("7");
		product.setExtendedProductDescDto(extendedProductDescDto);
		product.setArchiveReasonCode(0);
		product.setUpc12("9999999999");

		MissingImagesDto missingImagesDto = new MissingImagesDto();
		missingImagesDto.setIslowRes1(false);
		missingImagesDto.setIslowRes2(false);
		missingImagesDto.setIslowRes3(false);
		missingImagesDto.setIslowRes7(false);
		missingImagesDto.setIslowRes8(false);
		missingImagesDto.setIslowRes9(false);

		missingImagesDto.setIshiRes1(false);
		missingImagesDto.setIshiRes2(false);
		missingImagesDto.setIshiRes3(false);
		missingImagesDto.setIshiRes7(false);
		missingImagesDto.setIshiRes8(false);
		missingImagesDto.setIshiRes9(false);

		missingImagesDto.setIsmock1(false);
		missingImagesDto.setIsmock2(false);
		missingImagesDto.setIsmock3(false);
		missingImagesDto.setIsmock7(false);
		missingImagesDto.setIsmock8(false);
		missingImagesDto.setIsmock9(false);

		missingImagesDto.setIsjpg(false);

		product.setMissingImagesDto(missingImagesDto);
		/** Image data setting */
		String filePath = "C:/Users/vmuchand/Desktop/impro base64/imgurl.txt";
		String data = readAllBytesJava7(filePath);

		EditImageDto editImageDto = new EditImageDto();
		StdImageDto stdImageDto = new StdImageDto();
		stdImageDto.setFile1(data);
		stdImageDto.setFile2(data);
		stdImageDto.setFile3(data);
		stdImageDto.setFile7(data);
		stdImageDto.setFile8(data);
		stdImageDto.setFile9(data);

		HiResImageDto hiResImageDto = new HiResImageDto();
		hiResImageDto.setFile1(data);
		hiResImageDto.setFile2(data);
		hiResImageDto.setFile3(data);
		hiResImageDto.setFile7(data);
		hiResImageDto.setFile8(data);
		hiResImageDto.setFile9(data);

		MockImageDto mockImageDto = new MockImageDto();
		mockImageDto.setFile1(data);
		mockImageDto.setFile2(data);
		mockImageDto.setFile3(data);
		mockImageDto.setFile7(data);
		mockImageDto.setFile8(data);
		mockImageDto.setFile9(data);

		JpgImageDto jpgImageDto = new JpgImageDto();
		jpgImageDto.setFile1(data);

		editImageDto.setStdImageDto(stdImageDto);
		editImageDto.setHiResImageDto(hiResImageDto);
		editImageDto.setMockImageDto(mockImageDto);
		editImageDto.setJpgImageDto(jpgImageDto);
		product.setEditImageDto(editImageDto);
		/*******************************************************/
		try {
			imageService.addImage(product);

		} catch (Exception e) {
 
			fail(" Add Product failed " + e.getMessage());
		}
		LOG.info("Test image add End");

	}
	
	/**
	 * Test case to add image while  editing
	 * 
	 * @throws IOException
	 */
	@Test
	public void testEditImageProductADD() throws IOException {
		LOG.info("Test image add start");
		ProductDto product = new ProductDto();
		// product.setProductCode("20072");
		product.setBitMapName("2222212345S");
		product.setArchiveFlag(false);


		MissingImagesDto missingImagesDto = new MissingImagesDto();
		product.setMissingImagesDto(missingImagesDto);
		/** Image data setting */
		String filePath = "C:/Users/vmuchand/Desktop/impro base64/imgurl.txt";
		String data = readAllBytesJava7(filePath);

		EditImageDto editImageDto = new EditImageDto();
		StdImageDto stdImageDto = new StdImageDto();
		stdImageDto.setFile1(data);
		stdImageDto.setFile2(data);
		stdImageDto.setFile3(data);
		stdImageDto.setFile7(data);
		stdImageDto.setFile8(data);
		stdImageDto.setFile9(data);
		
		stdImageDto.setAddFLag1(true);
		stdImageDto.setAddFLag2(true);
		stdImageDto.setAddFLag3(true);
		stdImageDto.setAddFLag7(true);
		stdImageDto.setAddFLag8(true);
		stdImageDto.setAddFLag9(true);
		
		HiResImageDto hiResImageDto = new HiResImageDto();
		hiResImageDto.setFile1(data);
		hiResImageDto.setFile2(data);
		hiResImageDto.setFile3(data);
		hiResImageDto.setFile7(data);
		hiResImageDto.setFile8(data);
		hiResImageDto.setFile9(data);
	
		hiResImageDto.setAddFLag1(true);
		hiResImageDto.setAddFLag2(true);
		hiResImageDto.setAddFLag3(true);
		hiResImageDto.setAddFLag7(true);
		hiResImageDto.setAddFLag8(true);
		hiResImageDto.setAddFLag9(true);

		MockImageDto mockImageDto = new MockImageDto();
		mockImageDto.setFile1(data);
		mockImageDto.setFile2(data);
		mockImageDto.setFile3(data);
		mockImageDto.setFile7(data);
		mockImageDto.setFile8(data);
		mockImageDto.setFile9(data);

		mockImageDto.setAddFLag1(true);
		mockImageDto.setAddFLag2(true);
		mockImageDto.setAddFLag3(true);
		mockImageDto.setAddFLag7(true);
		mockImageDto.setAddFLag8(true);
		mockImageDto.setAddFLag9(true);

		JpgImageDto jpgImageDto = new JpgImageDto();
		jpgImageDto.setFile1(data);
		jpgImageDto.setAddFLag1(true);
		
		editImageDto.setStdImageDto(stdImageDto);
		editImageDto.setHiResImageDto(hiResImageDto);
		editImageDto.setMockImageDto(mockImageDto);
		editImageDto.setJpgImageDto(jpgImageDto);
		product.setEditImageDto(editImageDto);
		/*******************************************************/
		try {
			imageService.editImage(product);

		} catch (Exception e) {
 
			fail(" Add Product failed " + e.getMessage());
		}
		LOG.info("Test image Add End");

	}
	
	/**
	 * @param filePath
	 * @return
	 */
	private static String readAllBytesJava7(String filePath) {
		String content = "";
		try {
			content = new String(Files.readAllBytes(Paths.get(filePath)));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return content;
	}
	
	/**
	 * Test case to  testRenameImageProduct
	 * 
	 * @throws IOException
	 */
	@Test
	public void testRenameImageProduct() throws IOException {
		LOG.info("Test image rename start");
		ProductDto product = new ProductDto();
		// product.setProductCode("20072");
		product.setBitMapName("2222212345S");
		product.setArchiveFlag(false);
		product.setOldBitMapName("4444412345S");

		try {
			
			imageService.bitmapChange(product);

		} catch (Exception e) {
 
			fail(" Image Folder change" + e.getMessage());
		}
		LOG.info("Test image rename End");

	}
	
	/**
	 * Test case to Edit image and archive the old image
	 * 
	 * @throws IOException
	 */
	@Test
	public void testEditImageProductReplace() throws IOException {
		LOG.info("Test image edit start");
		ProductDto product = new ProductDto();
		// product.setProductCode("20072");
		product.setBitMapName("2222212345S");
		product.setArchiveFlag(false);


		MissingImagesDto missingImagesDto = new MissingImagesDto();

		missingImagesDto.setIsjpg(false);
		missingImagesDto.setLowRes1Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.1.png");
		missingImagesDto.setLowRes2Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.2.png");
		missingImagesDto.setLowRes3Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.3.png");
		missingImagesDto.setLowRes7Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.7.png");
		missingImagesDto.setLowRes8Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.8.png");
		missingImagesDto.setLowRes9Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.9.png");
		
		missingImagesDto.setHiRes1Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.1.png");
		missingImagesDto.setHiRes2Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.2.png");
		missingImagesDto.setHiRes3Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.3.png");
		missingImagesDto.setHiRes7Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.7.png");
		missingImagesDto.setHiRes8Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.8.png");
		missingImagesDto.setHiRes9Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.9.png");

		missingImagesDto.setMock1Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.1.png");
		missingImagesDto.setMock2Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.2.png");
		missingImagesDto.setMock3Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.3.png");
		missingImagesDto.setMock7Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.7.png");
		missingImagesDto.setMock8Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.8.png");
		missingImagesDto.setMock9Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.9.png");
		
		missingImagesDto.setJpgUrl("https://s3.amazonaws.com/impro03/active/Jpg/22222/12345S.1.png");
		product.setMissingImagesDto(missingImagesDto);
		/** Image data setting */
		String filePath = "C:/Users/vmuchand/Desktop/impro base64/imgurl.txt";
		String data = readAllBytesJava7(filePath);

		EditImageDto editImageDto = new EditImageDto();
		StdImageDto stdImageDto = new StdImageDto();
		stdImageDto.setFile1(data);
		stdImageDto.setFile2(data);
		stdImageDto.setFile3(data);
		stdImageDto.setFile7(data);
		stdImageDto.setFile8(data);
		stdImageDto.setFile9(data);
		stdImageDto.setEditFLag1(true);
		stdImageDto.setEditFLag2(true);
		stdImageDto.setEditFLag3(true);
		stdImageDto.setEditFLag7(true);
		stdImageDto.setEditFLag8(true);
		stdImageDto.setEditFLag9(true);
				
		HiResImageDto hiResImageDto = new HiResImageDto();
		hiResImageDto.setFile1(data);
		hiResImageDto.setFile2(data);
		hiResImageDto.setFile3(data);
		hiResImageDto.setFile7(data);
		hiResImageDto.setFile8(data);
		hiResImageDto.setFile9(data);
		
		hiResImageDto.setEditFLag1(true);
		hiResImageDto.setEditFLag2(true);
		hiResImageDto.setEditFLag3(true);
		hiResImageDto.setEditFLag7(true);
		hiResImageDto.setEditFLag8(true);
		hiResImageDto.setEditFLag9(true);

		MockImageDto mockImageDto = new MockImageDto();
		mockImageDto.setFile1(data);
		mockImageDto.setFile2(data);
		mockImageDto.setFile3(data);
		mockImageDto.setFile7(data);
		mockImageDto.setFile8(data);
		mockImageDto.setFile9(data);
		
		mockImageDto.setEditFLag1(true);
		mockImageDto.setEditFLag2(true);
		mockImageDto.setEditFLag3(true);
		mockImageDto.setEditFLag7(true);
		mockImageDto.setEditFLag8(true);
		mockImageDto.setEditFLag9(true);
		
		JpgImageDto jpgImageDto = new JpgImageDto();
		jpgImageDto.setFile1(data);
		jpgImageDto.setEditFLag1(true);
		
		editImageDto.setStdImageDto(stdImageDto);
		editImageDto.setHiResImageDto(hiResImageDto);
		editImageDto.setMockImageDto(mockImageDto);
		editImageDto.setJpgImageDto(jpgImageDto);
		product.setEditImageDto(editImageDto);
		/*******************************************************/
		try {
			imageService.editImage(product);

		} catch (Exception e) {
 
			fail(" Edit Product failed " + e.getMessage());
		}
		LOG.info("Test image Edit End");

	}
	
	/**
	 * Test case to delete image
	 * 
	 * @throws IOException
	 */
	@Test
	public void testEditImageProductDelete() throws IOException {
		LOG.info("Test image Delete start");
		ProductDto product = new ProductDto();
		// product.setProductCode("20072");
		product.setBitMapName("2222212345S");
		product.setArchiveFlag(false);


		MissingImagesDto missingImagesDto = new MissingImagesDto();
		missingImagesDto.setLowRes1Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.1.png");
		missingImagesDto.setLowRes2Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.2.png");
		missingImagesDto.setLowRes3Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.3.png");
		missingImagesDto.setLowRes7Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.7.png");
		missingImagesDto.setLowRes8Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.8.png");
		missingImagesDto.setLowRes9Url("https://s3.amazonaws.com/impro03/active/Std/22222/12345S.9.png");
		
		missingImagesDto.setHiRes1Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.1.png");
		missingImagesDto.setHiRes2Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.2.png");
		missingImagesDto.setHiRes3Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.3.png");
		missingImagesDto.setHiRes7Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.7.png");
		missingImagesDto.setHiRes8Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.8.png");
		missingImagesDto.setHiRes9Url("https://s3.amazonaws.com/impro03/active/HiRes/22222/12345S.9.png");

		missingImagesDto.setMock1Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.1.png");
		missingImagesDto.setMock2Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.2.png");
		missingImagesDto.setMock3Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.3.png");
		missingImagesDto.setMock7Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.7.png");
		missingImagesDto.setMock8Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.8.png");
		missingImagesDto.setMock9Url("https://s3.amazonaws.com/impro03/active/Mock/22222/12345S.9.png");
		
		missingImagesDto.setJpgUrl("https://s3.amazonaws.com/impro03/active/Jpg/22222/12345S.1.png");
		product.setMissingImagesDto(missingImagesDto);
		/** Image data setting */
		String filePath = "C:/Users/vmuchand/Desktop/impro base64/imgurl.txt";
		String data = readAllBytesJava7(filePath);

		EditImageDto editImageDto = new EditImageDto();
		StdImageDto stdImageDto = new StdImageDto();
		stdImageDto.setFile1(data);
		stdImageDto.setFile2(data);
		stdImageDto.setFile3(data);
		stdImageDto.setFile7(data);
		stdImageDto.setFile8(data);
		stdImageDto.setFile9(data);

		stdImageDto.setDeleteFLag1(true);
		stdImageDto.setDeleteFLag2(true);
		stdImageDto.setDeleteFLag3(true);
		stdImageDto.setDeleteFLag7(true);
		stdImageDto.setDeleteFLag8(true);
		stdImageDto.setDeleteFLag9(true);
		
		HiResImageDto hiResImageDto = new HiResImageDto();
		hiResImageDto.setFile1(data);
		hiResImageDto.setFile2(data);
		hiResImageDto.setFile3(data);
		hiResImageDto.setFile7(data);
		hiResImageDto.setFile8(data);
		hiResImageDto.setFile9(data);
	
		hiResImageDto.setDeleteFLag1(true);
		hiResImageDto.setDeleteFLag2(true);
		hiResImageDto.setDeleteFLag3(true);
		hiResImageDto.setDeleteFLag7(true);
		hiResImageDto.setDeleteFLag8(true);
		hiResImageDto.setDeleteFLag9(true);

		MockImageDto mockImageDto = new MockImageDto();
		mockImageDto.setFile1(data);
		mockImageDto.setFile2(data);
		mockImageDto.setFile3(data);
		mockImageDto.setFile7(data);
		mockImageDto.setFile8(data);
		mockImageDto.setFile9(data);
		
		mockImageDto.setDeleteFLag1(true);
		mockImageDto.setDeleteFLag2(true);
		mockImageDto.setDeleteFLag3(true);
		mockImageDto.setDeleteFLag7(true);
		mockImageDto.setDeleteFLag8(true);
		mockImageDto.setDeleteFLag9(true);

		

		JpgImageDto jpgImageDto = new JpgImageDto();
		jpgImageDto.setFile1(data);

		jpgImageDto.setDeleteFLag1(true);
		
		editImageDto.setStdImageDto(stdImageDto);
		editImageDto.setHiResImageDto(hiResImageDto);
		editImageDto.setMockImageDto(mockImageDto);
		editImageDto.setJpgImageDto(jpgImageDto);
		product.setEditImageDto(editImageDto);
		/*******************************************************/
		try {
			imageService.editImage(product);

		} catch (Exception e) {
 
			fail(" Delete Product failed " + e.getMessage());
		}
		LOG.info("Test image Delete End");

	}
	
	@Test
	public void getAllBase64String() throws ImproException  {
		List<String> prefexList=new ArrayList();
		String 	bucketName="1-jda-planogram-ko-support";
		prefexList.add("/");
		//prefexList.add("2 - jda product library");
		for (Bucket bucket : s3client.listBuckets()) {
			System.out.println(bucket.getName());
		}

		for (String prefix : prefexList) {
			List<String> abc=listKeysInDirectory(bucketName,prefix);
			System.out.println(abc);
		}
			
			
		

		/*ProductDto dto = new ProductDto();
		dto.setBitMapName("1111122222");
		dto.setImageType(AppConstants.IMAGE_TYPE_STD);
		imageService.getAllBase64String(dto);
		LOG.info("done..."+dto); */
	}
	public List<String> listKeysInDirectory(String bucketName, String prefix) {
	    String delimiter = "/";
	    if (!prefix.endsWith(delimiter)) {
	        prefix += delimiter;
	    }

	    ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
	            .withBucketName(bucketName).withPrefix(prefix)
	            .withDelimiter(delimiter);
	    ObjectListing objects = s3client.listObjects(listObjectsRequest);
	    return objects.getCommonPrefixes();
	}

	
	@Test
	public void archiveImages()throws ImproException{
		List<ProductDto> productList=new ArrayList<ProductDto>();
		ProductDto dto = new ProductDto();
		dto.setBitMapName("1111122222");
		productList.add(dto);
		imageService.archiveImages(productList);
		LOG.info("Done");
	}
}
