package com.ko.impro.service.impl.segment;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dto.SegmentDto;
import com.ko.impro.dto.SubCategoryDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.service.segment.SegmentService;

/**
 * Test Cases of SubSegmentService methods
 */
public class SegmentServiceImplTest extends BaseTest {
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SegmentServiceImplTest.class);

	/**
	 * Reference of SubSegmentService
	 */
	@Autowired(required = true)
	private SegmentService segmentService;

	/**
	 * Test case to Load Segment
	 */
	@Test
	public void testLoadSegment() {
		try {
			List<SegmentDto> cat = segmentService.loadSegment();

			assertTrue(!cat.isEmpty(), "Records Present");
			for (SegmentDto dto : cat) {
				LOG.info(dto.getSegmentDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to Load Segments ListOfString
	 */
	@Test
	public void testLoadSegmentsListOfString() {
		try {
			List<String> subCatIdList = new ArrayList<String>();
			subCatIdList.add("1");
			subCatIdList.add("2");
			subCatIdList.add("5");
			List<SegmentDto> segmentList = segmentService.loadSegment(subCatIdList);

			assertTrue(!segmentList.isEmpty(), "Records Present");
			for (SegmentDto dto : segmentList) {
				LOG.info(dto.getSegmentDescription());
			}
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	/**
	 * Test case to Add Segment
	 */
	@Test
	public void testAddSegment() throws ImproException {
		try {
			SegmentDto segmentDto = new SegmentDto();
			SubCategoryDto subCatDto = new SubCategoryDto();
			subCatDto.setSubCategoryCode("2");
			subCatDto.setSubCategoryDescription("Old SubCategory");

			segmentDto.setSegmentCode("4");
			segmentDto.setSegmentDescription("New Segment");

			segmentDto.setSubCategoryDto(subCatDto);
			segmentService.addSegment(segmentDto);
			LOG.info("added...!");
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to Delete Segment
	 */
	@Test
	public void testDeleteSegment() throws ImproException {
		try {
			SegmentDto segmentDto = new SegmentDto();
			segmentDto.setSegmentCode("25");
			segmentService.deleteSegment(segmentDto);
			LOG.info("deleted...!");
		} catch (ImproException e) {
			fail(loadingMsg);
		}

	}

	/**
	 * Test case to Edit Segment
	 */
	@Test
	public void testEditSegment() throws ImproException {

		try {
			SegmentDto segmentDto = new SegmentDto();
			SubCategoryDto subCatDto = new SubCategoryDto();
			subCatDto.setSubCategoryCode("5");

			segmentDto.setSegmentCode("16");
			segmentDto.setSegmentDescription("updated");
			segmentDto.setSubCategoryDto(subCatDto);

			segmentService.editSegment(segmentDto);
			LOG.info("updated...!");
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to Load Segment By Id
	 */
	@Test
	public void testLoadSegmentById() {
		try {
			SegmentDto sd = segmentService.loadSegment(15);
			if (LOG.isInfoEnabled()) {
				LOG.info("SegmentCode " + sd.getSegmentCode());
				LOG.info("Segment Description " + sd.getSegmentDescription());
				LOG.info("SubCategory Associated with Segment");
			}
			SubCategoryDto dto = sd.getSubCategoryDto();
			LOG.info(dto.getSubCategoryCode());
			LOG.info(dto.getSubCategoryDescription());
		} catch (ImproException e) {
			fail(loadingMsg);
		}
	}

	/**
	 * Test case to CheckDuplicate
	 */
	@Test
	public void testCheckDuplicate() {
		try {
			String segDescription = "Water";
			boolean flag = segmentService.checkDuplicateSegment(segDescription);
			if (flag)
				LOG.info("Duplicate Present ");
			else
				LOG.info("Duplicate Not Present ");
			assertTrue(flag, "Flag Status");

		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("checkDuplicate failed");
		}
	}

	/*void updateProduct() {
		try {
			SegmentDto dto = new SegmentDto();
			SubCategoryDto subCat = new SubCategoryDto();
			dto.setSegmentCode("15");
			dto.setOldValue("2");
			subCat.setSubCategoryCode("4");
			dto.setSubCategoryDto(subCat);
			segmentService.updateProduct(dto);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("Update Product Failed");
		}
	}

	void checkPresenceInProduct_SubCategory() {
		try {
			int segmentCode = 4;
			boolean flag = SegmentDao.checkPresenceInProduct_SubCategory(segmentCode);
			if (flag)
				LOG.info("Segment-SubCategory present in Product table :" + flag);
			else
				LOG.info("Segment-SubCategory present in Product table :" + flag);
			assertTrue(flag);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("Failed to check");
		}
	}

	void checkPresenceInProduct_SubSegment() {
		try {
			int segmentCode = 4;
			boolean flag = SegmentDao.checkPresenceInProduct_SubSegment(segmentCode);
			if (flag)
				LOG.info("Segment-SubSegment present in Product table :" + flag);
			else
				LOG.info("Segment-SubSegment present in Product table :" + flag);
			assertTrue(flag);
		} catch (ImproException e) {
			LOG.error(e.getMessage());
			fail("Failed to check");
		}
	}*/
}

