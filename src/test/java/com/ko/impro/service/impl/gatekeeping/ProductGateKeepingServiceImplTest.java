package com.ko.impro.service.impl.gatekeeping;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dto.ProductGateKeepingDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.report.ExcelGeneratorFactory;
import com.ko.impro.report.GatekeepingExcelReportGenerator;
import com.ko.impro.service.gatekeeping.ProductGateKeepingService;
import com.ko.impro.util.AppConstants;

/**
 * Test Cases of ProductServiceImpl methods
 */
public class ProductGateKeepingServiceImplTest extends BaseTest {

	private static final Logger LOG = LoggerFactory.getLogger(ProductGateKeepingServiceImplTest.class);
	/**
	 * Reference of ProductService
	 */
	@Autowired(required = true)
	private ProductGateKeepingService productService;

	
	/**
	 * Test case to get ExcelGenerator
	 */
	public GatekeepingExcelReportGenerator getExcelGenerator() throws IOException {
		return ExcelGeneratorFactory.getInstance().getGateKeepingExcelReportGenerator(AppConstants.PRODUCT_FILENAME,
				AppConstants.PRODUCT_SHEETNAME);
	}

	/**
	 * Method used to export all product details when the users click exportButton
	 * 
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testExportAllProducts() throws ImproException, IOException, InvalidFormatException, ParseException {
		List<ProductGateKeepingDto> productDtoList = productService.loadGateKeeping(true);
		GatekeepingExcelReportGenerator excelGenerator = getExcelGenerator();
		XSSFWorkbook workBook = excelGenerator.generateReport(productDtoList);
		try (FileOutputStream outputStream = new FileOutputStream("report.xlsx")) {
			workBook.write(outputStream);
		}
	}

	
}
