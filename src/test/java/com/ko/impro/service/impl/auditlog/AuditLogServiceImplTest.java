package com.ko.impro.service.impl.auditlog;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;

import com.ko.impro.base.BaseTest;
import com.ko.impro.dto.AuditLogDto;
import com.ko.impro.dto.AuditLogRequestDto;
import com.ko.impro.exception.ImproException;
import com.ko.impro.report.AuditLogExcelReportGenerator;
import com.ko.impro.report.ExcelGeneratorFactory;
import com.ko.impro.service.auditlog.AuditLogService;
import com.ko.impro.util.AppConstants;

/**
 * Test classes to check methods of AuditLog
 *
 */
public class AuditLogServiceImplTest extends BaseTest {
	
	/**
	 * Reference of CategoryDao
	 */
	@Autowired(required = true)
	private AuditLogService auditService;

	/**
	 * Method used to export all product details when the users click exportButton
	 * 
	 * @throws ImproException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void testExportAllAuditLog() throws ImproException, IOException, InvalidFormatException, ParseException {
		try (FileOutputStream outputStream = new FileOutputStream("report.xlsx")) {
			final List<AuditLogDto> auditLogDtoList = auditService.exportAuditLog();
			final	AuditLogExcelReportGenerator excelGenerator = getExcelGenerator();
			final XSSFWorkbook workBook = excelGenerator.generateReport(auditLogDtoList);
			workBook.write(outputStream);
		} catch (Exception e) {
			fail(loadingMsg);
		}
	}

	/**Method to create Excel Generator
	 * @return
	 * @throws IOException
	 */
	public AuditLogExcelReportGenerator getExcelGenerator() throws IOException {
		final ExcelGeneratorFactory excelFactory = new ExcelGeneratorFactory();
		return excelFactory.getAuditLogExcelReportGenerator(AppConstants.AUDIT_FILENAME,
				AppConstants.AUDIT_SHEETNAME);
	}
}
